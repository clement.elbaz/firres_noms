/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use cleanutils;
use config::IndexConfig;
use std::collections::BTreeMap;
use std::fmt::Write;

#[derive(Debug, Clone)]
pub struct WordIndex {
    index: BTreeMap<String, WordInIndexEntry>,
    document_total: u64,
    whitelist: Option<Box<WordIndex>>,
    config: IndexConfig,
}

impl WordIndex {
    pub fn new(whitelist: Option<Box<WordIndex>>, config: IndexConfig) -> WordIndex {
        WordIndex {
            index: BTreeMap::new(),
            document_total: 0,
            whitelist,
            config,
        }
    }

    pub fn compute_reverse_index(&self) -> Vec<String> {
        println!("Compute reverse index...");
        let mut result = vec![String::new(); self.index.len()];

        for word in self.index.keys() {
            let entry = self.index.get(word).unwrap();

            result[entry.unique_index] = word.clone();
        }

        println!("Reverse index computed.");

        result
    }

    pub fn publish_document(&mut self, document: &String) -> Vec<WordInDocEntry> {
        let mut index_document: BTreeMap<String, WordInDocEntry> = BTreeMap::new();

        let cleaned_document = cleanutils::clean_document(document);

        let mut stacked_words = Vec::new();

        for uncleaned_word in cleaned_document.split_whitespace() {
            stacked_words.push(uncleaned_word.to_string());

            if stacked_words.len() > self.config.consecutive_words as usize {
                stacked_words.remove(0);
            }

            let uncleaned_assembled_words = self.assemble_words(&stacked_words);

            for uncleaned_assembled_word in uncleaned_assembled_words {
                let is_first_word_capitalized = {
                    let first_word = uncleaned_assembled_word.split_whitespace().next().unwrap();
                    first_word != first_word.to_lowercase()
                };

                let assembled_word = cleanutils::clean_word(uncleaned_assembled_word);
                let accept_word = self.accept_word(&assembled_word);

                if accept_word {
                    self.publish_word(
                        assembled_word,
                        is_first_word_capitalized,
                        &mut index_document,
                    );
                }
            }
        }

        let mut indexed_document: Vec<WordInDocEntry> = index_document.values().cloned().collect();

        indexed_document.sort_by_key(|k| k.word.clone());

        self.publish_indexed_document(&indexed_document);

        indexed_document
    }

    fn accept_word(&self, word: &String) -> bool {
        match &self.whitelist {
            None => true,
            Some(whitelist_index) => whitelist_index.index.contains_key(word),
        }
    }

    /**
        Example : if input is ["microsoft", "word", "2007"]
        then input is ["microsoft word 2007", "word 2007", "2007"]

    */
    fn assemble_words(&mut self, stacked_words: &Vec<String>) -> Vec<String> {
        let mut assembled_words = Vec::new();

        for i in 0..stacked_words.len() {
            let mut assembled_word = String::new();
            for j in i..stacked_words.len() {
                write!(assembled_word, "{} ", stacked_words[j]).unwrap();
            }
            assembled_word = assembled_word.trim().to_string();
            assembled_words.push(assembled_word);
        }

        assembled_words
    }

    fn publish_word(
        &mut self,
        word: String,
        is_word_capitalized: bool,
        index_document: &mut BTreeMap<String, WordInDocEntry>,
    ) {
        // This is not the best thing performance-wise (2 or 3 traversals are needed on the B-Tree every time while we could theoretically go with one), but it is the most readable thing I've found within Rust memory model.
        if !index_document.contains_key(&word) {
            index_document.insert(
                word.clone(),
                WordInDocEntry {
                    word: word.clone(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 0,
                },
            );
        }

        let word_doc_entry = index_document.get_mut(&word).unwrap(); // We just made sure there was a value in any case

        word_doc_entry.term_frequency += 1;
        word_doc_entry.at_least_one_occurence_was_capitalized =
            word_doc_entry.at_least_one_occurence_was_capitalized || is_word_capitalized;
    }

    fn publish_indexed_document(&mut self, indexed_document: &Vec<WordInDocEntry>) {
        self.document_total += 1;

        for word_doc_entry in indexed_document {
            let word = &word_doc_entry.word;

            if !self.index.contains_key(word) {
                let unique_index = self.index.len();

                self.index.insert(
                    word.clone(),
                    WordInIndexEntry {
                        word: word.clone(),
                        document_with_word_count: 0,
                        unique_index,
                    },
                );
            }

            let word_index_entry = self.index.get_mut(word).unwrap(); // We just made sure there was a value in any case

            word_index_entry.document_with_word_count += 1
        }
    }

    fn get_idf(&self, word: &String) -> f64 {
        let document_total = self.document_total as f64;
        let documents_with_word = match self.index.get(word) {
            Some(word_entry) => word_entry.document_with_word_count,
            None => 0,
        } as f64;

        // We use the '+1' denominator trick to avoid division by zero, see https://en.wikipedia.org/wiki/Tf%E2%80%93idf#Inverse_document_frequency_2
        (document_total / (documents_with_word + 1.0)).ln()
    }

    fn get_tf(&self, word_entry: &WordInDocEntry) -> f64 {
        let mut tf = word_entry.term_frequency as f64;

        // Logarithmically scaled frequency @see https://en.wikipedia.org/wiki/Tf%E2%80%93idf#Term_frequency_2
        tf = (1.0 + tf).ln();

        // Boost on capitalized words (a capitalized word is a word with a capital letter anywhere : iPhone is considered capitalized, for instance.
        if self.config.boost_capitalized_words && word_entry.at_least_one_occurence_was_capitalized
        {
            tf = 2.0 * tf;
        }

        // Boost on words starting with 'lib' that are not 'library'
        if self.config.boost_lib
            && word_entry.word.starts_with("lib")
            && !word_entry.word.starts_with("library")
        {
            tf = 2.0 * tf;
        }

        tf
    }

    pub fn get_tf_idf(&self, word_entry: &WordInDocEntry) -> f64 {
        let tf = self.get_tf(word_entry);
        let idf = self.get_idf(&word_entry.word);

        let mut tfidf = tf * idf;

        let stacked_words_count = word_entry.word.split_whitespace().count();

        // We boost multiple words entry by a linear factor of the word count
        if self.config.boost_multiple_words {
            tfidf = tfidf * stacked_words_count as f64;
        }

        tfidf
    }

    pub fn get_unique_index(&self, word_entry: &WordInDocEntry) -> usize {
        self.index[&word_entry.word].unique_index
    }

    pub fn len(&self) -> usize {
        self.index.len()
    }
}

#[derive(Debug, Clone)]
pub struct WordInIndexEntry {
    pub word: String,
    pub document_with_word_count: u64,
    pub unique_index: usize,
}

#[derive(Clone, PartialEq, Debug)]
pub struct WordInDocEntry {
    pub word: String,
    pub at_least_one_occurence_was_capitalized: bool,
    pub term_frequency: u8,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_multiple_words() {
        let mut whitelist = WordIndex::new(None, provide_config());

        let entry1 = whitelist.publish_document(&"The Lord of the Rings".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of the rings".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of the".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of the rings".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "rings".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
                WordInDocEntry {
                    word: "the lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the lord of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the lord of the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the rings".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
            ],
            entry1
        );

        let entry2 = whitelist.publish_document(&"The Lord of the Flies".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "flies".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of the flies".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of the".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of the flies".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
                WordInDocEntry {
                    word: "the flies".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the lord of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the lord of the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
            ],
            entry2
        );

        let entry3 = whitelist.publish_document(&"The Ace of Spades".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "ace".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "ace of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "ace of spades".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of spades".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "spades".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the ace".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the ace of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the ace of spades".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
            ],
            entry3
        );

        assert_eq!(
            2.0 * (3.0 as f64).ln() * (3.0 / 4.0 as f64).ln(),
            whitelist.get_tf_idf(&entry1[8])
        );
        assert_eq!(
            ((2.0 as f64).ln() * (3.0 / 2.0 as f64).ln()) * 2.0,
            whitelist.get_tf_idf(&entry1[12])
        );

        let mut index = WordIndex::new(Some(Box::new(whitelist)), provide_config());

        let entry4 = index.publish_document(&"The Lord of the Ragondins".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord of the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of the".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
                WordInDocEntry {
                    word: "the lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the lord of".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the lord of the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
            ],
            entry4
        );

        assert_eq!(0.0, index.get_idf(&"ragondins".to_string()));
    }

    #[test]
    fn test_whitelist() {
        let mut config = provide_config();
        config.consecutive_words = 1;

        let mut whitelist = WordIndex::new(None, config.clone());

        whitelist.publish_document(&"The Lord of the Rings".to_string());
        whitelist.publish_document(&"The Lord of the Flies".to_string());
        whitelist.publish_document(&"The Ace of Spades".to_string());

        let mut index = WordIndex::new(Some(Box::new(whitelist)), config.clone());

        let entry4 = index.publish_document(&"The Lord of the Ragondins".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
            ],
            entry4
        );

        assert_eq!(0.0, index.get_idf(&"ragondins".to_string()));
    }

    #[test]
    fn test_index() {
        let mut config = provide_config();
        config.consecutive_words = 1;
        let mut index = WordIndex::new(None, config);

        let entry1 = index.publish_document(&"The Lord of the Rings".to_string());
        let entry2 = index.publish_document(&"The Lord of the Flies".to_string());
        let entry3 = index.publish_document(&"The Ace of liBerty".to_string());

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "rings".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
            ],
            entry1
        );

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "flies".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "lord".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 2,
                },
            ],
            entry2
        );

        assert_eq!(
            vec![
                WordInDocEntry {
                    word: "ace".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "liberty".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "of".to_string(),
                    at_least_one_occurence_was_capitalized: false,
                    term_frequency: 1,
                },
                WordInDocEntry {
                    word: "the".to_string(),
                    at_least_one_occurence_was_capitalized: true,
                    term_frequency: 1,
                },
            ],
            entry3
        );

        // We use the '+1' denominator trick to avoid division by zero, see https://en.wikipedia.org/wiki/Tf%E2%80%93idf#Inverse_document_frequency_2
        assert_eq!((3.0 / 2.0 as f64).ln(), index.get_idf(&"ace".to_string()));
        assert_eq!((3.0 / 2.0 as f64).ln(), index.get_idf(&"flies".to_string()));
        assert_eq!((3.0 / 3.0 as f64).ln(), index.get_idf(&"lord".to_string()));
        assert_eq!((3.0 / 4.0 as f64).ln(), index.get_idf(&"of".to_string()));
        assert_eq!((3.0 / 2.0 as f64).ln(), index.get_idf(&"rings".to_string()));
        assert_eq!(
            (3.0 / 2.0 as f64).ln(),
            index.get_idf(&"liberty".to_string())
        );
        assert_eq!((3.0 / 4.0 as f64).ln(), index.get_idf(&"the".to_string()));

        assert_eq!(
            2.0 * (3.0 as f64).ln() * (3.0 / 4.0 as f64).ln(),
            index.get_tf_idf(&entry1[3])
        );
        assert_eq!(
            2.0 * (2.0 as f64).ln() * (3.0 / 2.0 as f64).ln(),
            index.get_tf_idf(&entry2[0])
        );
        assert_eq!(
            2.0 * 2.0 * (2.0 as f64).ln() * (3.0 / 2.0 as f64).ln(),
            index.get_tf_idf(&entry3[1])
        );
    }

    fn provide_config() -> IndexConfig {
        IndexConfig {
            consecutive_words: 4,
            boost_multiple_words: true,
            boost_capitalized_words: true,
            boost_lib: true,
        }
    }
}
