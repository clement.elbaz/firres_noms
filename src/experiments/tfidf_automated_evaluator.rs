/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use config::FirresConfig;
use std::collections::btree_map::BTreeMap;
use std::fs;
use std::fs::File;
use std::io::{BufReader, BufWriter};
use std::path::PathBuf;
use time::timeatlas::TimeAtlas;
use time::timeframe::TimeFrame;
use vulnerability::cve;
use vulnerability::cve::CVEDictionaryEntry;

use experiments::tfidf_groundtruth::CpeUrl;
use experiments::tfidf_groundtruth::TFIDFGroundTruth;

pub struct TFIDFAutomatedEvaluator {
    evaluation_cache: EvaluationCache,
    cache_filename: PathBuf,
    evaluation_first_frame_id: u32,
    evaluation_nb_frames: u32,
    keyword_truncation: f64,
    ground_truth: TFIDFGroundTruth,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct EvaluationCache {
    samples: BTreeMap<String, SampleResult>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct SampleResult {
    keyword_untruncated_list_length: i32,
    keyword_truncated_list_length: i32,
    first_relevant_keyword_position: i32,
    number_of_necessary_keywords_for_reconstruction: i32,
    highest_keyword_weight: f64,
    untruncated_keyword_vector_norm: f64,
    truncated_keyword_vector_norm: f64,
    description: String,
    top_three_keywords: Vec<String>,
}

impl TFIDFAutomatedEvaluator {
    pub fn new(
        cache_filename: PathBuf,
        evaluation_first_frame_id: u32,
        evaluation_nb_frames: u32,
        keyword_truncation: f64,
        ground_truth: TFIDFGroundTruth,
    ) -> TFIDFAutomatedEvaluator {
        let f = File::open(&cache_filename);
        let evaluation_cache = match f {
            Err(_e) => EvaluationCache {
                samples: BTreeMap::new(),
            },
            Ok(file) => serde_json::from_reader(BufReader::new(file)).unwrap(),
        };

        let f = BufWriter::new(File::create(&cache_filename).unwrap());
        serde_json::to_writer(f, &evaluation_cache).unwrap();

        TFIDFAutomatedEvaluator {
            evaluation_cache,
            cache_filename,
            evaluation_first_frame_id,
            evaluation_nb_frames,
            keyword_truncation,
            ground_truth,
        }
    }

    pub fn evaluate(&mut self, time_atlas: &TimeAtlas, firres_config: &FirresConfig) {
        let mut current_frame =
            time_atlas.fetch_frame(self.evaluation_first_frame_id, firres_config);

        while self.need_more_frames(&current_frame) {
            // additionnal brackets needed to guide Rust borrow checker
            {
                let one_day_vulnerabilities = current_frame
                    .get_vulnerabilities_published_during_frame(time_atlas.get_nb_days_per_frame());

                println!(
                    "{} vulnerabilities will be analyzed on this day.",
                    one_day_vulnerabilities.len()
                );

                for entry in one_day_vulnerabilities {
                    let result = self.evaluate_vulnerability_analysis(entry, &current_frame);
                    if result.is_some() {
                        self.evaluation_cache
                            .samples
                            .insert(entry.cve_id.clone(), result.unwrap());
                    } else {
                        println!("{} did not have any CPE URLs ground truth.", entry.cve_id);
                    }
                }
            }
            self.write_evaluation_to_disk();

            println!("Let's go into the future...");
            current_frame = time_atlas.get_next_frame(current_frame, firres_config);
            println!(
                "Welcome to the future. Current date is now {}.",
                &current_frame.current_date
            );
        }

        self.print_summary();
    }

    fn need_more_frames(&self, current_frame: &TimeFrame) -> bool {
        (current_frame.frame_id as u32)
            < (self.evaluation_first_frame_id as u32) + self.evaluation_nb_frames
    }

    fn write_evaluation_to_disk(&mut self) {
        // Remove out of date file and recreate up to date file. This is not production-ready (but the whole codebase isn't, for that matter).
        fs::remove_file(&self.cache_filename).unwrap();
        let f = BufWriter::new(File::create(&self.cache_filename).unwrap());
        serde_json::to_writer(f, &self.evaluation_cache).unwrap();
    }

    fn print_summary(&self) {
        println!("=========== EVALUATION RESULTS ===========");
        println!(
            "Total evaluated vulnerabilities : {}",
            self.evaluation_cache.samples.len()
        );

        for entry_name in self.evaluation_cache.samples.keys() {
            let result = self.evaluation_cache.samples.get(entry_name).unwrap();

            println!("{} - First relevant keyword position : {} | Number of necessary keywords for reconstruction : {}", entry_name,
            result.first_relevant_keyword_position, result.number_of_necessary_keywords_for_reconstruction);
        }
        println!("==========================================");
    }

    fn evaluate_vulnerability_analysis(
        &self,
        entry: &CVEDictionaryEntry,
        time_frame: &TimeFrame,
    ) -> Option<SampleResult> {
        if !self.ground_truth.cpe_urls.contains_key(&entry.cve_id) {
            return None;
        }

        let cpe_urls = self.ground_truth.cpe_urls.get(&entry.cve_id).unwrap();

        let relevant_keywords = self.compute_tfidf_stringarray(entry, time_frame);

        let mut first_relevant_keyword_position = -1;

        'outerlooprelevance: for i in 0..relevant_keywords.len() {
            for cpe_url in cpe_urls {
                if self.is_keyword_relevant(&relevant_keywords[i].0, cpe_url) {
                    first_relevant_keyword_position = i as i32;
                    break 'outerlooprelevance;
                }
            }
        }

        let mut number_of_necessary_keywords_for_reconstruction = -1;

        'outerloopreconstruct: for i in 0..relevant_keywords.len() {
            let first_i_keywords = relevant_keywords[0..i + 1]
                .iter()
                .map(|keyword| keyword.0.clone())
                .collect();

            for cpe_url in cpe_urls {
                if self.reconstruct_software_name_from_keywords(
                    &cpe_url.software_name,
                    &first_i_keywords,
                ) {
                    number_of_necessary_keywords_for_reconstruction = i as i32;
                    break 'outerloopreconstruct;
                }
            }
        }

        let keyword_untruncated_list_length = relevant_keywords.len() as i32;
        let untruncated_keyword_vector_norm = self.compute_norm(&relevant_keywords);

        let truncated_keywords_list = self.truncate(relevant_keywords);

        let keyword_truncated_list_length = truncated_keywords_list.len() as i32;
        let truncated_keyword_vector_norm = self.compute_norm(&truncated_keywords_list);

        let highest_keyword_weight = if truncated_keywords_list.len() > 0 {
            truncated_keywords_list[0].1
        } else {
            0.0
        };

        let top_three_keywords = truncated_keywords_list
            .iter()
            .take(3)
            .map(|keyword| keyword.0.clone())
            .collect::<Vec<_>>();

        Some(SampleResult {
            keyword_untruncated_list_length,
            keyword_truncated_list_length,
            first_relevant_keyword_position,
            number_of_necessary_keywords_for_reconstruction,
            highest_keyword_weight,
            untruncated_keyword_vector_norm,
            truncated_keyword_vector_norm,
            description: entry.description.clone(),
            top_three_keywords: top_three_keywords,
        })
    }

    fn truncate(&self, untruncated_keywords: Vec<(String, f64)>) -> Vec<(String, f64)> {
        if self.keyword_truncation >= 1.0 || untruncated_keywords.len() == 0 {
            untruncated_keywords
        } else {
            let norm = self.compute_norm(&untruncated_keywords);

            let target_norm = self.keyword_truncation * norm;

            let target_squared_norm = target_norm * target_norm;

            let mut current_squared_norm = norm * norm;

            let mut keywords = untruncated_keywords;

            loop {
                let tentative_squared_norm = {
                    let least_relevant_keyword = keywords.last().unwrap();
                    current_squared_norm - least_relevant_keyword.1 * least_relevant_keyword.1
                };
                if target_squared_norm <= tentative_squared_norm {
                    keywords.pop().unwrap();
                    current_squared_norm = tentative_squared_norm;
                } else {
                    break;
                }
            }

            keywords
        }
    }

    fn compute_norm(&self, keywords: &Vec<(String, f64)>) -> f64 {
        let mut squared_norm: f64 = 0.0;
        for keyword in keywords {
            squared_norm += keyword.1 * keyword.1;
        }

        squared_norm.sqrt()
    }

    fn compute_tfidf_stringarray(
        &self,
        entry: &CVEDictionaryEntry,
        time_frame: &TimeFrame,
    ) -> Vec<(String, f64)> {
        let mut result = Vec::new();

        let index_features =
            cve::compute_cve_index_score_vector(&entry.words, &time_frame.cve_index);

        for feature_id in index_features.sorted_keys() {
            let word = &time_frame.reverse_index[*feature_id as usize];
            let score = index_features.get_feature_value(*feature_id);
            result.push((word.clone(), score));
        }

        result.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap());

        result
    }

    fn is_keyword_relevant(&self, keyword: &String, cpe_url: &CpeUrl) -> bool {
        cpe_url.software_name.contains(keyword.as_str())
            || cpe_url.software_vendor.contains(keyword.as_str())
    }

    fn reconstruct_software_name_from_keywords(
        &self,
        software_name: &String,
        keywords: &Vec<String>,
    ) -> bool {
        let mut mutated_software_name = software_name.clone();

        // We order the keywords by descending size as not doing so can lead to incorrect results.
        // Example : CVE-2018-14048 is a vuln about libpng (which is a software name), containing both keywords libpng and png in its description.
        // If you remove png before libpng, the software name libpng becomes 'lib' and when we tries to remove 'libpng', we can't anymore
        // However if we systematically try libpng before, we don't run into this problem.
        let mut keywords_ordered_by_size = keywords.clone();
        keywords_ordered_by_size.sort_by(|a, b| b.len().cmp(&a.len()));

        for keyword in keywords_ordered_by_size {
            mutated_software_name = mutated_software_name.replace(keyword.as_str(), "");

            if mutated_software_name.trim().is_empty() {
                return true;
            }
        }

        false
    }
}
