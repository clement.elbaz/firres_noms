/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use config::{FirresConfig, TFIDFXPConfig};
use config_crate;
use experiments::tfidf_automated_evaluator::TFIDFAutomatedEvaluator;
use experiments::tfidf_groundtruth::TFIDFGroundTruth;
use std::fs;
use std::fs::File;
use std::io;
use std::io::BufWriter;
use std::io::Write;
use std::path::PathBuf;
use time::timeatlas::TimeAtlas;
use toml;

pub mod tfidf_automated_evaluator;
pub mod tfidf_groundtruth;

pub fn launch_relevance_experiment(firres_config: &FirresConfig, time_atlas: &TimeAtlas) {
    let xp_path = create_new_experiment();
    let config = get_config_for_experiment(&xp_path, &firres_config.experiments.tfidf);

    let mut tfidf_automated_evaluation_cache_filename = xp_path.clone();
    tfidf_automated_evaluation_cache_filename.push("tfidf.automated.evaluation.json");

    let tfidf_groundtruth = TFIDFGroundTruth::new(&config.groundtruth_repository);

    let mut tfidf_automated_evaluator = TFIDFAutomatedEvaluator::new(
        tfidf_automated_evaluation_cache_filename,
        config.automated_starting_frame_id,
        config.automated_evaluation_nb_frames,
        config.keyword_truncation,
        tfidf_groundtruth,
    );

    tfidf_automated_evaluator.evaluate(time_atlas, firres_config);
}

fn get_config_for_experiment(xp_path: &PathBuf, default_config: &TFIDFXPConfig) -> TFIDFXPConfig {
    let mut config_file_path = xp_path.clone();
    config_file_path.push("config.toml");

    if config_file_path.exists() {
        let mut config = config_crate::Config::default();
        config
            .merge(config_crate::File::from(config_file_path))
            .unwrap();

        config.try_into().unwrap()
    } else {
        let new_config = default_config.clone();

        let serialized_default_config = toml::to_string_pretty(&new_config).unwrap();
        let mut f = BufWriter::new(File::create(config_file_path).unwrap());

        f.write(serialized_default_config.as_bytes()).unwrap();

        new_config
    }
}

fn create_new_experiment() -> PathBuf {
    let mut xp_path = PathBuf::new();
    xp_path.push("experiments");

    println!("Please enter the name of the experiment =>");
    let mut xp_name = String::new();
    io::stdin().read_line(&mut xp_name).unwrap();

    xp_path.push(xp_name.trim());

    if xp_path.exists() {
        if xp_path.is_dir() {
            if !ask_yes_no("This experiment already exists. Resume it ? (y/n)".to_string()) {
                xp_path = create_new_experiment();
            }
        } else {
            println!("Invalid experiment name.");
            xp_path = create_new_experiment();
        }
    }

    fs::create_dir_all(xp_path.clone()).unwrap();

    xp_path
}

fn ask_yes_no(question: String) -> bool {
    println!("{}", question);
    loop {
        let mut answer = String::new();
        io::stdin().read_line(&mut answer).unwrap();

        match answer.trim().as_ref() {
            "y" => return true,
            "n" => return false,
            _ => println!("Please answer the question !"),
        }
    }
}
