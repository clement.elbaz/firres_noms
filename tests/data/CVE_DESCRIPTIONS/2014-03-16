CVE-2013-4057
Cross-site request forgery (CSRF) vulnerability in the XML Pack in IBM InfoSphere Information Server 8.5.x through 8.5 FP3, 8.7.x through 8.7 FP2, and 9.1.x through 9.1.2.0 allows remote attackers to hijack the authentication of arbitrary users.
CVE-2013-4058
Multiple SQL injection vulnerabilities in IBM InfoSphere Information Server 8.x through 8.5 FP3, 8.7.x through 8.7 FP2, and 9.1.x through 9.1.2.0 allow remote authenticated users to execute arbitrary SQL commands via unspecified interfaces.
CVE-2013-4059
Multiple cross-site scripting (XSS) vulnerabilities in IBM InfoSphere Information Server 8.x through 8.5 FP3, 8.7.x through 8.7 FP2, and 9.1.x through 9.1.2.0 allow remote attackers to inject arbitrary web script or HTML via unspecified interfaces.
CVE-2013-6208
Unspecified vulnerability in HP Smart Update Manager 5.3.5 before build 70 on Linux allows local users to gain privileges via unknown vectors.
CVE-2013-6210
Unspecified vulnerability in HP Unified Functional Testing before 12.0 allows remote attackers to execute arbitrary code via unknown vectors, aka ZDI-CAN-1932.
CVE-2014-0338
Multiple cross-site scripting (XSS) vulnerabilities in the firewall policy management pages in WatchGuard Fireware XTM before 11.8.3 allow remote attackers to inject arbitrary web script or HTML via the pol_name parameter.
CVE-2014-0339
Cross-site scripting (XSS) vulnerability in view.cgi in Webmin before 1.680 allows remote attackers to inject arbitrary web script or HTML via the search parameter.
CVE-2014-0850
Cross-site scripting (XSS) vulnerability in IBM InfoSphere Master Data Management Reference Data Management (RDM) Hub 10.1 and 11.0 before 11.0.0.0-MDM-IF008 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-0873
Multiple cross-site request forgery (CSRF) vulnerabilities in the (1) Data Stewardship, (2) Business Admin, and (3) Product interfaces in IBM InfoSphere Master Data Management (MDM) Server 8.5 before 8.5.0.82, 9.0.1 before 9.0.1.38, 9.0.2 before 9.0.2.35, 10.0 before 10.0.0.0.26, and 10.1 before 10.1.0.0.15 allow remote attackers to hijack the authentication of arbitrary users.
CVE-2014-0895
Buffer overflow in the vsflex8l ActiveX control in IBM SPSS SamplePower 3.0.1 before FP1 3.0.1-IM-S3SAMPC-WIN32-FP001-IF02 allows remote attackers to execute arbitrary code via a crafted ComboList property value.
CVE-2014-1700
Use-after-free vulnerability in modules/speech/SpeechSynthesis.cpp in Blink, as used in Google Chrome before 33.0.1750.149, allows remote attackers to cause a denial of service or possibly have unspecified other impact by leveraging improper handling of a certain utterance data structure.
CVE-2014-1701
The GenerateFunction function in bindings/scripts/code_generator_v8.pm in Blink, as used in Google Chrome before 33.0.1750.149, does not implement a certain cross-origin restriction for the EventTarget::dispatchEvent function, which allows remote attackers to conduct Universal XSS (UXSS) attacks via vectors involving events.
CVE-2014-1702
Use-after-free vulnerability in the DatabaseThread::cleanupDatabaseThread function in modules/webdatabase/DatabaseThread.cpp in the web database implementation in Blink, as used in Google Chrome before 33.0.1750.149, allows remote attackers to cause a denial of service or possibly have unspecified other impact by leveraging improper handling of scheduled tasks during shutdown of a thread.
CVE-2014-1703
Use-after-free vulnerability in the WebSocketDispatcherHost::SendOrDrop function in content/browser/renderer_host/websocket_dispatcher_host.cc in the Web Sockets implementation in Google Chrome before 33.0.1750.149 might allow remote attackers to bypass the sandbox protection mechanism by leveraging an incorrect deletion in a certain failure case.
CVE-2014-1704
Multiple unspecified vulnerabilities in Google V8 before 3.23.17.18, as used in Google Chrome before 33.0.1750.149, allow attackers to cause a denial of service or possibly have other impact via unknown vectors.
CVE-2014-1705
Google V8, as used in Google Chrome before 33.0.1750.152 on OS X and Linux and before 33.0.1750.154 on Windows, allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2014-1706
crosh in Google Chrome OS before 33.0.1750.152 allows attackers to inject commands via unspecified vectors.
CVE-2014-1707
Directory traversal vulnerability in CrosDisks in Google Chrome OS before 33.0.1750.152 has unspecified impact and attack vectors.
CVE-2014-1708
The boot implementation in Google Chrome OS before 33.0.1750.152 does not properly consider file persistence, which allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2014-1710
The AsyncPixelTransfersCompletedQuery::End function in gpu/command_buffer/service/query_manager.cc in Google Chrome, as used in Google Chrome OS before 33.0.1750.152, does not check whether a certain position is within the bounds of a shared-memory segment, which allows remote attackers to cause a denial of service (GPU command-buffer memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2014-1711
The GPU driver in the kernel in Google Chrome OS before 33.0.1750.152 allows remote attackers to cause a denial of service (out-of-bounds write) or possibly have unspecified other impact via unknown vectors.
CVE-2014-1713
Use-after-free vulnerability in the AttributeSetter function in bindings/templates/attributes.cpp in the bindings in Blink, as used in Google Chrome before 33.0.1750.152 on OS X and Linux and before 33.0.1750.154 on Windows, allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors involving the document.location value.
CVE-2014-1714
The ScopedClipboardWriter::WritePickledData function in ui/base/clipboard/scoped_clipboard_writer.cc in Google Chrome before 33.0.1750.152 on OS X and Linux and before 33.0.1750.154 on Windows does not verify a certain format value, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to the clipboard.
CVE-2014-1715
Directory traversal vulnerability in Google Chrome before 33.0.1750.152 on OS X and Linux and before 33.0.1750.154 on Windows has unspecified impact and attack vectors.
CVE-2014-2246
Cross-site scripting (XSS) vulnerability in the integrated web server on Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-2247
The integrated web server on Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 allows remote attackers to inject headers via unspecified vectors.
CWE-80: Improper Neutralization of Script-Related HTML Tags in a Web Page (Basic XSS)
CVE-2014-2248
Open redirect vulnerability in the integrated web server on Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CWE-601: URL Redirection to Untrusted Site (“Open Redirect”)
CVE-2014-2249
Cross-site request forgery (CSRF) vulnerability on Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 and SIMATIC S7-1200 CPU PLC devices with firmware before 4.0 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2014-2251
The random-number generator on Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 does not have sufficient entropy, which makes it easier for remote attackers to defeat cryptographic protection mechanisms and hijack sessions via unspecified vectors.
CWE-331: Insufficient Entropy
CVE-2014-2253
Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 allow remote attackers to cause a denial of service (defect-mode transition) via crafted Profinet packets.
CWE-404: Improper Resource Shutdown or Release
CVE-2014-2255
Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 allow remote attackers to cause a denial of service (defect-mode transition) via crafted HTTP packets.
CWE-404: Improper Resource Shutdown or Release
CVE-2014-2257
Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 allow remote attackers to cause a denial of service (defect-mode transition) via crafted ISO-TSAP packets.
CWE-404: Improper Resource Shutdown or Release
CVE-2014-2259
Siemens SIMATIC S7-1500 CPU PLC devices with firmware before 1.5.0 allow remote attackers to cause a denial of service (defect-mode transition) via crafted HTTPS packets.
CWE-404: Improper Resource Shutdown or Release
