CVE-2017-11632
An issue was discovered on Wireless IP Camera 360 devices. A root account with a known SHA-512 password hash exists, which makes it easier for remote attackers to obtain administrative access via a TELNET session.
CVE-2017-11633
An issue was discovered on Wireless IP Camera 360 devices. Remote attackers can discover RTSP credentials by connecting to TCP port 9527 and reading the InsertConnect field.
CVE-2017-11634
An issue was discovered on Wireless IP Camera 360 devices. Remote attackers can discover a weakly encoded admin password by connecting to TCP port 9527 and reading the password field of the debugging information, e.g., nTBCS19C corresponds to a password of 123456.
CVE-2017-11635
An issue was discovered on Wireless IP Camera 360 devices. Attackers can read recordings by navigating to /mnt/idea0 or /mnt/idea1 on the SD memory card.
CVE-2017-15696
When an Apache Geode cluster before v1.4.0 is operating in secure mode, the Geode configuration service does not properly authorize configuration requests. This allows an unprivileged user who gains access to the Geode locator to extract configuration data and previously deployed application code.
CVE-2017-16229
In the Ox gem 2.8.1 for Ruby, the process crashes with a stack-based buffer over-read in the read_from_str function in sax_buf.c when a crafted input is supplied to sax_parse.
CVE-2017-16813
A denial-of-service issue was discovered in the Foxit MobilePDF app before 6.1 for iOS. This occurs when a user uploads a file that includes a hexadecimal Unicode character in the "filename" parameter via Wi-Fi, since the app could fail to parse this.
CVE-2017-16814
A Directory Traversal issue was discovered in the Foxit MobilePDF app before 6.1 for iOS. This occurs by abusing the URL + escape character during a Wi-Fi transfer, which could be exploited by attackers to bypass intended restrictions on local application files.
CVE-2017-1774
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 136818.
CVE-2017-18195
An issue was discovered in tools/conversations/view_ajax.php in Concrete5 before 8.3.0. An unauthenticated user can enumerate comments from all blog posts by POSTing requests to /index.php/tools/required/conversations/view_ajax with incremental 'cnvID' integers.
CVE-2017-18200
The f2fs implementation in the Linux kernel before 4.14 mishandles reference counts associated with f2fs_wait_discard_bios calls, which allows local users to cause a denial of service (BUG), as demonstrated by fstrim.
CVE-2017-18201
An issue was discovered in GNU libcdio before 2.0.0. There is a double free in get_cdtext_generic() in lib/driver/_cdio_generic.c.
CVE-2017-9425
The Facetag extension 0.0.3 for Piwigo allows XSS via the name parameter to ws.php in a facetag.changeTag action.
CVE-2017-9426
ws.php in the Facetag extension 0.0.3 for Piwigo allows SQL injection via the imageId parameter in a facetag.changeTag or facetag.listTags action.
CVE-2018-0908
Microsoft Identity Manager 2016 SP1 allows an attacker to gain elevated privileges when it does not properly sanitize a specially crafted attribute value being displayed to a user on an affected MIM 2016 server, aka "Microsoft Identity Manager XSS Elevation of Privilege Vulnerability."
CVE-2018-1377
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 stores user credentials in plain in clear text which can be read by a local user. IBM X-Force ID: 137778.
CVE-2018-5762
The TLS implementation in the TCP/IP networking module in Unisys ClearPath MCP systems with TCP-IP-SW 58.1 before 58.160, 59.1 before 059.1a.17 (IC #17), and 60.0 before 60.044 might allow remote attackers to decrypt TLS ciphertext data by leveraging a Bleichenbacher RSA padding oracle, aka a ROBOT attack.
CVE-2018-7249
An issue was discovered in secdrv.sys as shipped in Microsoft Windows Vista, Windows 7, Windows 8, and Windows 8.1 before KB3086255, and as shipped in Macrovision SafeDisc. Two carefully timed calls to IOCTL 0xCA002813 can cause a race condition that leads to a use-after-free. When exploited, an unprivileged attacker can run arbitrary code in the kernel.
CVE-2018-7250
An issue was discovered in secdrv.sys as shipped in Microsoft Windows Vista, Windows 7, Windows 8, and Windows 8.1 before KB3086255, and as shipped in Macrovision SafeDisc. An uninitialized kernel pool allocation in IOCTL 0xCA002813 allows a local unprivileged attacker to leak 16 bits of uninitialized kernel PagedPool data.
CVE-2018-7448
Remote code execution vulnerability in /cmsms-2.1.6-install.php/index.php in CMS Made Simple version 2.1.6 allows remote attackers to inject arbitrary PHP code via the "timezone" parameter in step 4 of a fresh installation procedure.
CVE-2018-7463
SQL injection vulnerability in files.php in the "files" component in ASANHAMAYESH CMS 3.4.6 allows a remote attacker to execute arbitrary SQL commands via the "id" parameter.
CVE-2018-7479
YzmCMS 3.6 allows remote attackers to discover the full path via a direct request to application/install/templates/s1.php.
CVE-2018-7484
An issue was discovered in PureVPN through 5.19.4.0 on Windows. The client installation grants the Everyone group Full Control permission to the installation directory. In addition, the PureVPNService.exe service, which runs under NT Authority\SYSTEM privileges, tries to load several dynamic-link libraries using relative paths instead of the absolute path. When not using a fully qualified path, the application will first try to load the library from the directory from which the application is started. As the residing directory of PureVPNService.exe is writable to all users, this makes the application susceptible to privilege escalation through DLL hijacking.
CVE-2018-7485
The SQLWriteFileDSN function in odbcinst/SQLWriteFileDSN.c in unixODBC 2.3.5 has strncpy arguments in the wrong order, which allows attackers to cause a denial of service or possibly have unspecified other impact.
CVE-2018-7486
Blue River Mura CMS before v7.0.7029 supports inline function calls with an [m] tag and [/m] end tag, without proper restrictions on file types or pathnames, which allows remote attackers to execute arbitrary code via an [m]$.dspinclude("../pathname/executable.jpeg")[/m] approach, where executable.jpeg contains ColdFusion Markup Language code. This can be exploited in conjunction with a CKFinder feature that allows file upload.
CVE-2018-7487
There is a heap-based buffer overflow in the LoadPCX function of in_pcx.cpp in sam2p 0.49.4. A Crafted input will lead to a denial of service or possibly unspecified other impact.
CVE-2018-7489
FasterXML jackson-databind before 2.7.9.3, 2.8.x before 2.8.11.1 and 2.9.x before 2.9.5 allows unauthenticated remote code execution because of an incomplete fix for the CVE-2017-7525 deserialization flaw. This is exploitable by sending maliciously crafted JSON input to the readValue method of the ObjectMapper, bypassing a blacklist that is ineffective if the c3p0 libraries are available in the classpath.
CVE-2018-7490
uWSGI before 2.0.17 mishandles a DOCUMENT_ROOT check during use of the --php-docroot option, allowing directory traversal.
CVE-2018-7491
In PrestaShop through 1.7.2.5, a UI-Redressing/Clickjacking vulnerability was found that might lead to state-changing impact in the context of a user or an admin, because the generateHtaccess function in classes/Tools.php sets neither X-Frame-Options nor 'Content-Security-Policy "frame-ancestors' values.
CVE-2018-7492
A NULL pointer dereference was found in the net/rds/rdma.c __rds_rdma_map() function in the Linux kernel before 4.14.7 allowing local attackers to cause a system panic and a denial-of-service, related to RDS_GET_MR and RDS_GET_MR_FOR_DEST.
