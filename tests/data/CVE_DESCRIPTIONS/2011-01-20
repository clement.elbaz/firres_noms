CVE-2010-2743
The kernel-mode drivers in Microsoft Windows XP SP3 do not properly perform indexing of a function-pointer table during the loading of keyboard layouts from disk, which allows local users to gain privileges via a crafted application, as demonstrated in the wild in July 2010 by the Stuxnet worm, aka "Win32k Keyboard Layout Vulnerability."  NOTE: this might be a duplicate of CVE-2010-3888 or CVE-2010-3889.
CVE-2010-3928
Ruby Version Manager (RVM) before 1.2.1 writes file contents to a terminal without sanitizing non-printable characters, which might allow remote attackers to execute arbitrary commands via a crafted file, related to an "escape sequence injection vulnerability." NOTE: some of these details are obtained from third party information.
CVE-2010-3931
Cross-site scripting (XSS) vulnerability in multiple Rocomotion products, including P board 1.18 and other versions, P forum 1.30 and earlier, P up board 1.38 and other versions, P diary R 1.13 and earlier, P link 1.11 and earlier, P link compact 1.04 and earlier, pplog 3.31 and earlier, pplog2 3.37 and earlier, PM bbs 1.07 and earlier, PM up bbs 1.08 and earlier, and PM forum 1.18 and earlier, allows remote attackers to inject arbitrary web script or HTML via unknown vectors.
CVE-2010-4071
Cross-site scripting (XSS) vulnerability in AgentTicketZoom in OTRS 2.4.x before 2.4.9, when RichText is enabled, allows remote attackers to inject arbitrary web script or HTML via JavaScript in an HTML e-mail.
CVE-2010-4267
Stack-based buffer overflow in the hpmud_get_pml function in io/hpmud/pml.c in Hewlett-Packard Linux Imaging and Printing (HPLIP) 1.6.7, 3.9.8, 3.10.9, and probably other versions allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted SNMP response with a large length value.
CVE-2010-4331
Multiple cross-site scripting (XSS) vulnerabilities in Seo Panel 2.2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) default_news or (2) sponsors cookies, which are not properly handled by (a) controllers/index.ctrl.php or (b) controllers/settings.ctrl.php.
CVE-2010-4338
ocrodjvu 0.4.6-1 on Debian GNU/Linux allows local users to modify arbitrary files via a symlink attack on temporary files that are generated when Cuneiform is invoked as the OCR engine.
CVE-2010-4351
The JNLP SecurityManager in IcedTea (IcedTea.so) 1.7 before 1.7.7, 1.8 before 1.8.4, and 1.9 before 1.9.4 for Java OpenJDK returns from the checkPermission method instead of throwing an exception in certain circumstances, which might allow context-dependent attackers to bypass the intended security policy by creating instances of ClassLoader.
CVE-2010-4701
Heap-based buffer overflow in the CDrawPoly::Serialize function in fxscover.exe in Microsoft Windows Fax Services Cover Page Editor 5.2 r2 in Windows XP Professional SP3, Server 2003 R2 Enterprise Edition SP2, and Windows 7 Professional allows remote attackers to execute arbitrary code via a long record in a Fax Cover Page (.cov) file. NOTE: some of these details are obtained from third party information.
CVE-2010-4702
SQL injection vulnerability in JRadio (com_jradio) component before 1.5.1 for Joomla! allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2010-4703
SQL injection vulnerability in default.asp in HotWebScripts HotWeb Rentals allows remote attackers to execute arbitrary SQL commands via the PageId parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2011-0008
A certain Fedora patch for parse.c in sudo before 1.7.4p5-1.fc14 on Fedora 14 does not properly interpret a system group (aka %group) in the sudoers file during authorization decisions for a user who belongs to that group, which allows local users to leverage an applicable sudoers file and gain root privileges via a sudo command.  NOTE: this vulnerability exists because of a CVE-2009-0034 regression.
CVE-2011-0495
Stack-based buffer overflow in the ast_uri_encode function in main/utils.c in Asterisk Open Source before 1.4.38.1, 1.4.39.1, 1.6.1.21, 1.6.2.15.1, 1.6.2.16.1, 1.8.1.2, 1.8.2.; and Business Edition before C.3.6.2; when running in pedantic mode allows remote authenticated users to execute arbitrary code via crafted caller ID data in vectors involving the (1) SIP channel driver, (2) URIENCODE dialplan function, or (3) AGI dialplan function.
CVE-2011-0496
Unspecified vulnerability in Sybase EAServer 5.x and 6.x before 6.3 ESD#2, as used in Appeon, Replication Server Messaging Edition (RSME), and WorkSpace, allows remote attackers to install arbitrary web services and execute arbitrary code, related to a "design vulnerability."
Per: http://www.sybase.com/detail?id=1091057

' Remote exploitation of a design vulnerability in Sybase EAServer could allow an attacker to install arbitrary web services, this condition can result in arbitrary code execution allowing attacker to gain control over the affected machine.

This also affects those products that include EAServer: Appeon, Replication Server Messaging Edition, and WorkSpace.'
CVE-2011-0497
Directory traversal vulnerability in Sybase EAServer 6.x before 6.3 ESD#2, as used in Appeon, Replication Server Messaging Edition (RSME), and WorkSpace, allows remote attackers to read arbitrary files via "../\" (dot dot forward-slash backslash) sequences in a crafted request.
CVE-2011-0498
Stack-based buffer overflow in Nokia Multimedia Player 1.00.55.5010, and possibly other versions, allows user-assisted remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long entry in a playlist (.npl) file.
CVE-2011-0499
Buffer overflow in VideoSpirit Pro 1.6.8.1 and possibly earlier versions, and VideoSpirit Lite 1.4.0.1 and possibly other versions, allows user-assisted remote attackers to execute arbitrary code via a VideoSpirit project (.visprj) file containing a valitem element with a long "name" attribute.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2011-0500
Buffer overflow in VideoSpirit Pro 1.6.8.1, 1.68, and earlier; and VideoSpirit Lite 1.4.0.1 and possibly other versions; allows user-assisted remote attackers to execute arbitrary code via a VideoSpirit project (.visprj) file containing a valitem element with a long "value" attribute, as demonstrated using a valitem with the mp3 name.
CVE-2011-0501
Stack-based buffer overflow in Music Animation Machine MIDI Player 2006aug19 Release 035 and possibly other versions allows user-assisted remote attackers to execute arbitrary code via a long line in a .mamx file.
CVE-2011-0502
Music Animation Machine MIDI Player 2006aug19 Release 035 and possibly other versions allows user-assisted remote attackers to cause a denial of service (crash) and possibly have other unspecified impact via a long line in a MIDI (.mid) file.
Per: https://secunia.com/advisories/42790

'Successful exploitation allows execution of arbitrary code.'
CVE-2011-0503
Cross-site request forgery (CSRF) vulnerability in VaM Shop 1.6, 1.6.1, and probably earlier versions allows remote attackers to hijack the authentication of administrators for requests that (1) change user status via admin/customers.php or (2) change user permissions via admin/accounting.php.  NOTE: some of these details are obtained from third party information.
CVE-2011-0504
Multiple cross-site scripting (XSS) vulnerabilities in VaM Shop 1.6, 1.6.1, and probably earlier versions llow remote attackers to inject arbitrary web script or HTML via the (1) status parameter to admin/orders.php, (2) search parameter to admin/customers.php, or (3) STORE_NAME parameter to admin/configuration.php.
CVE-2011-0505
Directory traversal vulnerability in system/system.php in Zwii 2.1.1, when magic_quotes_gpc is disabled and register_globals is enabled, allows remote attackers to include and execute arbitrary local files via directory traversal sequences in the set[template][value] parameter.
CVE-2011-0506
Directory traversal vulnerability in modules/profile/user.php in Ax Developer CMS (AxDCMS) 0.1.1 allows remote attackers to execute arbitrary code via a .. (dot dot) in the aXconf[default_language] parameter.
CVE-2011-0507
FTPService.exe in Blackmoon FTP 3.1 Build 1735 and Build 1736 (3.1.7.1736), and possibly other versions before 3.1.8.1737, allows remote attackers to cause a denial of service (crash) via a large number of PORT commands with long arguments, which triggers a NULL pointer dereference.  NOTE: some of these details are obtained from third party information.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2011-0508
Cross-site scripting (XSS) vulnerability in system/modules/comments/Comments.php in Contao CMS 2.9.2, and possibly other versions before 2.9.3, allows remote attackers to inject arbitrary web script or HTML via the HTTP X_FORWARDED_FOR header, which is stored by system/libraries/Environment.php but not properly handled by a comments action to main.php.
CVE-2011-0509
Cross-site scripting (XSS) vulnerability in Vaadin before 6.4.9 allows remote attackers to inject arbitrary web script or HTML via unknown vectors related to the index page.
CVE-2011-0510
SQL injection vulnerability in cart.php in Advanced Webhost Billing System (AWBS) 2.9.2 and possibly earlier allows remote attackers to execute arbitrary SQL commands via the oid parameter in an add_other action.
CVE-2011-0511
SQL injection vulnerability in the allCineVid component (com_allcinevid) 1.0.0 for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter to index.php.
CVE-2011-0512
SQL injection vulnerability in team.php in the Teams Structure module 3.0 for PHP-Fusion allows remote attackers to execute arbitrary SQL commands via the team_id parameter.
CVE-2011-0513
DCR.sys driver in SecurStar DriveCrypt 5.4, 5.3, and earlier allows local users to execute arbitrary code via a crafted argument to the 0x00073800 IOCTL.
CVE-2011-0514
The RDS service (rds.exe) in HP Data Protector Manager 6.11 allows remote attackers to cause a denial of service (crash) via a packet with a large data size to TCP port 1530.
CVE-2011-0515
KisKrnl.sys 2011.1.13.89 and earlier in Kingsoft AntiVirus 2011 SP5.2 allows local users to cause a denial of service (crash) via a crafted request that is not properly handled by the KiFastCallEntry hook.
CVE-2011-0516
SQL injection vulnerability in mainx_a.php in E-PROMPT C BetMore Site Suite 4.0 through 4.2.0 allows remote attackers to execute arbitrary SQL commands via the bid parameter.
CVE-2011-0517
Stack-based buffer overflow in Sielco Sistemi Winlog Pro 2.07.00 and earlier, when Run TCP/IP server is enabled, allows remote attackers to cause a denial of service (crash) and execute arbitrary code via a crafted 0x02 opcode to TCP port 46823.
CVE-2011-0518
Directory traversal vulnerability in core/lib/router.php in LotusCMS Fraise 3.0, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via the system parameter to index.php.
CVE-2011-0519
SQL injection vulnerability in gallery.php in Gallarific PHP Photo Gallery script 2.1 and possibly other versions allows remote attackers to execute arbitrary SQL commands via the id parameter.
