CVE-2008-0082
An ActiveX control (Messenger.UIAutomation.1) in Windows Messenger 4.7 and 5.1 is marked as safe-for-scripting, which allows remote attackers to control the Messenger application, and "change state," obtain contact information, and establish audio or video connections without notification via unknown vectors.
CVE-2008-0120
Integer overflow in Microsoft PowerPoint Viewer 2003 allows remote attackers to execute arbitrary code via a PowerPoint file with a malformed picture index that triggers memory corruption, related to handling of CString objects, aka "Memory Allocation Vulnerability."
CVE-2008-0121
A "memory calculation error" in Microsoft PowerPoint Viewer 2003 allows remote attackers to execute arbitrary code via a PowerPoint file with an invalid picture index that triggers memory corruption, aka "Memory Calculation Vulnerability."
CVE-2008-1448
The MHTML protocol handler in a component of Microsoft Outlook Express 5.5 SP2 and 6 through SP1, and Windows Mail, does not assign the correct Internet Explorer Security Zone to UNC share pathnames, which allows remote attackers to bypass intended access restrictions and read arbitrary files via an mhtml: URI in conjunction with a redirection, aka "URL Parsing Cross-Domain Information Disclosure Vulnerability."
CVE-2008-1455
A "memory calculation error" in Microsoft Office PowerPoint 2000 SP3, 2002 SP3, 2003 SP2, and 2007 through SP1; Office Compatibility Pack for Word, Excel, and PowerPoint 2007 through SP1; and Office 2004 for Mac allows remote attackers to execute arbitrary code via a PowerPoint file with crafted list values that trigger memory corruption, aka "Parsing Overflow Vulnerability."
CVE-2008-1456
Array index vulnerability in the Event System in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 allows remote authenticated users to execute arbitrary code via a crafted event subscription request that is used to access an array of function pointers.
CVE-2008-1457
The Event System in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 does not properly validate per-user subscriptions, which allows remote authenticated users to execute arbitrary code via a crafted event subscription request.
CVE-2008-1668
ftpd.c in (1) wu-ftpd 2.4.2 and (2) ftpd in HP HP-UX B.11.11 assigns uid 0 to the FTP client in certain operating-system misconfigurations in which PAM authentication can succeed even though no passwd entry is available for a user, which allows remote attackers to gain privileges, as demonstrated by a login attempt for an LDAP account when nsswitch.conf does not specify LDAP for passwd information.
CVE-2008-2245
Heap-based buffer overflow in the InternalOpenColorProfile function in mscms.dll in Microsoft Windows Image Color Management System (MSCMS) in the Image Color Management (ICM) component on Windows 2000 SP4, XP SP2 and SP3, and Server 2003 SP1 and SP2 allows remote attackers to execute arbitrary code via a crafted image file.
CVE-2008-2246
Microsoft Windows Vista through SP1 and Server 2008 do not properly import the default IPsec policy from a Windows Server 2003 domain to a Windows Server 2008 domain, which prevents IPsec rules from being enforced and allows remote attackers to bypass intended access restrictions.
CVE-2008-2254
Microsoft Internet Explorer 6 and 7 accesses uninitialized memory, which allows remote attackers to cause a denial of service (crash) and execute arbitrary code via unknown vectors, aka "HTML Object Memory Corruption Vulnerability."
CVE-2008-2255
Microsoft Internet Explorer 5.01, 6, and 7 accesses uninitialized memory, which allows remote attackers to cause a denial of service (crash) and execute arbitrary code via unknown vectors, a different vulnerability than CVE-2008-2254, aka "HTML Object Memory Corruption Vulnerability."
CVE-2008-2256
Microsoft Internet Explorer 5.01, 6, and 7 does not properly handle objects that have been incorrectly initialized or deleted, which allows remote attackers to cause a denial of service (crash) and execute arbitrary code via unknown vectors, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2008-2257
Microsoft Internet Explorer 5.01, 6, and 7 accesses uninitialized memory in certain conditions, which allows remote attackers to cause a denial of service (crash) and execute arbitrary code via vectors related to a document object "appended in a specific order," aka "HTML Objects Memory Corruption Vulnerability" or "XHTML Rendering Memory Corruption Vulnerability," a different vulnerability than CVE-2008-2258.
CVE-2008-2258
Microsoft Internet Explorer 5.01, 6, and 7 accesses uninitialized memory in certain conditions, which allows remote attackers to cause a denial of service (crash) and execute arbitrary code via vectors related to a document object "appended in a specific order" with "particular functions ... performed on" document objects, aka "HTML Objects Memory Corruption Vulnerability" or "Table Layout Memory Corruption Vulnerability," a different vulnerability than CVE-2008-2257.
CVE-2008-2259
Microsoft Internet Explorer 6 and 7 does not perform proper "argument validation" during print preview, which allows remote attackers to execute arbitrary code via unknown vectors, aka "HTML Component Handling Vulnerability."
CVE-2008-2938
Directory traversal vulnerability in Apache Tomcat 4.1.0 through 4.1.37, 5.5.0 through 5.5.26, and 6.0.0 through 6.0.16, when allowLinking and UTF-8 are enabled, allows remote attackers to read arbitrary files via encoded directory traversal sequences in the URI, a different vulnerability than CVE-2008-2370.  NOTE: versions earlier than 6.0.18 were reported affected, but the vendor advisory lists 6.0.16 as the last affected version.
CVE-2008-3338
Multiple buffer overflows in TIBCO Hawk (1) AMI C library (libtibhawkami) and (2) Hawk HMA (tibhawkhma), as used in TIBCO Hawk before 4.8.1; Runtime Agent (TRA) before 5.6.0; iProcess Engine 10.3.0 through 10.6.2 and 11.0.0; and Mainframe Service Tracker before 1.1.0 might allow remote attackers to execute arbitrary code via a crafted message.
CVE-2008-3514
VMware VirtualCenter 2.5 before Update 2 and 2.0.2 before Update 5 relies on client-side "enabled/disabled functionality" for access control, which allows remote attackers to determine valid user names by enabling functionality in the GUI and then making an "attempt to assign permissions to other system users."
Patch information with appropriate login and password:

http://www.vmware.com/security/advisories/VMSA-2008-0012.html


4. Solution

   Please review the patch/release notes for your product and version
   and verify the md5sum of your downloaded file.

   VirtualCenter
   -------------
   
   VMware VirtualCenter 2.5 Update 2 build 104263
   www.vmware.com/download/download.do
   DVD iso image
   md5sum: 83de404fa073bc1fde9acd080f21e688
   Zip file
   md5sum: 3297f1e47c6b018ac8190f11bd022d5b
   Release Notes
   www.vmware.com/support/vi3/doc/vi3_esx35u2_vc25u2_rel_notes.html

   VMware VirtualCenter 2.0.2 Update 5 build 104182
   www.vmware.com/downloads/download.do
   DVD iso image
   md5sum: 5fee5d2d97b482e0d0cb47da7d8e7c34
   Zip file
   md5sum: cd468aab309745c12ee5516652aafbcb
   Release Notes
   www.vmware.com/support/vi3/doc/releasenotes_vc202u5.html

CVE-2008-3515
Multiple cross-site scripting (XSS) vulnerabilities in files generated by Adobe Presenter 6 and 7 before 7.0.1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors involving (1) viewer.swf and (2) loadflash.js, a different vulnerability than CVE-2008-3516.
CVE-2008-3516
Multiple cross-site scripting (XSS) vulnerabilities in files generated by Adobe Presenter 6 and 7 before 7.0.1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors involving (1) viewer.swf and (2) loadflash.js, a different vulnerability than CVE-2008-3515.
CVE-2008-3649
SQL injection vulnerability in categorydetail.php in Article Friendly Standard allows remote attackers to execute arbitrary SQL commands via the Cat parameter.
Regarding Access Complexity:

http://secunia.com/advisories/31292:

"Input passed to the "autid" parameter in authordetail.php and to the "Cat" parameter in categorydetail.php is not properly sanitised before being used in SQL queries. This can be exploited to manipulate SQL queries by injecting arbitrary SQL code.

Successful exploitation requires that "magic_quotes_gpc" is disabled."
CVE-2008-3650
Multiple unspecified vulnerabilities in Horde Groupware Webmail before Edition 1.1.1 (final) have unknown impact and attack vectors related to "unescaped output," possibly cross-site scripting (XSS), in the (1) object browser and (2) contact view.
CVE-2008-3651
Memory leak in racoon/proposal.c in the racoon daemon in ipsec-tools before 0.7.1 allows remote authenticated users to cause a denial of service (memory consumption) via invalid proposals.
CVE-2008-3652
src/racoon/handler.c in racoon in ipsec-tools does not remove an "orphaned ph1" (phase 1) handle when it has been initiated remotely, which allows remote attackers to cause a denial of service (resource consumption).
CVE-2008-3653
Multiple unspecified vulnerabilities in TikiWiki CMS/Groupware before 2.0 have unknown impact and attack vectors.
CVE-2008-3654
Unspecified vulnerability in TikiWiki CMS/Groupware before 2.0 allows attackers to obtain "path and PHP configuration" via unknown vectors.
CVE-2008-3655
Ruby 1.8.5 and earlier, 1.8.6 through 1.8.6-p286, 1.8.7 through 1.8.7-p71, and 1.9 through r18423 does not properly restrict access to critical variables and methods at various safe levels, which allows context-dependent attackers to bypass intended access restrictions via (1) untrace_var, (2) $PROGRAM_NAME, and (3) syslog at safe level 4, and (4) insecure methods at safe levels 1 through 3.
CVE-2008-3656
Algorithmic complexity vulnerability in the WEBrick::HTTPUtils.split_header_value function in WEBrick::HTTP::DefaultFileHandler in WEBrick in Ruby 1.8.5 and earlier, 1.8.6 through 1.8.6-p286, 1.8.7 through 1.8.7-p71, and 1.9 through r18423 allows context-dependent attackers to cause a denial of service (CPU consumption) via a crafted HTTP request that is processed by a backtracking regular expression.
CVE-2008-3657
The dl module in Ruby 1.8.5 and earlier, 1.8.6 through 1.8.6-p286, 1.8.7 through 1.8.7-p71, and 1.9 through r18423 does not check "taintness" of inputs, which allows context-dependent attackers to bypass safe levels and execute dangerous functions by accessing a library using DL.dlopen.
CVE-2008-3666
Unspecified vulnerability in Sun Solaris 10 and OpenSolaris before snv_96 allows (1) context-dependent attackers to cause a denial of service (panic) via vectors involving creation of a crafted file and use of the sendfilev system call, as demonstrated by a file served by an Apache 2.2.x web server with EnableSendFile configured; and (2) local users to cause a denial of service (panic) via a call to the sendfile system call, as reachable through the sendfilev library.
CVE-2008-3667
Stack-based buffer overflow in Maxthon Browser 2.0 and earlier allows remote attackers to execute arbitrary code via a long Content-type HTTP header.
CVE-2008-3668
Multiple cross-site scripting (XSS) vulnerabilities in the Yogurt Social Network module 3.2 rc1 for XOOPS allow remote attackers to inject arbitrary web script or HTML via the uid parameter to (1) friends.php, (2) seutubo.php, (3) album.php, (4) scrapbook.php, (5) index.php, or (6) tribes.php; or (7) the description field of a new scrap.
CVE-2008-3669
SQL injection vulnerability in comments.php in ZeeScripts Reviews Opinions Rating Posting Engine Web-Site PHP Script (aka ZeeReviews) allows remote attackers to execute arbitrary SQL commands via the ItemID parameter.
CVE-2008-3670
SQL injection vulnerability in authordetail.php in Article Friendly Pro allows remote attackers to execute arbitrary SQL commands via the autid parameter.
CVE-2008-3671
Acronis True Image Echo Server 9.x build 8072 on Linux does not properly encrypt backups to an FTP server, which allows remote attackers to obtain sensitive information.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3672
SQL injection vulnerability in showcategory.php in PozScripts Classified Ads allows remote attackers to execute arbitrary SQL commands via the cid parameter, a different vector than CVE-2008-3673. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3673
SQL injection vulnerability in browsecats.php in PozScripts Classified Ads allows remote attackers to execute arbitrary SQL commands via the cid parameter, a different vector than CVE-2008-3672.
CVE-2008-3674
SQL injection vulnerability in ugroups.php in PozScripts TubeGuru Video Sharing Script allows remote attackers to execute arbitrary SQL commands via the UID parameter.
