CVE-2007-2839
gfax 0.4.2 and probably other versions creates temporary files insecurely, which allows local users to execute arbitrary commands via unknown vectors.
CVE-2007-3011
The DBAsciiAccess CGI Script in the web interface in Fujitsu-Siemens Computers ServerView before 4.50.09 allows remote attackers to execute arbitrary commands via shell metacharacters in the Servername subparameter of the ParameterList parameter.
CVE-2007-3012
The web interface in Fujitsu-Siemens Computers PRIMERGY BX300 Switch Blade allows remote attackers to obtain sensitive information by canceling the authentication dialog when accessing a sub-page, which still displays the form field contents of the sub-page, as demonstrated using (1) config/ip_management.htm and (2) config/snmp_config.htm.
CVE-2007-3567
MySQLDumper 1.21b through 1.23 REV227 uses a "Limit GET" statement in the .htaccess authentication mechanism, which allows remote attackers to bypass authentication requirements via HTTP POST requests.
CVE-2007-3568
The _LoadBMP function in imlib 1.9.15 and earlier allows context-dependent attackers to cause a denial of service (infinite loop) via a BMP image with a Bits Per Page (BPP) value of 0.
CVE-2007-3569
Multiple cross-site scripting (XSS) vulnerabilities in Oliver Library Management System allow remote attackers to inject arbitrary web script or HTML via the (1) updateform and (2) displayform parameter to (a) gateway/gateway.exe; the (3) TERMS, (4) database, (5) srchad, (6) SuggestedSearch, and (7) searchform parameters to the (b) "Basic Search page"; and (8) username parameter when (c) logging on.
CVE-2007-3570
The Linux Access Gateway in Novell Access Manager before 3.0 SP1 Release Candidate 1 (RC1) allows remote attackers to bypass unspecified security controls via Fullwidth/Halfwidth Unicode encoded data in a HTTP POST request.
CVE-2007-3571
The Apache Web Server as used in Novell NetWare 6.5 and GroupWise allows remote attackers to obtain sensitive information via a certain directive to Apache that causes the HTTP-Header response to be modified, which may reveal the server's internal IP address.
CVE-2007-3572
Incomplete blacklist vulnerability in cgi-bin/runDiagnostics.cgi in the web interface on the Yoggie Pico and Pico Pro allows remote attackers to execute arbitrary commands via shell metacharacters in the param parameter, as demonstrated by URL encoded "`" (backtick) characters (%60 sequences).
The vendor has addressed this issue through the release of the following product update: http://www.yoggie.com/supportcase.asp 

CVE-2007-3573
Multiple SQL injection vulnerabilities in akocomment allow remote attackers to execute arbitrary SQL commands via the (1) acparentid or (2) acitemid parameter to an unspecified component, different vectors than CVE-2006-1421.
CVE-2007-3574
Multiple cross-site scripting (XSS) vulnerabilities in setup.cgi on the Cisco Linksys WAG54GS Wireless-G ADSL Gateway with 1.00.06 firmware allow remote attackers to inject arbitrary web script or HTML via the (1) c4_trap_ip_, (2) devname, (3) snmp_getcomm, or (4) snmp_setcomm parameter.
CVE-2007-3575
SQL injection vulnerability in includes/functions in FreeDomain.co.nr Clone allows remote attackers to execute arbitrary SQL commands via the logindomain parameter to members.php.
CVE-2007-3576
** DISPUTED **  Microsoft Internet Explorer 6 executes web script from URIs of arbitrary scheme names ending with the "script" character sequence, using the (1) vbscript: handler for scheme names with 7 through 9 characters, and the (2) javascript: handler for scheme names with 10 or more characters, which might allow remote attackers to bypass certain XSS protection schemes.  NOTE: other researchers dispute the significance of this issue, stating "this only works when typed in the address bar."
CVE-2007-3577
PHPIDS before 20070703 does not properly handle use of the substr method in (1) document.location.search and (2) document.referrer; (3) certain use of document.location.hash; (4) certain "window[eval" and similar expressions; (5) certain Function expressions; (6) certain '=' expressions, as demonstrated by a 'whatever="something"' sequence; and (7) certain "with" expressions, which allows remote attackers to inject arbitrary web script.
CVE-2007-3578
PHPIDS before 20070703 does not properly handle (1) arithmetic expressions and (2) unclosed comments, which allows remote attackers to inject arbitrary web script.
CVE-2007-3579
PHPIDS before 20070703 does not properly handle setting the .text property of a SCRIPT element before its attachment to the DOM, which allows remote attackers to inject arbitrary web script.
CVE-2007-3580
PHPIDS does not properly handle certain code containing newlines, as demonstrated by a try/catch block within a loop, which allows user-assisted remote attackers to inject arbitrary web script.
CVE-2007-3581
The Jedox Palo 1.5 client transmits the password in cleartext, which might allow remote attackers to obtain the password by sniffing the network, as demonstrated by starting Excel with the Palo plugin, opening a cube, and performing an Insert View.
CVE-2007-3582
SQL injection vulnerability in index.php in SuperCali PHP Event Calendar 0.4.0 allows remote attackers to execute arbitrary SQL commands via the o parameter.
CVE-2007-3583
SQL injection vulnerability in details_news.php in Girlserv ads 1.5 and earlier allows remote attackers to execute arbitrary SQL commands via the idnew parameter.
CVE-2007-3584
SQL injection vulnerability in viewforum.php in PNphpBB2 1.2i and earlier for Postnuke allows remote attackers to execute arbitrary SQL commands via the order parameter.
CVE-2007-3585
PHP remote file inclusion vulnerability in games.php in MyCMS 0.9.8 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the id parameter.
CVE-2007-3586
Multiple direct static code injection vulnerabilities in MyCMS 0.9.8 and earlier allow remote attackers to inject arbitrary PHP code into (1) a _score.txt file via the score parameter, or (2) a _setby.txt file via a login cookie, which is then included by games.php.  NOTE: programs that use games.php might include (a) snakep.php, (b) tetrisp.php, and possibly other site-specific files.
CVE-2007-3587
MyCMS 0.9.8 and earlier allows remote attackers to gain privileges via the admin cookie parameter, as demonstrated by a post to admin/settings.php that injects PHP code into settings.inc, which can then be executed via a direct request to index.php.
CVE-2007-3588
SQL injection vulnerability in reply.php in VBZooM 1.12 allows remote attackers to execute arbitrary SQL commands via the UserID parameter to sub-join.php.  NOTE: this may be the same as CVE-2006-3691.4.
CVE-2007-3589
Multiple SQL injection vulnerabilities in b1gbb 2.24.0 allow remote attackers to execute arbitrary SQL commands via the id parameter to (1) showthread.php or (2) showboard.php.
CVE-2007-3590
Cross-site scripting (XSS) vulnerability in visitenkarte.php in b1gBB 2.24.0 allows remote attackers to inject arbitrary web script or HTML via the user parameter.
