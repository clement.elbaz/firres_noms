CVE-2014-2363
Morpho Itemiser 3 8.17 has hardcoded administrative credentials, which makes it easier for remote attackers to obtain access via a login request.
<a href="http://cwe.mitre.org/data/definitions/798.html" target="_blank">CWE-798: Use of Hard-coded Credentials</a>
CVE-2014-2625
Directory traversal vulnerability in the storedNtxFile function in HP Network Virtualization 8.6 (aka Shunra Network Virtualization) allows remote attackers to read arbitrary files via crafted input, aka ZDI-CAN-2023.
CVE-2014-2626
Directory traversal vulnerability in the toServerObject function in HP Network Virtualization 8.6 (aka Shunra Network Virtualization) allows remote attackers to create files, and consequently execute arbitrary code, via crafted input, aka ZDI-CAN-2024.
CVE-2014-2966
The ISO-8859-1 encoder in Resin Pro before 4.0.40 does not properly perform Unicode transformations, which allows remote attackers to bypass intended text restrictions via crafted characters, as demonstrated by bypassing an XSS protection mechanism.
CVE-2014-3071
Cross-site scripting (XSS) vulnerability in the Data Quality Console in IBM InfoSphere Information Server 11.3 allows remote attackers to inject arbitrary web script or HTML via a crafted URL for adding a project connection.
CVE-2014-3301
The ProfileAction controller in Cisco WebEx Meetings Server (CWMS) 1.5(.1.131) and earlier allows remote attackers to obtain sensitive information by reading stack traces in returned messages, aka Bug ID CSCuj81700.
CVE-2014-3305
Cross-site request forgery (CSRF) vulnerability in the web framework in Cisco WebEx Meetings Server 1.5(.1.131) and earlier allows remote attackers to hijack the authentication of unspecified victims via unknown vectors, aka Bug ID CSCuj81735.
CVE-2014-3324
Multiple cross-site scripting (XSS) vulnerabilities in the login page in the administrative web interface in Cisco TelePresence Server Software 4.0(2.8) allow remote attackers to inject arbitrary web script or HTML via a crafted parameter, aka Bug ID CSCup90060.
Per: http://tools.cisco.com/security/center/viewAlert.x?alertId=35031

"The security vulnerability applies to the following combinations of products.

Primary Products:

Cisco	Cisco TelePresence Server Software	3.0 (2.24) | 3.1 (1.98) | 4.0 (1.57), (2.8)"
CVE-2014-3326
SQL injection vulnerability in the web framework in Cisco Security Manager 4.5 and 4.6 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors, aka Bug ID CSCup26957.
CVE-2014-3328
The Intercluster Sync Agent Service in Cisco Unified Presence Server allows remote attackers to cause a denial of service via a TCP SYN flood, aka Bug ID CSCun34125.
<a href="http://cwe.mitre.org/data/definitions/400.html" target="_blank">CWE-400: Uncontrolled Resource Consumption ('Resource Exhaustion')</a>
CVE-2014-4747
The Classic Meeting Server in IBM Sametime 8.x through 8.5.2.1 allows physically proximate attackers to discover a meeting password hash by leveraging access to an unattended workstation to read HTML source code within a victim's browser.
CVE-2014-4748
Cross-site scripting (XSS) vulnerability in the Classic Meeting Server in IBM Sametime 8.x through 8.5.2.1 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-4857
Cross-site scripting (XSS) vulnerability in Gurock TestRail before 3.1.3 allows remote attackers to inject arbitrary web script or HTML via the Created By field in a project activity.
CVE-2014-4858
Multiple SQL injection vulnerabilities in CWPLogin.aspx in Sabre AirCentre Crew products 2010.2.12.20008 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) username or (2) password field.
CVE-2014-4971
Microsoft Windows XP SP3 does not validate addresses in certain IRP handler routines, which allows local users to write data to arbitrary memory locations, and consequently gain privileges, via a crafted address in an IOCTL call, related to (1) the MQAC.sys driver in the MQ Access Control subsystem and (2) the BthPan.sys driver in the Bluetooth Personal Area Networking subsystem.
CWE-123: Write-what-where Condition

<a href="http://cwe.mitre.org/data/definitions/123.html">CWE-123: Write-what-where Condition</a>
CVE-2014-4979
Apple QuickTime allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a malformed version number and flags in an mvhd atom.
