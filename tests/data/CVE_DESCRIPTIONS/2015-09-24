CVE-2015-4476
Mozilla Firefox before 41.0 on Android allows user-assisted remote attackers to spoof address-bar attributes by leveraging lack of navigation after a paste of a URL with a nonstandard scheme, as demonstrated by spoofing an SSL attribute.
CVE-2015-4500
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-4501
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 41.0 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-4502
js/src/proxy/Proxy.cpp in Mozilla Firefox before 41.0 mishandles certain receiver arguments, which allows remote attackers to bypass intended window access restrictions via a crafted web site.
CVE-2015-4503
The TCP Socket API implementation in Mozilla Firefox before 41.0 mishandles array boundaries that were established with a navigator.mozTCPSocket.open method call and send method calls, which allows remote TCP servers to obtain sensitive information from process memory by reading packet data, as demonstrated by availability of this API in a Firefox OS application.
CVE-2015-4504
The lut_inverse_interp16 function in the QCMS library in Mozilla Firefox before 41.0 allows remote attackers to obtain sensitive information or cause a denial of service (buffer over-read and application crash) via crafted attributes in the ICC 4 profile of an image.
CVE-2015-4505
updater.exe in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 on Windows allows local users to write to arbitrary files by conducting a junction attack and waiting for an update operation by the Mozilla Maintenance Service.
CVE-2015-4506
Buffer overflow in the vp9_init_context_buffers function in libvpx, as used in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3, allows remote attackers to execute arbitrary code via a crafted VP9 file.
CVE-2015-4507
The SavedStacks class in the JavaScript implementation in Mozilla Firefox before 41.0, when the Debugger API is enabled, allows remote attackers to cause a denial of service (getSlotRef assertion failure and application exit) or possibly execute arbitrary code via a crafted web site.
CVE-2015-4508
Mozilla Firefox before 41.0, when reader mode is enabled, allows remote attackers to spoof the relationship between address-bar URLs and web content via a crafted web site.
CVE-2015-4509
Use-after-free vulnerability in the HTMLVideoElement interface in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 allows remote attackers to execute arbitrary code via crafted JavaScript code that modifies the URI table of a media element, aka ZDI-CAN-3176.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-4510
Race condition in the WorkerPrivate::NotifyFeatures function in Mozilla Firefox before 41.0 allows remote attackers to execute arbitrary code or cause a denial of service (use-after-free and application crash) by leveraging improper interaction between shared workers and the IndexedDB implementation.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-4511
Heap-based buffer overflow in the nestegg_track_codec_data function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 allows remote attackers to execute arbitrary code via a crafted header in a WebM video.
CVE-2015-4512
gfx/2d/DataSurfaceHelpers.cpp in Mozilla Firefox before 41.0 on Linux improperly attempts to use the Cairo library with 32-bit color-depth surface creation followed by 16-bit color-depth surface display, which allows remote attackers to obtain sensitive information from process memory or cause a denial of service (out-of-bounds read) by using a CANVAS element to trigger 2D rendering.
CVE-2015-4516
Mozilla Firefox before 41.0 allows remote attackers to bypass certain ECMAScript 5 (aka ES5) API protection mechanisms and modify immutable properties, and consequently execute arbitrary JavaScript code with chrome privileges, via a crafted web page that does not use ES5 APIs.
CVE-2015-4517
NetworkUtils.cpp in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 might allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unknown vectors.
CVE-2015-4519
Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 allow user-assisted remote attackers to bypass intended access restrictions and discover a redirect's target URL via crafted JavaScript code that executes after a drag-and-drop action of an image into a TEXTBOX element.
CVE-2015-4520
Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 allow remote attackers to bypass CORS preflight protection mechanisms by leveraging (1) duplicate cache-key generation or (2) retrieval of a value from an incorrect HTTP Access-Control-* response header.
CVE-2015-4521
The ConvertDialogOptions function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 might allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unknown vectors.
CVE-2015-4522
The nsUnicodeToUTF8::GetMaxLength function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 might allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unknown vectors, related to an "overflow."
CVE-2015-6303
The Cisco Spark application 2015-07-04 for mobile operating systems does not properly verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate, aka Bug IDs CSCut36742 and CSCut36844.
CVE-2015-6304
Cross-site request forgery (CSRF) vulnerability in Cisco TelePresence Server software 3.0(2.24) allows remote attackers to hijack the authentication of arbitrary users, aka Bug IDs CSCut63718, CSCut63724, and CSCut63760.
CVE-2015-7174
The nsAttrAndChildArray::GrowBy function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 might allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unknown vectors, related to an "overflow."
CVE-2015-7175
The XULContentSinkImpl::AddText function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 might allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unknown vectors, related to an "overflow."
CVE-2015-7176
The AnimationThread function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 uses an incorrect argument to the sscanf function, which might allow remote attackers to cause a denial of service (stack-based buffer overflow and application crash) or possibly have unspecified other impact via unknown vectors.
CVE-2015-7177
The InitTextures function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 might allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unknown vectors.
CVE-2015-7178
The ProgramBinary::linkAttributes function in libGLES in ANGLE, as used in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 on Windows, mishandles shader access, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via crafted (1) OpenGL or (2) WebGL content.
CVE-2015-7179
The VertexBufferInterface::reserveVertexSpace function in libGLES in ANGLE, as used in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 on Windows, incorrectly allocates memory for shader attribute arrays, which allows remote attackers to execute arbitrary code or cause a denial of service (buffer overflow and application crash) via crafted (1) OpenGL or (2) WebGL content.
CVE-2015-7180
The ReadbackResultWriterD3D11::Run function in Mozilla Firefox before 41.0 and Firefox ESR 38.x before 38.3 misinterprets the return value of a function call, which might allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unknown vectors.
CVE-2015-7327
Mozilla Firefox before 41.0 does not properly restrict the availability of High Resolution Time API times, which allows remote attackers to track last-level cache access, and consequently obtain sensitive information, via crafted JavaScript code that makes performance.now calls.
