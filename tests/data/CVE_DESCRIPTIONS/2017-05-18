CVE-2017-3980
A directory traversal vulnerability in the ePO Extension in McAfee ePolicy Orchestrator (ePO) 5.9.0, 5.3.2, and 5.1.3 and earlier allows remote authenticated users to execute a command of their choice via an authenticated ePO session.
CVE-2017-6195
Ipswitch MOVEit Transfer (formerly DMZ) allows pre-authentication blind SQL injection. The fixed versions are MOVEit Transfer 2017 9.0.0.201, MOVEit DMZ 8.3.0.30, and MOVEit DMZ 8.2.0.20.
CVE-2017-6621
A vulnerability in the web interface of Cisco Prime Collaboration Provisioning could allow an unauthenticated, remote attacker to access sensitive data. The attacker could use this information to conduct additional reconnaissance attacks. The vulnerability is due to insufficient protection of sensitive data when responding to an HTTP request on the web interface. An attacker could exploit the vulnerability by sending a crafted HTTP request to the application to access specific system files. An exploit could allow the attacker to obtain sensitive information about the application which could include user credentials. This vulnerability affects Cisco Prime Collaboration Provisioning Software Releases 10.6 through 11.5. Cisco Bug IDs: CSCvc99626.
CVE-2017-6622
A vulnerability in the web interface for Cisco Prime Collaboration Provisioning could allow an unauthenticated, remote attacker to bypass authentication and perform command injection with root privileges. The vulnerability is due to missing security constraints in certain HTTP request methods, which could allow access to files via the web interface. An attacker could exploit this vulnerability by sending a crafted HTTP request to the targeted application. This vulnerability affects Cisco Prime Collaboration Provisioning Software Releases prior to 12.1. Cisco Bug IDs: CSCvc98724.
CVE-2017-6623
A vulnerability in a script file that is installed as part of the Cisco Policy Suite (CPS) Software distribution for the CPS appliance could allow an authenticated, local attacker to escalate their privilege level to root. The vulnerability is due to incorrect sudoers permissions on the script file. An attacker could exploit this vulnerability by authenticating to the device and providing crafted user input at the CLI, using this script file to escalate their privilege level and execute commands as root. A successful exploit could allow the attacker to acquire root-level privileges and take full control of the appliance. The user has to be logged-in to the device with valid credentials for a specific set of users. The Cisco Policy Suite application is vulnerable when running software versions 10.0.0, 10.1.0, or 11.0.0. Cisco Bug IDs: CSCvc07366.
CVE-2017-6652
A vulnerability in the web framework of the Cisco TelePresence IX5000 Series could allow an unauthenticated, remote attacker to access arbitrary files on an affected device. The vulnerability is due to insufficient input validation. An attacker could exploit this vulnerability by using directory traversal techniques to read files within the Cisco TelePresence IX5000 Series filesystem. This vulnerability affects Cisco TelePresence IX5000 Series devices running software version 8.2.0. Cisco Bug IDs: CSCvc52325.
CVE-2017-7433
An absolute path traversal vulnerability (CWE-36) in Micro Focus Vibe 4.0.2 and earlier allows a remote authenticated attacker to download arbitrary files from the server by submitting a specially crafted request to the viewFile endpoint. Note that the attack can be performed without authentication if Guest access is enabled (Guest access is disabled by default).
CVE-2017-7503
It was found that the Red Hat JBoss EAP 7.0.5 implementation of javax.xml.transform.TransformerFactory is vulnerable to XXE. An attacker could use this flaw to launch DoS or SSRF attacks, or read files from the server where EAP is deployed.
CVE-2017-8338
A vulnerability in MikroTik Version 6.38.5 could allow an unauthenticated remote attacker to exhaust all available CPU via a flood of UDP packets on port 500 (used for L2TP over IPsec), preventing the affected router from accepting new connections; all devices will be disconnected from the router and all logs removed automatically.
CVE-2017-8769
** DISPUTED ** Facebook WhatsApp Messenger before 2.16.323 for Android uses the SD card for cleartext storage of files (Audio, Documents, Images, Video, and Voice Notes) associated with a chat, even after that chat is deleted. There may be users who expect file deletion to occur upon chat deletion, or who expect encryption (consistent with the application's use of an encrypted database to store chat text). NOTE: the vendor reportedly indicates that they do not "consider these to be security issues" because a user may legitimately want to preserve any file for use "in other apps like the Google Photos gallery" regardless of whether its associated chat is deleted.
CVE-2017-9038
GNU Binutils 2.28 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted ELF file, related to the byte_get_little_endian function in elfcomm.c, the get_unwind_section_word function in readelf.c, and ARM unwind information that contains invalid word offsets.
CVE-2017-9039
GNU Binutils 2.28 allows remote attackers to cause a denial of service (memory consumption) via a crafted ELF file with many program headers, related to the get_program_headers function in readelf.c.
CVE-2017-9040
GNU Binutils 2017-04-03 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash), related to the process_mips_specific function in readelf.c, via a crafted ELF file that triggers a large memory-allocation attempt.
CVE-2017-9041
GNU Binutils 2.28 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted ELF file, related to MIPS GOT mishandling in the process_mips_specific function in readelf.c.
CVE-2017-9042
readelf.c in GNU Binutils 2017-04-12 has a "cannot be represented in type long" issue, which might allow remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a crafted ELF file.
CVE-2017-9043
readelf.c in GNU Binutils 2017-04-12 has a "shift exponent too large for type unsigned long" issue, which might allow remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a crafted ELF file.
CVE-2017-9044
The print_symbol_for_build_attribute function in readelf.c in GNU Binutils 2017-04-12 allows remote attackers to cause a denial of service (invalid read and SEGV) via a crafted ELF file.
CVE-2017-9045
The Google I/O 2017 application before 5.1.4 for Android downloads multiple .json files from http://storage.googleapis.com without SSL, which makes it easier for man-in-the-middle attackers to spoof Feed and Schedule data by creating a modified blocks_v4.json file.
CVE-2017-9047
A buffer overflow was discovered in libxml2 20904-GITv2.9.4-16-g0741801. The function xmlSnprintfElementContent in valid.c is supposed to recursively dump the element content definition into a char buffer 'buf' of size 'size'. The variable len is assigned strlen(buf). If the content->type is XML_ELEMENT_CONTENT_ELEMENT, then (i) the content->prefix is appended to buf (if it actually fits) whereupon (ii) content->name is written to the buffer. However, the check for whether the content->name actually fits also uses 'len' rather than the updated buffer length strlen(buf). This allows us to write about "size" many bytes beyond the allocated memory. This vulnerability causes programs that use libxml2, such as PHP, to crash.
CVE-2017-9048
libxml2 20904-GITv2.9.4-16-g0741801 is vulnerable to a stack-based buffer overflow. The function xmlSnprintfElementContent in valid.c is supposed to recursively dump the element content definition into a char buffer 'buf' of size 'size'. At the end of the routine, the function may strcat two more characters without checking whether the current strlen(buf) + 2 < size. This vulnerability causes programs that use libxml2, such as PHP, to crash.
CVE-2017-9049
libxml2 20904-GITv2.9.4-16-g0741801 is vulnerable to a heap-based buffer over-read in the xmlDictComputeFastKey function in dict.c. This vulnerability causes programs that use libxml2, such as PHP, to crash. This vulnerability exists because of an incomplete fix for libxml2 Bug 759398.
CVE-2017-9050
libxml2 20904-GITv2.9.4-16-g0741801 is vulnerable to a heap-based buffer over-read in the xmlDictAddString function in dict.c. This vulnerability causes programs that use libxml2, such as PHP, to crash. This vulnerability exists because of an incomplete fix for CVE-2016-1839.
CVE-2017-9051
libav before 12.1 is vulnerable to an invalid read of size 1 due to NULL pointer dereferencing in the nsv_read_chunk function in libavformat/nsvdec.c.
CVE-2017-9052
An issue, also known as DW201703-006, was discovered in libdwarf 2017-03-21. A heap-based buffer over-read in dwarf_formsdata() is due to a failure to check a pointer for being in bounds (in a few places in this function) and a failure in a check in dwarf_attr_list().
CVE-2017-9053
An issue, also known as DW201703-005, was discovered in libdwarf 2017-03-21. A heap-based buffer over-read in _dwarf_read_loc_expr_op() is due to a failure to check a pointer for being in bounds (in a few places in this function).
CVE-2017-9054
An issue, also known as DW201703-002, was discovered in libdwarf 2017-03-21. In _dwarf_decode_s_leb128_chk() a byte pointer was dereferenced just before it was checked for being in bounds, leading to a heap-based buffer over-read.
CVE-2017-9055
An issue, also known as DW201703-001, was discovered in libdwarf 2017-03-21. In dwarf_formsdata() a few data types were not checked for being in bounds, leading to a heap-based buffer over-read.
CVE-2017-9058
In libytnef in ytnef through 1.9.2, there is a heap-based buffer over-read due to incorrect boundary checking in the SIZECHECK macro in lib/ytnef.c.
CVE-2017-9059
The NFSv4 implementation in the Linux kernel through 4.11.1 allows local users to cause a denial of service (resource consumption) by leveraging improper channel callback shutdown when unmounting an NFSv4 filesystem, aka a "module reference and kernel daemon" leak.
CVE-2017-9061
In WordPress before 4.7.5, a cross-site scripting (XSS) vulnerability exists when attempting to upload very large files, because the error message does not properly restrict presentation of the filename.
CVE-2017-9062
In WordPress before 4.7.5, there is improper handling of post meta data values in the XML-RPC API.
CVE-2017-9063
In WordPress before 4.7.5, a cross-site scripting (XSS) vulnerability related to the Customizer exists, involving an invalid customization session.
CVE-2017-9064
In WordPress before 4.7.5, a Cross Site Request Forgery (CSRF) vulnerability exists in the filesystem credentials dialog because a nonce is not required for updating credentials.
CVE-2017-9065
In WordPress before 4.7.5, there is a lack of capability checks for post meta data in the XML-RPC API.
CVE-2017-9066
In WordPress before 4.7.5, there is insufficient redirect validation in the HTTP class, leading to SSRF.
CVE-2017-9067
In MODX Revolution before 2.5.7, when PHP 5.3.3 is used, an attacker is able to include and execute arbitrary files on the web server due to insufficient validation of the action parameter to setup/index.php, aka directory traversal.
CVE-2017-9068
In MODX Revolution before 2.5.7, an attacker is able to trigger Reflected XSS by injecting payloads into several fields on the setup page, as demonstrated by the database_type parameter.
CVE-2017-9069
In MODX Revolution before 2.5.7, a user with file upload permissions is able to execute arbitrary code by uploading a file with the name .htaccess.
CVE-2017-9070
In MODX Revolution before 2.5.7, a user with resource edit permissions can inject an XSS payload into the title of any post via the pagetitle parameter to connectors/index.php.
CVE-2017-9071
In MODX Revolution before 2.5.7, an attacker might be able to trigger XSS by injecting a payload into the HTTP Host header of a request. This is exploitable only in conjunction with other issues such as Cache Poisoning.
CVE-2017-9072
Two CalendarXP products have XSS in common parts of HTML files. CalendarXP FlatCalendarXP through 9.9.290 has XSS in iflateng.htm and nflateng.htm. CalendarXP PopCalendarXP through 9.8.308 has XSS in ipopeng.htm and npopeng.htm.
CVE-2017-9073
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2017-0176.  Reason: This candidate is a reservation duplicate of CVE-2017-0176.  Notes: All CVE users should reference CVE-2017-0176 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
