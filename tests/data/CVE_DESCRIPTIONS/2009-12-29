CVE-2009-3295
The prep_reprocess_req function in kdc/do_tgs_req.c in the cross-realm referral implementation in the Key Distribution Center (KDC) in MIT Kerberos 5 (aka krb5) 1.7 before 1.7.1 allows remote attackers to cause a denial of service (NULL pointer dereference and daemon crash) via a ticket request.
CVE-2009-4444
Microsoft Internet Information Services (IIS) 5.x and 6.x uses only the portion of a filename before a ; (semicolon) character to determine the file extension, which allows remote attackers to bypass intended extension restrictions of third-party upload applications via a filename with a (1) .asp, (2) .cer, or (3) .asa first extension, followed by a semicolon and a safe extension, as demonstrated by the use of asp.dll to handle a .asp;.jpg file.
CVE-2009-4445
Microsoft Internet Information Services (IIS), when used in conjunction with unspecified third-party upload applications, allows remote attackers to create empty files with arbitrary extensions via a filename containing an initial extension followed by a : (colon) and a safe extension, as demonstrated by an upload of a .asp:.jpg file that results in creation of an empty .asp file, related to support for the NTFS Alternate Data Streams (ADS) filename syntax.  NOTE: it could be argued that this is a vulnerability in the third-party product, not IIS, because the third-party product should be applying its extension restrictions to the portion of the filename before the colon.
CVE-2009-4446
Cross-site scripting (XSS) vulnerability in admin.php in phpInstantGallery 1.1 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO.
CVE-2009-4447
Jax Guestbook 3.5.0 allows remote attackers to bypass authentication and modify administrator settings via a direct request to admin/guestbook.admin.php.
CVE-2009-4448
inc/functions_time.php in MyBB (aka MyBulletinBoard) 1.4.10, and possibly earlier versions, allows remote attackers to cause a denial of service (CPU consumption) via a crafted request with a large year value, which triggers a long loop, as reachable through member.php and possibly other vectors.
CVE-2009-4449
Directory traversal vulnerability in MyBB (aka MyBulletinBoard) 1.4.10, and possibly earlier versions, when changing the user avatar from the gallery, allows remote authenticated users to determine the existence of files via directory traversal sequences in the avatar and possibly the gallery parameters, related to (1) admin/modules/user/users.php and (2) usercp.php.
CVE-2009-4450
Multiple cross-site scripting (XSS) vulnerabilities in map.php in LiveZilla 3.1.8.3 allow remote attackers to inject arbitrary web script or HTML via the (1) lat, (2) lng, and (3) zom parameters, which are not properly handled when processed with templates/map.tpl.
CVE-2009-4451
Unrestricted file upload vulnerability in upper.php in kandalf upper 0.1 allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in fileup/.
CVE-2009-4452
Kaspersky Anti-Virus 5.0 (5.0.712); Antivirus Personal 5.0.x; Anti-Virus 6.0 (6.0.3.837), 7 (7.0.1.325), 2009 (8.0.0.x), and 2010 (9.0.0.463); and Internet Security 7 (7.0.1.325), 2009 (8.0.0.x), and 2010 (9.0.0.463); use weak permissions (Everyone:Full Control) for the BASES directory, which allows local users to gain SYSTEM privileges by replacing an executable or DLL with a Trojan horse.
CVE-2009-4453
Insecure method vulnerability in SoftCab Sound Converter ActiveX control (sndConverter.ocx) 1.2 allows remote attackers to create or overwrite arbitrary files via the SaveFormat method.  NOTE: some of these details are obtained from third party information.
Per: http://cwe.mitre.org/data/definitions/749.html

'CWE-749: Exposed Dangerous Method or Function'
CVE-2009-4454
vccleaner in VideoCache 1.9.2 allows local users with Squid proxy user privileges to overwrite arbitrary files via a symlink attack on /var/log/videocache/vccleaner.log.
CVE-2009-4455
The default configuration of Cisco ASA 5500 Series Adaptive Security Appliance (Cisco ASA) 7.0, 7.1, 7.2, 8.0, 8.1, and 8.2 allows portal traffic to access arbitrary backend servers, which might allow remote authenticated users to bypass intended access restrictions and access unauthorized web sites via a crafted URL obfuscated with ROT13 and a certain encoding.  NOTE: this issue was originally reported as a vulnerability related to lack of restrictions to URLs listed in the Cisco WebVPN bookmark component, but the vendor states that "The bookmark feature is not a security feature."
