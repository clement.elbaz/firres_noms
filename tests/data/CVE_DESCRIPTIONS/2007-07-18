CVE-2007-3268
The TFTP implementation in IBM Tivoli Provisioning Manager for OS Deployment 5.1 before Fix Pack 3 allows remote attackers to cause a denial of service (rembo.exe crash and multiple service outage) via a read (RRQ) request with an invalid blksize (blocksize), which triggers a divide-by-zero error.
CVE-2007-3564
libcurl 7.14.0 through 7.16.3, when built with GnuTLS support, does not check SSL/TLS certificate expiration or activation dates, which allows remote attackers to bypass certain access restrictions.
CVE-2007-3734
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 2.0.0.5 and Thunderbird before 2.0.0.5 allow remote attackers to cause a denial of service (crash) via unspecified vectors that trigger memory corruption.
CVE-2007-3735
Multiple unspecified vulnerabilities in the JavaScript engine in Mozilla Firefox before 2.0.0.5 and Thunderbird before 2.0.0.5 allow remote attackers to cause a denial of service (crash) via unspecified vectors that trigger memory corruption.
CVE-2007-3736
Cross-site scripting (XSS) vulnerability in Mozilla Firefox before 2.0.0.5 allows remote attackers to inject arbitrary web script "into another site's context" via a "timing issue" involving the (1) addEventListener or (2) setTimeout function, probably by setting events that activate after the context has changed.
CVE-2007-3737
Mozilla Firefox before 2.0.0.5 allows remote attackers to execute arbitrary code with chrome privileges by calling an event handler from an unspecified "element outside of a document."
CVE-2007-3738
Multiple unspecified vulnerabilities in Mozilla Firefox before 2.0.0.5 allow remote attackers to execute arbitrary code via a crafted XPCNativeWrapper.
CVE-2007-3762
Stack-based buffer overflow in the IAX2 channel driver (chan_iax2) in Asterisk before 1.2.22 and 1.4.x before 1.4.8, Business Edition before B.2.2.1, AsteriskNOW before beta7, Appliance Developer Kit before 0.5.0, and s800i before 1.0.2 allows remote attackers to execute arbitrary code by sending a long (1) voice or (2) video RTP frame.
CVE-2007-3763
The IAX2 channel driver (chan_iax2) in Asterisk before 1.2.22 and 1.4.x before 1.4.8, Business Edition before B.2.2.1, AsteriskNOW before beta7, Appliance Developer Kit before 0.5.0, and s800i before 1.0.2 allows remote attackers to cause a denial of service (crash) via a crafted (1) LAGRQ or (2) LAGRP frame that contains information elements of IAX frames, which results in a NULL pointer dereference when Asterisk does not properly set an associated variable.
CVE-2007-3764
The Skinny channel driver (chan_skinny) in Asterisk before 1.2.22 and 1.4.x before 1.4.8, Business Edition before B.2.2.1, AsteriskNOW before beta7, Appliance Developer Kit before 0.5.0, and s800i before 1.0.2 allows remote attackers to cause a denial of service (crash) via a certain data length value in a crafted packet, which results in an "overly large memcpy."
CVE-2007-3765
The STUN implementation in Asterisk 1.4.x before 1.4.8, AsteriskNOW before beta7, Appliance Developer Kit before 0.5.0, and s800i before 1.0.2 allows remote attackers to cause a denial of service (crash) via a crafted STUN length attribute in a STUN packet sent on an RTP port.
CVE-2007-3825
Multiple stack-based buffer overflows in the RPC implementation in alert.exe before 8.0.255.0 in CA (formerly Computer Associates) Alert Notification Server, as used in Threat Manager for the Enterprise, Protection Suites, certain BrightStor ARCserve products, and BrightStor Enterprise Backup, allow remote attackers to execute arbitrary code by sending certain data to unspecified RPC procedures.
CVE-2007-3853
Multiple unspecified vulnerabilities in Oracle Database 10.1.0.5 and 10.2.0.3 allow remote authenticated users to have unknown impact via (1) DBMS_JAVA_TEST in the JavaVM component (DB01), (2) Oracle Text component (DB09), and (3) MDSYS.SDO_GEOR_INT in the Spatial component (DB15).  NOTE: a reliable researcher claims that DB01 is SQL injection in DBMS_PRVTAQIS.
CVE-2007-3854
Multiple unspecified vulnerabilities in Oracle Database 9.0.1.5+, 9.2.0.7, and 10.1.0.5 allow remote authenticated users to have unknown impact via (1) SYS.DBMS_PRVTAQIS in the Advanced Queuing component (DB02) and (2) MDSYS.MD in the Spatial component (DB12).  NOTE: Oracle has not disputed reliable researcher claims that DB02 is for SQL injection and DB12 is for a buffer overflow.
CVE-2007-3855
Multiple unspecified vulnerabilities in Oracle Database 9.0.1.5+, 9.2.0.8, 9.2.0.8DV, 10.1.0.5, and 10.2.0.3 allows remote authenticated users to have an unknown impact via (1) SYS.DBMS_DRS in the DataGuard component (DB03), (2) SYS.DBMS_STANDARD in the PL/SQL component (DB10), (3) MDSYS.RTREE_IDX in the Spatial component (DB16), and (4) SQL Compiler (DB17).  NOTE: a reliable researcher claims that DB17 is for using Views to perform unauthorized insert, update, or delete actions.
CVE-2007-3856
Unspecified vulnerability in the Oracle Data Mining component for Oracle Database 10g Release 2 10.2.0.2 and 10.2.0.3, 10g 10.1.0.5, and Oracle9i Database Release 2 9.2.0.7, 9.2.0.8, and 9.2.0.8DV has unknown impact and remote authenticated attack vectors related to DMSYS.DMP_SYS, aka DB04.
As the vulnerability impact is unspecified, the impact has been set to a default value of "Obtain Other Access."
CVE-2007-3857
Multiple unspecified vulnerabilities in Oracle Database 10.1.0.5 allow remote authenticated users to have an unknown impact via (a) the Oracle Text component, including (1) unspecified vectors (DB05), (2) CTXSYS.DRVXMD (DB06), (3) CTXSYS.DRI_MOVE_CTXSYS (DB07), (4) CTXSYS.DRVXMD (DB08), and (b) JavaVM (DB14).
As the vulnerability impact is unspecified, the impact has been set to a default value of "Obtain Other Access."
CVE-2007-3858
Multiple unspecified vulnerabilities in Oracle Database 10.2.0.3 allow remote authenticated users to have an unknown impact via (1) EXFSYS.DBMS_RLMGR_UTL in Rules Manager (DB11) and (2) Program Interface (DB13).
As the vulnerability impact is unspecified, the impact has been set to a default value of "Obtain Other Access."
CVE-2007-3859
Unspecified vulnerability in the Oracle Internet Directory component for Oracle Database 9.2.0.8 and 9.2.0.8DV; Application Server 9.0.4.3, 10.1.2.0.2, and 10.1.2.2; and Collaboration Suite 10.1.2 has unknown impact and remote attack vectors, aka OID01.
As the vulnerability impact is unspecified, the impact has been set to a default value of "Obtain Other Access."
CVE-2007-3860
Unspecified vulnerability in Oracle Application Express (formerly Oracle HTML DB) 2.2.0.00.32 up to 3.0.0.00.20 allows developers to have an unknown impact via unknown attack vectors, aka APEX01.  NOTE: a reliable researcher states that this is SQL injection in the wwv_flow_security.check_db_password function due to insufficient checks for '"' characters.
CVE-2007-3861
Unspecified vulnerability in Oracle Jdeveloper in Oracle Application Server 10.1.2.2 and Collaboration Suite 10.1.2 allows context-dependent attackers to have an unknown impact via custom applications that use JBO.KEY, aka JDEV01.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3862
Unspecified vulnerability in Oracle Application Server 9.0.4.3 and 10.1.2.0.2 allows remote attackers to have an unknown impact via Oracle Single Sign On, aka AS01.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3863
Unspecified vulnerability in Oracle JDeveloper for Application Server 10.1.2.2 and 10.1.3.1, and Collaboration Suite 10.1.2, allows context-dependent attackers to have an unknown impact via custom applications that use JBO.SERVER, aka JDEV02.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3864
Multiple unspecified vulnerabilities in Oracle Collaboration Suite 10.1.2 have unknown impact and remote attack vectors via (1) Instant Messaging/Presence (OCS01) and (2) Oracle Single Sign On (AS02).
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3865
Unspecified vulnerability in the Oracle Customer Intelligence component in Oracle E-Business Suite 12.0.1 has unknown impact and remote attack vectors, aka APPS01.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3866
Multiple unspecified vulnerabilities in Oracle E-Business Suite 11.5.10CU2 and 12.0.1 allow remote attackers to have an unknown impact via (a) Oracle Configurator (APPS02), (b) Oracle iExpenses (APPS03), (c) Oracle Application Object Library (APPS09), and (1) APPS12, (2) APPS13, and (3) APPS14 in (d) Oracle Payables.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3867
Multiple unspecified vulnerabilities in Oracle E-Business Suite 11.5.10CU2 have unknown impact and attack vectors, related to (1) APPS04, (2) APPS05, and (3) APPS06 in (a) Oracle Application Object Library, (4) APPS07 in Oracle Customer Intelligence, (5) APPS08 in Oracle Payments, (7) APPS10 in Oracle Human Resources, and (8) APPS11 in iRecruitment.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3868
Multiple unspecified vulnerabilities in PeopleTools in Oracle PeopleSoft Enterprise 8.22.15, 8.47.13, 8.48.10, and 8.49.02 allows remote authenticated users or attackers to have an unknown impact via multiple vectors, aka (1) PSE01, (2) PSE02, and (3) PSE03.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3869
Multiple unspecified vulnerabilities in the Customer Relationship Management Online Marketing component in Oracle PeopleSoft Enterprise 8.9 Bundle 26 and 9.0 Bundle 7 allow remote authenticated users to have an unknown impact, aka (1) PSE04 and (2) PSE05.
As the impact type is unspecified, it has been set to a default value of "Obtain Other Access (e.g. application account)."
CVE-2007-3870
Multiple unspecified vulnerabilities in the Human Capital Management component in Oracle PeopleSoft Enterprise 8.9 Bundle 11 allow local users to have unknown impact via unknown vectors, aka (1) PSE06 and (2) PSE07.
9.0 Bundle 7
CVE-2007-3881
SQL injection vulnerability in index.php in Pictures Rating (Picture Rating) allows remote attackers to execute arbitrary SQL commands via the msgid parameter.
CVE-2007-3882
SQL injection vulnerability in index.php in Expert Advisor allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-3883
The Data Dynamics ActiveBar ActiveX control (actbar3.ocx) 3.2 and earlier allows remote attackers to create or overwrite files via a full pathname in (1) the second argument to the Save method, or the first argument to the (2) SaveLayoutChanges or (3) SaveMenuUsageData method.
CVE-2007-3884
SQL injection vulnerability in philboard_forum.asp in husrevforum 1.0.1 allows remote attackers to execute arbitrary SQL commands via the forumid parameter.  NOTE: it was later reported that 2.0.1 is also affected.
CVE-2007-3885
Cross-site scripting (XSS) vulnerability in philboard_search.asp in husrevforum 1.0.1 allows remote attackers to inject arbitrary web script or HTML via the searchterms parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3886
Cross-site scripting (XSS) vulnerability in default.asp in Element CMS allows remote attackers to inject arbitrary web script or HTML via the s parameter in a search pID action.
CVE-2007-3887
Multiple cross-site scripting (XSS) vulnerabilities in mesaj_formu.asp in ASP Ziyaretci Defteri 1.1 allow remote attackers to inject arbitrary web script or HTML via the (1) Isim, (2) Mesajiniz, and (3) E-posta fields.  NOTE: these probably correspond to the isim, mesaj, and posta parameters to save.php.
CVE-2007-3888
Multiple cross-site scripting (XSS) vulnerabilities in Insanely Simple Blog 0.5 and earlier allow remote attackers to inject arbitrary web script or HTML via (1) the search action, possibly related to the term parameter to index.php; or (2) an anonymous blog entry, possibly involving the (a) posted_by, (b) subject, and (c) content parameters to index.php; as demonstrated by the onmouseover attribute of certain elements.  NOTE: some of these details are obtained from third party information.
CVE-2007-3889
Multiple SQL injection vulnerabilities in Insanely Simple Blog 0.5 and earlier allow remote attackers to execute arbitrary SQL commands via the current_subsection parameter to index.php and other unspecified vectors.
