CVE-2007-4850
curl/interface.c in the cURL library (aka libcurl) in PHP 5.2.4 and 5.2.5 allows context-dependent attackers to bypass safe_mode and open_basedir restrictions and read arbitrary files via a file:// request containing a \x00 sequence, a different vulnerability than CVE-2006-2563.
CVE-2007-5764
Buffer overflow in the pioout program in printers.rte in IBM AIX 5.2, 5.3, and 6.1 allows local users to gain privileges via a long command line option.
CVE-2007-6415
scponly 4.6 and earlier allows remote authenticated users to bypass intended restrictions and execute arbitrary code by invoking scp, as implemented by OpenSSH, with the -F and -o options.
CVE-2008-0441
IBM Tivoli Business Service Manager (TBSM) 4.1.1 stores passwords in cleartext (1) after external authentication, which triggers writing the password to SM_server.log; and (2) after a reconfig action; which allows local users to obtain sensitive information.
CVE-2008-0442
PHP remote file inclusion vulnerability in inc/linkbar.php in Small Axe Weblog 0.3.1 allows remote attackers to execute arbitrary PHP code via a URL in the ffile parameter, a different vector than CVE-2008-0376.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-0443
Heap-based buffer overflow in the FileUploader.FUploadCtl.1 ActiveX control in FileUploader.dll 2.0.0.2 in Lycos FileUploader Module allows remote attackers to execute arbitrary code via a long HandwriterFilename property value.  NOTE: some of these details are obtained from third party information.
CVE-2008-0444
Cross-site scripting (XSS) vulnerability in Electronic Logbook (ELOG) before 2.7.0 allows remote attackers to inject arbitrary web script or HTML via subtext parameter to unspecified components.
CVE-2008-0445
The replace_inline_img function in elogd in Electronic Logbook (ELOG) before 2.7.1 allows remote attackers to cause a denial of service (infinite loop) via crafted logbook entries.  NOTE: some of these details are obtained from third party information.
CVE-2008-0446
SQL injection vulnerability in voircom.php in LulieBlog 1.02 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-0447
SQL injection vulnerability in index.php in Foojan WMS PHP Weblog 1.0 allows remote attackers to execute arbitrary SQL commands via the story parameter.
CVE-2008-0448
PHP remote file inclusion vulnerability in utils/class_HTTPRetriever.php in phpSearch allows remote attackers to execute arbitrary PHP code via a URL in the libcurlemuinc parameter.
CVE-2008-0449
SQL injection vulnerability in paypalresult.asp in VP-ASP Shopping Cart 6.50 and earlier allows remote attackers to execute arbitrary SQL commands via unspecified vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-0450
Multiple PHP remote file inclusion vulnerabilities in BLOG:CMS 4.2.1.c allow remote attackers to execute arbitrary PHP code via a URL in the (1) DIR_PLUGINS parameter to (a) index.php, and the (2) DIR_LIBS parameter to (b) media.php and (c) xmlrpc/server.php in admin/.
CVE-2008-0451
Multiple SQL injection vulnerabilities in PacerCMS 0.6 allow remote authenticated users to execute arbitrary SQL commands via the id parameter to (1) siteadmin/article-edit.php; and unspecified parameters to (2) submitted-edit.php, (3) page-edit.php, (4) section-edit.php, (5) staff-edit.php, and (6) staff-access.php in siteadmin/.
CVE-2008-0452
Directory traversal vulnerability in articles.php in Siteman 1.1.9 allows remote attackers to read arbitrary files via directory traversal sequences in the cat parameter in a viewart action.
CVE-2008-0453
SQL injection vulnerability in list.php in Easysitenetwork Recipe allows remote attackers to execute arbitrary SQL commands via the categoryid parameter.
CVE-2008-0454
Cross-zone scripting vulnerability in the Internet Explorer web control in Skype 3.6.0.244, and earlier 3.5.x and 3.6.x versions, on Windows allows user-assisted remote attackers to inject arbitrary web script or HTML in the Local Machine Zone via the Title field of a (1) Dailymotion and possibly (2) Metacafe movie in the Skype video gallery, accessible through a search within the "Add video to chat" dialog, aka "videomood XSS."
CVE-2008-0455
Cross-site scripting (XSS) vulnerability in the mod_negotiation module in the Apache HTTP Server 2.2.6 and earlier in the 2.2.x series, 2.0.61 and earlier in the 2.0.x series, and 1.3.39 and earlier in the 1.3.x series allows remote authenticated users to inject arbitrary web script or HTML by uploading a file with a name containing XSS sequences and a file extension, which leads to injection within a (1) "406 Not Acceptable" or (2) "300 Multiple Choices" HTTP response when the extension is omitted in a request for the file.
CVE-2008-0456
CRLF injection vulnerability in the mod_negotiation module in the Apache HTTP Server 2.2.6 and earlier in the 2.2.x series, 2.0.61 and earlier in the 2.0.x series, and 1.3.39 and earlier in the 1.3.x series allows remote authenticated users to inject arbitrary HTTP headers and conduct HTTP response splitting attacks by uploading a file with a multi-line name containing HTTP header sequences and a file extension, which leads to injection within a (1) "406 Not Acceptable" or (2) "300 Multiple Choices" HTTP response when the extension is omitted in a request for the file.
CVE-2008-0458
Directory traversal vulnerability in function/sources.php in SLAED CMS 2.5 Lite allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the newlang parameter to index.php.
CVE-2008-0459
Directory traversal vulnerability in update/index.php in Liquid-Silver CMS 0.35, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the update parameter.
CVE-2008-0460
Cross-site scripting (XSS) vulnerability in api.php in (1) MediaWiki 1.11 through 1.11.0rc1, 1.10 through 1.10.2, 1.9 through 1.9.4, and 1.8; and (2) the BotQuery extension for MediaWiki 1.7 and earlier; when Internet Explorer is used, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-0461
SQL injection vulnerability in index.php in the Search module in PHP-Nuke 8.0 FINAL and earlier, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the sid parameter in a comments action to modules.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-0462
Cross-site scripting (XSS) vulnerability in the Archive 5.x before 5.x-1.8 module for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-0463
Cross-site scripting (XSS) vulnerability in the Workflow 4.7.x before 4.7.x-1.2 and 5.x before 5.x-1.2 module for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors involving node properties.
CVE-2008-0464
Directory traversal vulnerability in archiv.cgi in absofort aconon Mail 2007 Enterprise SQL 11.7.0 and Mail 2004 Enterprise SQL 11.5.1 allows remote attackers to read arbitrary files via a .. (dot dot) in the template parameter.
CVE-2008-0465
Directory traversal vulnerability in optimizer.php in Seagull 0.6.3 allows remote attackers to read arbitrary files via a .. (dot dot) in the files parameter.
The vendor has released a patch for 0.6.3.  A patch can be found at the following location: 

http://seagullproject.org/download/


