CVE-2007-1558
The APOP protocol allows remote attackers to guess the first 3 characters of a password via man-in-the-middle (MITM) attacks that use crafted message IDs and MD5 collisions.  NOTE: this design-level issue potentially affects all products that use APOP, including (1) Thunderbird 1.x before 1.5.0.12 and 2.x before 2.0.0.4, (2) Evolution, (3) mutt, (4) fetchmail before 6.3.8, (5) SeaMonkey 1.0.x before 1.0.9 and 1.1.x before 1.1.2, (6) Balsa 2.3.16 and earlier, (7) Mailfilter before 0.8.2, and possibly other products.
CVE-2007-1745
The chm_decompress_stream function in libclamav/chmunpack.c in Clam AntiVirus (ClamAV) before 0.90.2 leaks file descriptors, which has unknown impact and attack vectors involving a crafted CHM file, a different vulnerability than CVE-2007-0897.  NOTE: some of these details are obtained from third party information.
CVE-2007-1997
Integer signedness error in the (1) cab_unstore and (2) cab_extract functions in libclamav/cab.c in Clam AntiVirus (ClamAV) before 0.90.2 allow remote attackers to execute arbitrary code via a crafted CHM file that contains a negative integer, which passes a signed comparison and leads to a stack-based buffer overflow.
CVE-2007-2030
lharc.c in lha does not securely create temporary files, which might allow local users to read or write files by creating a file before LHA is invoked.
CVE-2007-2031
Buffer overflow in the HTTP proxy service for 3proxy 0.5 to 0.5.3g, and 0.6b-devel before 20070413, might allow remote attackers to execute arbitrary code via crafted transparent requests.
CVE-2007-2032
Cisco Wireless Control System (WCS) before 4.0.96.0 has a hard-coded FTP username and password for backup operations, which allows remote attackers to read and modify arbitrary files via unspecified vectors related to "properties of the FTP server," aka Bug ID CSCse93014.
CVE-2007-2033
Unspecified vulnerability in Cisco Wireless Control System (WCS) before 4.0.81.0 allows remote authenticated users to read any configuration page by changing the group membership of user accounts, aka Bug ID CSCse78596.
CVE-2007-2034
Unspecified vulnerability in Cisco Wireless Control System (WCS) before 4.0.87.0 allows remote authenticated users to gain the privileges of the SuperUsers group, and manage the application and its networks, related to the group membership of user accounts, aka Bug ID CSCsg05190.
CVE-2007-2035
Cisco Wireless Control System (WCS) before 4.0.66.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain network organization data via a direct request for files in certain directories, aka Bug ID CSCsg04301.
CVE-2007-2036
The SNMP implementation in the Cisco Wireless LAN Controller (WLC) before 20070419 uses the default read-only community public, and the default read-write community private, which allows remote attackers to read and modify SNMP variables, aka Bug ID CSCse02384.
CVE-2007-2037
Cisco Wireless LAN Controller (WLC) before 3.2.116.21, and 4.0.x before 4.0.155.0, allows remote attackers on a local network to cause a denial of service (device crash) via malformed Ethernet traffic.
CVE-2007-2038
The Network Processing Unit (NPU) in the Cisco Wireless LAN Controller (WLC) before 3.2.193.5, 4.0.x before 4.0.206.0, and 4.1.x allows remote attackers on a local wireless network to cause a denial of service (loss of packet forwarding) via (1) crafted SNAP packets, (2) malformed 802.11 traffic, or (3) packets with certain header length values, aka Bug ID CSCsg36361.
CVE-2007-2039
The Network Processing Unit (NPU) in the Cisco Wireless LAN Controller (WLC) before 3.2.171.5, 4.0.x before 4.0.206.0, and 4.1.x allows remote attackers on a local wireless network to cause a denial of service (loss of packet forwarding) via (1) crafted SNAP packets, (2) malformed 802.11 traffic, or (3) packets with certain header length values, aka Bug IDs CSCsg15901 and CSCsh10841.
CVE-2007-2040
Cisco Aironet 1000 Series and 1500 Series Lightweight Access Points before 3.2.185.0, and 4.0.x before 4.0.206.0, have a hard-coded password, which allows attackers with physical access to perform arbitrary actions on the device, aka Bug ID CSCsg15192.
CVE-2007-2041
Cisco Wireless LAN Controller (WLC) before 4.0.206.0 saves the WLAN ACL configuration with an invalid checksum, which prevents WLAN ACLs from being loaded at boot time, and might allow remote attackers to bypass intended access restrictions, aka Bug ID CSCse58195.
CVE-2007-2042
Multiple PHP remote file inclusion vulnerabilities in the Avant-Garde Solutions MOSMedia Lite 1.0.6 and earlier module for Mambo allow remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter to (1) support.html.php or (2) info.html.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2043
Multiple PHP remote file inclusion vulnerabilities in the Avant-Garde Solutions MOSMedia (com_mosmedia) 1.08 and earlier module for Mambo and Joomla! allow remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter to (1) media.tab.php or (2) media.divs.php.
CVE-2007-2044
PHP remote file inclusion vulnerability in mod_weather.php in the Antonis Ventouris Weather module for Mambo and Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the absolute_path parameter.
CVE-2007-2045
Unspecified vulnerability in the IP implementation in Sun Solaris 8 and 9 allows remote attackers to cause a denial of service (CPU consumption) via crafted IP packets, probably related to fragmented packets with duplicate or missing fragments.
CVE-2007-2046
Multiple CRLF injection vulnerabilities in adclick.php in (a) Openads (phpAdsNew) 2.0.11 and earlier and (b) Openads for PostgreSQL (phpPgAds) 2.0.11 and earlier allow remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via CRLF sequences in (1) the dest parameter and (2) the Referer HTTP header.  NOTE: some of these details are obtained from third party information.
CVE-2007-2047
CRLF injection vulnerability in www/delivery/ck.php in Openads 2.3 (aka Max Media Manager, MMM) before 0.3.31-alpha-pr3 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via CRLF sequences in the destination parameter. NOTE: some of these details are obtained from third party information.
CVE-2007-2048
Directory traversal vulnerability in /console in the Management Console in webMethods Glue 6.5.1 and earlier allows remote attackers to read arbitrary system files via a .. (dot dot) in the resource parameter.
CVE-2007-2049
Multiple PHP remote file inclusion vulnerabilities in the Calendar Module (com_calendar) 1.5.5 for Mambo allow remote attackers to execute arbitrary PHP code via a URL in the absolute_path parameter to (1) com_calendar.php or (2) mod_calendar.php.
CVE-2007-2050
Multiple directory traversal vulnerabilities in header.php in RicarGBooK 1.2.1 allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in (1) a lang cookie or (2) the language parameter.
CVE-2007-2051
Buffer overflow in the parsecmd function in bftpd before 1.8 has unknown impact and attack vectors related to the confstr variable.
CVE-2007-2052
Off-by-one error in the PyLocale_strxfrm function in Modules/_localemodule.c for Python 2.4 and 2.5 causes an incorrect buffer size to be used for the strxfrm function, which allows context-dependent attackers to read portions of memory via unknown manipulations that trigger a buffer over-read due to missing null termination.
