CVE-2013-5653
The getenv and filenameforall functions in Ghostscript 9.10 ignore the "-dSAFER" argument, which allows remote attackers to read data via a crafted postscript file.
CVE-2016-10040
Stack-based buffer overflow in QXmlSimpleReader in Qt 4.8.5 allows remote attackers to cause a denial of service (application crash) via a xml file with multiple nested open tags.
CVE-2016-10200
Race condition in the L2TPv3 IP Encapsulation feature in the Linux kernel before 4.8.14 allows local users to gain privileges or cause a denial of service (use-after-free) by making multiple bind system calls without properly ascertaining whether a socket has the SOCK_ZAPPED status, related to net/l2tp/l2tp_ip.c and net/l2tp/l2tp_ip6.c.
CVE-2016-4946
Multiple cross-site scripting (XSS) vulnerabilities in Cloudera HUE 3.9.0 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) First name or (2) Last name field in the HUE Users page.
CVE-2016-4947
Cloudera HUE 3.9.0 and earlier allows remote attackers to enumerate user accounts via a request to desktop/api/users/autocomplete.
CVE-2016-4948
Multiple cross-site scripting (XSS) vulnerabilities in Cloudera Manager 5.5 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) Template Name field when renaming a template; (2) KDC Server host, (3) Kerberos Security Realm, (4) Kerberos Encryption Types, (5) Advanced Configuration Snippet (Safety Valve) for [libdefaults] section of krb5.conf, (6) Advanced Configuration Snippet (Safety Valve) for the Default Realm in krb5.conf, (7) Advanced Configuration Snippet (Safety Valve) for remaining krb5.conf, or (8) Active Directory Account Prefix fields in the Kerberos wizard; or (9) classicWizard parameter to cmf/cloudera-director/redirect.
CVE-2016-4949
Cloudera Manager 5.5 and earlier allows remote attackers to obtain sensitive information via a (1) stderr.log or (2) stdout.log value in the filename parameter to /cmf/process/<process_id>/logs.
CVE-2016-4950
Cloudera Manager 5.5 and earlier allows remote attackers to enumerate user sessions via a request to /api/v11/users/sessions.
CVE-2016-5315
The setByteArray function in tif_dir.c in libtiff 4.0.6 and earlier allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted tiff image.
CVE-2016-6239
The mmap extension __MAP_NOFAULT in OpenBSD 5.8 and 5.9 allows attackers to cause a denial of service (kernel panic and crash) via a large size value.
CVE-2016-6240
Integer truncation error in the amap_alloc function in OpenBSD 5.8 and 5.9 allows local users to execute arbitrary code with kernel privileges via a large size value.
CVE-2016-6241
Integer overflow in the amap_alloc1 function in OpenBSD 5.8 and 5.9 allows local users to execute arbitrary code with kernel privileges via a large size value.
CVE-2016-6242
OpenBSD 5.8 and 5.9 allows local users to cause a denial of service (assertion failure and kernel panic) via a large ident value in a kevent system call.
CVE-2016-6243
thrsleep in kern/kern_synch.c in OpenBSD 5.8 and 5.9 allows local users to cause a denial of service (kernel panic) via a crafted value in the tsp parameter of the __thrsleep system call.
CVE-2016-6244
The sys_thrsigdivert function in kern/kern_sig.c in the OpenBSD kernel 5.9 allows remote attackers to cause a denial of service (panic) via a negative "ts.tv_sec" value.
CVE-2016-6245
OpenBSD 5.8 and 5.9 allows local users to cause a denial of service (kernel panic) via a large size in a getdents system call.
CVE-2016-6246
OpenBSD 5.8 and 5.9 allows certain local users with kern.usermount privileges to cause a denial of service (kernel panic) by mounting a tmpfs with a VNOVAL in the (1) username, (2) groupname, or (3) device name of the root node.
CVE-2016-6247
OpenBSD 5.8 and 5.9 allows certain local users to cause a denial of service (kernel panic) by unmounting a filesystem with an open vnode on the mnt_vnodelist.
CVE-2016-6255
Portable UPnP SDK (aka libupnp) before 1.6.21 allows remote attackers to write to arbitrary files in the webroot via a POST request without a registered handler.
CVE-2016-6350
OpenBSD 5.8 and 5.9 allows local users to cause a denial of service (NULL pointer dereference and panic) via a sysctl call with a path starting with 10,9.
CVE-2016-6522
Integer overflow in the uvm_map_isavail function in uvm/uvm_map.c in OpenBSD 5.9 allows local users to cause a denial of service (kernel panic) via a crafted mmap call, which triggers the new mapping to overlap with an existing mapping.
CVE-2016-7135
Directory traversal vulnerability in Plone CMS 5.x through 5.0.6 and 4.2.x through 4.3.11 allows remote administrators to read arbitrary files via a .. (dot dot) in the path parameter in a getFile action to Plone/++theme++barceloneta/@@plone.resourceeditor.filemanager-actions.
CVE-2016-7136
z3c.form in Plone CMS 5.x through 5.0.6 and 4.x through 4.3.11 allows remote attackers to conduct cross-site scripting (XSS) attacks via a crafted GET request.
CVE-2016-7137
Multiple open redirect vulnerabilities in Plone CMS 5.x through 5.0.6, 4.x through 4.3.11, and 3.3.x through 3.3.6 allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the referer parameter to (1) %2b%2bgroupdashboard%2b%2bplone.dashboard1%2bgroup/%2b/portlets.Actions or (2) folder/%2b%2bcontextportlets%2b%2bplone.footerportlets/%2b /portlets.Actions or the (3) came_from parameter to /login_form.
CVE-2016-7138
Cross-site scripting (XSS) vulnerability in the URL checking infrastructure in Plone CMS 5.x through 5.0.6, 4.x through 4.3.11, and 3.3.x through 3.3.6 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-7139
Cross-site scripting (XSS) vulnerability in an unspecified page template in Plone CMS 5.x through 5.0.6, 4.x through 4.3.11, and 3.3.x through 3.3.6 allows remote attackers to inject arbitrary web script or HTML via unknown vectors.
CVE-2016-7140
Multiple cross-site scripting (XSS) vulnerabilities in the ZMI page in Zope2 in Plone CMS 5.x through 5.0.6, 4.x through 4.3.11, and 3.3.x through 3.3.6 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-7145
The m_authenticate function in ircd/m_authenticate.c in nefarious2 allows remote attackers to spoof certificate fingerprints and consequently log in as another user via a crafted AUTHENTICATE parameter.
CVE-2016-7780
SQL injection vulnerability in cron/find_help.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the version parameter.
CVE-2016-7781
SQL injection vulnerability in framework/modules/blog/controllers/blogController.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the author parameter.
CVE-2016-7782
SQL injection vulnerability in framework/core/models/expConfig.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the src parameter.
CVE-2016-7783
SQL injection vulnerability in framework/core/models/expRecord.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the title parameter.
CVE-2016-7784
SQL injection vulnerability in the getSection function in framework/core/subsystems/expRouter.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the section parameter.
CVE-2016-7788
SQL injection vulnerability in framework/modules/users/models/user.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2016-7789
SQL injection vulnerability in framework/core/models/expConfig.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the apikey parameter.
CVE-2016-8863
Heap-based buffer overflow in the create_url_list function in gena/gena_device.c in Portable UPnP SDK (aka libupnp) before 1.6.21 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a valid URI followed by an invalid one in the CALLBACK header of an SUBSCRIBE request.
CVE-2016-8940
IBM Tivoli Storage Manager (IBM Spectrum Protect) 6.1, 6.2, 6.3, and 7.1 does not perform sufficient authority checking on SQL queries. As a result, an attacker is able to submit SQL queries that access database tables that are not intended for access or use by administrators. The access of these product specific database tables may allow access to passwords or other sensitive information for the product. IBM Reference #: 1998946.
CVE-2016-8971
IBM WebSphere MQ 8.0 could allow an authenticated user with queue manager permissions to cause a segmentation fault which would result in the box having to be rebooted to resume normal operations. IBM Reference #: 1998663.
CVE-2016-9019
SQL injection vulnerability in the activate_address function in framework/modules/addressbook/controllers/addressController.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the is_what parameter.
CVE-2016-9020
SQL injection vulnerability in framework/modules/help/controllers/helpController.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the version parameter.
CVE-2016-9087
SQL injection vulnerability in framework/modules/filedownloads/controllers/filedownloadController.php in Exponent CMS 2.3.9 and earlier allows remote attackers to execute arbitrary SQL commands via the fileid parameter.
CVE-2016-9148
Cross-site scripting (XSS) vulnerability in CA Service Desk Manager (formerly CA Service Desk) 12.9 and 14.1 allows remote attackers to inject arbitrary web script or HTML via the QBE.EQ.REF_NUM parameter.
CVE-2016-9164
Directory traversal vulnerability in diag.jsp file in CA Unified Infrastructure Management (formerly CA Nimsoft Monitor) 8.4 SP1 and earlier and CA Unified Infrastructure Management Snap (formerly CA Nimsoft Monitor Snap) allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2016-9245
In F5 BIG-IP systems 12.1.0 - 12.1.2, malicious requests made to virtual servers with an HTTP profile can cause the TMM to restart. The issue is exposed with BIG-IP APM profiles, regardless of settings. The issue is also exposed with the non-default "Normalize URI" configuration options used in iRules and/or BIG-IP LTM policies. An attacker may be able to disrupt traffic or cause the BIG-IP system to fail over to another device in the device group.
CVE-2016-9571
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2016-9606.  Reason: This candidate is a duplicate of CVE-2016-9606.  Reason: this ID was intended for one issue, but was associated with two issues.  Notes: All CVE users should reference CVE-2016-9606 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2016-9643
The regex code in Webkit 2.4.11 allows remote attackers to cause a denial of service (memory consumption) as demonstrated in a large number of ($ (open parenthesis and dollar) followed by {-2,16} and a large number of +) (plus close parenthesis).
CVE-2016-9693
IBM Business Process Manager 7.5, 8.0, and 8.5 has a file download capability that is vulnerable to a set of attacks. Ultimately, an attacker can cause an unauthenticated victim to download a malicious payload. An existing file type restriction can be bypassed so that the payload might be considered executable and cause damage on the victim's machine. IBM Reference #: 1998655.
CVE-2016-9720
IBM QRadar 7.2 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM Reference #: 1999533.
CVE-2016-9723
IBM QRadar 7.2 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM Reference #: 1999534.
CVE-2016-9724
IBM QRadar 7.2 is vulnerable to a denial of service, caused by an XML External Entity Injection (XXE) error when processing XML data. A remote attacker could exploit this vulnerability to expose highly sensitive information or consume all available memory resources. IBM Reference #: 1999537.
CVE-2016-9725
IBM QRadar Incident Forensics 7.2 allows for Cross-Origin Resource Sharing (CORS), which is a mechanism that allows web sites to request resources from external sites, avoiding the need to duplicate them. IBM Reference #: 1999539.
CVE-2016-9726
IBM QRadar Incident Forensics 7.2 could allow a remote authenticated attacker to execute arbitrary commands on the system. By sending a specially-crafted request, an attacker could exploit this vulnerability to execute arbitrary commands on the system. IBM Reference #: 1999542.
CVE-2016-9727
IBM QRadar 7.2 could allow a remote authenticated attacker to execute arbitrary commands on the system. By sending a specially-crafted request, an attacker could exploit this vulnerability to execute arbitrary commands on the system. IBM Reference #: 1999542.
CVE-2016-9728
IBM Qradar 7.2 is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, information in the back-end database. IBM Reference #: 1999543.
CVE-2016-9729
IBM QRadar 7.2 does not perform an authentication check for a critical resource or functionality allowing anonymous users access to protected areas. IBM Reference #: 1999545.
CVE-2016-9730
IBM QRadar Incident Forensics 7.2 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM Reference #: 1999549.
CVE-2016-9740
IBM QRadar 7.2 could allow a remote attacker to consume all resources on the server due to not properly restricting the size or amount of resources requested by an actor. IBM Reference #: 1999556.
CVE-2017-1124
IBM Maximo Asset Management 7.1, 7.5, and 7.6 could allow a local attacker to obtain sensitive information using HTTP Header Injection. IBM Reference #: 1998053.
CVE-2017-1133
IBM QRadar 7.2 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM Reference #: 1999534.
CVE-2017-2636
Race condition in drivers/tty/n_hdlc.c in the Linux kernel through 4.10.1 allows local users to gain privileges or cause a denial of service (double free) by setting the HDLC line discipline.
CVE-2017-3159
Apache Camel's camel-snakeyaml component is vulnerable to Java object de-serialization vulnerability. De-serializing untrusted data can lead to security flaws.
CVE-2017-5681
The RSA-CRT implementation in the Intel QuickAssist Technology (QAT) Engine for OpenSSL versions prior to 0.5.19 may allow remote attackers to obtain private RSA keys by conducting a Lenstra side-channel attack.
CVE-2017-6508
CRLF injection vulnerability in the url_parse function in url.c in Wget through 1.19.1 allows remote attackers to inject arbitrary HTTP headers via CRLF sequences in the host subcomponent of a URL.
CVE-2017-6509
Smith0r/burgundy-cms before 2017-03-06 is vulnerable to a reflected XSS in admin/components/menu/views/menuitems.php (id parameter).
CVE-2017-6511
andrzuk/FineCMS before 2017-03-06 is vulnerable to a reflected XSS in index.php because of missing validation of the action parameter in application/classes/application.php.
