CVE-2012-1537
Heap-based buffer overflow in DirectPlay in DirectX 9.0 through 11.1 in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, and Windows Server 2012 allows remote attackers to execute arbitrary code via a crafted Office document, aka "DirectPlay Heap Overflow Vulnerability."
CVE-2012-2539
Microsoft Word 2003 SP3, 2007 SP2 and SP3, and 2010 SP1; Word Viewer; Office Compatibility Pack SP2 and SP3; and Office Web Apps 2010 SP1 allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via crafted RTF data, aka "Word RTF 'listoverridecount' Remote Code Execution Vulnerability."
CVE-2012-2549
The IP-HTTPS server in Windows Server 2008 R2 and R2 SP1 and Server 2012 does not properly validate certificates, which allows remote attackers to bypass intended access restrictions via a revoked certificate, aka "Revoked Certificate Bypass Vulnerability."
CVE-2012-2556
The OpenType Font (OTF) driver in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT allows remote attackers to execute arbitrary code via a crafted OpenType font file, aka "OpenType Font Parsing Vulnerability."
CVE-2012-4774
Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allow remote attackers to execute arbitrary code via a crafted (1) file name or (2) subfolder name that triggers use of unallocated memory as the destination of a copy operation, aka "Windows Filename Parsing Vulnerability."
CVE-2012-4781
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "InjectHTMLStream Use After Free Vulnerability."
CVE-2012-4782
Use-after-free vulnerability in Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "CMarkup Use After Free Vulnerability."
CVE-2012-4786
The kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, Windows 7 Gold and SP1, Windows 8, Windows Server 2012, and Windows RT allow remote attackers to execute arbitrary code via a crafted TrueType Font (TTF) file, aka "TrueType Font Parsing Vulnerability."
CVE-2012-4787
Use-after-free vulnerability in Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to an object that (1) was not properly initialized or (2) is deleted, aka "Improper Ref Counting Use After Free Vulnerability."
CVE-2012-4791
Microsoft Exchange Server 2007 SP3 and 2010 SP1 and SP2 allows remote authenticated users to cause a denial of service (Information Store service hang) by subscribing to a crafted RSS feed, aka "RSS Feed May Cause Exchange DoS Vulnerability."
CVE-2012-4971
Multiple SQL injection vulnerabilities in Layton Helpbox 4.4.0 allow remote attackers to execute arbitrary SQL commands via the (1) reqclass parameter to editrequestenduser.asp; the (2) sys_request_id parameter to editrequestuser.asp; the (3) sys_request_id parameter to enduseractions.asp; the (4) sys_request_id or (5) confirm parameter to enduserreopenrequeststatus.asp; the (6) searchsql, (7) back, or (8) status parameter to enduserrequests.asp; the (9) sys_userpwd parameter to validateenduserlogin.asp; the (10) sys_userpwd parameter to validateuserlogin.asp; the (11) sql parameter to editenduseruser.asp; the (12) sql parameter to manageenduserrequestclasses.asp; the (13) sql parameter to resetpwdenduser.asp; the (14) sql parameter to disableloginenduser.asp; the (15) sql parameter to deleteenduseruser.asp; the (16) sql parameter to manageendusers.asp; or the (17) site parameter to statsrequestagereport.asp.
CVE-2012-4972
Multiple cross-site scripting (XSS) vulnerabilities in Layton Helpbox 4.4.0 allow remote attackers to inject arbitrary web script or HTML via the (1) sys_solution_id, (2) sys_requesttype_id, (3) sys_problem_desc, (4) sys_solution_desc, (5) sys_problemsummary, (6) usr_Action_testing, (7) usr_Escalation, or (8) usr_Additional_Resources parameter to writesolutionuser.asp or the (9) sys_solution_id parameter to deletesolution.asp.
CVE-2012-4974
Layton Helpbox 4.4.0 allows remote authenticated users to change the login context and gain privileges via a modified (1) loggedinenduser, (2) loggedinendusername, (3) loggedinuserusergroup, (4) loggedinuser, or (5) loggedinusername cookie.
CVE-2012-4975
editrequestuser.asp in Layton Helpbox 4.4.0 allows remote authenticated users to change arbitrary support-ticket data via a modified sys_request_id parameter.
CVE-2012-4976
selectawasset.asp in Layton Helpbox 4.4.0 allows remote attackers to discover ODBC database credentials via an element=sys_asset_id request, which is not properly handled during construction of an error page.
CVE-2012-4977
Layton Helpbox 4.4.0 allows remote attackers to discover cleartext credentials for the login page by sniffing the network.
CVE-2012-5139
Use-after-free vulnerability in Google Chrome before 23.0.1271.97 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to visibility events.
CVE-2012-5140
Use-after-free vulnerability in Google Chrome before 23.0.1271.97 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to the URL loader.
CVE-2012-5141
Google Chrome before 23.0.1271.97 does not properly restrict instantiation of the Chromoting client plug-in, which has unspecified impact and attack vectors.
CVE-2012-5142
Google Chrome before 23.0.1271.97 does not properly handle history navigation, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via unspecified vectors.
CVE-2012-5143
Integer overflow in Google Chrome before 23.0.1271.97 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to PPAPI image buffers.
CVE-2012-5144
Google Chrome before 23.0.1271.97, and Libav 0.7.x before 0.7.7 and 0.8.x before 0.8.5, do not properly perform AAC decoding, which allows remote attackers to cause a denial of service (stack memory corruption) or possibly have unspecified other impact via vectors related to "an off-by-one overwrite when switching to LTP profile from MAIN."
CVE-2012-5675
Adobe ColdFusion 9.0 through 9.0.2, and 10, allows local users to bypass intended shared-hosting sandbox permissions via unspecified vectors.
CVE-2012-5676
Buffer overflow in Adobe Flash Player before 10.3.183.48 and 11.x before 11.5.502.135 on Windows, before 10.3.183.48 and 11.x before 11.5.502.136 on Mac OS X, before 10.3.183.48 and 11.x before 11.2.202.258 on Linux, before 11.1.111.29 on Android 2.x and 3.x, and before 11.1.115.34 on Android 4.x; Adobe AIR before 3.5.0.880 on Windows and before 3.5.0.890 on Mac OS X; and Adobe AIR SDK before 3.5.0.880 on Windows and before 3.5.0.890 on Mac OS X allows attackers to execute arbitrary code via unspecified vectors.
CVE-2012-5677
Integer overflow in Adobe Flash Player before 10.3.183.48 and 11.x before 11.5.502.135 on Windows, before 10.3.183.48 and 11.x before 11.5.502.136 on Mac OS X, before 10.3.183.48 and 11.x before 11.2.202.258 on Linux, before 11.1.111.29 on Android 2.x and 3.x, and before 11.1.115.34 on Android 4.x; Adobe AIR before 3.5.0.880 on Windows and before 3.5.0.890 on Mac OS X; and Adobe AIR SDK before 3.5.0.880 on Windows and before 3.5.0.890 on Mac OS X allows attackers to execute arbitrary code via unspecified vectors.
CVE-2012-5678
Adobe Flash Player before 10.3.183.48 and 11.x before 11.5.502.135 on Windows, before 10.3.183.48 and 11.x before 11.5.502.136 on Mac OS X, before 10.3.183.48 and 11.x before 11.2.202.258 on Linux, before 11.1.111.29 on Android 2.x and 3.x, and before 11.1.115.34 on Android 4.x; Adobe AIR before 3.5.0.880 on Windows and before 3.5.0.890 on Mac OS X; and Adobe AIR SDK before 3.5.0.880 on Windows and before 3.5.0.890 on Mac OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
