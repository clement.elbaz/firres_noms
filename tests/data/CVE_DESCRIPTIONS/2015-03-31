CVE-2014-2027
eGroupware before 1.8.006.20140217 allows remote attackers to conduct PHP object injection attacks, delete arbitrary files, and possibly execute arbitrary code via the (1) addr_fields or (2) trans parameter to addressbook/csv_import.php, (3) cal_fields or (4) trans parameter to calendar/csv_import.php, (5) info_fields or (6) trans parameter to csv_import.php in (a) projectmanager/ or (b) infolog/, or (7) processed parameter to preferences/inc/class.uiaclprefs.inc.php.
CVE-2014-2830
Stack-based buffer overflow in cifskey.c or cifscreds.c in cifs-utils before 6.4, as used in pam_cifscreds, allows remote attackers to have unspecified impact via unknown vectors.
CVE-2014-7876
Unspecified vulnerability in HP Integrated Lights-Out (iLO) firmware 2 before 2.27 and 4 before 2.03 and iLO Chassis Management (CM) firmware before 1.30 allows remote attackers to gain privileges, execute arbitrary code, or cause a denial of service via unknown vectors.
CVE-2014-9209
Untrusted search path vulnerability in the Clean Utility application in Rockwell Automation FactoryTalk Services Platform before 2.71.00 and FactoryTalk View Studio 8.00.00 and earlier allows local users to gain privileges via a Trojan horse DLL in an unspecified directory.
<a href="http://cwe.mitre.org/data/definitions/426.html">CWE-426: Untrusted Search Path</a>
CVE-2014-9462
The _validaterepo function in sshpeer in Mercurial before 3.2.4 allows remote attackers to execute arbitrary commands via a crafted repository name in a clone command.
CVE-2014-9706
The build_index_from_tree function in index.py in Dulwich before 0.9.9 allows remote attackers to execute arbitrary code via a commit with a directory path starting with .git/, which is not properly handled when checking out a working tree.
CVE-2014-9707
EmbedThis GoAhead 3.0.0 through 3.4.1 does not properly handle path segments starting with a . (dot), which allows remote attackers to conduct directory traversal attacks, cause a denial of service (heap-based buffer overflow and crash), or possibly execute arbitrary code via a crafted URI.
CVE-2014-9708
Embedthis Appweb before 4.6.6 and 5.x before 5.2.1 allows remote attackers to cause a denial of service (NULL pointer dereference) via a Range header with an empty value, as demonstrated by "Range: x=,".
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-0838
Buffer overflow in the C implementation of the apply_delta function in _pack.c in Dulwich before 0.9.9 allows remote attackers to execute arbitrary code via a crafted pack file.
CVE-2015-0900
Cross-site scripting (XSS) vulnerability in schedule.cgi in Nishishi Factory Fumy Teacher's Schedule Board 1.10 through 2.21 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-0901
Cross-site scripting (XSS) vulnerability in the duwasai flashy theme 1.3 and earlier for WordPress allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-0984
Directory traversal vulnerability in the FTP server on Honeywell Excel Web XL1000C50 52 I/O, XL1000C100 104 I/O, XL1000C500 300 I/O, XL1000C1000 600 I/O, XL1000C50U 52 I/O UUKL, XL1000C100U 104 I/O UUKL, XL1000C500U 300 I/O UUKL, and XL1000C1000U 600 I/O UUKL controllers before 2.04.01 allows remote attackers to read files under the web root, and consequently obtain administrative login access, via a crafted pathname.
Further research has revealed that this vulnerability allows authenticated shell access as an administrator. The score has been updated to properly reflect this.
CVE-2015-0985
Cross-site request forgery (CSRF) vulnerability in XZERES 442SR OS on 442SR wind turbines allows remote attackers to hijack the authentication of admins for requests that modify the default user's password via a GET request.
CVE-2015-2106
Unspecified vulnerability in HP Integrated Lights-Out (iLO) firmware 2 before 2.27, 3 before 1.82, and 4 before 2.10 allows remote attackers to bypass intended access restrictions or cause a denial of service via unknown vectors.
CVE-2015-2108
Unspecified vulnerability in Powershell Operations in HP Operations Orchestration 9.x and 10.x allows remote authenticated users to obtain sensitive information via unknown vectors.
CVE-2015-2109
Unspecified vulnerability in HP Operations Orchestration 10.x allows remote attackers to bypass authentication, and obtain sensitive information or modify data, via unknown vectors.
CVE-2015-2684
Shibboleth Service Provider (SP) before 2.5.4 allows remote authenticated users to cause a denial of service (crash) via a crafted SAML message.
CVE-2015-2753
FreeXL before 1.0.0i allows remote attackers to cause a denial of service (stack corruption) or possibly execute arbitrary code via a crafted sector in a workbook.
CVE-2015-2754
FreeXL before 1.0.0i allows remote attackers to cause a denial of service (stack corruption) and possibly execute arbitrary code via a crafted workbook, related to a "premature EOF."
CVE-2015-2776
The parse_SST function in FreeXL before 1.0.0i allows remote attackers to cause a denial of service (memory consumption) via a crafted shared strings table in a workbook.
