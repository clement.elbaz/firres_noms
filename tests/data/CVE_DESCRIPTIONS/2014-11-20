CVE-2014-2382
The DfDiskLo.sys driver in Faronics Deep Freeze Standard and Enterprise 8.10 and earlier allows local administrators to cause a denial of service (crash) and execute arbitrary code via a crafted IOCTL request that writes to arbitrary memory locations, related to the IofCallDriver function.
CVE-2014-3625
Directory traversal vulnerability in Pivotal Spring Framework 3.0.4 through 3.2.x before 3.2.12, 4.0.x before 4.0.8, and 4.1.x before 4.1.2 allows remote attackers to read arbitrary files via unspecified vectors, related to static resource handling.
CVE-2014-8387
cgi/utility.cgi in Advantech EKI-6340 2.05 Wi-Fi Mesh Access Point allows remote authenticated users to execute arbitrary commands via shell metacharacters in the pinghost parameter to ping.cgi.
CVE-2014-8493
ZTE ZXHN H108L with firmware 4.0.0d_ZRQ_GR4 allows remote attackers to modify the CWMP configuration via a crafted request to Forms/access_cwmp_1.
CVE-2014-8767
Integer underflow in the olsr_print function in tcpdump 3.9.6 through 4.6.2, when in verbose mode, allows remote attackers to cause a denial of service (crash) via a crafted length value in an OLSR frame.
CVE-2014-8768
Multiple Integer underflows in the geonet_print function in tcpdump 4.5.0 through 4.6.2, when in verbose mode, allow remote attackers to cause a denial of service (segmentation fault and crash) via a crafted length value in a Geonet frame.
CVE-2014-8769
tcpdump 3.8 through 4.6.2 might allow remote attackers to obtain sensitive information from memory or cause a denial of service (packet loss or segmentation fault) via a crafted Ad hoc On-Demand Distance Vector (AODV) packet, which triggers an out-of-bounds memory access.
CVE-2014-8995
SQL injection vulnerability in Maarch LetterBox 2.8 allows remote attackers to execute arbitrary SQL commands via the UserId cookie.
CVE-2014-8996
Multiple cross-site scripting (XSS) vulnerabilities in Nibbleblog before 4.0.2 allow remote attackers to inject arbitrary web script or HTML via the (1) author_name or (2) content parameter to index.php.
CVE-2014-8997
Unrestricted file upload vulnerability in the Photo functionality in DigitalVidhya Digi Online Examination System 2.0 allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in assets/uploads/images/.
CVE-2014-8998
lib/message.php in X7 Chat 2.0.0 through 2.0.5.1 allows remote authenticated users to execute arbitrary PHP code via a crafted HTTP header to index.php, which is processed by the preg_replace function with the eval switch.
CVE-2014-8999
SQL injection vulnerability in htdocs/modules/system/admin.php in XOOPS before 2.5.7 Final allows remote authenticated users to execute arbitrary SQL commands via the selgroups parameter.
CVE-2014-9000
Mule Enterprise Management Console (MMC) does not properly restrict access to handler/securityService.rpc, which allows remote authenticated users to gain administrator privileges and execute arbitrary code via a crafted request that adds a new user.  NOTE: this issue was originally reported for ESB Runtime 3.5.1, but it originates in MMC.
CVE-2014-9001
reminders/index.php in Incredible PBX 11 2.0.6.5.0 allows remote authenticated users to execute arbitrary commands via shell metacharacters in the (1) APPTMIN, (2) APPTHR, (3) APPTDA, (4) APPTMO, (5) APPTYR, or (6) APPTPHONE parameters.
CVE-2014-9002
Lantronix xPrintServer does not properly restrict access to ips/, which allows remote attackers to execute arbitrary commands via the c parameter in an rpc action.
CVE-2014-9003
Cross-site request forgery (CSRF) vulnerability in Lantronix xPrintServer allows remote attackers to hijack the authentication of administrators for requests that modify configuration, as demonstrated by executing arbitrary commands using the c parameter in the rpc action.
CVE-2014-9004
Cross-site scripting (XSS) vulnerability in vldPersonals before 2.7.1 allows remote attackers to inject arbitrary web script or HTML via the id parameter in a member_profile action to index.php.
CVE-2014-9005
Multiple SQL injection vulnerabilities in vldPersonals before 2.7.1 allow remote attackers to execute arbitrary SQL commands via the (1) country, (2) gender1, or ((3) gender2 parameter in a search action to index.php.
CVE-2014-9006
Monstra 3.0.1 and earlier uses a cookie to track how many login attempts have been attempted, which allows remote attackers to conduct brute force login attacks by deleting the login_attempts cookie or setting it to certain values.
CVE-2014-9019
Multiple cross-site request forgery (CSRF) vulnerabilities in ZTE ZXDSL 831CII allow remote attackers to hijack the authentication of administrators for requests that (1) change the admin user name or (2) conduct cross-site scripting (XSS) attacks via the sysUserName parameter in a save action to adminpasswd.cgi or (3) change the admin user password via the sysPassword parameter in a save action to adminpasswd.cgi.
CVE-2014-9020
Cross-site scripting (XSS) vulnerability in the Quick Stats page (psilan.cgi) in ZTE ZXDSL 831 and 831CII allows remote attackers to inject arbitrary web script or HTML via the domainname parameter in a save action.  NOTE: this issue was SPLIT from CVE-2014-9021 per ADT1 due to different affected products and codebases.
CVE-2014-9021
Multiple cross-site scripting (XSS) vulnerabilities in ZTE ZXDSL 831 allow remote attackers to inject arbitrary web script or HTML via the (1) tr69cAcsURL, (2) tr69cAcsUser, (3) tr69cAcsPwd, (4) tr69cConnReqPwd, or (5) tr69cDebugEnable parameter to the TR-069 client page (tr69cfg.cgi); the (6) timezone parameter to the Time and date page (sntpcfg.sntp); or the (7) hostname parameter in a save action to the Quick Stats page (psilan.cgi).  NOTE: this issue was SPLIT from CVE-2014-9020 per ADT1 due to different affected products and codebases.
CVE-2014-9022
The Webform Component Roles module 6.x-1.x before 6.x-1.8 and 7.x-1.x before 7.x-1.8 for Drupal allows remote attackers to bypass the "disabled" restriction and modify read-only components via a crafted form.
CVE-2014-9023
The Twilio module 7.x-1.x before 7.x-1.9 for Drupal does not properly restrict access to the Twilio administration pages, which allows remote authenticated users to read and modify authentication tokens by leveraging the "access administration pages" Drupal permission.
CVE-2014-9024
The Protected Pages module 7.x-2.x before 7.x-2.4 for Drupal allows remote attackers to bypass the password protection via a crafted path.
CVE-2014-9025
The default checkout completion rule in the commerce_order module in the Drupal Commerce module 7.x-1.x before 7.x-1.10 for Drupal uses the email address as the username for new accounts created at checkout, which allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2014-9026
The Ubercart module 7.x-3.x before 7.x-3.7 for Drupal does not properly protect the per-user order history view, which allows remote authenticated users with the "view own orders" permission to obtain sensitive information via unspecified vectors.
CVE-2014-9027
Multiple cross-site request forgery (CSRF) vulnerabilities in ZTE ZXDSL 831CII allow remote attackers to hijack the authentication of administrators for requests that disable modem lan ports via the (1) enblftp, (2) enblhttp, (3) enblsnmp, (4) enbltelnet, (5) enbltftp, (6) enblicmp, or (7) enblssh parameter to accesslocal.cmd.
