CVE-2010-4728
Zikula before 1.3.1 uses the rand and srand PHP functions for random number generation, which makes it easier for remote attackers to defeat protection mechanisms based on randomization by predicting a return value, as demonstrated by the authid protection mechanism.
CVE-2010-4729
Zikula before 1.2.3 does not use the authid protection mechanism for (1) the lostpassword form and (2) mailpasswd processing, which makes it easier for remote attackers to generate a flood of password requests and possibly conduct cross-site request forgery (CSRF) attacks via multiple form submissions.
CVE-2011-0526
Cross-site scripting (XSS) vulnerability in index.php in Vanilla Forums before 2.0.17 allows remote attackers to inject arbitrary web script or HTML via the Target parameter in a /entry/signin action.
CVE-2011-0535
Cross-site request forgery (CSRF) vulnerability in the Users module in Zikula before 1.2.5 allows remote attackers to hijack the authentication of administrators for requests that change account privileges via an edit access_permissions action to index.php.
CVE-2011-0538
Wireshark 1.2.0 through 1.2.14, 1.4.0 through 1.4.3, and 1.5.0 frees an uninitialized pointer during processing of a .pcap file in the pcap-ng format, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via a malformed file.
CVE-2011-0885
A certain Comcast Business Gateway configuration of the SMC SMCD3G-CCR with firmware before 1.4.0.49.2 has a default password of D0nt4g3tme for the mso account, which makes it easier for remote attackers to obtain administrative access via the (1) web interface or (2) TELNET interface.
CVE-2011-0886
Multiple cross-site request forgery (CSRF) vulnerabilities in the web interface on the SMC SMCD3G-CCR (aka Comcast Business Gateway) with firmware before 1.4.0.49.2 allow remote attackers to (1) hijack the intranet connectivity of arbitrary users for requests that perform a login via goform/login, or hijack the authentication of administrators for requests that (2) enable external logins via an mso_remote_enable action to goform/RemoteRange or (3) change DNS settings via a manual_dns_enable action to goform/Basic.
CVE-2011-0887
The web management portal on the SMC SMCD3G-CCR (aka Comcast Business Gateway) with firmware before 1.4.0.49.2 uses predictable session IDs based on time values, which makes it easier for remote attackers to hijack sessions via a brute-force attack on the userid cookie.
CVE-2011-0908
Open redirect vulnerability in Vanilla Forums before 2.0.17.6 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the Target parameter to an unspecified component, a different vulnerability than CVE-2011-0526.
CVE-2011-0909
Cross-site scripting (XSS) vulnerability in Vanilla Forums before 2.0.17.6 allows remote attackers to inject arbitrary web script or HTML via the p parameter to an unspecified component, a different vulnerability than CVE-2011-0526.
CVE-2011-0910
The cookie implementation in Vanilla Forums before 2.0.17.6 makes it easier for remote attackers to spoof signed requests, and consequently obtain access to arbitrary user accounts, via HMAC timing attacks.
CVE-2011-0911
Cross-site scripting (XSS) vulnerability in the Users module in Zikula before 1.2.5 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: it is possible that this overlaps CVE-2011-0535.
CVE-2011-0912
Argument injection vulnerability in IBM Lotus Notes 8.0.x before 8.0.2 FP6 and 8.5.x before 8.5.1 FP5 allows remote attackers to execute arbitrary code via a cai:// URL containing a --launcher.library option that specifies a UNC share pathname for a DLL file, aka SPR PRAD82YJW2.
CVE-2011-0913
Stack-based buffer overflow in ndiiop.exe in the DIIOP implementation in the server in IBM Lotus Domino before 8.5.3 allows remote attackers to execute arbitrary code via a GIOP getEnvironmentString request, related to the local variable cache.
CVE-2011-0914
Integer signedness error in ndiiop.exe in the DIIOP implementation in the server in IBM Lotus Domino before 8.5.3 allows remote attackers to execute arbitrary code via a GIOP client request, leading to a heap-based buffer overflow.
CVE-2011-0915
Stack-based buffer overflow in nrouter.exe in IBM Lotus Domino before 8.5.3 allows remote attackers to execute arbitrary code via a long name parameter in a Content-Type header in a malformed Notes calendar (aka iCalendar or iCal) meeting request, aka SPR KLYH87LL23.
CVE-2011-0916
Stack-based buffer overflow in the SMTP service in IBM Lotus Domino allows remote attackers to execute arbitrary code via long arguments in a filename parameter in a malformed MIME e-mail message, aka SPR KLYH889M8H.
CVE-2011-0917
Buffer overflow in nLDAP.exe in IBM Lotus Domino allows remote attackers to execute arbitrary code via a long string in an LDAP Bind operation, aka SPR KLYH87LMVX.
CVE-2011-0918
Stack-based buffer overflow in the NRouter (aka Router) service in IBM Lotus Domino allows remote attackers to execute arbitrary code via long filenames associated with Content-ID and ATTACH:CID headers in attachments in malformed calendar-request e-mail messages, aka SPR KLYH87LKRE.
CVE-2011-0919
Multiple stack-based buffer overflows in the (1) POP3 and (2) IMAP services in IBM Lotus Domino allow remote attackers to execute arbitrary code via non-printable characters in an envelope sender address, aka SPR KLYH87LLVJ.
CVE-2011-0920
The Remote Console in IBM Lotus Domino, when a certain unsupported configuration involving UNC share pathnames is used, allows remote attackers to bypass authentication and execute arbitrary code via unspecified vectors, aka SPR PRAD89WGRS.
