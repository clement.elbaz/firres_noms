CVE-2012-4234
Cross-site scripting (XSS) vulnerability in the group moderation screen in the control center (control.php) in Phorum before 5.2.19 allows remote attackers to inject arbitrary web script or HTML via the group parameter.
CVE-2012-4768
Cross-site scripting (XSS) vulnerability in the Download Monitor plugin before 3.3.5.9 for WordPress allows remote attackers to inject arbitrary web script or HTML via the dlsearch parameter to the default URI.
CVE-2012-6153
http/conn/ssl/AbstractVerifier.java in Apache Commons HttpClient before 4.2.3 does not properly verify that the server hostname matches a domain name in the subject's Common Name (CN) or subjectAltName field of the X.509 certificate, which allows man-in-the-middle attackers to spoof SSL servers via a certificate with a subject that specifies a common name in a field that is not the CN field.  NOTE: this issue exists because of an incomplete fix for CVE-2012-5783.
CVE-2014-2685
The GenericConsumer class in the Consumer component in ZendOpenId before 2.0.2 and the Zend_OpenId_Consumer class in Zend Framework 1 before 1.12.4 violate the OpenID 2.0 protocol by ensuring only that at least one field is signed, which allows remote attackers to bypass authentication by leveraging an assertion from an OpenID provider.
CVE-2014-2957
The dmarc_process function in dmarc.c in Exim before 4.82.1, when EXPERIMENTAL_DMARC is enabled, allows remote attackers to execute arbitrary code via the From header in an email, which is passed to the expand_string function.
CVE-2014-2972
expand.c in Exim before 4.83 expands mathematical comparisons twice, which allows local users to gain privileges and execute arbitrary commands via a crafted lookup value.
CVE-2014-3075
Cross-site scripting (XSS) vulnerability in IBM Business Process Manager (BPM) 7.5.x through 8.5.5 and WebSphere Lombardi Edition 7.2.0.x allows remote authenticated users to inject arbitrary web script or HTML via an uploaded file.
CVE-2014-3094
Stack-based buffer overflow in IBM DB2 9.7 through FP9a, 9.8 through FP5, 10.1 through FP4, and 10.5 before FP4 on Linux, UNIX, and Windows allows remote authenticated users to execute arbitrary code via a crafted ALTER MODULE statement.
CVE-2014-3095
The SQL engine in IBM DB2 9.5 through FP10, 9.7 through FP9a, 9.8 through FP5, 10.1 through FP4, and 10.5 before FP4 on Linux, UNIX, and Windows allows remote authenticated users to cause a denial of service (daemon crash) via a crafted UNION clause in a subquery of a SELECT statement.
CVE-2014-3353
Cisco IOS XR 4.3(.2) and earlier, as used in Cisco Carrier Routing System (CRS), allows remote attackers to cause a denial of service (CPU consumption and IPv6 packet drops) via a malformed IPv6 packet, aka Bug ID CSCuo95165.
CVE-2014-3529
The OPC SAX setup in Apache POI before 3.10.1 allows remote attackers to read arbitrary files via an OpenXML file containing an XML external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-3574
Apache POI before 3.10.1 and 3.11.x before 3.11-beta2 allows remote attackers to cause a denial of service (CPU consumption and crash) via a crafted OOXML file, aka an XML Entity Expansion (XEE) attack.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-4758
IBM Business Process Manager (BPM) 7.5.x through 8.5.5 and WebSphere Lombardi Edition 7.2.x allow remote authenticated users to bypass intended access restrictions and send requests to internal services via a callService URL.
CVE-2014-4759
An unspecified Ajax service in the Content Management toolkit in IBM Business Process Manager (BPM) 8.5.x through 8.5.5 allows remote authenticated users to obtain sensitive information by performing a document-attachment search and then reading document properties in the search results.
CVE-2014-4805
IBM DB2 10.5 before FP4 on Linux and AIX creates temporary files during CDE table LOAD operations, which allows local users to obtain sensitive information by reading a file while a LOAD is occurring.
CVE-2014-5269
Plack::App::File in Plack before 1.0031 removes trailing slash characters from paths, which allows remote attackers to bypass the whitelist of generated files and obtain sensitive information via a crafted path, related to Plack::Middleware::Static.
CVE-2014-5285
Unspecified vulnerability in the Authentication Module in TIBCO Spotfire Server before 4.5.2, 5.0.x before 5.0.3, 5.5.x before 5.5.2, 6.0.x before 6.0.3, and 6.5.x before 6.5.1 allows remote attackers to gain privileges, and obtain sensitive information or modify data, via unknown vectors.
CVE-2014-5377
ReadUsersFromMasterServlet in ManageEngine DeviceExpert before 5.9 build 5981 allows remote attackers to obtain user account credentials via a direct request.
CVE-2014-5461
Buffer overflow in the vararg functions in ldo.c in Lua 5.1 through 5.2.x before 5.2.3 allows context-dependent attackers to cause a denial of service (crash) via a small number of arguments to a function with a large number of fixed arguments.
CVE-2014-5504
SolarWinds Log and Event Manager before 6.0 uses "static" credentials, which makes it easier for remote attackers to obtain access to the database and execute arbitrary code via unspecified vectors, related to HyperSQL.
CVE-2014-5505
Stack-based buffer overflow in SAP Crystal Reports allows remote attackers to execute arbitrary code via a crafted data source string in an RPT file.
CVE-2014-5506
Double free vulnerability in SAP Crystal Reports allows remote attackers to execute arbitrary code via crafted connection string record in an RPT file.
<a href="http://cwe.mitre.org/data/definitions/415.html" target="_blank">CWE-415: Double Free</a>
CVE-2014-6060
The get_option function in dhcpcd 4.0.0 through 6.x before 6.4.3 allows remote DHCP servers to cause a denial of service by resetting the DHO_OPTIONSOVERLOADED option in the (1) bootfile or (2) servername section, which triggers the option to be processed again.
