CVE-2017-12630
In Apache Drill 1.11.0 and earlier when submitting form from Query page users are able to pass arbitrary script or HTML which will take effect on Profile page afterwards. Example: after submitting special script that returns cookie information from Query page, malicious user may obtain this information from Profile page afterwards.
CVE-2017-14583
NetApp Clustered Data ONTAP versions 9.x prior to 9.1P10 and 9.2P2 are susceptible to a vulnerability which allows an attacker to cause a Denial of Service (DoS) in SMB environments.
CVE-2017-15103
A security-check flaw was found in the way the Heketi 5 server API handled user requests. An authenticated Heketi user could send specially crafted requests to the Heketi server, resulting in remote command execution as the user running Heketi server and possibly privilege escalation.
CVE-2017-15104
An access flaw was found in Heketi 5, where the heketi.json configuration file was world readable. An attacker having local access to the Heketi server could read plain-text passwords from the heketi.json file.
CVE-2017-15700
A flaw in the org.apache.sling.auth.core.AuthUtil#isRedirectValid method in Apache Sling Authentication Service 1.4.0 allows an attacker, through the Sling login form, to trick a victim to send over their credentials.
CVE-2017-16997
elf/dl-load.c in the GNU C Library (aka glibc or libc6) 2.19 through 2.26 mishandles RPATH and RUNPATH containing $ORIGIN for a privileged (setuid or AT_SECURE) program, which allows local users to gain privileges via a Trojan horse library in the current working directory, related to the fillin_rpath and decompose_rpath functions. This is associated with misinterpretion of an empty RPATH/RUNPATH token as the "./" directory. NOTE: this configuration of RPATH/RUNPATH for a privileged program is apparently very uncommon; most likely, no such program is shipped with any common Linux distribution.
CVE-2017-17643
FS Lynda Clone 1.0 has SQL Injection via the keywords parameter to tutorial/.
CVE-2017-17645
Bus Booking Script 1.0 has SQL Injection via the txtname parameter to admin/index.php.
CVE-2017-17649
Readymade Video Sharing Script 3.2 has HTML Injection via the single-video-detail.php comment parameter.
CVE-2017-17651
Paid To Read Script 2.0.5 has SQL Injection via the admin/userview.php uid parameter, the admin/viewemcamp.php fnum parameter, or the admin/viewvisitcamp.php fn parameter.
CVE-2017-17721
CWEBNET/WOSummary/List in ZUUSE BEIMS ContractorWeb .NET 5.18.0.0 allows SQL injection via the tradestatus, assetno, assignto, building, domain, jobtype, site, trade, woType, workorderno, or workorderstatus parameter.
CVE-2017-17727
DedeCMS through 5.6 allows arbitrary file upload and PHP code execution by embedding the PHP code in a .jpg file, which is used in the templet parameter to member/article_edit.php.
CVE-2017-17730
DedeCMS through 5.7 has SQL Injection via the logo parameter to plus/flink_add.php.
CVE-2017-17731
DedeCMS through 5.7 has SQL Injection via the $_FILES superglobal to plus/recommend.php.
CVE-2017-17733
Maccms 8.x allows remote command execution via the wd parameter in an index.php?m=vod-search request.
CVE-2017-17734
CMS Made Simple (CMSMS) before 2.2.5 does not properly cache login information in sessions.
CVE-2017-17735
CMS Made Simple (CMSMS) before 2.2.5 does not properly cache login information in cookies.
CVE-2017-17737
The BrightSign Digital Signage (4k242) device (Firmware 6.2.63 and below) has XSS via the REF parameter to /network_diagnostics.html or /storage_info.html.
CVE-2017-17738
The BrightSign Digital Signage (4k242) device (Firmware 6.2.63 and below) allows renaming and modifying files via /tools.html.
CVE-2017-17739
The BrightSign Digital Signage (4k242) device (Firmware 6.2.63 and below) has directory traversal via the /storage.html rp parameter, allowing an attacker to read or write to files.
CVE-2017-17740
contrib/slapd-modules/nops/nops.c in OpenLDAP through 2.4.45, when both the nops module and the memberof overlay are enabled, attempts to free a buffer that was allocated on the stack, which allows remote attackers to cause a denial of service (slapd crash) via a member MODDN operation.
CVE-2017-17741
The KVM implementation in the Linux kernel through 4.14.7 allows attackers to obtain potentially sensitive information from kernel memory, aka a write_mmio stack-based out-of-bounds read, related to arch/x86/kvm/x86.c and include/trace/events/kvm.h.
