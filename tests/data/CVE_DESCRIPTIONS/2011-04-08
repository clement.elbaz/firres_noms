CVE-2011-0465
xrdb.c in xrdb before 1.0.9 in X.Org X11R7.6 and earlier allows remote attackers to execute arbitrary commands via shell metacharacters in a hostname obtained from a (1) DHCP or (2) XDMCP message.
CVE-2011-0536
Multiple untrusted search path vulnerabilities in elf/dl-object.c in certain modified versions of the GNU C Library (aka glibc or libc6), including glibc-2.5-49.el5_5.6 and glibc-2.12-1.7.el6_0.3 in Red Hat Enterprise Linux, allow local users to gain privileges via a crafted dynamic shared object (DSO) in a subdirectory of the current working directory during execution of a (1) setuid or (2) setgid program that has $ORIGIN in (a) RPATH or (b) RUNPATH within the program itself or a referenced library. NOTE: this issue exists because of an incorrect fix for CVE-2010-3847.
Per: http://cwe.mitre.org/data/definitions/426.html 
'CWE-426: Untrusted Search Path'
CVE-2011-0997
dhclient in ISC DHCP 3.0.x through 4.2.x before 4.2.1-P1, 3.1-ESV before 3.1-ESV-R1, and 4.1-ESV before 4.1-ESV-R2 allows remote attackers to execute arbitrary commands via shell metacharacters in a hostname obtained from a DHCP message, as demonstrated by a hostname that is provided to dhclient-script.
CVE-2011-1071
The GNU C Library (aka glibc or libc6) before 2.12.2 and Embedded GLIBC (EGLIBC) allow context-dependent attackers to execute arbitrary code or cause a denial of service (memory consumption) via a long UTF8 string that is used in an fnmatch call, aka a "stack extension attack," a related issue to CVE-2010-2898, CVE-2010-1917, and CVE-2007-4782, as originally reported for use of this library by Google Chrome.
CVE-2011-1183
Apache Tomcat 7.0.11, when web.xml has no login configuration, does not follow security constraints, which allows remote attackers to bypass intended access restrictions via HTTP requests to a meta-data complete web application.  NOTE: this vulnerability exists because of an incorrect fix for CVE-2011-1088 and CVE-2011-1419.
CVE-2011-1475
The HTTP BIO connector in Apache Tomcat 7.0.x before 7.0.12 does not properly handle HTTP pipelining, which allows remote attackers to read responses intended for other clients in opportunistic circumstances by examining the application data in HTTP packets, related to "a mix-up of responses for requests from different users."
CVE-2011-1491
The login form in Roundcube Webmail before 0.5.1 does not properly handle a correctly authenticated but unintended login attempt, which makes it easier for remote authenticated users to obtain sensitive information by arranging for a victim to login to the attacker's account and then compose an e-mail message, related to a "login CSRF" issue.
CVE-2011-1492
steps/utils/modcss.inc in Roundcube Webmail before 0.5.1 does not properly verify that a request is an expected request for an external Cascading Style Sheets (CSS) stylesheet, which allows remote authenticated users to trigger arbitrary outbound TCP connections from the server, and possibly obtain sensitive information, via a crafted request.
CVE-2011-1658
ld.so in the GNU C Library (aka glibc or libc6) 2.13 and earlier expands the $ORIGIN dynamic string token when RPATH is composed entirely of this token, which might allow local users to gain privileges by creating a hard link in an arbitrary directory to a (1) setuid or (2) setgid program with this RPATH value, and then executing the program with a crafted value for the LD_PRELOAD environment variable, a different vulnerability than CVE-2010-3847 and CVE-2011-0536.  NOTE: it is not expected that any standard operating-system distribution would ship an applicable setuid or setgid program.
CVE-2011-1659
Integer overflow in posix/fnmatch.c in the GNU C Library (aka glibc or libc6) 2.13 and earlier allows context-dependent attackers to cause a denial of service (application crash) via a long UTF8 string that is used in an fnmatch call with a crafted pattern argument, a different vulnerability than CVE-2011-1071.
