CVE-2008-3547
Buffer overflow in the server in OpenTTD 0.6.1 and earlier allows remote authenticated users to cause a denial of service (persistent game disruption) or possibly execute arbitrary code via vectors involving many long names for "companies and clients."
CVE-2009-0081
The graphics device interface (GDI) implementation in the kernel in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 does not properly validate input received from user mode, which allows remote attackers to execute arbitrary code via a crafted (1) Windows Metafile (aka WMF) or (2) Enhanced Metafile (aka EMF) image file, aka "Windows Kernel Input Validation Vulnerability."
CVE-2009-0082
The kernel in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 does not properly validate handles, which allows local users to gain privileges via a crafted application that triggers unspecified "actions," aka "Windows Kernel Handle Validation Vulnerability."
CVE-2009-0083
The kernel in Microsoft Windows 2000 SP4, XP SP2 and SP3, and Server 2003 SP1 does not properly handle invalid pointers, which allows local users to gain privileges via an application that triggers use of a crafted pointer, aka "Windows Kernel Invalid Pointer Vulnerability."
CVE-2009-0085
The Secure Channel (aka SChannel) authentication component in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008, when certificate authentication is used, does not properly validate the client's key exchange data in Transport Layer Security (TLS) handshake messages, which allows remote attackers to spoof authentication by crafting a TLS packet based on knowledge of the certificate but not the private key, aka "SChannel Spoofing Vulnerability."
CVE-2009-0191
Foxit Reader 2.3 before Build 3902 and 3.0 before Build 1506, including 3.0.2009.1301, does not properly handle a JBIG2 symbol dictionary segment with zero new symbols, which allows remote attackers to execute arbitrary code via a crafted PDF file that triggers a dereference of an uninitialized memory location.
CVE-2009-0836
Foxit Reader 2.3 before Build 3902 and 3.0 before Build 1506, including 1120 and 1301, does not require user confirmation before performing dangerous actions defined in a PDF file, which allows remote attackers to execute arbitrary programs and have unspecified other impact via a crafted file, as demonstrated by the "Open/Execute a file" action.
CVE-2009-0837
Stack-based buffer overflow in Foxit Reader 3.0 before Build 1506, including 1120 and 1301, allows remote attackers to execute arbitrary code via a long (1) relative path or (2) absolute path in the filename argument in an action, as demonstrated by the "Open/Execute a file" action.
CVE-2009-0860
Cross-site scripting (XSS) vulnerability in the web user interface in the login application in NetMRI 3.0.1 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, related to error pages.
CVE-2009-0861
Cross-site scripting (XSS) vulnerability in phpDenora before 1.2.3 allows remote attackers to inject arbitrary web script or HTML via an IRC channel name.  NOTE: some of these details are obtained from third party information.
CVE-2009-0862
Cross-site scripting (XSS) vulnerability in the hook_cntrlr_error_output function in modules/page/hooks/listeners.php in the admincp component in TangoCMS 2.2.x (aka Eagle) before 2.2.4 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2009-0863
SQL injection vulnerability in admin/delete_page.php in S-Cms 1.1 Stable allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-0864
S-Cms 1.1 Stable allows remote attackers to bypass authentication and obtain administrative access via an OK value for the login cookie.
CVE-2009-0865
Directory traversal vulnerability in the SnapShotToFile method in the GeoVision LiveX (aka LiveX_v8200) ActiveX control 8.1.2 and 8.2.0 in LIVEX_~1.OCX allows remote attackers to create or overwrite arbitrary files via a .. (dot dot) in the argument, possibly involving the PlayX and SnapShotX methods.
CVE-2009-0866
pHNews Alpha 1 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for extra/genbackup.php.
CVE-2009-0867
The HRM-S service in Fujitsu Enhanced Support Facility 3.0 and 3.0.1 allows remote attackers to obtain (1) hardware and (2) software information via unspecified requests in a client connection.
Per: http://www.fujitsu.com/global/support/software/security/products-f/esf-200901e.html

For the Patches, please contact a Fujitsu system engineer or your partner(s).
CVE-2009-0868
CRLF injection vulnerability in the WebLink template in Fujitsu Jasmine2000 Enterprise Edition allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via unspecified vectors.
CVE-2009-0869
Buffer overflow in the client in IBM Tivoli Storage Manager (TSM) HSM 5.3.2.0 through 5.3.5.0, 5.4.0.0 through 5.4.2.5, and 5.5.0.0 through 5.5.1.4 on Windows allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via unspecified vectors.
CVE-2009-0870
The NFSv4 Server module in the kernel in Sun Solaris 10, and OpenSolaris before snv_111, allow local users to cause a denial of service (infinite loop and system hang) by accessing an hsfs filesystem that is shared through NFSv4, related to the rfs4_op_readdir function.
