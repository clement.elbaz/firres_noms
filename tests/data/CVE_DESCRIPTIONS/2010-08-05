CVE-2009-2696
Cross-site scripting (XSS) vulnerability in jsp/cal/cal2.jsp in the calendar application in the examples web application in Apache Tomcat on Red Hat Enterprise Linux 5, Desktop Workstation 5, and Linux Desktop 5 allows remote attackers to inject arbitrary web script or HTML via the time parameter, related to "invalid HTML." NOTE: this is due to a missing fix for CVE-2009-0781.
CVE-2010-1871
JBoss Seam 2 (jboss-seam2), as used in JBoss Enterprise Application Platform 4.3.0 for Red Hat Linux, does not properly sanitize inputs for JBoss Expression Language (EL) expressions, which allows remote attackers to execute arbitrary code via a crafted URL.  NOTE: this is only a vulnerability when the Java Security Manager is not properly configured.
CVE-2010-2487
Multiple cross-site scripting (XSS) vulnerabilities in MoinMoin 1.7.3 and earlier, 1.8.x before 1.8.8, and 1.9.x before 1.9.3 allow remote attackers to inject arbitrary web script or HTML via crafted content, related to (1) Page.py, (2) PageEditor.py, (3) PageGraphicalEditor.py, (4) action/CopyPage.py, (5) action/Load.py, (6) action/RenamePage.py, (7) action/backup.py, (8) action/login.py, (9) action/newaccount.py, and (10) action/recoverpass.py.
CVE-2010-2526
The cluster logical volume manager daemon (clvmd) in lvm2-cluster in LVM2 before 2.02.72, as used in Red Hat Global File System (GFS) and other products, does not verify client credentials upon a socket connection, which allows local users to cause a denial of service (daemon exit or logical-volume change) or possibly have unspecified other impact via crafted control commands.
CVE-2010-2546
Multiple heap-based buffer overflows in loaders/load_it.c in libmikmod, possibly 3.1.12, might allow remote attackers to execute arbitrary code via (1) crafted samples or (2) crafted instrument definitions in an Impulse Tracker file, related to panpts, pitpts, and IT_ProcessEnvelope.  NOTE: some of these details are obtained from third party information.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2009-3995.
CVE-2010-2547
Use-after-free vulnerability in kbx/keybox-blob.c in GPGSM in GnuPG 2.x through 2.0.16 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a certificate with a large number of Subject Alternate Names, which is not properly handled in a realloc operation when importing the certificate or verifying its signature.
Per: http://lists.gnupg.org/pipermail/gnupg-announce/2010q3/000302.html

'GnuPG 1.x is NOT affected because it does not come with the GPGSM
tool.'
CVE-2010-2709
Stack-based buffer overflow in webappmon.exe in HP OpenView Network Node Manager (OV NNM) 7.51 and 7.53 allows remote attackers to execute arbitrary code via a long OvJavaLocale value in a cookie.
CVE-2010-2713
The vte_sequence_handler_window_manipulation function in vteseq.c in libvte (aka libvte9) in VTE 0.25.1 and earlier, as used in gnome-terminal, does not properly handle escape sequences, which allows remote attackers to execute arbitrary commands or obtain potentially sensitive information via a (1) window title or (2) icon title sequence.  NOTE: this issue exists because of a CVE-2003-0070 regression.
Per: http://cwe.mitre.org/data/definitions/77.html

'CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')'
CVE-2010-2725
BarnOwl before 1.6.2 does not check the return code of calls to the (1) ZPending and (2) ZReceiveNotice functions in libzephyr, which allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unknown vectors.
CVE-2010-2790
Multiple cross-site scripting (XSS) vulnerabilities in the formatQuery function in frontends/php/include/classes/class.curl.php in Zabbix before 1.8.3rc1 allow remote attackers to inject arbitrary web script or HTML via the (1) filter_set, (2) show_details, (3) filter_rst, or (4) txt_select parameters to the triggers page (tr_status.php).  NOTE: some of these details are obtained from third party information.
CVE-2010-2791
mod_proxy in httpd in Apache HTTP Server 2.2.9, when running on Unix, does not close the backend connection if a timeout occurs when reading a response from a persistent connection, which allows remote attackers to obtain a potentially sensitive response intended for a different client in opportunistic circumstances via a normal HTTP request.  NOTE: this is the same issue as CVE-2010-2068, but for a different OS and set of affected versions.
CVE-2010-2795
phpCAS before 1.1.2 allows remote authenticated users to hijack sessions via a query string containing a crafted ticket value.
CVE-2010-2796
Cross-site scripting (XSS) vulnerability in phpCAS before 1.1.2, when proxy mode is enabled, allows remote attackers to inject arbitrary web script or HTML via a callback URL.
CVE-2010-2860
The EMC Celerra Network Attached Storage (NAS) appliance accepts external network traffic to IP addresses intended for an intranet network within the appliance, which allows remote attackers to read, create, or modify arbitrary files in the user data directory via NFS requests.
CVE-2010-2862
Integer overflow in CoolType.dll in Adobe Reader 8.2.3 and 9.3.3, and Acrobat 9.3.3, allows remote attackers to execute arbitrary code via a TrueType font with a large maxCompositePoints value in a Maximum Profile (maxp) table.
CVE-2010-2931
Stack-based buffer overflow in SigPlus Pro 3.74 ActiveX control allows remote attackers to execute arbitrary code via a long eighth argument (HexString) to the LCDWriteString method.
CVE-2010-2932
Buffer overflow in BarCodeWiz BarCode 3.29 ActiveX control (BarcodeWiz.dll) allows remote attackers to execute arbitrary code via a long argument to the LoadProperties method.
CVE-2010-2933
SQL injection vulnerability in AV Scripts AV Arcade 3 allows remote attackers to execute arbitrary SQL commands via the ava_code cookie to the "main page," related to index.php and the login task.
CVE-2010-2965
The WDB target agent debug service in Wind River VxWorks 6.x, 5.x, and earlier, as used on the Rockwell Automation 1756-ENBT series A with firmware 3.2.6 and 3.6.1 and other products, allows remote attackers to read or modify arbitrary memory locations, perform function calls, or manage tasks via requests to UDP port 17185, a related issue to CVE-2005-3804.
CVE-2010-2966
The INCLUDE_SECURITY functionality in Wind River VxWorks 6.x, 5.x, and earlier uses the LOGIN_USER_NAME and LOGIN_USER_PASSWORD (aka LOGIN_PASSWORD) parameters to create hardcoded credentials, which makes it easier for remote attackers to obtain access via a (1) telnet, (2) rlogin, or (3) FTP session.
CVE-2010-2967
The loginDefaultEncrypt algorithm in loginLib in Wind River VxWorks before 6.9 does not properly support a large set of distinct possible passwords, which makes it easier for remote attackers to obtain access via a (1) telnet, (2) rlogin, or (3) FTP session.
CVE-2010-2968
The FTP daemon in Wind River VxWorks does not close the TCP connection after a number of failed login attempts, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2010-2969
Multiple cross-site scripting (XSS) vulnerabilities in MoinMoin 1.7.3 and earlier, and 1.9.x before 1.9.3, allow remote attackers to inject arbitrary web script or HTML via crafted content, related to (1) action/LikePages.py, (2) action/chart.py, and (3) action/userprofile.py, a similar issue to CVE-2010-2487.
CVE-2010-2970
Multiple cross-site scripting (XSS) vulnerabilities in MoinMoin 1.9.x before 1.9.3 allow remote attackers to inject arbitrary web script or HTML via crafted content, related to (1) action/SlideShow.py, (2) action/anywikidraw.py, and (3) action/language_setup.py, a similar issue to CVE-2010-2487.
CVE-2010-2971
loaders/load_it.c in libmikmod, possibly 3.1.12, does not properly account for the larger size of name##env relative to name##tick and name##node, which allows remote attackers to trigger a buffer over-read and possibly have unspecified other impact via a crafted Impulse Tracker file, a related issue to CVE-2010-2546.  NOTE: this issue exists because of an incomplete fix for CVE-2009-3995.
CVE-2010-2972
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2010-1797.  Reason: This candidate is a duplicate of CVE-2010-1797.  Notes: All CVE users should reference CVE-2010-1797 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
Per: http://xforce.iss.net/xforce/xfdb/60856

'Platforms Affected:

    * Apple iPhone OS 4.0 iPodtouch
    * Apple iPhone OS 4.0
    * Apple iPhone OS 4.0.1 iPodtouch
    * Apple iPhone OS 4.0.1 '


Per: http://www.securityfocus.com/bid/42151/discuss

'versions 4.0.1 and prior are vulnerable.'

CVE-2010-2973
Integer overflow in IOSurface in Apple iOS before 4.0.2 on the iPhone and iPod touch, and before 3.2.2 on the iPad, allows local users to gain privileges via vectors involving IOSurface properties, as demonstrated by JailbreakMe.
Per: http://xforce.iss.net/xforce/xfdb/60856

'Platforms Affected:

    * Apple iPhone OS 4.0 iPodtouch
    * Apple iPhone OS 4.0
    * Apple iPhone OS 4.0.1 iPodtouch
    * Apple iPhone OS 4.0.1 '


Per: http://www.securityfocus.com/bid/42151/discuss

'versions 4.0.1 and prior are vulnerable.'
CVE-2010-2974
Stack-based buffer overflow in the IConfigurationAccess interface in the Invensys Wonderware Archestra ConfigurationAccessComponent ActiveX control in Wonderware Application Server (WAS) before 3.1 SP2 P01, as used in the Wonderware Archestra Integrated Development Environment (IDE) and the InFusion Integrated Engineering Environment (IEE), allows remote attackers to execute arbitrary code via the first argument to the UnsubscribeData method.
Per: http://www.kb.cert.org/vuls/id/703189

'According to Invensys, users that are using IAS 2.1 (all versions)-IDE, WAS 3.0 (all versions)-IDE, WAS 3.1 (all versions)-IDE, InFusion CE 2.0-IEE, InFusion FE 1.0 (all versions)-IEE, InFusion FE 2.0-IEE, InFusion SCADA 2.0–IEE should apply the vendor security update. This vulnerability is not present in Wonderware Application Server 3.1 Service Pack 2 Patch 01 (WAS 3.1 SP2 P01).'
