CVE-2014-9685
Multiple cross-site scripting (XSS) vulnerabilities in Vanilla Forums before 2.0.18.13 and 2.1.x before 2.1.1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-0819
The UITour::onPageEvent function in Mozilla Firefox before 36.0 does not ensure that an API call originates from a foreground tab, which allows remote attackers to conduct spoofing and clickjacking attacks by leveraging access to a UI Tour web site.
CVE-2015-0820
Mozilla Firefox before 36.0 does not properly restrict transitions of JavaScript objects from a non-extensible state to an extensible state, which allows remote attackers to bypass a Caja Compiler sandbox protection mechanism or a Secure EcmaScript sandbox protection mechanism via a crafted web site.
CVE-2015-0821
Mozilla Firefox before 36.0 allows user-assisted remote attackers to read arbitrary files or execute arbitrary JavaScript code with chrome privileges via a crafted web site that is accessed with unspecified mouse and keyboard actions.
CVE-2015-0822
The Form Autocompletion feature in Mozilla Firefox before 36.0, Firefox ESR 31.x before 31.5, and Thunderbird before 31.5 allows remote attackers to read arbitrary files via crafted JavaScript code.
CVE-2015-0823
Multiple use-after-free vulnerabilities in OpenType Sanitiser, as used in Mozilla Firefox before 36.0, might allow remote attackers to trigger problematic Developer Console information or possibly have unspecified other impact by leveraging incorrect macro expansion, related to the ots::ots_gasp_parse function.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-0824
The mozilla::layers::BufferTextureClient::AllocateForSurface function in Mozilla Firefox before 36.0 allows remote attackers to cause a denial of service (out-of-bounds write of zero values, and application crash) via vectors that trigger use of DrawTarget and the Cairo library for image drawing.
CVE-2015-0825
Stack-based buffer underflow in the mozilla::MP3FrameParser::ParseBuffer function in Mozilla Firefox before 36.0 allows remote attackers to obtain sensitive information from process memory via a malformed MP3 file that improperly interacts with memory allocation during playback.
CVE-2015-0826
The nsTransformedTextRun::SetCapitalization function in Mozilla Firefox before 36.0 allows remote attackers to execute arbitrary code or cause a denial of service (out-of-bounds read of heap memory) via a crafted Cascading Style Sheets (CSS) token sequence that triggers a restyle or reflow operation.
CVE-2015-0827
Heap-based buffer overflow in the mozilla::gfx::CopyRect function in Mozilla Firefox before 36.0, Firefox ESR 31.x before 31.5, and Thunderbird before 31.5 allows remote attackers to obtain sensitive information from uninitialized process memory via a malformed SVG graphic.
CVE-2015-0828
Double free vulnerability in the nsXMLHttpRequest::GetResponse function in Mozilla Firefox before 36.0, when a nonstandard memory allocator is used, allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via crafted JavaScript code that makes an XMLHttpRequest call with zero bytes of data.
<a href="http://cwe.mitre.org/data/definitions/415.html">CWE-415: Double Free</a>
CVE-2015-0829
Buffer overflow in libstagefright in Mozilla Firefox before 36.0 allows remote attackers to execute arbitrary code via a crafted MP4 video that is improperly handled during playback.
CVE-2015-0830
The WebGL implementation in Mozilla Firefox before 36.0 does not properly allocate memory for copying an unspecified string to a shader's compilation log, which allows remote attackers to cause a denial of service (application crash) via crafted WebGL content.
CVE-2015-0831
Use-after-free vulnerability in the mozilla::dom::IndexedDB::IDBObjectStore::CreateIndex function in Mozilla Firefox before 36.0, Firefox ESR 31.x before 31.5, and Thunderbird before 31.5 allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via crafted content that is improperly handled during IndexedDB index creation.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-0832
Mozilla Firefox before 36.0 does not properly recognize the equivalence of domain names with and without a trailing . (dot) character, which allows man-in-the-middle attackers to bypass the HPKP and HSTS protection mechanisms by constructing a URL with this character and leveraging access to an X.509 certificate for a domain with this character.
CVE-2015-0833
Multiple untrusted search path vulnerabilities in updater.exe in Mozilla Firefox before 36.0, Firefox ESR 31.x before 31.5, and Thunderbird before 31.5 on Windows, when the Maintenance Service is not used, allow local users to gain privileges via a Trojan horse DLL in (1) the current working directory or (2) a temporary directory, as demonstrated by bcrypt.dll.
<a href="http://cwe.mitre.org/data/definitions/426.html">CWE-426: Untrusted Search Path</a>
CVE-2015-0834
The WebRTC subsystem in Mozilla Firefox before 36.0 recognizes turns: and stuns: URIs but accesses the TURN or STUN server without using TLS, which makes it easier for man-in-the-middle attackers to discover credentials by spoofing a server and completing a brute-force attack within a short time window.
CVE-2015-0835
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 36.0 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-0836
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 36.0, Firefox ESR 31.x before 31.5, and Thunderbird before 31.5 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-2043
Multiple cross-site scripting (XSS) vulnerabilities in Visualware MyConnection Server 8.2b allow remote attackers to inject arbitrary web script or HTML via the (1) bt, (2) variable, or (3) et parameter to myspeed/db/historyitem.
CVE-2015-2082
Cross-site scripting (XSS) vulnerability in Login.aspx in UNIT4 Prosoft HRMS before 8.14.330.43 allows remote attackers to inject arbitrary web script or HTML via the txtUserID parameter.
CVE-2015-2083
Cross-site request forgery (CSRF) vulnerability in Ilch CMS allows remote attackers to hijack the authentication of administrators for requests that add a value to a profile field via a profilefields request to admin.php.
CVE-2015-2084
Cross-site request forgery (CSRF) vulnerability in the Easy Social Icons plugin before 1.2.3 for WordPress allows remote attackers to hijack the authentication of administrators for requests that conduct cross-site scripting (XSS) attacks via the image_file parameter in an edit action in the cnss_social_icon_add page to wp-admin/admin.php.
