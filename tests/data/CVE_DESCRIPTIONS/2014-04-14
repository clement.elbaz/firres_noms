CVE-2010-5298
Race condition in the ssl3_read_bytes function in s3_pkt.c in OpenSSL through 1.0.1g, when SSL_MODE_RELEASE_BUFFERS is enabled, allows remote attackers to inject data across sessions or cause a denial of service (use-after-free and parsing error) via an SSL connection in a multithreaded environment.
CVE-2014-0077
drivers/vhost/net.c in the Linux kernel before 3.13.10, when mergeable buffers are disabled, does not properly validate packet lengths, which allows guest OS users to cause a denial of service (memory corruption and host OS crash) or possibly gain privileges on the host OS via crafted packets, related to the handle_rx and get_rx_bufs functions.
CVE-2014-0128
Squid 3.1 before 3.3.12 and 3.4 before 3.4.4, when SSL-Bump is enabled, allows remote attackers to cause a denial of service (assertion failure) via a crafted range request, related to state management.
CVE-2014-0155
The ioapic_deliver function in virt/kvm/ioapic.c in the Linux kernel through 3.14.1 does not properly validate the kvm_irq_delivery_to_apic return value, which allows guest OS users to cause a denial of service (host OS crash) via a crafted entry in the redirection table of an I/O APIC.  NOTE: the affected code was moved to the ioapic_service function before the vulnerability was announced.
CVE-2014-0159
Buffer overflow in the GetStatistics64 remote procedure call (RPC) in OpenAFS 1.4.8 before 1.6.7 allows remote attackers to cause a denial of service (crash) via a crafted statsVersion argument.
CVE-2014-0612
Unspecified vulnerability in Juniper Junos before 11.4R10-S1, before 11.4R11, 12.1X44 before 12.1X44-D26, 12.1X44 before 12.1X44-D30, 12.1X45 before 12.1X45-D20, and 12.1X46 before 12.1X46-D10, when Dynamic IPsec VPN is configured, allows remote attackers to cause a denial of service (new Dynamic VPN connection failures and CPU and disk consumption) via unknown vectors.
CVE-2014-0614
Juniper Junos 13.2 before 13.2R3 and 13.3 before 13.3R1, when PIM is enabled, allows remote attackers to cause a denial of service (kernel panic and crash) via a large number of crafted IGMP packets.
CVE-2014-2706
Race condition in the mac80211 subsystem in the Linux kernel before 3.13.7 allows remote attackers to cause a denial of service (system crash) via network traffic that improperly interacts with the WLAN_STA_PS_STA state (aka power-save mode), related to sta_info.c and tx.c.
CVE-2014-2711
Cross-site scripting (XSS) vulnerability in J-Web in Juniper Junos before 11.4R11, 11.4X27 before 11.4X27.62 (BBE), 12.1 before 12.1R9, 12.1X44 before 12.1X44-D35, 12.1X45 before 12.1X45-D25, 12.1X46 before 12.1X46-D20, 12.2 before 12.2R7, 12.3 before 12.3R6, 13.1 before 13.1R4, 13.2 before 13.2R3, and 13.3 before 13.3R1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-2712
Cross-site scripting (XSS) vulnerability in J-Web in Juniper Junos before 10.0S25, 10.4 before 10.4R10, 11.4 before 11.4R11, 12.1 before 12.1R9, 12.1X44 before 12.1X44-D30, 12.1X45 before 12.1X45-D20, 12.1X46 before 12.1X46-D10, and 12.2 before 12.2R1 allows remote attackers to inject arbitrary web script or HTML via unspecified parameters to index.php.
CVE-2014-2713
Juniper Junos before 11.4R11, 12.1 before 12.1R9, 12.2 before 12.2R7, 12.3R4 before 12.3R4-S3, 13.1 before 13.1R4, 13.2 before 13.2R2, and 13.3 before 13.3R1, as used in MX Series and T4000 routers, allows remote attackers to cause a denial of service (PFE restart) via a crafted IP packet to certain (1) Trio or (2) Cassis-based Packet Forwarding Engine (PFE) modules.
CVE-2014-2714
The Enhanced Web Filtering (EWF) in Juniper Junos before 10.4R15, 11.4 before 11.4R9, 12.1 before 12.1R7, 12.1X44 before 12.1X44-D20, 12.1X45 before 12.1X45-D10, and 12.1X46 before 12.1X46-D10, as used in the SRX Series services gateways, allows remote attackers to cause a denial of service (flow daemon crash and restart) via a crafted URL.
CVE-2014-2739
The cma_req_handler function in drivers/infiniband/core/cma.c in the Linux kernel 3.14.x through 3.14.1 attempts to resolve an RDMA over Converged Ethernet (aka RoCE) address that is properly resolved within a different module, which allows remote attackers to cause a denial of service (incorrect pointer dereference and system crash) via crafted network traffic.
CVE-2014-2851
Integer overflow in the ping_init_sock function in net/ipv4/ping.c in the Linux kernel through 3.14.1 allows local users to cause a denial of service (use-after-free and system crash) or possibly gain privileges via a crafted application that leverages an improperly managed reference counter.
CVE-2014-2852
OpenAFS before 1.6.7 delays the listen thread when an RXS_CheckResponse fails, which allows remote attackers to cause a denial of service (performance degradation) via an invalid packet.
