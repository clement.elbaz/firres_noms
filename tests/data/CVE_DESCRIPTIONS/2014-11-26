CVE-2014-2037
Openswan 2.6.40 allows remote attackers to cause a denial of service (NULL pointer dereference and IKE daemon restart) via IKEv2 packets that lack expected payloads.  NOTE: this vulnerability exists because of an incomplete fix for CVE 2013-6466.
CVE-2014-6093
Cross-site scripting (XSS) vulnerability in IBM WebSphere Portal 7.0.x before 7.0.0.2 CF29, 8.0.x through 8.0.0.1 CF14, and 8.5.x before 8.5.0 CF02 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-6196
Cross-site scripting (XSS) vulnerability in IBM Web Experience Factory (WEF) 6.1.5 through 8.5.0.1, as used in WebSphere Dashboard Framework (WDF) and Lotus Widget Factory (LWF), allows remote attackers to inject arbitrary web script or HTML by leveraging a Dojo builder error in an unspecified WebSphere Portal configuration, leading to improper construction of a response page by an application.
CVE-2014-6609
The res_pjsip_pubsub module in Asterisk Open Source 12.x before 12.5.1 allows remote authenticated users to cause a denial of service (crash) via crafted headers in a SIP SUBSCRIBE request for an event package.
CVE-2014-6610
Asterisk Open Source 11.x before 11.12.1 and 12.x before 12.5.1 and Certified Asterisk 11.6 before 11.6-cert6, when using the res_fax_spandsp module, allows remote authenticated users to cause a denial of service (crash) via an out of call message, which is not properly handled in the ReceiveFax dialplan application.
CVE-2014-7141
The pinger in Squid 3.x before 3.4.8 allows remote attackers to obtain sensitive information or cause a denial of service (out-of-bounds read and crash) via a crafted type in an (1) ICMP or (2) ICMP6 packet.
CVE-2014-7142
The pinger in Squid 3.x before 3.4.8 allows remote attackers to obtain sensitive information or cause a denial of service (crash) via a crafted (1) ICMP or (2) ICMP6 packet size.
CVE-2014-7247
Unspecified vulnerability in JustSystems Ichitaro 2008 through 2011; Ichitaro Government 6, 7, 2008, 2009, and 2010; Ichitaro Pro; Ichitaro Pro 2; Ichitaro 2011 Sou; Ichitaro 2012 Shou; Ichitaro 2013 Gen; and Ichitaro 2014 Tetsu allows remote attackers to execute arbitrary code via a crafted file.
CVE-2014-8005
Race condition in the lighttpd module in Cisco IOS XR 5.1 and earlier on Network Convergence System 6000 devices allows remote attackers to cause a denial of service (process reload) by establishing many TCP sessions, aka Bug ID CSCuq45239.
CVE-2014-8419
Wibu-Systems CodeMeter Runtime before 5.20 uses weak permissions (read and write access for all users) for codemeter.exe, which allows local users to gain privileges via a Trojan horse file.
CVE-2014-8551
The WinCC server in Siemens SIMATIC WinCC 7.0 through SP3, 7.2 before Update 9, and 7.3 before Update 2; SIMATIC PCS 7 7.1 through SP4, 8.0 through SP2, and 8.1; and TIA Portal 13 before Update 6 allows remote attackers to execute arbitrary code via crafted packets.
CVE-2014-8552
The WinCC server in Siemens SIMATIC WinCC 7.0 through SP3, 7.2 before Update 9, and 7.3 before Update 2; SIMATIC PCS 7 7.1 through SP4, 8.0 through SP2, and 8.1; and TIA Portal 13 before Update 6 allows remote attackers to read arbitrary files via crafted packets.
CVE-2014-8962
Stack-based buffer overflow in stream_decoder.c in libFLAC before 1.3.1 allows remote attackers to execute arbitrary code via a crafted .flac file.
CVE-2014-9028
Heap-based buffer overflow in stream_decoder.c in libFLAC before 1.3.1 allows remote attackers to execute arbitrary code via a crafted .flac file.
CVE-2014-9093
LibreOffice before 4.3.5 allows remote attackers to cause a denial of service (invalid write operation and crash) and possibly execute arbitrary code via a crafted RTF file.
CVE-2014-9094
Multiple cross-site scripting (XSS) vulnerabilities in deploy/designer/preview.php in the Digital Zoom Studio (DZS) Video Gallery plugin for WordPress allow remote attackers to inject arbitrary web script or HTML via the (1) swfloc or (2) designrand parameter.
CVE-2014-9095
Multiple SQL injection vulnerabilities in Raritan Power IQ 4.1.0 and 4.2.1 allow remote attackers to execute arbitrary SQL commands via the (1) sort or (2) dir parameter to license/records.
CVE-2014-9096
Multiple SQL injection vulnerabilities in recover.php in Pligg CMS 2.0.1 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) id or (2) n parameter.
CVE-2014-9097
Multiple SQL injection vulnerabilities in the Apptha WordPress Video Gallery (contus-video-gallery) plugin 2.5, possibly as distributed before 2014-07-23, for WordPress allow (1) remote attackers to execute arbitrary SQL commands via the vid parameter in a myextract action to wp-admin/admin-ajax.php or (2) remote authenticated users to execute arbitrary SQL commands via the playlistId parameter in the newplaylist page or (3) videoId parameter in a newvideo page to wp-admin/admin.php.
CVE-2014-9098
Multiple cross-site scripting (XSS) vulnerabilities in the Apptha WordPress Video Gallery (contus-video-gallery) plugin 2.5, possibly before 2014-07-23, for WordPress allow remote authenticated users to inject arbitrary web script or HTML via the videoadssearchQuery parameter to (1) videoads/videoads.php, (2) video/video.php, or (3) playlist/playlist.php.
CVE-2014-9099
Cross-site request forgery (CSRF) vulnerability in the WhyDoWork AdSense plugin 1.2 for WordPress allows remote attackers to hijack the authentication of administrators for requests that have unspecified impact via a request to the whydowork_adsense page in wp-admin/options-general.php.
CVE-2014-9100
Cross-site scripting (XSS) vulnerability in the WhyDoWork AdSense plugin 1.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via the idcode parameter in the whydowork_adsense page to wp-admin/options-general.php.
CVE-2014-9101
Multiple cross-site request forgery (CSRF) vulnerabilities in Oxwall 1.7.0 (build 7907 and 7906) and SkaDate Lite 2.0 (build 7651) allow remote attackers to hijack the authentication of administrators for requests that conduct cross-site scripting (XSS) attacks or possibly have other unspecified impact via the (1) label parameter to admin/users/roles/, (2) lang[1][base][questions_account_type_5615100a931845eca8da20cfdf7327e0] in an AddAccountType action or (3) qst_name parameter in an addQuestion action to admin/questions/ajax-responder/, or (4) form_name or (5) restrictedUsername parameter to admin/restricted-usernames.
CVE-2014-9102
Multiple SQL injection vulnerabilities in the Kunena component before 3.0.6 for Joomla! allow remote authenticated users to execute arbitrary SQL commands via the index value in an array parameter, as demonstrated by the topics[] parameter in an unfavorite action to index.php.
CVE-2014-9103
Multiple cross-site scripting (XSS) vulnerabilities in the Kunena component before 3.0.6 for Joomla! allow remote attackers to inject arbitrary web script or HTML via the (1) index value of an array parameter or the filename parameter in the Content-Disposition header to the (2) file or (3) profile image upload functionality.
CVE-2014-9104
Multiple cross-site request forgery (CSRF) vulnerabilities in the XML-RPC API in the Desktop Client in OpenVPN Access Server 1.5.6 and earlier allow remote attackers to hijack the authentication of administrators for requests that (1) disconnecting established VPN sessions, (2) connect to arbitrary VPN servers, or (3) create VPN profiles and execute arbitrary commands via crafted API requests.
