CVE-2008-0017
The http-index-format MIME type parser (nsDirIndexParser) in Firefox 3.x before 3.0.4, Firefox 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 does not check for an allocation failure, which allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via an HTTP index response with a crafted 200 header, which triggers memory corruption and a buffer overflow.
CVE-2008-4989
The _gnutls_x509_verify_certificate function in lib/x509/verify.c in libgnutls in GnuTLS before 2.6.1 trusts certificate chains in which the last certificate is an arbitrary trusted, self-signed certificate, which allows man-in-the-middle attackers to insert a spoofed certificate for any Distinguished Name (DN).
CVE-2008-5012
Mozilla Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 do not properly change the source URI when processing a canvas element and an HTTP redirect, which allows remote attackers to bypass the same origin policy and access arbitrary images that are not directly accessible to the attacker.  NOTE: this issue can be leveraged to enumerate software on the client by performing redirections related to moz-icon.
CVE-2008-5013
Mozilla Firefox 2.x before 2.0.0.18 and SeaMonkey 1.x before 1.1.13 do not properly check when the Flash module has been dynamically unloaded properly, which allows remote attackers to execute arbitrary code via a crafted SWF file that "dynamically unloads itself from an outside JavaScript function," which triggers an access of an expired memory address.
CVE-2008-5014
jslock.cpp in Mozilla Firefox 3.x before 3.0.2, Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code by modifying the window.__proto__.__proto__ object in a way that causes a lock on a non-native object, which triggers an assertion failure related to the OBJ_IS_NATIVE function.
CVE-2008-5015
Mozilla Firefox 3.x before 3.0.4 assigns chrome privileges to a file: URI when it is accessed in the same tab from a chrome or privileged about: page, which makes it easier for user-assisted attackers to execute arbitrary JavaScript with chrome privileges via malicious code in a file that has already been saved on the local system.
CVE-2008-5016
The layout engine in Mozilla Firefox 3.x before 3.0.4, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to cause a denial of service (crash) via multiple vectors that trigger an assertion failure or other consequences.
CVE-2008-5017
Integer overflow in xpcom/io/nsEscape.cpp in the browser engine in Mozilla Firefox 3.x before 3.0.4, Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to cause a denial of service (crash) via unknown vectors.
CVE-2008-5018
The JavaScript engine in Mozilla Firefox 3.x before 3.0.4, Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to cause a denial of service (crash) via vectors related to "insufficient class checking" in the Date class.
CVE-2008-5019
The session restore feature in Mozilla Firefox 3.x before 3.0.4 and 2.x before 2.0.0.18 allows remote attackers to violate the same origin policy to conduct cross-site scripting (XSS) attacks and execute arbitrary JavaScript with chrome privileges via unknown vectors.
CVE-2008-5021
nsFrameManager in Firefox 3.x before 3.0.4, Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code by modifying properties of a file input element while it is still being initialized, then using the blur method to access uninitialized memory.
CVE-2008-5022
The nsXMLHttpRequest::NotifyEventListeners method in Firefox 3.x before 3.0.4, Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to bypass the same-origin policy and execute arbitrary script via multiple listeners, which bypass the inner window check.
CVE-2008-5023
Firefox 3.x before 3.0.4, Firefox 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to bypass the protection mechanism for codebase principals and execute arbitrary script via the -moz-binding CSS property in a signed JAR file.
CVE-2008-5024
Mozilla Firefox 3.x before 3.0.4, Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 do not properly escape quote characters used for XML processing, which allows remote attackers to conduct XML injection attacks via the default namespace in an E4X document.
CVE-2008-5045
Heap-based buffer overflow in Network-Client FTP Now 2.6, and possibly other versions, allows remote FTP servers to cause a denial of service (crash) via a 200 server response that is exactly 1024 characters long.
CVE-2008-5046
SQL injection vulnerability in index.php in Mole Group Pizza Script allows remote attackers to execute arbitrary SQL commands via the manufacturers_id parameter.
CVE-2008-5047
SQL injection vulnerability in admin/index.php in Mole Group Rental Script allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2008-5048
Buffer overflow in Atepmon.sys in ISecSoft Anti-Trojan Elite 4.2.1 and earlier, and possibly 4.2.2, allows local users to cause a denial of service (crash) and possibly execute arbitrary code via long inputs to the 0x00222494 IOCTL.
CVE-2008-5049
Buffer overflow in AKEProtect.sys 3.3.3.0 in ISecSoft Anti-Keylogger Elite 3.3.0 and earlier, and possibly other versions including 3.3.3, allows local users to gain privileges via long inputs to the (1) 0x002224A4, (2) 0x002224C0, and (3) 0x002224CC IOCTL.
CVE-2008-5050
Off-by-one error in the get_unicode_name function (libclamav/vba_extract.c) in Clam Anti-Virus (ClamAV) before 0.94.1 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a crafted VBA project file, which triggers a heap-based buffer overflow.
CVE-2008-5051
SQL injection vulnerability in the JooBlog (com_jb2) component 0.1.1 for Joomla! allows remote attackers to execute arbitrary SQL commands via the PostID parameter to index.php.
CVE-2008-5052
The AppendAttributeValue function in the JavaScript engine in Mozilla Firefox 2.x before 2.0.0.18, Thunderbird 2.x before 2.0.0.18, and SeaMonkey 1.x before 1.1.13 allows remote attackers to cause a denial of service (crash) via unknown vectors that trigger memory corruption, as demonstrated by e4x/extensions/regress-410192.js.
CVE-2008-5053
PHP remote file inclusion vulnerability in admin.rssreader.php in the Simple RSS Reader (com_rssreader) 1.0 component for Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_live_site parameter.
CVE-2008-5054
Multiple SQL injection vulnerabilities in Develop It Easy Membership System 1.3 allow remote attackers to execute arbitrary SQL commands via the (1) email and (2) password parameters to customer_login.php and the (3) user_name and (4) user_pass parameters to admin/index.php. NOTE: some of these details are obtained from third party information.
CVE-2008-5055
SQL injection vulnerability in department_offline_context.php in ActiveCampaign TrioLive before 1.58.7 allows remote attackers to execute arbitrary SQL commands via the department_id parameter to index.php.
CVE-2008-5056
Cross-site scripting (XSS) vulnerability in department_offline_context.php in ActiveCampaign TrioLive before 1.58.7 allows remote attackers to inject arbitrary web script or HTML via the department_id parameter to index.php.
CVE-2008-5057
SQL injection vulnerability in film.asp in Yigit Aybuga Dizi Portali allows remote attackers to execute arbitrary SQL commands via the film parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-5058
SQL injection vulnerability in siteadmin/loginsucess.php in Pre Simple CMS allows remote attackers to execute arbitrary SQL commands via the user parameter, as reachable from siteadmin/adminlogin.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-5059
Cross-site scripting (XSS) vulnerability in index.php in ModernBill 4.4 and earlier allows remote attackers to inject arbitrary web script or HTML via a Javascript event in the new_language parameter in a login action.
ModernBill has changed their name to Parallels Plesk Billing.  http://www.modernbill.com/
CVE-2008-5060
Multiple PHP remote file inclusion vulnerabilities in ModernBill 4.4 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the DIR parameter to (1) export_batch.inc.php, (2) run_auto_suspend.cron.php, and (3) send_email_cache.php in include/scripts/; (4) include/misc/mod_2checkout/2checkout_return.inc.php; and (5) include/html/nettools.popup.php, different vectors than CVE-2006-4034 and CVE-2005-1054.
CVE-2008-5061
Cross-site scripting (XSS) vulnerability in php/cal_default.php in Mini Web Calendar (mwcal) 1.2 allows remote attackers to inject arbitrary web script or HTML via the URL.
CVE-2008-5062
Directory traversal vulnerability in php/cal_pdf.php in Mini Web Calendar (mwcal) 1.2 allows remote attackers to read arbitrary files via directory traversal sequences in the thefile parameter.
CVE-2008-5063
PHP remote file inclusion vulnerability in Admin/ADM_Pagina.php in OTManager 2.4 allows remote attackers to execute arbitrary PHP code via a URL in the Tipo parameter.
CVE-2008-5064
SQL injection vulnerability in liga.php in H&H WebSoccer 2.80 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-5065
TlGuestBook 1.2 allows remote attackers to bypass authentication and gain administrative access by setting the tlGuestBook_login cookie to admin.
CVE-2008-5066
PHP remote file inclusion vulnerability in upload/admin/frontpage_right.php in Agares Media ThemeSiteScript 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the loadadminpage parameter.
CVE-2008-5067
Cross-site scripting (XSS) vulnerability in search.php in Kmita Catalogue 2.x allows remote attackers to inject arbitrary web script or HTML via the q parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-5068
Multiple cross-site scripting (XSS) vulnerabilities in Kmita Gallery allow remote attackers to inject arbitrary web script or HTML via the (1) begin parameter to index.php and the (2) searchtext parameter to search.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
