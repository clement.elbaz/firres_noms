CVE-2017-18266
The open_envvar function in xdg-open in xdg-utils before 1.1.3 does not validate strings before launching the program specified by the BROWSER environment variable, which might allow remote attackers to conduct argument-injection attacks via a crafted URL, as demonstrated by %s in this environment variable.
CVE-2017-18267
The FoFiType1C::cvtGlyph function in fofi/FoFiType1C.cc in Poppler through 0.64.0 allows remote attackers to cause a denial of service (infinite recursion) via a crafted PDF file, as demonstrated by pdftops.
CVE-2017-2601
Jenkins before versions 2.44, 2.32.2 is vulnerable to a persisted cross-site scripting in parameter names and descriptions (SECURITY-353). Users with the permission to configure jobs were able to inject JavaScript into parameter names and descriptions.
CVE-2017-6289
In Android before the 2018-05-05 security patch level, NVIDIA Trusted Execution Environment (TEE) contains a memory corruption (due to unusual root cause) vulnerability, which if run within the speculative execution of the TEE, may lead to local escalation of privileges. This issue is rated as critical. Android: A-72830049. Reference: N-CVE-2017-6289.
CVE-2017-6293
In Android before the 2018-05-05 security patch level, NVIDIA Tegra X1 TZ contains a vulnerability in Widevine TA where the software writes data past the end, or before the beginning, of the intended buffer, which may lead to escalation of Privileges. This issue is rated as high. Android: A-69377364. Reference: N-CVE-2017-6293.
CVE-2018-10314
Cross-site scripting (XSS) vulnerability in Open-AudIT Community 2.2.0 allows remote attackers to inject arbitrary web script or HTML via a crafted name of a component, as demonstrated by the action parameter in the Discover -> Audit Scripts -> List Scripts -> Download section.
CVE-2018-10655
DLPnpAuditor.exe in DeviceLock Plug and Play Auditor (freeware) 5.72 has a Unicode Buffer Overflow (SEH).
CVE-2018-10706
An integer overflow in the transferMulti function of a smart contract implementation for Social Chain (SCA), an Ethereum ERC20 token, allows attackers to accomplish an unauthorized increase of digital assets, aka the "multiOverflow" issue.
CVE-2018-10803
Cross-site scripting (XSS) vulnerability in the add credentials functionality in Zoho ManageEngine NetFlow Analyzer v12.3 before 12.3.125 (build 123125) allows remote attackers to inject arbitrary web script or HTML via a crafted description value. This can be exploited through CSRF.
CVE-2018-10942
modules/attributewizardpro/file_upload.php in the Attribute Wizard addon 1.6.9 for PrestaShop 1.4.0.1 through 1.6.1.18 allows remote attackers to execute arbitrary code by uploading a .phtml file.
CVE-2018-10949
mailboxd in Zimbra Collaboration Suite 8.8 before 8.8.8; 8.7 before 8.7.11.Patch3; and 8.6 allows Account Enumeration by leveraging a Discrepancy between the "HTTP 404 - account is not active" and "HTTP 401 - must authenticate" errors.
CVE-2018-10950
mailboxd in Zimbra Collaboration Suite 8.8 before 8.8.8; 8.7 before 8.7.11.Patch3; and 8.6 before 8.6.0.Patch10 allows Information Exposure through Verbose Error Messages containing a stack dump, tracing data, or full user-context dump.
CVE-2018-10951
mailboxd in Zimbra Collaboration Suite 8.8 before 8.8.8; 8.7 before 8.7.11.Patch3; and 8.6 before 8.6.0.Patch10 allows zimbraSSLPrivateKey read access via a GetServer, GetAllServers, or GetAllActiveServers call in the Admin SOAP API.
CVE-2018-10952
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x00222088.
CVE-2018-10953
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x0022204C.
CVE-2018-10954
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x00222550.
CVE-2018-10955
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x00222548.
CVE-2018-10957
CSRF exists on D-Link DIR-868L devices, leading to (for example) a change to the Admin password. hedwig.cgi and pigwidgeon.cgi are two of the affected components.
CVE-2018-10958
In types.cpp in Exiv2 0.26, a large size value may lead to a SIGABRT during an attempt at memory allocation for an Exiv2::Internal::PngChunk::zlibUncompress call.
CVE-2018-10962
An issue was discovered in Shanghai 2345 Security Guard 3.7.0. 2345MPCSafe.exe, 2345SafeTray.exe, and 2345Speedup.exe allow local users to bypass intended process protections, and consequently terminate processes, because mouse_event is not properly considered.
CVE-2018-10963
The TIFFWriteDirectorySec() function in tif_dirwrite.c in LibTIFF through 4.0.9 allows remote attackers to cause a denial of service (assertion failure and application crash) via a crafted file, a different vulnerability than CVE-2017-13726.
CVE-2018-10971
An issue was discovered in Free Lossless Image Format (FLIF) 0.3. The Plane function in image/image.hpp allows remote attackers to cause a denial of service (attempted excessive memory allocation) via a crafted file.
CVE-2018-10972
An issue was discovered in Free Lossless Image Format (FLIF) 0.3. The TransformPaletteC::process function in transform/palette_C.hpp allows remote attackers to cause a denial of service (heap-based buffer overflow) or possibly have unspecified other impact via a crafted file.
CVE-2018-10973
An integer overflow in the transferMulti function of a smart contract implementation for KoreaShow, an Ethereum ERC20 token, allows attackers to accomplish an unauthorized increase of digital assets via crafted _value parameters.
CVE-2018-10974
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x00222100.
CVE-2018-10975
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x00222104.
CVE-2018-10976
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x00222050.
CVE-2018-10977
In 2345 Security Guard 3.7, the driver file (2345BdPcSafe.sys, X64 version) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCTL 0x002220E4.
CVE-2018-10981
An issue was discovered in Xen through 4.10.x allowing x86 HVM guest OS users to cause a denial of service (host OS infinite loop) in situations where a QEMU device model attempts to make invalid transitions between states of a request.
CVE-2018-10982
An issue was discovered in Xen through 4.10.x allowing x86 HVM guest OS users to cause a denial of service (unexpectedly high interrupt number, array overrun, and hypervisor crash) or possibly gain hypervisor privileges by setting up an HPET timer to deliver interrupts in IO-APIC mode, aka vHPET interrupt injection.
CVE-2018-1115
postgresql before versions 10.4, 9.6.9 is vulnerable in the adminpack extension, the pg_catalog.pg_logfile_rotate() function doesn't follow the same ACLs than pg_rorate_logfile. If the adminpack is added to a database, an attacker able to connect to it could exploit this to force log rotation.
CVE-2018-1118
Linux kernel vhost since version 4.8 does not properly initialize memory in messages passed between virtual guests and the host operating system in the vhost/vhost.c:vhost_new_msg() function. This can allow local privileged users to read some kernel memory contents when reading from the /dev/vhost-net device file.
CVE-2018-1130
Linux kernel before version 4.16-rc7 is vulnerable to a null pointer dereference in dccp_write_xmit() function in net/dccp/output.c in that allows a local user to cause a denial of service by a number of certain crafted system calls.
CVE-2018-3612
Intel NUC kits with insufficient input validation in system firmware, potentially allows a local attacker to elevate privileges to System Management Mode (SMM).
CVE-2018-3617
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2018-3691.  Reason: This candidate is a reservation duplicate of CVE-2018-3691.  Notes: All CVE users should reference CVE-2018-3691 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-3649
DLL injection vulnerability in the installation executables (Autorun.exe and Setup.exe) for Intel's wireless drivers and related software in Intel Dual Band Wireless-AC, Tri-Band Wireless-AC and Wireless-AC family of products allows a local attacker to cause escalation of privilege via remote code execution.
CVE-2018-6246
In Android before the 2018-05-05 security patch level, NVIDIA Widevine Trustlet contains a vulnerability in Widevine TA where the software reads data past the end, or before the beginning, of the intended buffer, which may lead to Information Disclosure. This issue is rated as moderate. Android: A-69383916. Reference: N-CVE-2018-6246.
CVE-2018-6254
In Android before the 2018-05-05 security patch level, NVIDIA Media Server contains an out-of-bounds read (due to improper input validation) vulnerability which could lead to local information disclosure. This issue is rated as moderate. Android: A-64340684. Reference: N-CVE-2018-6254.
CVE-2018-7933
Huawei home gateway products HiRouter-CD20 and WS5200 with the versions before HiRouter-CD20-10 1.9.6 and the versions before WS5200-10 1.9.6 have a path traversal vulnerability. Due to the lack of validation while these home gateway products install APK plugins, an attacker tricks a user into installing a malicious APK plugin, and plugin can overwrite arbitrary file of devices. Successful exploit may result in arbitrary code execution or privilege escalation.
CVE-2018-7940
Huawei smart phones Mate 10 and Mate 10 Pro with earlier versions than 8.0.0.129(SP2C00) and earlier versions than 8.0.0.129(SP2C01) have an authentication bypass vulnerability. An attacker with high privilege obtains the smart phone and bypass the activation function by some specific operations.
CVE-2018-7941
Huawei iBMC V200R002C60 have an authentication bypass vulnerability. A remote attacker with low privilege may craft specific messages to upload authentication certificate to the affected products. Due to improper validation of the upload authority, successful exploit may cause privilege elevation.
CVE-2018-8060
HWiNFO AMD64 Kernel driver version 8.98 and lower allows an unprivileged user to send an IOCTL to the device driver. If input and/or output buffer pointers are NULL or if these buffers' data are invalid, a NULL/invalid pointer access occurs, resulting in a Windows kernel panic aka Blue Screen. This affects IOCTLs higher than 0x85FE2600 with the HWiNFO32 symbolic device name.
CVE-2018-8061
HWiNFO AMD64 Kernel driver version 8.98 and lower allows an unprivileged user to send IOCTL 0x85FE2608 to the device driver with the HWiNFO32 symbolic device name, resulting in direct physical memory read or write.
CVE-2018-8824
modules/bamegamenu/ajax_phpcode.php in the Responsive Mega Menu (Horizontal+Vertical+Dropdown) Pro module 1.0.32 for PrestaShop 1.5.5.0 through 1.7.2.5 allows remote attackers to execute a SQL Injection through function calls in the code parameter.
CVE-2018-8910
Cross-site scripting (XSS) vulnerability in Attachment Preview in Synology Drive before 1.0.1-10253 allows remote authenticated users to inject arbitrary web script or HTML via malicious attachments.
CVE-2018-8914
SQL injection vulnerability in UPnP DMA in Synology Media Server before 1.7.6-2842 and before 1.4-2654 allows remote attackers to execute arbitrary SQL commands via the ObjectID parameter.
CVE-2018-8915
Cross-site scripting (XSS) vulnerability in Notification Center in Synology Calendar before 2.1.1-0502 allows remote authenticated users to inject arbitrary web script or HTML via title parameter.
CVE-2018-9111
Cross Site Scripting (XSS) exists on the Foxconn FEMTO AP-FC4064-T AP_GT_B38_5.8.3lb15-W47 LTE Build 15 via the configuration of a user account. An attacker can execute arbitrary script on an unsuspecting user's browser.
CVE-2018-9112
A low privileged admin account with a weak default password of admin exists on the Foxconn FEMTO AP-FC4064-T AP_GT_B38_5.8.3lb15-W47 LTE Build 15. In addition, its web management page relies on the existence or values of cookies when performing security-critical operations. One can gain privileges by modifying cookies.
CVE-2018-9849
Pulse Secure Pulse Connect Secure 8.1.x before 8.1R14, 8.2.x before 8.2R11, and 8.3.x before 8.3R5 do not properly process nested XML entities, which allows remote attackers to cause a denial of service (memory consumption and memory errors) via a crafted XML document.
