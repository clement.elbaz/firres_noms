CVE-2013-1636
Cross-site scripting (XSS) vulnerability in open-flash-chart.swf in Open Flash Chart (aka Open-Flash Chart), as used in the Pretty Link Lite plugin before 1.6.3 for WordPress, JNews (com_jnews) component 8.0.1 for Joomla!, and CiviCRM 3.1.0 through 4.2.9 and 4.3.0 through 4.3.3, allows remote attackers to inject arbitrary web script or HTML via the get-data parameter.
CVE-2013-3943
Cross-site scripting (XSS) vulnerability in DotNetNuke (DNN) before 6.2.9 and 7.x before 7.1.1 allows remote authenticated users to inject arbitrary web script or HTML via vectors related to the Display Name field in the Manage Profile.
CVE-2013-4649
Cross-site scripting (XSS) vulnerability in DotNetNuke (DNN) before 6.2.9 and 7.x before 7.1.1 allows remote attackers to inject arbitrary web script or HTML via the __dnnVariable parameter to the default URI.
CVE-2013-5117
SQL injection vulnerability in the RSS page (DNNArticleRSS.aspx) in the ZLDNN DNNArticle module before 10.1 for DotNetNuke allows remote attackers to execute arbitrary SQL commands via the categoryid parameter.
CVE-2013-7335
Open redirect vulnerability in DotNetNuke (DNN) before 6.2.9 and 7.x before 7.1.1 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2014-0297
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0308, CVE-2014-0312, and CVE-2014-0324.
CVE-2014-0298
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0299
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0305 and CVE-2014-0311.
CVE-2014-0300
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to gain privileges via a crafted application, aka "Win32k Elevation of Privilege Vulnerability."
CVE-2014-0301
Double free vulnerability in qedit.dll in DirectShow in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, and Windows Server 2012 Gold and R2 allows remote attackers to execute arbitrary code via a crafted JPEG image, aka "DirectShow Memory Corruption Vulnerability."
CVE-2014-0302
Microsoft Internet Explorer 6 through 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0303.
CVE-2014-0303
Microsoft Internet Explorer 6 through 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0302.
CVE-2014-0304
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0305
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0299 and CVE-2014-0311.
CVE-2014-0306
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0307
Use-after-free vulnerability in Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a certain sequence of manipulations of a TextRange element, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0308
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0297, CVE-2014-0312, and CVE-2014-0324.
CVE-2014-0309
Microsoft Internet Explorer 8 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0311
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0299 and CVE-2014-0305.
CVE-2014-0312
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0297, CVE-2014-0308, and CVE-2014-0324.
CVE-2014-0313
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0321.
CVE-2014-0314
Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-0317
The Security Account Manager Remote (SAMR) protocol implementation in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, and Windows Server 2012 Gold and R2 does not properly determine the user-lockout state, which makes it easier for remote attackers to bypass the account lockout policy and obtain access via a brute-force attack, aka "SAMR Security Feature Bypass Vulnerability."
CVE-2014-0319
Microsoft Silverlight 5 before 5.1.30214.0 and Silverlight 5 Developer Runtime before 5.1.30214.0 allow attackers to bypass the DEP and ASLR protection mechanisms via unspecified vectors, aka "Silverlight DEP/ASLR Bypass Vulnerability."
CVE-2014-0321
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0313.
CVE-2014-0323
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to obtain sensitive information from kernel memory or cause a denial of service (system hang) via a crafted application, aka "Win32k Information Disclosure Vulnerability."
CVE-2014-0324
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-0297, CVE-2014-0308, and CVE-2014-0312.
CVE-2014-0503
Adobe Flash Player before 11.7.700.272 and 11.8.x through 12.0.x before 12.0.0.77 on Windows and OS X, and before 11.2.202.346 on Linux, allows remote attackers to bypass the Same Origin Policy via unspecified vectors.
CVE-2014-0504
Adobe Flash Player before 11.7.700.272 and 11.8.x through 12.0.x before 12.0.0.77 on Windows and OS X, and before 11.2.202.346 on Linux, allows attackers to read the clipboard via unspecified vectors.
CVE-2014-2240
Stack-based buffer overflow in the cf2_hintmap_build function in cff/cf2hints.c in FreeType before 2.5.3 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large number of stem hints in a font file.
