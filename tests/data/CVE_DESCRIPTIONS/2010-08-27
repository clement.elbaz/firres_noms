CVE-2010-3138
Untrusted search path vulnerability in the Indeo Codec in iac25_32.ax in Microsoft Windows XP SP3 allows local users to gain privileges via a Trojan horse iacenc.dll file in the current working directory, as demonstrated by access through BS.Player or Media Player Classic to a directory that contains a .avi, .mka, .ra, or .ram file, aka "Indeo Codec Insecure Library Loading Vulnerability." NOTE: some of these details are obtained from third party information.
Per: http://cwe.mitre.org/data/definitions/426.html 

'CWE-426 - 'Untrusted Search Path Vulnerability'
CVE-2010-3139
Untrusted search path vulnerability in Microsoft Windows Progman Group Converter (grpconv.exe) allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse imm.dll that is located in the same folder as a .grp file.
CVE-2010-3140
Untrusted search path vulnerability in Microsoft Windows Internet Communication Settings on Windows XP SP3 allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse schannel.dll that is located in the same folder as an ISP file.
Per: http://cwe.mitre.org/data/definitions/426.html 

'CWE-426 - 'Untrusted Search Path Vulnerability'
CVE-2010-3141
Untrusted search path vulnerability in Microsoft PowerPoint 2010 allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse pptimpconv.dll that is located in the same folder as a .odp, .pot, .potm, .potx, .ppa, .pps, .ppsm, .ppsx, .ppt, .pptm, .pptx, .pwz, .sldm, or .sldx file.
CVE-2010-3142
Untrusted search path vulnerability in Microsoft Office PowerPoint 2007 allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse rpawinet.dll that is located in the same folder as a .odp, .pothtml, .potm, .potx, .ppa, .ppam, .pps, .ppt, .ppthtml, .pptm, .pptxml, .pwz, .sldm, .sldx, and .thmx file.
Per: http://cwe.mitre.org/data/definitions/426.html 

'CWE-426 - 'Untrusted Search Path Vulnerability'
CVE-2010-3143
Untrusted search path vulnerability in Microsoft Windows Contacts allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse wab32res.dll that is located in the same folder as a .contact, .group, .p7c, .vcf, or .wab file.  NOTE: the codebase for this product may overlap the codebase for the product referenced in CVE-2010-3147.
CVE-2010-3144
Untrusted search path vulnerability in the Internet Connection Signup Wizard in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 allows local users to gain privileges via a Trojan horse smmscrpt.dll file in the current working directory, as demonstrated by a directory that contains an ISP or INS file, aka "Internet Connection Signup Wizard Insecure Library Loading Vulnerability."
CVE-2010-3145
Untrusted search path vulnerability in the BitLocker Drive Encryption API, as used in sdclt.exe in Backup Manager in Microsoft Windows Vista SP1 and SP2, allows local users to gain privileges via a Trojan horse fveapi.dll file in the current working directory, as demonstrated by a directory that contains a Windows Backup Catalog (.wbcat) file, aka "Backup Manager Insecure Library Loading Vulnerability."
CVE-2010-3146
Multiple untrusted search path vulnerabilities in Microsoft Groove 2007 SP2 allow local users to gain privileges via a Trojan horse (1) mso.dll or (2) GroovePerfmon.dll file in the current working directory, as demonstrated by a directory that contains a Groove vCard (.vcg) or Groove Tool Archive (.gta) file, aka "Microsoft Groove Insecure Library Loading Vulnerability."
Per: https://technet.microsoft.com/en-us/security/bulletin/ms11-016 Access Vector: Network per "This is a remote code execution vulnerability"
Per: http://cwe.mitre.org/data/definitions/426.html 
'CWE-426: Untrusted Search Path'
CVE-2010-3147
Untrusted search path vulnerability in wab.exe 6.00.2900.5512 in Windows Address Book in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 allows local users to gain privileges via a Trojan horse wab32res.dll file in the current working directory, as demonstrated by a directory that contains a Windows Address Book (WAB), VCF (aka vCard), or P7C file, aka "Insecure Library Loading Vulnerability."  NOTE: the codebase for this product may overlap the codebase for the product referenced in CVE-2010-3143.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2010-3148
Untrusted search path vulnerability in Microsoft Visio 2003 SP3 allows local users to gain privileges via a Trojan horse mfc71enu.dll file in the current working directory, as demonstrated by a directory that contains a .vsd, .vdx, .vst, or .vtx file, aka "Microsoft Visio Insecure Library Loading Vulnerability."
CVE-2010-3149
Untrusted search path vulnerability in Adobe Device Central CS5 3.0.0(376), 3.0.1.0 (3027), and probably other versions allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse qtcf.dll that is located in the same folder as an ADCP file.
CVE-2010-3150
Untrusted search path vulnerability in Adobe Premier Pro CS4 4.0.0 (314 (MC: 160820)) allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse ibfs32.dll that is located in the same folder as a .pproj, .prfpset, .prexport, .prm, .prmp, .prpreset, .prproj, .prsl, .prtl, or .vpr file.
CVE-2010-3151
Untrusted search path vulnerability in Adobe On Location CS4 Build 315 allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse ibfs32.dll that is located in the same folder as an OLPROJ file.
er: http://cwe.mitre.org/data/definitions/426.html 

'CWE-426 - 'Untrusted Search Path Vulnerability'
CVE-2010-3152
Untrusted search path vulnerability in Adobe Illustrator CS4 14.0.0, CS5 15.0.1 and earlier, and possibly other versions allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse dwmapi.dll or aires.dll that is located in the same folder as an .ait or .eps file.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2010-3153
Untrusted search path vulnerability in Adobe InDesign CS4 6.0, InDesign CS5 7.0.2 and earlier, Adobe InDesign Server CS5 7.0.2 and earlier, and Adobe InCopy CS5 7.0.2 and earlier allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse ibfs32.dll that is located in the same folder as an .indl, .indp, .indt, or .inx file.
CVE-2010-3154
Untrusted search path vulnerability in Adobe Extension Manager CS5 5.0.298 allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse dwmapi.dll that is located in the same folder as a .mxi or .mxp file.
CVE-2010-3155
Untrusted search path vulnerability in Adobe ExtendScript Toolkit (ESTK) CS5 3.5.0.52 allows local users, and possibly remote attackers, to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse dwmapi.dll that is located in the same folder as a .jsx file.
