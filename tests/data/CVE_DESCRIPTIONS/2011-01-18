CVE-2009-5051
Hastymail2 before RC 8 does not set the secure flag for the session cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.
CVE-2010-4166
Multiple SQL injection vulnerabilities in Joomla! 1.5.x before 1.5.22 allow remote attackers to execute arbitrary SQL commands via (1) the filter_order parameter in a com_weblinks category action to index.php, (2) the filter_order_Dir parameter in a com_weblinks category action to index.php, or (3) the filter_order_Dir parameter in a com_messages action to administrator/index.php.
CVE-2010-4263
The igb_receive_skb function in drivers/net/igb/igb_main.c in the Intel Gigabit Ethernet (aka igb) subsystem in the Linux kernel before 2.6.34, when Single Root I/O Virtualization (SR-IOV) and promiscuous mode are enabled but no VLANs are registered, allows remote attackers to cause a denial of service (NULL pointer dereference and panic) and possibly have unspecified other impact via a VLAN tagged frame.
Per: http://cwe.mitre.org/data/definitions/476.html
'CWE-476: NULL Pointer Dereference'
CVE-2010-4530
Signedness error in ccid_serial.c in libccid in the USB Chip/Smart Card Interface Devices (CCID) driver, as used in pcscd in PCSC-Lite 1.5.3 and possibly other products, allows physically proximate attackers to execute arbitrary code via a smart card with a crafted serial number that causes a negative value to be used in a memcpy operation, which triggers a buffer overflow.  NOTE: some sources refer to this issue as an integer overflow.
CVE-2010-4531
Stack-based buffer overflow in the ATRDecodeAtr function in the Answer-to-Reset (ATR) Handler (atrhandler.c) for pcscd in PCSC-Lite 1.5.3, and possibly other 1.5.x and 1.6.x versions, allows physically proximate attackers to cause a denial of service (crash) and possibly execute arbitrary code via a smart card with an ATR message containing a long attribute value.
CVE-2010-4646
Cross-site scripting (XSS) vulnerability in Hastymail2 before 1.01 allows remote attackers to inject arbitrary web script or HTML via a crafted background attribute within a cell in a TABLE element, related to improper use of the htmLawed filter.
CVE-2010-4696
Multiple SQL injection vulnerabilities in Joomla! 1.5.x before 1.5.22 allow remote attackers to execute arbitrary SQL commands via the (1) filter_order or (2) filter_order_Dir parameter in a com_contact action to index.php, a different vulnerability than CVE-2010-4166.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2010-4697
Use-after-free vulnerability in the Zend engine in PHP before 5.2.15 and 5.3.x before 5.3.4 might allow context-dependent attackers to cause a denial of service (heap memory corruption) or have unspecified other impact via vectors related to use of __set, __get, __isset, and __unset methods on objects accessed by a reference.
CVE-2010-4698
Stack-based buffer overflow in the GD extension in PHP before 5.2.15 and 5.3.x before 5.3.4 allows context-dependent attackers to cause a denial of service (application crash) via a large number of anti-aliasing steps in an argument to the imagepstext function.
CVE-2010-4699
The iconv_mime_decode_headers function in the Iconv extension in PHP before 5.3.4 does not properly handle encodings that are unrecognized by the iconv and mbstring (aka Multibyte String) implementations, which allows remote attackers to trigger an incomplete output array, and possibly bypass spam detection or have unspecified other impact, via a crafted Subject header in an e-mail message, as demonstrated by the ks_c_5601-1987 character set.
CVE-2010-4700
The set_magic_quotes_runtime function in PHP 5.3.2 and 5.3.3, when the MySQLi extension is used, does not properly interact with use of the mysqli_fetch_assoc function, which might make it easier for context-dependent attackers to conduct SQL injection attacks via crafted input that had been properly handled in earlier PHP versions.
CVE-2011-0010
check.c in sudo 1.7.x before 1.7.4p5, when a Runas group is configured, does not require a password for command execution that involves a gid change but no uid change, which allows local users to bypass an intended authentication requirement via the -g option to a sudo command.
CVE-2011-0272
Unspecified vulnerability in HP LoadRunner 9.52 allows remote attackers to execute arbitrary code via network traffic to TCP port 5001 or 5002, related to the HttpTunnel feature.
CVE-2011-0408
pngrtran.c in libpng 1.5.x before 1.5.1 allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a crafted palette-based PNG image that triggers a buffer overflow, related to the png_do_expand_palette function, the png_do_rgb_to_gray function, and an integer underflow.  NOTE: some of these details are obtained from third party information.
CVE-2011-0486
Cross-site scripting (XSS) vulnerability in cognos.cgi in IBM Cognos 8 Business Intelligence (BI) 8.4.1 before FP1 allows remote attackers to inject arbitrary web script or HTML via the pathinfo parameter.
CVE-2011-0487
ICQ 7 does not verify the authenticity of updates, which allows man-in-the-middle attackers to execute arbitrary code via a crafted file that is fetched through an automatic-update mechanism.
CVE-2011-0488
Stack-based buffer overflow in NTWebServer.exe in the test web service in InduSoft NTWebServer, as distributed in Advantech Studio 6.1 and InduSoft Web Studio 7.0, allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via a long request to TCP port 80.
CVE-2011-0489
The server components in Objectivity/DB 10.0 do not require authentication for administrative commands, which allows remote attackers to modify data, obtain sensitive information, or cause a denial of service by sending requests over TCP to (1) the Lock Server or (2) the Advanced Multithreaded Server, as demonstrated by commands that are ordinarily sent by the (a) ookillls and (b) oostopams applications.  NOTE: some of these details are obtained from third party information.
