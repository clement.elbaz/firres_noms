CVE-2014-9752
Unrestricted file upload vulnerability in mods/_core/properties/lib/course.inc.php in ATutor before 2.2 patch 6 allows remote authenticated users to execute arbitrary PHP code by uploading a file with a PHP extension as a customicon for a new course, then accessing it via a direct request to the file in content/.
<a href="http://cwe.mitre.org/data/definitions/434.html">CWE-434: Unrestricted Upload of File with Dangerous Type</a>
CVE-2015-2924
The receive_ra function in rdisc/nm-lndp-rdisc.c in the Neighbor Discovery (ND) protocol implementation in the IPv6 stack in NetworkManager 1.x allows remote attackers to reconfigure a hop-limit setting via a small hop_limit value in a Router Advertisement (RA) message, a similar issue to CVE-2015-2922.
CVE-2015-2925
The prepend_path function in fs/dcache.c in the Linux kernel before 4.2.4 does not properly handle rename actions inside a bind mount, which allows local users to bypass an intended container protection mechanism by renaming a directory, related to a "double-chroot attack."
CVE-2015-5257
drivers/usb/serial/whiteheat.c in the Linux kernel before 4.2.4 allows physically proximate attackers to cause a denial of service (NULL pointer dereference and OOPS) or possibly have unspecified other impact via a crafted USB device.  NOTE: this ID was incorrectly used for an Apache Cordova issue that has the correct ID of CVE-2015-8320.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-5307
The KVM subsystem in the Linux kernel through 4.2.6, and Xen 4.3.x through 4.6.x, allows guest OS users to cause a denial of service (host OS panic or hang) by triggering many #AC (aka Alignment Check) exceptions, related to svm.c and vmx.c.
CVE-2015-7312
Multiple race conditions in the Advanced Union Filesystem (aufs) aufs3-mmap.patch and aufs4-mmap.patch patches for the Linux kernel 3.x and 4.x allow local users to cause a denial of service (use-after-free and BUG) or possibly gain privileges via a (1) madvise or (2) msync system call, related to mm/madvise.c and mm/msync.c.
CVE-2015-7712
Multiple eval injection vulnerabilities in mods/_standard/gradebook/edit_marks.php in ATutor 2.2 and earlier allow remote authenticated users with the AT_PRIV_GRADEBOOK privilege to execute arbitrary PHP code via the (1) asc or (2) desc parameter.
<a href="https://cwe.mitre.org/data/definitions/95.html">CWE-95: Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')</a>
CVE-2015-7815
Directory traversal vulnerability in core/ViewDataTable/Factory.php in Piwik before 2.15.0 allows remote attackers to include and execute arbitrary local files via the viewDataTable parameter.
CVE-2015-7816
The DisplayTopKeywords function in plugins/Referrers/Controller.php in Piwik before 2.15.0 allows remote attackers to conduct PHP object injection attacks, conduct Server-Side Request Forgery (SSRF) attacks, and execute arbitrary PHP code via a crafted HTTP header.
<a href="https://cwe.mitre.org/data/definitions/918.html">CWE-918: Server-Side Request Forgery (SSRF)</a>
CVE-2015-7872
The key_gc_unused_keys function in security/keys/gc.c in the Linux kernel through 4.2.6 allows local users to cause a denial of service (OOPS) via crafted keyctl commands.
CVE-2015-7897
The media scanning functionality in the face recognition library in android.media.process in Samsung Galaxy S6 Edge before G925VVRU4B0G9 allows remote attackers to gain privileges or cause a denial of service (memory corruption) via a crafted BMP image file.
CVE-2015-8104
The KVM subsystem in the Linux kernel through 4.2.6, and Xen 4.3.x through 4.6.x, allows guest OS users to cause a denial of service (host OS panic or hang) by triggering many #DB (aka Debug) exceptions, related to svm.c.
CVE-2015-8215
net/ipv6/addrconf.c in the IPv6 stack in the Linux kernel before 4.0 does not validate attempted changes to the MTU value, which allows context-dependent attackers to cause a denial of service (packet loss) via a value that is (1) smaller than the minimum compliant value or (2) larger than the MTU of an interface, as demonstrated by a Router Advertisement (RA) message that is not validated by a daemon, a different vulnerability than CVE-2015-0272.  NOTE: the scope of CVE-2015-0272 is limited to the NetworkManager product.
