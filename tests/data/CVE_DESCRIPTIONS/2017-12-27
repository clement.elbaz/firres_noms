CVE-2015-6237
The RPC service in Tripwire (formerly nCircle) IP360 VnE Manager 7.2.2 before 7.2.6 allows remote attackers to bypass authentication and (1) enumerate users, (2) reset passwords, or (3) manipulate IP filter restrictions via crafted "privileged commands."
CVE-2015-7324
Multiple cross-site scripting (XSS) vulnerabilities in helpers/comment.php in the StackIdeas Komento (com_komento) component before 2.0.5 for Joomla! allow remote attackers to inject arbitrary web script or HTML via the (1) img or (2) url tag of a new comment.
CVE-2015-7666
Multiple cross-site scripting (XSS) vulnerabilities in the (1) cp_updateMessageItem and (2) cp_deleteMessageItem functions in cp_ppp_admin_int_message_list.inc.php in the Payment Form for PayPal Pro plugin before 1.0.2 for WordPress allow remote attackers to inject arbitrary web script or HTML via the cal parameter.
CVE-2015-7667
Multiple cross-site scripting (XSS) vulnerabilities in (1) templates/admanagement/admanagement.php and (2) templates/adspot/adspot.php in the ResAds plugin before 1.0.2 for WordPress allow remote attackers to inject arbitrary web script or HTML via the page parameter.
CVE-2015-7668
Cross-site scripting (XSS) vulnerability in includes/MapPinImageSave.php in the Easy2Map plugin before 1.3.0 for WordPress allows remote attackers to inject arbitrary web script or HTML via the map_id parameter.
CVE-2015-7669
Multiple directory traversal vulnerabilities in (1) includes/MapImportCSV2.php and (2) includes/MapImportCSV.php in the Easy2Map plugin before 1.3.0 for WordPress allow remote attackers to include and execute arbitrary files via the csvfile parameter related to "upload file functionality."
CVE-2016-6914
Ubiquiti UniFi Video before 3.8.0 for Windows uses weak permissions for the installation directory, which allows local users to gain SYSTEM privileges via a Trojan horse taskkill.exe file.
CVE-2017-11695
Heap-based buffer overflow in the alloc_segs function in lib/dbm/src/hash.c in Mozilla Network Security Services (NSS) allows context-dependent attackers to have unspecified impact using a crafted cert8.db file.
CVE-2017-11696
Heap-based buffer overflow in the __hash_open function in lib/dbm/src/hash.c in Mozilla Network Security Services (NSS) allows context-dependent attackers to have unspecified impact using a crafted cert8.db file.
CVE-2017-11697
The __hash_open function in hash.c:229 in Mozilla Network Security Services (NSS) allows context-dependent attackers to cause a denial of service (floating point exception and crash) via a crafted cert8.db file.
CVE-2017-11698
Heap-based buffer overflow in the __get_page function in lib/dbm/src/h_page.c in Mozilla Network Security Services (NSS) allows context-dependent attackers to have unspecified impact using a crafted cert8.db file.
CVE-2017-1191
An undisclosed vulnerability in CLM applications (including IBM Rational Collaborative Lifecycle Management 4.0, 5.0, and 6.0) with potential for failure to restrict URL Access. IBM X-Force ID: 123661.
CVE-2017-13056
The launchURL function in PDF-XChange Viewer 2.5 (Build 314.0) might allow remote attackers to execute arbitrary code via a crafted PDF file.
CVE-2017-1365
IBM Team Concert (RTC including IBM Rational Collaborative Lifecycle Management 4.0, 5.0., and 6.0) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-force ID: 126858.
CVE-2017-16768
Cross-site scripting (XSS) vulnerability in User Policy editor in Synology MailPlus Server before 1.4.0-0415 allows remote authenticated users to inject arbitrary HTML via the name parameter.
CVE-2017-16897
A vulnerability has been discovered in the Auth0 passport-wsfed-saml2 library affecting versions < 3.0.5. This vulnerability allows an attacker to impersonate another user and potentially elevate their privileges if the SAML identity provider does not sign the full SAML response (e.g., only signs the assertion within the response).
CVE-2017-1698
IBM WebSphere Portal 7.0, 8.0, 8.5, and 9.0 could reveal sensitive information from an error message that could lead to further attacks against the system. IBM X-Force ID: 124390.
CVE-2017-16995
The check_alu_op function in kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (memory corruption) or possibly have unspecified other impact by leveraging incorrect sign extension.
CVE-2017-16996
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (memory corruption) or possibly have unspecified other impact by leveraging register truncation mishandling.
CVE-2017-17010
Untrusted search path vulnerability in Content Manager Assistant for PlayStation version 3.55.7671.0901 and earlier allows an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2017-17832
ServersCheck Monitoring Software before 14.2.3 is prone to a cross-site scripting vulnerability as user supplied-data is not validated/sanitized when passed in the settings_SMS_ALERT_TYPE parameter, and JavaScript can be executed on settings-save.html (the Settings - SMS Alerts page).
CVE-2017-17840
An issue was discovered in Open-iSCSI through 2.0.875. A local attacker can cause the iscsiuio server to abort or potentially execute code by sending messages with incorrect lengths, which (due to lack of checking) can lead to buffer overflows, and result in aborts (with overflow checking enabled) or code execution. The process_iscsid_broadcast function in iscsiuio/src/unix/iscsid_ipc.c does not validate the payload length before a write operation.
CVE-2017-17843
An issue was discovered in Enigmail before 1.9.9 that allows remote attackers to trigger use of an intended public key for encryption, because incorrect regular expressions are used for extraction of an e-mail address from a comma-separated list, as demonstrated by a modified Full Name field and a homograph attack, aka TBE-01-002.
CVE-2017-17844
An issue was discovered in Enigmail before 1.9.9. A remote attacker can obtain cleartext content by sending an encrypted data block (that the attacker cannot directly decrypt) to a victim, and relying on the victim to automatically decrypt that block and then send it back to the attacker as quoted text, aka the TBE-01-005 "replay" issue.
CVE-2017-17845
An issue was discovered in Enigmail before 1.9.9. Improper Random Secret Generation occurs because Math.Random() is used by pretty Easy privacy (pEp), aka TBE-01-001.
CVE-2017-17846
An issue was discovered in Enigmail before 1.9.9. Regular expressions are exploitable for Denial of Service, because of attempts to match arbitrarily long strings, aka TBE-01-003.
CVE-2017-17847
An issue was discovered in Enigmail before 1.9.9. Signature spoofing is possible because the UI does not properly distinguish between an attachment signature, and a signature that applies to the entire containing message, aka TBE-01-021. This is demonstrated by an e-mail message with an attachment that is a signed e-mail message in message/rfc822 format.
CVE-2017-17848
An issue was discovered in Enigmail before 1.9.9. In a variant of CVE-2017-17847, signature spoofing is possible for multipart/related messages because a signed message part can be referenced with a cid: URI but not actually displayed. In other words, the entire containing message appears to be signed, but the recipient does not see any of the signed text.
CVE-2017-17849
A buffer overflow vulnerability in GetGo Download Manager 5.3.0.2712 and earlier could allow remote HTTP servers to execute arbitrary code on NAS devices via a long response.
CVE-2017-17850
An issue was discovered in Asterisk 13.18.4 and older, 14.7.4 and older, 15.1.4 and older, and 13.18-cert1 and older. A select set of SIP messages create a dialog in Asterisk. Those SIP messages must contain a contact header. For those messages, if the header was not present and the PJSIP channel driver was used, Asterisk would crash. The severity of this vulnerability is somewhat mitigated if authentication is enabled. If authentication is enabled, a user would have to first be authorized before reaching the crash point.
CVE-2017-17852
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (memory corruption) or possibly have unspecified other impact by leveraging mishandling of 32-bit ALU ops.
CVE-2017-17853
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (memory corruption) or possibly have unspecified other impact by leveraging incorrect BPF_RSH signed bounds calculations.
CVE-2017-17854
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (integer overflow and memory corruption) or possibly have unspecified other impact by leveraging unrestricted integer values for pointer arithmetic.
CVE-2017-17855
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (memory corruption) or possibly have unspecified other impact by leveraging improper use of pointers in place of scalars.
CVE-2017-17856
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (memory corruption) or possibly have unspecified other impact by leveraging the lack of stack-pointer alignment enforcement.
CVE-2017-17857
The check_stack_boundary function in kernel/bpf/verifier.c in the Linux kernel through 4.14.8 allows local users to cause a denial of service (memory corruption) or possibly have unspecified other impact by leveraging mishandling of invalid variable stack read operations.
CVE-2017-17859
Samsung Internet Browser 6.2.01.12 allows remote attackers to bypass the Same Origin Policy, and conduct UXSS attacks to obtain sensitive information, via vectors involving an IFRAME element inside XSLT data in one part of an MHTML file. Specifically, JavaScript code in another part of this MHTML file does not have a document.domain value corresponding to the domain that is hosting the MHTML file, but instead has a document.domain value corresponding to an arbitrary URL within the content of the MHTML file.
CVE-2017-17862
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 ignores unreachable code, even though it would still be processed by JIT compilers. This behavior, also considered an improper branch-pruning logic issue, could possibly be used by local users for denial of service.
CVE-2017-17863
kernel/bpf/verifier.c in the Linux kernel 4.9.x through 4.9.71 does not check the relationship between pointer values and the BPF stack, which allows local users to cause a denial of service (integer overflow or invalid memory access) or possibly have unspecified other impact.
CVE-2017-17864
kernel/bpf/verifier.c in the Linux kernel through 4.14.8 mishandles states_equal comparisons between the pointer data type and the UNKNOWN_VALUE data type, which allows local users to obtain potentially sensitive address information, aka a "pointer leak."
CVE-2017-17866
pdf/pdf-write.c in Artifex MuPDF before 1.12.0 mishandles certain length changes when a repair operation occurs during a clean operation, which allows remote attackers to cause a denial of service (buffer overflow and application crash) or possibly have unspecified other impact via a crafted PDF document.
CVE-2017-17868
In Liferay Portal 6.1.0, the tags section has XSS via a Public Render Parameter (p_r_p) value, as demonstrated by p_r_p_564233524_tag.
CVE-2017-17869
The mgl-instagram-gallery plugin for WordPress has XSS via the single-gallery.php media parameter.
CVE-2017-17870
The JBuildozer extension 1.4.1 for Joomla! has SQL Injection via the appid parameter in an entriessearch action.
CVE-2017-17871
The "JEXTN Question And Answer" extension 3.1.0 for Joomla! has SQL Injection via the an parameter in a view=tags action, or the ques-srch parameter.
CVE-2017-17872
The JEXTN Video Gallery extension 3.0.5 for Joomla! has SQL Injection via the id parameter in a view=category action.
CVE-2017-17873
Vanguard Marketplace Digital Products PHP 1.4 has SQL Injection via the PATH_INFO to the /p URI.
CVE-2017-17874
Vanguard Marketplace Digital Products PHP 1.4 allows arbitrary file upload via an "Add a new product" or "Add a product preview" action, which can make a .php file accessible under a uploads/ URI.
CVE-2017-17875
The JEXTN FAQ Pro extension 4.0.0 for Joomla! has SQL Injection via the id parameter in a view=category action.
CVE-2017-17876
Biometric Shift Employee Management System 3.0 allows remote attackers to bypass intended file-read restrictions via a user=download request with a pathname in the path parameter.
CVE-2017-17877
An issue was discovered in Valve Steam Link build 643. When the SSH daemon is enabled for local development, the device is publicly available via IPv6 TCP port 22 over the internet (with stateless address autoconfiguration) by default, which makes it easier for remote attackers to obtain access by guessing 24 bits of the MAC address and attempting a root login. This can be exploited in conjunction with CVE-2017-17878.
CVE-2017-17878
An issue was discovered in Valve Steam Link build 643. Root passwords longer than 8 characters are truncated because of the default use of DES (aka the CONFIG_FEATURE_DEFAULT_PASSWD_ALGO="des" setting).
CVE-2017-17879
In ImageMagick 7.0.7-16 Q16 x86_64 2017-12-21, there is a heap-based buffer over-read in ReadOneMNGImage in coders/png.c, related to length calculation and caused by an off-by-one error.
CVE-2017-17880
In ImageMagick 7.0.7-16 Q16 x86_64 2017-12-21, there is a stack-based buffer over-read in WriteWEBPImage in coders/webp.c, related to a WEBP_DECODER_ABI_VERSION check.
CVE-2017-17881
In ImageMagick 7.0.7-12 Q16, a memory leak vulnerability was found in the function ReadMATImage in coders/mat.c, which allows attackers to cause a denial of service via a crafted MAT image file.
CVE-2017-17882
In ImageMagick 7.0.7-12 Q16, a memory leak vulnerability was found in the function ReadXPMImage in coders/xpm.c, which allows attackers to cause a denial of service via a crafted XPM image file.
CVE-2017-17883
In ImageMagick 7.0.7-12 Q16, a memory leak vulnerability was found in the function ReadPGXImage in coders/pgx.c, which allows attackers to cause a denial of service via a crafted PGX image file.
CVE-2017-17884
In ImageMagick 7.0.7-16 Q16, a memory leak vulnerability was found in the function WriteOnePNGImage in coders/png.c, which allows attackers to cause a denial of service via a crafted PNG image file.
CVE-2017-17885
In ImageMagick 7.0.7-12 Q16, a memory leak vulnerability was found in the function ReadPICTImage in coders/pict.c, which allows attackers to cause a denial of service via a crafted PICT image file.
CVE-2017-17886
In ImageMagick 7.0.7-12 Q16, a memory leak vulnerability was found in the function ReadPSDChannelZip in coders/psd.c, which allows attackers to cause a denial of service via a crafted psd image file.
CVE-2017-17887
In ImageMagick 7.0.7-16 Q16, a memory leak vulnerability was found in the function GetImagePixelCache in magick/cache.c, which allows attackers to cause a denial of service via a crafted MNG image file that is processed by ReadOneMNGImage.
CVE-2017-17888
cgi-bin/write.cgi in Anti-Web through 3.8.7, as used on NetBiter / HMS, Ouman EH-net, Alliance System WS100 --> AWU 500, Sauter ERW100F001, Carlo Gavazzi SIU-DLG, AEDILIS SMART-1, SYXTHSENSE WebBiter, ABB SREA, and ASCON DY WebServer devices, allows remote authenticated users to execute arbitrary OS commands via crafted multipart/form-data content, a different vulnerability than CVE-2017-9097.
CVE-2017-17891
Readymade Video Sharing Script has CSRF via user-profile-edit.php.
CVE-2017-17892
Readymade Video Sharing Script has SQL Injection via the viewsubs.php chnlid parameter or the search_video.php search parameter.
CVE-2017-17893
Readymade Video Sharing Script has XSS via the search_video.php search parameter, the viewsubs.php chnlid parameter, or the user-profile-edit.php fname parameter.
CVE-2017-17894
Readymade Job Site Script has CSRF via the /job URI.
CVE-2017-17895
Readymade Job Site Script has SQL Injection via the location_name array parameter to the /job URI.
CVE-2017-17896
Readymade Job Site Script has XSS via the keyword parameter to the /job URI.
CVE-2017-17897
SQL injection vulnerability in comm/multiprix.php in Dolibarr ERP/CRM version 6.0.4 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2017-17898
Dolibarr ERP/CRM version 6.0.4 does not block direct requests to *.tpl.php files, which allows remote attackers to obtain sensitive information.
CVE-2017-17899
SQL injection vulnerability in adherents/subscription/info.php in Dolibarr ERP/CRM version 6.0.4 allows remote attackers to execute arbitrary SQL commands via the rowid parameter.
CVE-2017-17900
SQL injection vulnerability in fourn/index.php in Dolibarr ERP/CRM version 6.0.4 allows remote attackers to execute arbitrary SQL commands via the socid parameter.
CVE-2017-17903
FS Lynda Clone has CSRF via user/edit_profile, as demonstrated by adding content to the user panel.
CVE-2017-17904
FS Lynda Clone has XSS via the keywords parameter to tutorial/ or the edit_profile_first_name parameter to user/edit_profile.
CVE-2017-17905
PHP Scripts Mall Car Rental Script has CSRF via admin/sitesettings.php.
CVE-2017-17906
PHP Scripts Mall Car Rental Script has SQL Injection via the admin/carlistedit.php carid parameter.
CVE-2017-17907
PHP Scripts Mall Car Rental Script has XSS via the admin/areaedit.php carid parameter or the admin/sitesettings.php websitename parameter.
CVE-2017-17908
PHP Scripts Mall Responsive Realestate Script has CSRF via admin/general.
CVE-2017-17909
PHP Scripts Mall Responsive Realestate Script has XSS via the admin/general.php gplus parameter.
CVE-2017-17911
packages/core/contact.php in Archon 3.21 rev-1 has XSS in the referer parameter in an index.php?p=core/contact request, aka Open Bug Bounty ID OBB-278503.
CVE-2017-17912
In GraphicsMagick 1.4 snapshot-20171217 Q8, there is a heap-based buffer over-read in ReadNewsProfile in coders/tiff.c, in which LocaleNCompare reads heap data beyond the allocated region.
CVE-2017-17913
In GraphicsMagick 1.4 snapshot-20171217 Q8, there is a stack-based buffer over-read in WriteWEBPImage in coders/webp.c, related to an incompatibility with libwebp versions, 0.5.0 and later, that use a different structure type.
CVE-2017-17914
In ImageMagick 7.0.7-16 Q16, a vulnerability was found in the function ReadOnePNGImage in coders/png.c, which allows attackers to cause a denial of service (ReadOneMNGImage large loop) via a crafted mng image file.
CVE-2017-17915
In GraphicsMagick 1.4 snapshot-20171217 Q8, there is a heap-based buffer over-read in ReadMNGImage in coders/png.c, related to accessing one byte before testing whether a limit has been reached.
CVE-2017-17924
PHP Scripts Mall Professional Service Script allows remote attackers to obtain sensitive full-path information via the id parameter to admin/review_userwise.php.
CVE-2017-17925
PHP Scripts Mall Professional Service Script has XSS via the admin/general_settingupd.php website_title parameter.
CVE-2017-17926
PHP Scripts Mall Professional Service Script has a predicable registration URL, which makes it easier for remote attackers to register with an invalid or spoofed e-mail address.
CVE-2017-17927
PHP Scripts Mall Professional Service Script allows remote attackers to obtain sensitive full-path information via a crafted PATH_INFO to service-list/category/.
CVE-2017-17928
PHP Scripts Mall Professional Service Script has SQL injection via the admin/review.php id parameter.
CVE-2017-17929
PHP Scripts Mall Professional Service Script has XSS via the admin/bannerview.php view parameter.
CVE-2017-17930
PHP Scripts Mall Professional Service Script has CSRF via admin/general_settingupd.php, as demonstrated by modifying a setting in the user panel.
CVE-2017-17931
PHP Scripts Mall Resume Clone Script has SQL Injection via the forget.php username parameter.
CVE-2017-17934
ImageMagick 7.0.7-17 Q16 x86_64 has memory leaks in coders/msl.c, related to MSLPopImage and ProcessMSLScript, and associated with mishandling of MSLPushImage calls.
CVE-2017-17935
The File_read_line function in epan/wslua/wslua_file.c in Wireshark through 2.2.11 does not properly strip '\n' characters, which allows remote attackers to cause a denial of service (buffer underflow and application crash) via a crafted packet that triggers the attempted processing of an empty line.
CVE-2017-7152
An issue was discovered in certain Apple products. iOS before 11.2 is affected. The issue involves the "Mail Message Framework" component. It allows remote attackers to spoof the address bar via a crafted web site.
CVE-2017-7154
An issue was discovered in certain Apple products. iOS before 11.2 is affected. macOS before 10.13.2 is affected. tvOS before 11.2 is affected. The issue involves the "Kernel" component. It allows local users to bypass intended memory-read restrictions or cause a denial of service (system crash).
CVE-2017-7155
An issue was discovered in certain Apple products. macOS before 10.13.2 is affected. The issue involves the "Intel Graphics Driver" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2017-7156
An issue was discovered in certain Apple products. iOS before 11.2 is affected. Safari before 11.0.2 is affected. iCloud before 7.2 on Windows is affected. iTunes before 12.7.2 on Windows is affected. tvOS before 11.2 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site.
CVE-2017-7157
An issue was discovered in certain Apple products. iOS before 11.2 is affected. Safari before 11.0.2 is affected. iCloud before 7.2 on Windows is affected. iTunes before 12.7.2 on Windows is affected. tvOS before 11.2 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site.
CVE-2017-7158
An issue was discovered in certain Apple products. macOS before 10.13.2 is affected. The issue involves the "Screen Sharing Server" component. It allows attackers to obtain root privileges for reading files by leveraging screen-sharing access.
CVE-2017-7159
An issue was discovered in certain Apple products. macOS before 10.13.2 is affected. The issue involves the "IOAcceleratorFamily" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2017-7160
An issue was discovered in certain Apple products. iOS before 11.2 is affected. Safari before 11.0.2 is affected. iCloud before 7.2 on Windows is affected. iTunes before 12.7.2 on Windows is affected. tvOS before 11.2 is affected. The issue involves the "WebKit" component. It allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site.
CVE-2017-7162
An issue was discovered in certain Apple products. iOS before 11.2 is affected. macOS before 10.13.2 is affected. tvOS before 11.2 is affected. watchOS before 4.2 is affected. The issue involves the "IOKit" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2017-7163
An issue was discovered in certain Apple products. macOS before 10.13.2 is affected. The issue involves the "Intel Graphics Driver" component. It allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2017-9608
The dnxhd decoder in FFmpeg before 3.2.6, and 3.3.x before 3.3.3 allows remote attackers to cause a denial of service (NULL pointer dereference) via a crafted mov file.
CVE-2017-9944
A vulnerability has been identified in Siemens 7KT PAC1200 data manager (7KT1260) in all versions < V2.03. The integrated web server (port 80/tcp) of the affected devices could allow an unauthenticated remote attacker to perform administrative operations over the network.
