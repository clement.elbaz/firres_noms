CVE-2012-1622
Apache OFBiz 10.04.x before 10.04.02 allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2012-4377
Cross-site scripting (XSS) vulnerability in MediaWiki before 1.18.5 and 1.19.x before 1.19.2 allows remote attackers to inject arbitrary web script or HTML via a File: link to a nonexistent image.
CVE-2012-4378
Multiple cross-site scripting (XSS) vulnerabilities in MediaWiki before 1.18.5 and 1.19.x before 1.19.2, when unspecified JavaScript gadgets are used, allow remote attackers to inject arbitrary web script or HTML via the userlang parameter to w/index.php.
CVE-2014-2023
Multiple SQL injection vulnerabilities in the Tapatalk plugin 4.9.0 and earlier and 5.x through 5.2.1 for vBulletin allow remote attackers to execute arbitrary SQL commands via a crafted xmlrpc API request to (1) unsubscribe_forum.php or (2) unsubscribe_topic.php in mobiquo/functions/.
CVE-2017-12158
It was found that Keycloak would accept a HOST header URL in the admin console and use it to determine web resource locations. An attacker could use this flaw against an authenticated user to attain reflected XSS via a malicious server.
CVE-2017-12159
It was found that the cookie used for CSRF prevention in Keycloak was not unique to each session. An attacker could use this flaw to gain access to an authenticated user session, leading to possible information disclosure or further attacks.
CVE-2017-12160
It was found that Keycloak oauth would permit an authenticated resource to obtain an access/refresh token pair from the authentication server, permitting indefinite usage in the case of permission revocation. An attacker on an already compromised resource could use this flaw to grant himself continued permissions and possibly conduct further attacks.
CVE-2017-1220
IBM Tivoli Endpoint Manager (IBM BigFix Platform 9.2 and 9.5) discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 123860.
CVE-2017-1222
IBM Tivoli Endpoint Manager (IBM BigFix Platform 9.2 and 9.5) does not perform an authentication check for a critical resource or functionality allowing anonymous users access to protected areas. IBM X-Force ID: 123862.
CVE-2017-1225
IBM Tivoli Endpoint Manager (IBM BigFix Platform 9.2 and 9.5) stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history. IBM X-Force ID: 123904.
CVE-2017-1226
IBM Tivoli Endpoint Manager (IBM BigFix Platform 9.2 and 9.5) generates an error message in error logs that includes sensitive information about its environment which could be used in further attacks against the system. IBM X-Force ID: 123905.
CVE-2017-1228
IBM Tivoli Endpoint Manager (IBM BigFix Platform 9.2 and 9.5) could allow a remote attacker to obtain sensitive information, caused by the failure to properly enable the secure cookie attribute. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques. IBM X-Force ID: 123907.
CVE-2017-1230
IBM Tivoli Endpoint Manager (IBM BigFix Platform 9.2 and 9.5) uses insufficiently random numbers or values in a security context that depends on unpredictable numbers. This weakness may allow attackers to expose sensitive information by guessing tokens or identifiers. IBM X-Force ID: 123909.
CVE-2017-1232
IBM Tivoli Endpoint Manager (IBM BigFix Platform 9.2 and 9.5) transmits sensitive or security-critical data in cleartext in a communication channel that can be sniffed by unauthorized actors. IBM X-Force ID: 123911.
CVE-2017-15096
A flaw was found in GlusterFS in versions prior to 3.10. A null pointer dereference in send_brick_req function in glusterfsd/src/gf_attach.c may be used to cause denial of service.
CVE-2017-1521
IBM Tivoli Endpoint Manager (for Lifecycle/Power/Patch) Platform and Applications (IBM BigFix Platform 9.2 and 9.5) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 129831.
CVE-2017-15366
Before Thornberry NDoc version 8.0, laptop clients and the server have default database (Cache) users set up with a single password. This password is left behind in a cleartext log file during client installation on laptops. This password can be used to gain full admin/system access to client devices (if no firewall is present) or the NDoc server itself. Once the password is known to an attacker, local access is not required.
CVE-2017-15882
The London Trust Media Private Internet Access (PIA) application before 1.3.3.1 for Android allows remote attackers to cause a denial of service (application crash) via a large VPN server-list file.
CVE-2017-15906
The process_open function in sftp-server.c in OpenSSH before 7.6 does not properly prevent write operations in readonly mode, which allows attackers to create zero-length files.
CVE-2017-15907
SQL injection vulnerability in phpCollab 2.5.1 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter to newsdesk/newsdesk.php.
CVE-2017-15908
In systemd 223 through 235, a remote DNS server can respond with a custom crafted DNS NSEC resource record to trigger an infinite loop in the dns_packet_read_type_window() function of the 'systemd-resolved' service and cause a DoS of the affected service.
CVE-2017-15909
D-Link DGS-1500 Ax devices before 2.51B021 have a hardcoded password, which allows remote attackers to obtain shell access.
CVE-2017-15911
The Admin Console in Ignite Realtime Openfire Server before 4.1.7 allows arbitrary client-side JavaScript code execution on victims who click a crafted setup/setup-host-settings.jsp?domain= link, aka XSS. Session ID and data theft may follow as well as the possibility of bypassing CSRF protections, injection of iframes to establish communication channels, etc. The vulnerability is present after login into the application.
CVE-2017-15917
In Paessler PRTG Network Monitor 17.3.33.2830, it's possible to create a Map as a read-only user, by forging a request and sending it to the server.
CVE-2017-15919
The ultimate-form-builder-lite plugin before 1.3.7 for WordPress has SQL Injection, with resultant PHP Object Injection, via wp-admin/admin-ajax.php.
CVE-2017-15922
In GNU Libextractor 1.4, there is an out-of-bounds read in the EXTRACTOR_dvi_extract_method function in plugins/dvi_extractor.c.
CVE-2017-3771
System boot process is not adequately secured In Lenovo E95 and ThinkCentre M710s/M710t because systems were shipped from factory without completing BIOS/UEFI initialization process.
CVE-2017-5996
The agent in Bomgar Remote Support 15.2.x before 15.2.3, 16.1.x before 16.1.5, and 16.2.x before 16.2.4 allows DLL hijacking because of weak %SYSTEMDRIVE%\ProgramData permissions.
CVE-2017-7335
A Cross-Site Scripting (XSS) vulnerability in Fortinet FortiWLC 6.1-x (6.1-2, 6.1-4 and 6.1-5); 7.0-x (7.0-7, 7.0-8, 7.0-9, 7.0-10); and 8.x (8.0, 8.1, 8.2 and 8.3.0-8.3.2) allows an authenticated user to inject arbitrary web script or HTML via non-sanitized parameters "refresh" and "branchtotable" present in HTTP POST requests.
CVE-2017-7341
An OS Command Injection vulnerability in Fortinet FortiWLC 6.1-2 through 6.1-5, 7.0-7 through 7.0-10, 8.0 through 8.2, and 8.3.0 through 8.3.2 file management AP script download webUI page allows an authenticated admin user to execute arbitrary system console commands via crafted HTTP requests.
CVE-2017-7732
A reflected Cross-Site Scripting (XSS) vulnerability in Fortinet FortiMail 5.1 and earlier, 5.2.0 through 5.2.9, and 5.3.0 through 5.3.9 customized pre-authentication webmail login page allows attacker to inject arbitrary web script or HTML via crafted HTTP requests.
