CVE-2007-3017
The WYSIWYG editor applet in activeWeb contentserver CMS before 5.6.2964 only filters malicious tags from articles sent to admin/applets/wysiwyg/rendereditor.asp, which allows remote authenticated users to inject arbitrary JavaScript via a request to admin/worklist/worklist_edit.asp.
CVE-2007-3018
activeWeb contentserver CMS before 5.6.2964 does not limit the file-creation ability of editors who have restricted accounts, which allows these editors to create files in arbitrary directories.
CVE-2007-3796
The password reset feature in the Spam Quarantine HTTP interface for MailMarshal SMTP 6.2.0.x before 6.2.1 allows remote attackers to modify arbitrary account information via a UserId variable with a large amount of trailing whitespace followed by a malicious value, which triggers SQL buffer truncation due to length inconsistencies between variables.
"Successful exploitation requires that the attacker knows the email address of the target user."
The vendor has released version 6.2.1 to address this issue.
CVE-2007-3806
The glob function in PHP 5.2.3 allows context-dependent attackers to cause a denial of service and possibly execute arbitrary code via an invalid value of the flags parameter, probably related to memory corruption or an invalid read on win32 platforms, and possibly related to lack of initialization for a glob structure.
CVE-2007-3807
Multiple cross-site scripting (XSS) vulnerabilities in SiteScape Forum before 7.3 allow remote attackers to inject arbitrary web script or HTML via the user name field in the login procedure, and other unspecified vectors.
CVE-2007-3808
SQL injection vulnerability in includes/search.php in paFileDB 3.6 allows remote attackers to execute arbitrary SQL commands via the categories[] parameter in a search action to index.php, a different vector than CVE-2005-2000.
CVE-2007-3809
Multiple SQL injection vulnerabilities in Prozilla Directory Script allow remote attackers to execute arbitrary SQL commands via the cat_id parameter in a list action to directory.php, and other unspecified vectors.
CVE-2007-3810
SQL injection vulnerability in index.php in Realtor 747 allows remote attackers to execute arbitrary SQL commands via the categoryid parameter.
CVE-2007-3811
Multiple SQL injection vulnerabilities in eSyndiCat allow remote attackers to execute arbitrary SQL commands via (1) the id parameter to news.php or (2) the name parameter to page.php.
CVE-2007-3812
SQL injection vulnerability in forums.php in CMScout 1.23 and earlier allows remote attackers to execute arbitrary SQL commands via the f parameter in a forums action to index.php.
CVE-2007-3813
PHP remote file inclusion vulnerability in include/user.php in the NoBoard BETA module for MKPortal allows remote attackers to execute arbitrary PHP code via a URL in the MK_PATH parameter.
CVE-2007-3814
Multiple SQL injection vulnerabilities in MKPortal 1.1.1 allow remote attackers to execute arbitrary SQL commands via (1) the idurlo field in the delete_urlo function in (a) index.php in the urlobox module; the iden field in the (2) update_file and (3) del_file functions in (b) index.php in the reviews module; the (4) idnews field in the delete_news function and the (5) idcomm field in the del_comment function in (c) index.php in the news module; the (6) idcomm field in the delete_comments function in (d) index.php in the gallery module; the iden field in the (7) edit_file, (8) update_file, and (9) del_file functions in index.php in the gallery module; the (10) ide and (11) cat fields in the slide_update function in index.php in the gallery module; the iden field in the (12) update_file and (13) del_file functions in (d) index.php in the downloads module; and other unspecified vectors.
CVE-2007-3815
Buffer overflow in pirs32.exe in Poslovni informator Republike Slovenije (PIRS) 2007 allows local users to cause a denial of service (application crash) and possibly execute arbitrary code via a long search string in certain fields in the GUI.  NOTE: this may cross privilege boundaries if PIRS is used by data-entry workers who do not have full access to the underlying Windows environment.
CVE-2007-3816
** DISPUTED **  JWIG might allow context-dependent attackers to cause a denial of service (service degradation) via loops of references to external templates.  NOTE: this issue has been disputed by multiple third parties who state that only the application developer can trigger the issue, so no privilege boundaries are crossed.  However, it seems possible that this is a vulnerability class to which an JWIG application may be vulnerable if template contents can be influenced, but this would be an issue in the application itself, not JWIG.
CVE-2007-3817
Cross-site scripting (XSS) vulnerability in the LoginToboggan module 4.7.x-1.0, 4.7.x-1.x-dev, and 5.x-1.x-dev before 20070712 for Drupal, when configured to display a "Log out" link, allows remote attackers to inject arbitrary web script or HTML via a crafted username.  NOTE: Drupal sanitizes the username by removing certain characters, so this might not be a vulnerability on default installations.
CVE-2007-3818
Cross-site scripting (XSS) vulnerability in the LoginToboggan module 5.x-1.x-dev before 20070712 for Drupal allows remote authenticated users with "administer blocks" permission to inject arbitrary JavaScript and gain privileges via "the message displayed above the default user login block."
CVE-2007-3819
Opera 9.21 allows remote attackers to spoof the data: URI scheme in the address bar via a long URI with trailing whitespace, which prevents the beginning of the URI from being displayed.
CVE-2007-3820
konqueror/konq_combo.cc in Konqueror 3.5.7 allows remote attackers to spoof the data: URI scheme in the address bar via a long URI with trailing whitespace, which prevents the beginning of the URI from being displayed.
CVE-2007-3821
Cross-site request forgery (CSRF) vulnerability in Webcit before 7.11 allows remote attackers to modify configurations and perform other actions as arbitrary users via unspecified vectors.
CVE-2007-3822
Multiple cross-site scripting (XSS) vulnerabilities in Webcit before 7.11 allow remote attackers to inject arbitrary web script or HTML via (1) the who parameter to showuser; and other vectors involving (2) calendar mode, (3) bulletin board mode, (4) room names, and (5) uploaded file names.
CVE-2007-3823
The Logging Server (Logsrv.exe) in IPSwitch WS_FTP 7.5.29.0 allows remote attackers to cause a denial of service (daemon crash) by sending a crafted packet containing a long string to port 5151/udp.
CVE-2007-3824
SQL injection vulnerability in katgoster.asp in MzK Blog (tr) allows remote attackers to execute arbitrary SQL commands via the katID parameter.
CVE-2007-3826
Microsoft Internet Explorer 7 on Windows XP SP2 allows remote attackers to prevent users from leaving a site, spoof the address bar, and conduct phishing and other attacks via repeated document.open function calls after a user requests a new page, but before the onBeforeUnload function is called.
CVE-2007-3827
Mozilla Firefox allows for cookies to be set with a null domain (aka "domainless cookies"), which allows remote attackers to pass information between arbitrary domains and track user activity, as demonstrated by the domain attribute in the document.cookie variable in a javascript: window.
CVE-2007-3828
Unspecified vulnerability in mDNSResponder in Apple Mac OS X allows remote attackers to execute arbitrary code via unspecified vectors, a related issue to CVE-2007-2386.
CVE-2007-3829
Multiple stack-based buffer overflows in (a) InterActual Player 2.60.12.0717 and (b) Roxio CinePlayer 3.2 allow remote attackers to execute arbitrary code via a (1) long FailURL attribute in the IAMCE ActiveX Control (IAMCE.dll) or a (2) long URLCode attribute in the IAKey ActiveX Control (IAKey.dll).  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3830
Cross-site scripting (XSS) vulnerability in alert.php in ISS Proventia Network IPS GX5108 1.3 and GX5008 1.5 allows remote attackers to inject arbitrary web script or HTML via the reminder parameter.
CVE-2007-3831
PHP remote file inclusion in main.php in ISS Proventia Network IPS GX5108 1.3 and GX5008 1.5 allows remote attackers to execute arbitrary PHP code via a URL in the page parameter.
CVE-2007-3832
Buffer overflow in the AOL Instant Messenger (AIM) protocol handler in AIM.DLL in Cerulean Studios Trillian allows remote attackers to execute arbitrary code via a malformed aim: URI, as demonstrated by a long URI beginning with the aim:///#1111111/ substring.
CVE-2007-3833
The AOL Instant Messenger (AIM) protocol handler in Cerulean Studios Trillian allows remote attackers to create files with arbitrary contents via certain aim: URIs, as demonstrated by a URI that begins with the "aim: &c:\" substring and contains a full pathname in the ini field.  NOTE: this can be leveraged for code execution by writing to a Startup folder.
CVE-2007-3834
Multiple cross-site scripting (XSS) vulnerabilities in Ex Libris ALEPH allow remote attackers to inject arbitrary web script or HTML via unspecified vectors related to a URL that can be discovered through a keyword search.  NOTE: this may be related to the MetaLib XSS issue, CVE-2007-3835.
CVE-2007-3835
Cross-site scripting (XSS) vulnerability in Ex Libris MetaLib 3.13 and 4 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to a resource id that can be discovered through a search.
CVE-2007-3836
Format string vulnerability in HydraIRC 0.3.151 allows remote attackers to cause a denial of service via format string specifiers in certain data related to failed DCC file transfer negotiation.
CVE-2007-3837
Heap-based buffer overflow in HydraIRC 0.3.151 allows remote IRC servers to cause a denial of service (application crash) via a long CTCP request message containing '%' (percent) characters.
CVE-2007-3838
Cross-site scripting (XSS) vulnerability in takeprofedit.php in TBDev.NET DR 11-10-05-BETA-SF1:111005 and earlier allows remote attackers to inject arbitrary web script or HTML via the SRC attribute of a SCRIPT element in the avatar parameter.  NOTE: this may be related to the tracker program in the Janitor package.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3839
Cross-site scripting (XSS) vulnerability in takeprofedit.php in TBDev.NET DR 010306 and earlier allows remote attackers to inject arbitrary web script or HTML via a javascript: URI in the avatar parameter.  NOTE: this may be related to the tracker program in the Janitor package.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3840
SQL injection vulnerability in referralUrl.php in Traffic Stats allows remote attackers to execute arbitrary SQL commands via the offset parameter.
CVE-2007-3841
Unspecified vulnerability in Pidgin (formerly Gaim) 2.0.2 for Linux allows remote authenticated users, who are listed in a users list, to execute certain commands via unspecified vectors, aka ZD-00000035. NOTE: this information is based upon a vague advisory by a vulnerability information sales organization that does not coordinate with vendors or release actionable advisories. A CVE has been assigned for tracking purposes, but duplicates with other CVEs are difficult to determine.
CVE-2007-3842
Cross-site scripting (XSS) vulnerability in the 8e6 R3000 Enterprise Filter before 2.0.05 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: this may be the same as CVE-2007-2970.
