CVE-2015-5159
python-kdcproxy before 0.3.2 allows remote attackers to cause a denial of service via a large POST request.
CVE-2015-7266
The Interactive Advertising Bureau (IAB) OpenRTB 2.3 protocol implementation might allow remote attackers to conceal the status of ad transactions and potentially compromise bid integrity by leveraging failure to limit the time between bid responses and impression notifications, aka the Amnesia Bug.
CVE-2017-8931
Bitdefender GravityZone VMware appliance before 6.2.1-35 might allow attackers to gain access with root privileges via unspecified vectors.
CVE-2018-0734
The OpenSSL DSA signature algorithm has been shown to be vulnerable to a timing side channel attack. An attacker could use variations in the signing algorithm to recover the private key. Fixed in OpenSSL 1.1.1a (Affected 1.1.1). Fixed in OpenSSL 1.1.0j (Affected 1.1.0-1.1.0i). Fixed in OpenSSL 1.0.2q (Affected 1.0.2-1.0.2p).
CVE-2018-10532
An issue was discovered on EE 4GEE HH70VB-2BE8GB3 HH70_E1_02.00_19 devices. Hardcoded root SSH credentials were discovered to be stored within the "core_app" binary utilised by the EE router for networking services. An attacker with knowledge of the default password (oelinux123) could login to the router via SSH as the root user, which could allow for the loss of confidentiality, integrity, and availability of the system. This would also allow for the bypass of the "AP Isolation" mode that is supported by the router, as well as the settings for multiple Wireless networks, which a user may use for guest clients.
CVE-2018-10709
The AsrDrv101.sys and AsrDrv102.sys low-level drivers in ASRock RGBLED before v1.0.35.1, A-Tuning before v3.0.210, F-Stream before v3.0.210, and RestartToUEFI before v1.0.6.2 expose functionality to read and write CR register values. This could be leveraged in a number of ways to ultimately run code with elevated privileges.
CVE-2018-10710
The AsrDrv101.sys and AsrDrv102.sys low-level drivers in ASRock RGBLED before v1.0.35.1, A-Tuning before v3.0.210, F-Stream before v3.0.210, and RestartToUEFI before v1.0.6.2 expose functionality to read and write arbitrary physical memory. This could be leveraged by a local attacker to elevate privileges.
CVE-2018-10711
The AsrDrv101.sys and AsrDrv102.sys low-level drivers in ASRock RGBLED before v1.0.35.1, A-Tuning before v3.0.210, F-Stream before v3.0.210, and RestartToUEFI before v1.0.6.2 expose functionality to read and write Machine Specific Registers (MSRs). This could be leveraged to execute arbitrary ring-0 code.
CVE-2018-10712
The AsrDrv101.sys and AsrDrv102.sys low-level drivers in ASRock RGBLED before v1.0.35.1, A-Tuning before v3.0.210, F-Stream before v3.0.210, and RestartToUEFI before v1.0.6.2 expose functionality to read/write data from/to IO ports. This could be leveraged in a number of ways to ultimately run code with elevated privileges.
CVE-2018-14558
An issue was discovered on Tenda AC7 devices with firmware through V15.03.06.44_CN(AC7), AC9 devices with firmware through V15.03.05.19(6318)_CN(AC9), and AC10 devices with firmware through V15.03.06.23_CN(AC10). A command Injection vulnerability allows attackers to execute arbitrary OS commands via a crafted goform/setUsbUnload request. This occurs because the "formsetUsbUnload" function executes a dosystemCmd function with untrusted input.
CVE-2018-16461
A command injection vulnerability in libnmapp package for versions <0.4.16 allows arbitrary commands to be executed via arguments to the range options.
CVE-2018-16462
A command injection vulnerability in the apex-publish-static-files npm module version <2.0.1 which allows arbitrary shell command execution through a maliciously crafted argument.
CVE-2018-16463
A bug causing session fixation in Nextcloud Server prior to 14.0.0, 13.0.3 and 12.0.8 could potentially allow an attacker to obtain access to password protected shares.
CVE-2018-16464
A missing access check in Nextcloud Server prior to 14.0.0 could lead to continued access to password protected link shares when the owner had changed the password.
CVE-2018-16465
Missing state in Nextcloud Server prior to 14.0.0 would not enforce the use of a second factor at login if the the provider of the second factor failed to load.
CVE-2018-16466
Improper revalidation of permissions in Nextcloud Server prior to 14.0.0, 13.0.6 and 12.0.11 lead to not accepting access restrictions by acess tokens.
CVE-2018-16467
A missing check in Nextcloud Server prior to 14.0.0 could give unauthorized access to the previews of single file password protected shares.
CVE-2018-16468
In the Loofah gem for Ruby, through v2.2.2, unsanitized JavaScript may occur in sanitized output when a crafted SVG element is republished.
CVE-2018-16469
The merge.recursive function in the merge package <1.2.1 can be tricked into adding or modifying properties of the Object prototype. These properties will be present on all objects allowing for a denial of service attack.
CVE-2018-17782
A cross-site scripting (XSS) vulnerability in the Manage Filters page (manage_filter_page.php) in MantisBT 2.1.0 through 2.17.1 allows remote attackers (if access rights permit it) to inject arbitrary code (if CSP settings permit it) through a crafted project name.
CVE-2018-17783
A cross-site scripting (XSS) vulnerability in the Edit Filter page (manage_filter_edit page.php) in MantisBT 2.1.0 through 2.17.1 allows remote attackers (if access rights permit it) to inject arbitrary code (if CSP settings permit it) through a crafted project name.
CVE-2018-17931
If an attacker has physical access to the VGo Robot (Versions 3.0.3.52164 and 3.0.3.53662. Prior versions may also be affected) they may be able to alter scripts, which may allow code execution with root privileges.
CVE-2018-17933
VGo Robot (Versions 3.0.3.52164 and 3.0.3.53662. Prior versions may also be affected) connected to the VGo XAMPP. User accounts may be able to execute commands that are outside the scope of their privileges and within the scope of an admin account. If an attacker has access to VGo XAMPP Client credentials, they may be able to execute admin commands on the connected robot.
CVE-2018-18281
Since Linux kernel version 3.2, the mremap() syscall performs TLB flushes after dropping pagetable locks. If a syscall such as ftruncate() removes entries from the pagetables of a task that is in the middle of mremap(), a stale TLB entry can remain for a short time that permits access to a physical page after it has been released back to the page allocator and reused. This is fixed in the following kernel versions: 4.9.135, 4.14.78, 4.18.16, 4.19.
CVE-2018-18817
The Leostream Agent before Build 7.0.1.0 when used with Leostream Connection Broker 8.2.72 or earlier allows remote attackers to modify registry keys via the Leostream Agent API.
CVE-2018-18822
Grapixel New Media v2.0 allows SQL Injection via the pages.aspx pageref parameter.
CVE-2018-18825
Pagoda Linux panel V6.0 has XSS via the verification code associated with an invalid account login. A crafted code is mishandled during rendering of the login log.
CVE-2018-18826
There exists a heap-based buffer overflow in vc1_decode_p_mb_intfi in vc1_block.c in Libav 12.3, which allows attackers to cause a denial-of-service via a crafted aac file.
CVE-2018-18827
There exists a heap-based buffer over-read in ff_vc1_pred_dc in vc1_block.c in Libav 12.3, which allows attackers to cause a denial-of-service via a crafted aac file.
CVE-2018-18828
There exists a heap-based buffer overflow in vc1_decode_i_block_adv in vc1_block.c in Libav 12.3, which allows attackers to cause a denial-of-service via a crafted aac file.
CVE-2018-18829
There exists a NULL pointer dereference in ff_vc1_parse_frame_header_adv in vc1.c in Libav 12.3, which allows attackers to cause a denial-of-service through a crafted aac file.
CVE-2018-18830
An issue was discovered in com\mingsoft\basic\action\web\FileAction.java in MCMS 4.6.5. Since the upload interface does not verify the user login status, you can use this interface to upload files without setting a cookie. First, start an upload of JSP code with a .png filename, and then intercept the data packet. In the name parameter, change the suffix to jsp. In the response, the server returns the storage path of the file, which can be accessed to execute arbitrary JSP code.
CVE-2018-18831
An issue was discovered in com\mingsoft\cms\action\GeneraterAction.java in MCMS 4.6.5. An attacker can write a .jsp file (in the position parameter) to an arbitrary directory via a ../ Directory Traversal in the url parameter.
CVE-2018-18832
admin/check.asp in DKCMS 9.4 allows SQL Injection via an ASPSESSIONID cookie to admin/admin.asp.
CVE-2018-18834
An issue has been found in libIEC61850 v1.3. It is a heap-based buffer overflow in BerEncoder_encodeOctetString in mms/asn1/ber_encoder.c.
CVE-2018-18835
upload_template() in system/changeskin.php in DocCms 2016.5.12 allows remote attackers to execute arbitrary PHP code via a template file.
CVE-2018-18840
XSS was discovered in SEMCMS PHP V3.4 via the SEMCMS_SeoAndTag.php?Class=edit&CF=SeoAndTag tag_indexmetatit parameter.
CVE-2018-18841
XSS was discovered in SEMCMS PHP V3.4 via the SEMCMS_SeoAndTag.php?Class=edit&CF=SeoAndTag tag_indexkey parameter.
CVE-2018-18842
CSRF exists in zb_users/plugin/AppCentre/theme.js.php in Z-BlogPHP 1.5.2.1935 (Zero), which allows remote attackers to execute arbitrary PHP code.
CVE-2018-8858
If an attacker has access to the firmware from the VGo Robot (Versions 3.0.3.52164 and 3.0.3.53662. Prior versions may also be affected) they may be able to extract credentials.
