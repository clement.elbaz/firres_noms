CVE-2009-4134
Buffer underflow in the rgbimg module in Python 2.5 allows remote attackers to cause a denial of service (application crash) via a large ZSIZE value in a black-and-white (aka B/W) RGB image that triggers an invalid pointer dereference.
CVE-2010-0595
Cisco Mediator Framework 1.5.1 before 1.5.1.build.14-eng, 2.2 before 2.2.1.dev.1, and 3.0 before 3.0.9.release.1 on the Cisco Network Building Mediator NBM-2400 and NBM-4800 and the Richards-Zeta Mediator 2500 has a default password for the administrative user account and unspecified other accounts, which makes it easier for remote attackers to obtain privileged access, aka Bug ID CSCtb83495.
CVE-2010-0596
Unspecified vulnerability in Cisco Mediator Framework 2.2 before 2.2.1.dev.1 and 3.0 before 3.0.9.release.1 on the Cisco Network Building Mediator NBM-2400 and NBM-4800 and the Richards-Zeta Mediator 2500 allows remote authenticated users to read or modify the device configuration, and gain privileges, via a (1) HTTP or (2) HTTPS request, aka Bug ID CSCtb83607.
CVE-2010-0597
Unspecified vulnerability in Cisco Mediator Framework 1.5.1 before 1.5.1.build.14-eng, 2.2 before 2.2.1.dev.1, and 3.0 before 3.0.9.release.1 on the Cisco Network Building Mediator NBM-2400 and NBM-4800 and the Richards-Zeta Mediator 2500 allows remote authenticated users to read or modify the device configuration, and gain privileges or cause a denial of service (device reload), via a (1) XML RPC or (2) XML RPC over HTTPS request, aka Bug ID CSCtb83618.
CVE-2010-0598
Cisco Mediator Framework 1.5.1 before 1.5.1.build.14-eng, 2.2 before 2.2.1.dev.1, and 3.0 before 3.0.9.release.1 on the Cisco Network Building Mediator NBM-2400 and NBM-4800 and the Richards-Zeta Mediator 2500 does not encrypt HTTP sessions from operator workstations, which allows remote attackers to discover Administrator credentials by sniffing the network, aka Bug ID CSCtb83631.
CVE-2010-0599
Cisco Mediator Framework 1.5.1 before 1.5.1.build.14-eng, 2.2 before 2.2.1.dev.1, and 3.0 before 3.0.9.release.1 on the Cisco Network Building Mediator NBM-2400 and NBM-4800 and the Richards-Zeta Mediator 2500 does not encrypt XML RPC sessions from operator workstations, which allows remote attackers to discover Administrator credentials by sniffing the network, aka Bug ID CSCtb83505.
CVE-2010-0600
Cisco Mediator Framework 1.5.1 before 1.5.1.build.14-eng, 2.2 before 2.2.1.dev.1, and 3.0 before 3.0.9.release.1 on the Cisco Network Building Mediator NBM-2400 and NBM-4800 and the Richards-Zeta Mediator 2500 does not properly restrict network access to an unspecified configuration file, which allows remote attackers to read passwords and unspecified other account details via a (1) XML RPC or (2) XML RPC over HTTPS session, aka Bug ID CSCtb83512.
CVE-2010-1296
Multiple buffer overflows in Adobe Photoshop CS4 before 11.0.2 allow user-assisted remote attackers to execute arbitrary code via a crafted (1) .ASL, (2) .ABR, or (3) .GRD file.
CVE-2010-1449
Integer overflow in rgbimgmodule.c in the rgbimg module in Python 2.5 allows remote attackers to have an unspecified impact via a large image that triggers a buffer overflow.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2008-3143.12.
CVE-2010-1450
Multiple buffer overflows in the RLE decoder in the rgbimg module in Python 2.5 allow remote attackers to have an unspecified impact via an image file containing crafted data that triggers improper processing within the (1) longimagedata or (2) expandrow function.
CVE-2010-1459
The default configuration of ASP.NET in Mono before 2.6.4 has a value of FALSE for the EnableViewStateMac property, which allows remote attackers to conduct cross-site scripting (XSS) attacks, as demonstrated by the __VIEWSTATE parameter to 2.0/menu/menu1.aspx in the XSP sample project.
CVE-2010-1634
Multiple integer overflows in audioop.c in the audioop module in Python 2.6, 2.7, 3.1, and 3.2 allow context-dependent attackers to cause a denial of service (application crash) via a large fragment, as demonstrated by a call to audioop.lin2lin with a long string in the first argument, leading to a buffer overflow.  NOTE: this vulnerability exists because of an incorrect fix for CVE-2008-3143.5.
CVE-2010-1959
Unspecified vulnerability in HP TestDirector for Quality Center 9.2 before Patch8 allows remote attackers to modify data via unknown vectors.
CVE-2010-2084
Microsoft ASP.NET 2.0 does not prevent setting the InnerHtml property on a control that inherits from HtmlContainerControl, which allows remote attackers to conduct cross-site scripting (XSS) attacks via vectors related to an attribute.
CVE-2010-2085
The default configuration of ASP.NET in Microsoft .NET before 1.1 has a value of FALSE for the EnableViewStateMac property, which allows remote attackers to conduct cross-site scripting (XSS) attacks via the __VIEWSTATE parameter.
CVE-2010-2086
Apache MyFaces 1.1.7 and 1.2.8, as used in IBM WebSphere Application Server and other applications, does not properly handle an unencrypted view state, which allows remote attackers to conduct cross-site scripting (XSS) attacks or execute arbitrary Expression Language (EL) statements via vectors that involve modifying the serialized view object.
CVE-2010-2087
Oracle Mojarra 1.2_14 and 2.0.2, as used in IBM WebSphere Application Server, Caucho Resin, and other applications, does not properly handle an unencrypted view state, which allows remote attackers to conduct cross-site scripting (XSS) attacks or execute arbitrary Expression Language (EL) statements via vectors that involve modifying the serialized view object.
CVE-2010-2088
ASP.NET in Microsoft .NET 3.5 does not properly handle an unencrypted view state, which allows remote attackers to conduct cross-site scripting (XSS) attacks against the form control via the __VIEWSTATE parameter.
CVE-2010-2089
The audioop module in Python 2.7 and 3.2 does not verify the relationships between size arguments and byte string lengths, which allows context-dependent attackers to cause a denial of service (memory corruption and application crash) via crafted arguments, as demonstrated by a call to audioop.reverse with a one-byte string, a different vulnerability than CVE-2010-1634.
CVE-2010-2090
The npb_protocol_error function in sna V5router64 in IBM Communications Server for Windows 6.1.3 and Communications Server for AIX (aka CSAIX or CS/AIX) in sna.rte before 6.3.1.2 allows remote attackers to cause a denial of service (daemon crash) via APPC data containing a GDSID variable with a GDS length that is too small.
CVE-2010-2091
Microsoft Outlook Web Access (OWA) 8.2.254.0, when Internet Explorer 7 on Windows Server 2003 is used, does not properly handle the id parameter in a Folder IPF.Note action to the default URI, which might allow remote attackers to obtain sensitive information or conduct cross-site scripting (XSS) attacks via an invalid value.
CVE-2010-2092
SQL injection vulnerability in graph.php in Cacti 0.8.7e and earlier allows remote attackers to execute arbitrary SQL commands via a crafted rra_id parameter in a GET request in conjunction with a valid rra_id value in a POST request or a cookie, which causes the POST or cookie value to bypass the validation routine, but inserts the $_GET value into the resulting query.
CVE-2010-2093
Use-after-free vulnerability in the request shutdown functionality in PHP 5.2 before 5.2.13 and 5.3 before 5.3.2 allows context-dependent attackers to cause a denial of service (crash) via a stream context structure that is freed before destruction occurs.
CVE-2010-2094
Multiple format string vulnerabilities in the phar extension in PHP 5.3 before 5.3.2 allow context-dependent attackers to obtain sensitive information (memory contents) and possibly execute arbitrary code via a crafted phar:// URI that is not properly handled by the (1) phar_stream_flush, (2) phar_wrapper_unlink, (3) phar_parse_url, or (4) phar_wrapper_open_url functions in ext/phar/stream.c; and the (5) phar_wrapper_open_dir function in ext/phar/dirstream.c, which triggers errors in the php_stream_wrapper_log_error function.
CVE-2010-2095
SQL injection vulnerability in index.php in CMSQlite 1.2 and earlier allows remote attackers to execute arbitrary SQL commands via the c parameter.
CVE-2010-2096
Directory traversal vulnerability in index.php in CMSQlite 1.2 and earlier allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the mod parameter.
CVE-2010-2097
The (1) iconv_mime_decode, (2) iconv_substr, and (3) iconv_mime_encode functions in PHP 5.2 through 5.2.13 and 5.3 through 5.3.2 allow context-dependent attackers to obtain sensitive information (memory contents) by causing a userspace interruption of an internal function, related to the call time pass by reference feature.
CVE-2010-2098
Incomplete blacklist vulnerability in usersettings.php in e107 0.7.20 and earlier allows remote attackers to conduct SQL injection attacks via the loginname parameter.
Per: http://cwe.mitre.org/data/definitions/184.html

'CWE-184: Incomplete Blacklist'
CVE-2010-2099
bbcode/php.bb in e107 0.7.20 and earlier does not perform access control checks for all inputs that could contain the php bbcode tag, which allows remote attackers to execute arbitrary PHP code, as demonstrated using the toEmail method in contact.php, related to invocations of the toHTML method.
CVE-2010-2100
The (1) htmlentities, (2) htmlspecialchars, (3) str_getcsv, (4) http_build_query, (5) strpbrk, and (6) strtr functions in PHP 5.2 through 5.2.13 and 5.3 through 5.3.2 allow context-dependent attackers to obtain sensitive information (memory contents) by causing a userspace interruption of an internal function, related to the call time pass by reference feature.
CVE-2010-2101
The (1) strip_tags, (2) setcookie, (3) strtok, (4) wordwrap, (5) str_word_count, and (6) str_pad functions in PHP 5.2 through 5.2.13 and 5.3 through 5.3.2 allow context-dependent attackers to obtain sensitive information (memory contents) by causing a userspace interruption of an internal function, related to the call time pass by reference feature.
CVE-2010-2102
Buffer overflow in Webby Webserver 1.01 allows remote attackers to execute arbitrary code via a long HTTP GET request.
CVE-2010-2103
Cross-site scripting (XSS) vulnerability in axis2-admin/axis2-admin/engagingglobally in the administration console in Apache Axis2/Java 1.4.1, 1.5.1, and possibly other versions, as used in SAP Business Objects 12, 3com IMC, and possibly other products, allows remote attackers to inject arbitrary web script or HTML via the modules parameter.  NOTE: some of these details are obtained from third party information.
CVE-2010-2104
Directory traversal vulnerability in Orbit Downloader 3.0.0.4 and 3.0.0.5 allows user-assisted remote attackers to write arbitrary files via a metalink file containing directory traversal sequences in the name attribute of a file element.
