CVE-2014-0998
Integer signedness error in the vt console driver (formerly Newcons) in FreeBSD 9.3 before p10 and 10.1 before p6 allows local users to cause a denial of service (crash) and possibly gain privileges via a negative value in a VT_WAITACTIVE ioctl call, which triggers an array index error and out-of-bounds kernel memory access.
CVE-2014-6136
IBM Security AppScan Standard 8.x and 9.x before 9.0.1.1 FP1 supports unencrypted sessions, which allows remote attackers to obtain sensitive information by sniffing the network.
CVE-2014-6141
IBM Tivoli Monitoring (ITM) 6.2.0 through FP03, 6.2.1 through FP04, 6.2.2 through FP09, 6.2.3 through FP05, and 6.3.0 before FP04 allows remote authenticated users to bypass intended access restrictions and execute arbitrary commands by leveraging Take Action view authority to modify in-progress commands.
CVE-2014-6170
The HTTPInput node in IBM WebSphere Message Broker 7.0 before 7.0.0.8 and 8.0 before 8.0.0.6 and IBM Integration Bus 9.0 before 9.0.0.4 allows remote attackers to obtain sensitive information by triggering a SOAP fault.
CVE-2014-7882
Unspecified vulnerability in HP SiteScope 11.1x and 11.2x allows remote authenticated users to gain privileges via unknown vectors.
CVE-2014-8612
Multiple array index errors in the Stream Control Transmission Protocol (SCTP) module in FreeBSD 10.1 before p5, 10.0 before p17, 9.3 before p9, and 8.4 before p23 allow local users to (1) gain privileges via the stream id to the setsockopt function, when setting the SCTIP_SS_VALUE option, or (2) read arbitrary kernel memory via the stream id to the getsockopt function, when getting the SCTP_SS_PRIORITY option.
CVE-2014-8613
The sctp module in FreeBSD 10.1 before p5, 10.0 before p17, 9.3 before p9, and 8.4 before p23 allows remote attackers to cause a denial of service (NULL pointer dereference and kernel panic) via a crafted RE_CONFIG chunk.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2014-8918
IBM Security AppScan Standard 8.x and 9.x before 9.0.1.1 FP1 does not properly verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2015-0223
Unspecified vulnerability in Apache Qpid 0.30 and earlier allows remote attackers to bypass access restrictions on qpidd via unknown vectors, related to 0-10 connection handling.
CVE-2015-0313
Use-after-free vulnerability in Adobe Flash Player before 13.0.0.269 and 14.x through 16.x before 16.0.0.305 on Windows and OS X and before 11.2.202.442 on Linux allows remote attackers to execute arbitrary code via unspecified vectors, as exploited in the wild in February 2015, a different vulnerability than CVE-2015-0315, CVE-2015-0320, and CVE-2015-0322.
CVE-2015-0512
Open redirect vulnerability in EMC Unisphere Central before 4.0 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via an unspecified parameter.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-0595
The XMLAPI in Cisco WebEx Meetings Server 1.5(.1.131) and earlier allows remote attackers to obtain sensitive information by reading return messages from crafted GET requests, aka Bug ID CSCuj67079.
CVE-2015-0596
Cross-site request forgery (CSRF) vulnerability in Cisco WebEx Meetings Server 1.5(.1.131) and earlier allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCuj67163.
CVE-2015-0597
The Forgot Password feature in Cisco WebEx Meetings Server 1.5(.1.131) and earlier allows remote attackers to enumerate administrative accounts via crafted packets, aka Bug IDs CSCuj67166 and CSCuj67159.
CVE-2015-0866
Multiple cross-site scripting (XSS) vulnerabilities in Zoho ManageEngine SupportCenter Plus 7.9 before hotfix 7941 allow remote attackers to inject arbitrary web script or HTML via the (1) fromCustomer, (2) username, or (3) password parameter to HomePage.do.
CVE-2015-1049
The web server on Siemens SCALANCE X-200IRT switches with firmware before 5.2.0 allows remote attackers to hijack sessions via unspecified vectors.
CVE-2015-1357
Siemens Ruggedcom WIN51xx devices with firmware before SS4.4.4624.35, WIN52xx devices with firmware before SS4.4.4624.35, WIN70xx devices with firmware before BS4.4.4621.32, and WIN72xx devices with firmware before BS4.4.4621.32 allow context-dependent attackers to discover password hashes by reading (1) files or (2) security logs.
CVE-2015-1383
Cross-site scripting (XSS) vulnerability in the geo search widget in the Geo Mashup plugin before 1.8.3 for WordPress allows remote attackers to inject arbitrary web script or HTML via the search key.
CVE-2015-1385
Cross-site scripting (XSS) vulnerability in the Blubrry PowerPress Podcasting plugin before 6.0.1 for WordPress allows remote attackers to inject arbitrary web script or HTML via the cat parameter in a powerpress-editcategoryfeed action in the powerpressadmin_categoryfeeds.php page to wp-admin/admin.php.
CVE-2015-1393
SQL injection vulnerability in the Photo Gallery plugin before 1.2.11 for WordPress allows remote authenticated users to execute arbitrary SQL commands via the asc_or_desc parameter in a create gallery request in the galleries_bwg page to wp-admin/admin.php.
CVE-2015-1448
The integrated management service on Siemens Ruggedcom WIN51xx devices with firmware before SS4.4.4624.35, WIN52xx devices with firmware before SS4.4.4624.35, WIN70xx devices with firmware before BS4.4.4621.32, and WIN72xx devices with firmware before BS4.4.4621.32 allows remote attackers to bypass authentication and perform administrative actions via unspecified vectors.
CVE-2015-1449
Buffer overflow in the integrated web server on Siemens Ruggedcom WIN51xx devices with firmware before SS4.4.4624.35, WIN52xx devices with firmware before SS4.4.4624.35, WIN70xx devices with firmware before BS4.4.4621.32, and WIN72xx devices with firmware before BS4.4.4621.32 allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2015-1450
SQL injection vulnerability in Restaurant Biller allows remote attackers to execute arbitrary SQL commands via the cid parameter in a category action to index.php.
CVE-2015-1451
Multiple cross-site scripting (XSS) vulnerabilities in Fortinet FortiOS 5.0 Patch 7 build 4457 allow remote authenticated users to inject arbitrary web script or HTML via the (1) WTP Name or (2) WTP Active Software Version field in a CAPWAP Join request.
CVE-2015-1452
The Control and Provisioning of Wireless Access Points (CAPWAP) daemon in Fortinet FortiOS 5.0 Patch 7 build 4457 allows remote attackers to cause a denial of service (locked CAPWAP Access Controller) via a large number of ClientHello DTLS messages.
CVE-2015-1453
The qm class in Fortinet FortiClient 5.2.3.091 for Android uses a hardcoded encryption key of FoRtInEt!AnDrOiD, which makes it easier for attackers to obtain passwords and possibly other sensitive data by leveraging the key to decrypt data in the Shared Preferences.
CVE-2015-1454
Blue Coat ProxyClient before 3.3.3.3 and 3.4.x before 3.4.4.10 and Unified Agent before 4.1.3.151952 does not properly validate certain certificates, which allows man-in-the-middle attackers to spoof ProxySG Client Managers, and consequently modify configurations and execute arbitrary software updates, via a crafted certificate.
