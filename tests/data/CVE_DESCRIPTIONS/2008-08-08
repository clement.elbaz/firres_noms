CVE-2008-0964
Multiple stack-based buffer overflows in snoop on Sun Solaris 8 through 10 and OpenSolaris before snv_96, when the -o option is omitted, allow remote attackers to execute arbitrary code via a crafted SMB packet.
CVE-2008-0965
Multiple format string vulnerabilities in snoop on Sun Solaris 8 through 10 and OpenSolaris before snv_96, when the -o option is omitted, allow remote attackers to execute arbitrary code via format string specifiers in an SMB packet.
CVE-2008-1664
Unspecified vulnerability in libc on HP HP-UX B.11.23 and B.11.31 allows remote attackers to cause a denial of service via unknown vectors.
CVE-2008-1945
QEMU 0.9.0 does not properly handle changes to removable media, which allows guest OS users to read arbitrary files on the host OS by using the diskformat: parameter in the -usbdevice option to modify the disk-image header to identify a different format, a related issue to CVE-2008-2004.
CVE-2008-2377
Use-after-free vulnerability in the _gnutls_handshake_hash_buffers_clear function in lib/gnutls_handshake.c in libgnutls in GnuTLS 2.3.5 through 2.4.0 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via TLS transmission of data that is improperly used when the peer calls gnutls_handshake within a normal session, leading to attempted access to a deallocated libgcrypt handle.
CVE-2008-3272
The snd_seq_oss_synth_make_info function in sound/core/seq/oss/seq_oss_synth.c in the sound subsystem in the Linux kernel before 2.6.27-rc2 does not verify that the device number is within the range defined by max_synthdev before returning certain data to the caller, which allows local users to obtain sensitive information.
CVE-2008-3337
PowerDNS Authoritative Server before 2.9.21.1 drops malformed queries, which might make it easier for remote attackers to poison DNS caches of other products running on other servers, a different issue than CVE-2008-1447 and CVE-2008-3217.
CVE-2008-3532
The NSS plugin in libpurple in Pidgin 2.4.3 does not verify SSL certificates, which makes it easier for remote attackers to trick a user into accepting an invalid server certificate for a spoofed service.
CVE-2008-3534
The shmem_delete_inode function in mm/shmem.c in the tmpfs implementation in the Linux kernel before 2.6.26.1 allows local users to cause a denial of service (system crash) via a certain sequence of file create, remove, and overwrite operations, as demonstrated by the insserv program, related to allocation of "useless pages" and improper maintenance of the i_blocks count.
CVE-2008-3535
Off-by-one error in the iov_iter_advance function in mm/filemap.c in the Linux kernel before 2.6.27-rc2 allows local users to cause a denial of service (system crash) via a certain sequence of file I/O operations with readv and writev, as demonstrated by testcases/kernel/fs/ftest/ftest03 from the Linux Test Project.
CVE-2008-3550
The CQWeb login page in IBM Rational ClearQuest 7.0.1 allows remote attackers to obtain potentially sensitive information (page source code) via a combination of ?script? and ?/script? sequences in the id field, possibly related to a cross-site scripting (XSS) vulnerability.
CVE-2008-3551
Multiple unspecified vulnerabilities in Sun Java Platform Micro Edition (aka Java ME, J2ME, or mobile Java), as distributed in Sun Wireless Toolkit 2.5.2, allow remote attackers to execute arbitrary code via unknown vectors.  NOTE: as of 20080807, the only disclosure is a vague pre-advisory with no actionable information. However, because it is from a company led by a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2008-3552
Multiple unspecified vulnerabilities in Nokia Series 40 3rd edition FP1, and possibly later devices, allow remote attackers to execute arbitrary code via unknown vectors, probably related to MIDP privilege escalation and persistent MIDlets, aka "ISSUES 11-15." NOTE: as of 20080807, the only disclosure is a vague pre-advisory with no actionable information.  However, because it is from a company led by a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2008-3553
Multiple unspecified vulnerabilities in Nokia Series 40 3rd edition devices allow remote attackers to execute arbitrary code via unknown vectors, probably related to MIDP privilege escalation and persistent MIDlets, aka "ISSUES 3-10." NOTE: as of 20080807, the only disclosure is a vague pre-advisory with no actionable information. However, because it is from a company led by a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2008-3554
SQL injection vulnerability in index.php in Discuz! 6.0.1 allows remote attackers to execute arbitrary SQL commands via the searchid parameter in a search action.
CVE-2008-3555
Directory traversal vulnerability in index.php in (1) WSN Forum 4.1.43 and earlier, (2) Gallery 4.1.30 and earlier, (3) Knowledge Base (WSNKB) 4.1.36 and earlier, (4) Links 4.1.44 and earlier, and possibly (5) Classifieds before 4.1.30 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the TID parameter, as demonstrated by uploading a .jpg file containing PHP sequences.
CVE-2008-3556
Multiple SQL injection vulnerabilities in index.php in Battle.net Clan Script 1.5.2 allow remote attackers to execute arbitrary SQL commands via the (1) showmember parameter in a members action and the (2) thread parameter in a board action.  NOTE: vector 1 might be the same as CVE-2008-2522.
CVE-2008-3557
Free Hosting Manager 1.2 and 2.0 allows remote attackers to bypass authentication and gain administrative access by setting both the adminuser and loggedin cookies.
CVE-2008-3558
Stack-based buffer overflow in the WebexUCFObject ActiveX control in atucfobj.dll in Cisco WebEx Meeting Manager before 20.2008.2606.4919 allows remote attackers to execute arbitrary code via a long argument to the NewObject method.
CVE-2008-3559
Multiple cross-site scripting (XSS) vulnerabilities in KAPhotoservice allow remote attackers to inject arbitrary web script or HTML via the (1) filename parameter to search.asp and the (2) page parameter to order.asp.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3560
Cross-site scripting (XSS) vulnerability in kshop_search.php in the Kshop module 2.22 for Xoops allows remote attackers to inject arbitrary web script or HTML via the search parameter.
