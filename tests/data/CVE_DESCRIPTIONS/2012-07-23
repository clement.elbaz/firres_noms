CVE-2012-0305
Untrusted search path vulnerability in Symantec System Recovery 2011 before SP2 and Backup Exec System Recovery 2010 before SP5 allows local users to gain privileges via a Trojan horse DLL in the current working directory.
Per: http://cwe.mitre.org/data/definitions/426.html 'CWE-426: Untrusted Search Path'
CVE-2012-2574
SQL injection vulnerability in the management console in Symantec Web Gateway 5.0.x before 5.0.3.18 allows remote attackers to execute arbitrary SQL commands via unspecified vectors, related to a "blind SQL injection" issue.
CVE-2012-2953
The management console in Symantec Web Gateway 5.0.x before 5.0.3.18 allows remote attackers to execute arbitrary commands via crafted input to application scripts.
CVE-2012-2957
The management console in Symantec Web Gateway 5.0.x before 5.0.3.18 allows local users to gain privileges by modifying files, related to a "file inclusion" issue.
CVE-2012-2961
SQL injection vulnerability in the management console in Symantec Web Gateway 5.0.x before 5.0.3.18 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-2976
The management console in Symantec Web Gateway 5.0.x before 5.0.3.18 allows remote attackers to execute arbitrary shell commands via crafted input to application scripts, related to an "injection" issue.
CVE-2012-2977
The management console in Symantec Web Gateway 5.0.x before 5.0.3.18 allows remote attackers to change arbitrary passwords via crafted input to an application script.
CVE-2012-3387
Moodle 2.3.x before 2.3.1 uses only a client-side check for whether references are permitted in a file upload, which allows remote authenticated users to bypass intended alias (aka shortcut) restrictions via a client that omits this check.
CVE-2012-3388
The is_enrolled function in lib/accesslib.php in Moodle 2.2.x before 2.2.4 and 2.3.x before 2.3.1 does not properly interact with the caching feature, which might allow remote authenticated users to bypass an intended capability check via unspecified vectors that trigger caching of a user record.
CVE-2012-3389
Multiple cross-site scripting (XSS) vulnerabilities in mod/lti/typessettings.php in Moodle 2.2.x before 2.2.4 and 2.3.x before 2.3.1 allow remote attackers to inject arbitrary web script or HTML via the (1) lti_typename or (2) lti_toolurl parameter.
CVE-2012-3390
lib/filelib.php in Moodle 2.1.x before 2.1.7 and 2.2.x before 2.2.4 does not properly restrict file access after a block has been hidden, which allows remote authenticated users to obtain sensitive information by reading a file that is embedded in a block.
CVE-2012-3391
mod/forum/rsslib.php in Moodle 2.1.x before 2.1.7 and 2.2.x before 2.2.4 does not properly implement the requirement for posting before reading a Q&A forum, which allows remote authenticated users to bypass intended access restrictions by leveraging the student role and reading the RSS feed for a forum.
CVE-2012-3392
mod/forum/unsubscribeall.php in Moodle 2.1.x before 2.1.7 and 2.2.x before 2.2.4 does not consider whether a forum is optional, which allows remote authenticated users to bypass forum-subscription requirements by leveraging the student role and unsubscribing from all forums.
CVE-2012-3393
Cross-site scripting (XSS) vulnerability in repository/lib.php in Moodle 2.1.x before 2.1.7 and 2.2.x before 2.2.4 allows remote authenticated administrators to inject arbitrary web script or HTML by renaming a repository.
CVE-2012-3394
auth/ldap/ntlmsso_attempt.php in Moodle 2.0.x before 2.0.10, 2.1.x before 2.1.7, 2.2.x before 2.2.4, and 2.3.x before 2.3.1 redirects users from an https LDAP login URL to an http URL, which allows remote attackers to obtain sensitive information by sniffing the network.
CVE-2012-3395
SQL injection vulnerability in mod/feedback/complete.php in Moodle 2.0.x before 2.0.10, 2.1.x before 2.1.7, and 2.2.x before 2.2.4 allows remote authenticated users to execute arbitrary SQL commands via crafted form data.
CVE-2012-3396
Cross-site scripting (XSS) vulnerability in cohort/edit_form.php in Moodle 2.0.x before 2.0.10, 2.1.x before 2.1.7, 2.2.x before 2.2.4, and 2.3.x before 2.3.1 allows remote authenticated administrators to inject arbitrary web script or HTML via the idnumber field.  NOTE: this vulnerability exists because of an incorrect fix for CVE-2012-2365.
CVE-2012-3397
lib/modinfolib.php in Moodle 2.0.x before 2.0.10, 2.1.x before 2.1.7, 2.2.x before 2.2.4, and 2.3.x before 2.3.1 does not check for a group-membership requirement when determining whether an activity is unavailable or hidden, which allows remote authenticated users to bypass intended access restrictions by selecting an activity that is configured for a group of other users.
CVE-2012-3398
Algorithmic complexity vulnerability in Moodle 1.9.x before 1.9.19, 2.0.x before 2.0.10, 2.1.x before 2.1.7, and 2.2.x before 2.2.4 allows remote authenticated users to cause a denial of service (CPU consumption) by using the advanced-search feature on a database activity that has many records.
Per: http://cwe.mitre.org/data/definitions/407.html 'CWE-407: Algorithmic Complexity'
