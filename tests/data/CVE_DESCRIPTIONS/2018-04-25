CVE-2014-0872
The installation process in IBM Security Key Lifecycle Manager 2.5 stores unencrypted credentials, which might allow local users to obtain sensitive information by leveraging root access. IBM X-Force ID: 90988.
CVE-2014-0881
The TPM on Integrated Management Module II (IMM2) on IBM Flex System x222 servers with firmware 1.00 through 3.56 allows remote attackers to obtain sensitive key information or cause a denial of service by leveraging an incorrect configuration. IBM X-Force ID: 91146.
CVE-2014-0882
Integrated Management Module II (IMM2) on IBM Flex System, NeXtScale, System x3xxx, and System x iDataPlex systems might allow remote authenticated users to obtain sensitive account information via vectors related to generated Service Advisor data (FFDC). IBM X-Force ID: 91149.
CVE-2014-5014
The WordPress Flash Uploader plugin before 3.1.3 for WordPress allows remote attackers to execute arbitrary commands via vectors related to invalid characters in image_magic_path.
CVE-2017-12712
The authentication algorithm in Abbott Laboratories pacemakers manufactured prior to Aug 28, 2017, which involves an authentication key and time stamp, can be compromised or bypassed, which may allow a nearby attacker to issue unauthorized commands to the pacemaker via RF communications. CVSS v3 base score: 7.5, CVSS vector string: AV:A/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:H. Abbott has developed a firmware update to help mitigate the identified vulnerabilities.
CVE-2017-12714
Abbott Laboratories pacemakers manufactured prior to Aug 28, 2017 do not restrict or limit the number of correctly formatted "RF wake-up" commands that can be received, which may allow a nearby attacker to repeatedly send commands to reduce pacemaker battery life. CVSS v3 base score: 5.3, CVSS vector string: AV:A/AC:H/PR:N/UI:N/S:U/C:N/I:N/A:H. Abbott has developed a firmware update to help mitigate the identified vulnerabilities.
CVE-2017-12716
Abbott Laboratories Accent and Anthem pacemakers manufactured prior to Aug 28, 2017 transmit unencrypted patient information via RF communications to programmers and home monitoring units. Additionally, the Accent and Anthem pacemakers store the optional patient information without encryption. CVSS v3 base score: 3.1, CVSS vector string: AV:A/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N. Abbott has developed a firmware update to help mitigate the identified vulnerabilities.
CVE-2017-1750
IBM Jazz Reporting Service (JRS) 5.0 through 5.0.2 and 6.0 through 6.0.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 135523.
CVE-2017-6888
An error in the "read_metadata_vorbiscomment_()" function (src/libFLAC/stream_decoder.c) in FLAC version 1.3.2 can be exploited to cause a memory leak via a specially crafted FLAC file.
CVE-2017-7652
In Eclipse Mosquitto 1.4.14, if a Mosquitto instance is set running with a configuration file, then sending a HUP signal to server triggers the configuration to be reloaded from disk. If there are lots of clients connected so that there are no more file descriptors/sockets available (default limit typically 1024 file descriptors on Linux), then opening the configuration file will fail.
CVE-2018-10206
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. There is Stored XSS via the optional message field of a file request.
CVE-2018-10207
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. An attacker can exploit Missing Authorization on the FlexPaperViewer SWF reader, and export files that should have been restricted, via vectors involving page-by-page access to a document in SWF format.
CVE-2018-10208
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. There is anonymous reflected XSS on the error page via a /share/error?message= URI.
CVE-2018-10209
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. There is Stored XSS on the file or folder download pop-up via a crafted file or folder name.
CVE-2018-10210
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. Enumeration of users is possible through the password-reset feature.
CVE-2018-10211
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. There is improper authorization when listing the history of another user via a modified "vaultize_session_id" value in a cookie.
CVE-2018-10212
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. There is improper authorization leading to creation of folders within another account via a modified device value.
CVE-2018-10213
An issue was discovered in Vaultize Enterprise File Sharing 17.05.31. There is XSS in invitation mail received from a different user, who can modify the HTML in that mail before sending it.
CVE-2018-10310
A persistent cross-site scripting vulnerability has been identified in the web interface of the Catapult UK Cookie Consent plugin before 2.3.10 for WordPress that allows the execution of arbitrary HTML/script code in the context of a victim's browser.
CVE-2018-10361
An issue was discovered in KTextEditor 5.34.0 through 5.45.0. Insecure handling of temporary files in the KTextEditor's kauth_ktexteditor_helper service (as utilized in the Kate text editor) can allow other unprivileged users on the local system to gain root privileges. The attack occurs when one user (who has an unprivileged account but is also able to authenticate as root) writes a text file using Kate into a directory owned by a another unprivileged user. The latter unprivileged user conducts a symlink attack to achieve privilege escalation.
CVE-2018-10362
An issue was discovered in phpLiteAdmin 1.9.5 through 1.9.7.1. Due to loose comparison with '==' instead of '===' in classes/Authorization.php for the user-provided login password, it is possible to login with a simpler password if the password has the form of a power in scientific notation (like '2e2' for '200' or '0e1234' for '0'). This is possible because, in the loose comparison case, PHP interprets the string as a number in scientific notation, and thus converts it to a number. After that, the comparison with '==' casts the user input (e.g., the string '200' or '0') to a number, too. Hence the attacker can login with just a '0' or a simple number he has to brute force. Strong comparison with '===' prevents the cast into numbers.
CVE-2018-10366
An issue was discovered in the Users (aka Front-end user management) plugin 1.4.5 for October CMS. XSS exists in the name field.
CVE-2018-10367
An issue was discovered in WUZHI CMS 4.1.0. The content-management feature has Stored XSS via the title or content section.
CVE-2018-10368
An issue was discovered in WUZHI CMS 4.1.0. The "Extension Module -> System Announcement" feature has Stored XSS via an announcement.
CVE-2018-10372
process_cu_tu_index in dwarf.c in GNU Binutils 2.30 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted binary file, as demonstrated by readelf.
CVE-2018-10373
concat_filename in dwarf2.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.30, allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted binary file, as demonstrated by nm-new.
CVE-2018-10374
EasyCMS 1.3 has XSS via the s POST parameter (aka a search box value) in an index.php?s=/index/search/index.html request.
CVE-2018-10375
A file uploading vulnerability exists in /include/helpers/upload.helper.php in DedeCMS V5.7 SP2, which can be utilized by attackers to upload and execute arbitrary PHP code via the /dede/archives_do.php?dopost=uploadLitpic litpic parameter when "Content-Type: image/jpeg" is sent, but the filename ends in .php and contains PHP code.
CVE-2018-10376
An integer overflow in the transferProxy function of a smart contract implementation for SmartMesh (aka SMT), an Ethereum ERC20 token, allows attackers to accomplish an unauthorized increase of digital assets via crafted _fee and _value parameters, as exploited in the wild in April 2018, aka the "proxyOverflow" issue.
CVE-2018-1112
glusterfs server before versions 3.10.12, 4.0.2 is vulnerable when using 'auth.allow' option which allows any unauthenticated gluster client to connect from any network to mount gluster storage volumes. NOTE: this vulnerability exists because of a CVE-2018-1088 regression.
CVE-2018-1335
From Apache Tika versions 1.7 to 1.17, clients could send carefully crafted headers to tika-server that could be used to inject commands into the command line of the server running tika-server. This vulnerability only affects those running tika-server on a server that is open to untrusted clients. The mitigation is to upgrade to Tika 1.18.
CVE-2018-1338
A carefully crafted (or fuzzed) file can trigger an infinite loop in Apache Tika's BPGParser in versions of Apache Tika before 1.18.
CVE-2018-1339
A carefully crafted (or fuzzed) file can trigger an infinite loop in Apache Tika's ChmParser in versions of Apache Tika before 1.18.
CVE-2018-1363
IBM Jazz Reporting Service (JRS) 5.0 through 5.0.2 and 6.0 through 6.0.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 137448.
CVE-2018-5226
There was an argument injection vulnerability in Sourcetree for Windows via Mercurial repository tag name that is going to be deleted. An attacker with permission to create a tag on a Mercurial repository linked in Sourcetree for Windows is able to exploit this issue to gain code execution on the system. All versions of Sourcetree for Windows before 2.5.5.0 are affected by this vulnerability.
CVE-2018-5486
NetApp OnCommand Unified Manager for Linux versions 7.2 though 7.3 ship with the Java Debug Wire Protocol (JDWP) enabled which allows unauthorized local attackers to execute arbitrary code.
CVE-2018-8716
WSO2 Identity Server before 5.5.0 has XSS via the dashboard, allowing attacks by low-privileged attackers.
CVE-2018-8801
GitLab Community and Enterprise Editions version 8.3 up to 10.x before 10.3 are vulnerable to SSRF in the Services and webhooks component.
CVE-2018-8833
Heap-based buffer overflow vulnerabilities in Advantech WebAccess HMI Designer 2.1.7.32 and prior caused by processing specially crafted .pm3 files may allow remote code execution.
CVE-2018-8835
Double free vulnerabilities in Advantech WebAccess HMI Designer 2.1.7.32 and prior caused by processing specially crafted .pm3 files may allow remote code execution.
CVE-2018-8837
Processing specially crafted .pm3 files in Advantech WebAccess HMI Designer 2.1.7.32 and prior may cause the system to write outside the intended buffer area and may allow remote code execution.
CVE-2018-9101
A vulnerability in the conferencing component of Mitel MiVoice Connect, versions R1707-PREM SP1 (21.84.5535.0) and earlier, and Mitel ST 14.2, versions GA27 (19.49.5200.0) and earlier, could allow an unauthenticated attacker to conduct a reflected cross-site scripting (XSS) attack due to insufficient validation for the launch_presenter.php page. A successful exploit could allow an attacker to execute arbitrary scripts.
CVE-2018-9102
A vulnerability in the conferencing component of Mitel MiVoice Connect, versions R1707-PREM SP1 (21.84.5535.0) and earlier, and Mitel ST 14.2, versions GA27 (19.49.5200.0) and earlier, could allow an unauthenticated attacker to conduct an SQL injection attack due to insufficient input validation for the signin interface. A successful exploit could allow an attacker to extract sensitive information from the database.
CVE-2018-9103
A vulnerability in the conferencing component of Mitel MiVoice Connect, versions R1707-PREM SP1 (21.84.5535.0) and earlier, and Mitel ST 14.2, versions GA27 (19.49.5200.0) and earlier, could allow an unauthenticated attacker to conduct a reflected cross-site scripting (XSS) attack due to insufficient validation for the signin.php page. A successful exploit could allow an attacker to execute arbitrary scripts.
CVE-2018-9104
A vulnerability in the conferencing component of Mitel MiVoice Connect, versions R1707-PREM SP1 (21.84.5535.0) and earlier, and Mitel ST 14.2, versions GA27 (19.49.5200.0) and earlier, could allow an unauthenticated attacker to conduct a reflected cross-site scripting (XSS) attack due to insufficient validation for the api.php page. A successful exploit could allow an attacker to execute arbitrary scripts.
