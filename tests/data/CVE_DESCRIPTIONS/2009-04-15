CVE-2007-4514
Unspecified vulnerability in HP ProCurve Manager and HP ProCurve Manager Plus 2.3 and earlier allows remote attackers to obtain sensitive information from the ProCurve Manager server via unknown attack vectors.
CVE-2009-0077
The firewall engine in Microsoft Forefront Threat Management Gateway, Medium Business Edition (TMG MBE); and Internet Security and Acceleration (ISA) Server 2004 SP3, 2006, 2006 Supportability Update, and 2006 SP1; does not properly manage the session state of web listeners, which allows remote attackers to cause a denial of service (many stale sessions) via crafted packets, aka "Web Proxy TCP State Limited Denial of Service Vulnerability."
CVE-2009-0078
The Windows Management Instrumentation (WMI) provider in Microsoft Windows XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 does not properly implement isolation among a set of distinct processes that (1) all run under the NetworkService account or (2) all run under the LocalService account, which allows local users to gain privileges by accessing the resources of one of the processes, aka "Windows WMI Service Isolation Vulnerability."
CVE-2009-0079
The RPCSS service in Microsoft Windows XP SP2 and SP3 and Server 2003 SP1 and SP2 does not properly implement isolation among a set of distinct processes that (1) all run under the NetworkService account or (2) all run under the LocalService account, which allows local users to gain privileges by accessing the resources of one of the processes, aka "Windows RPCSS Service Isolation Vulnerability."
CVE-2009-0080
The ThreadPool class in Windows Vista Gold and SP1, and Server 2008, does not properly implement isolation among a set of distinct processes that (1) all run under the NetworkService account or (2) all run under the LocalService account, which allows local users to gain privileges by leveraging incorrect thread ACLs to access the resources of one of the processes, aka "Windows Thread Pool ACL Weakness Vulnerability."
CVE-2009-0084
Use-after-free vulnerability in DirectShow in Microsoft DirectX 8.1 and 9.0 allows remote attackers to execute arbitrary code via an MJPEG file or video stream with a malformed Huffman table, which triggers an exception that frees heap memory that is later accessed, aka "MJPEG Decompression Vulnerability."
CVE-2009-0086
Integer underflow in Windows HTTP Services (aka WinHTTP) in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 allows remote HTTP servers to execute arbitrary code via crafted parameter values in a response, related to error handling, aka "Windows HTTP Services Integer Underflow Vulnerability."
CVE-2009-0087
Unspecified vulnerability in the Word 6 text converter in WordPad in Microsoft Windows 2000 SP4, XP SP2 and SP3, and Server 2003 SP1 and SP2; and the Word 6 text converter in Microsoft Office Word 2000 SP3 and 2002 SP3; allows remote attackers to execute arbitrary code via a crafted Word 6 file that contains malformed data, aka "WordPad and Office Text Converter Memory Corruption Vulnerability."
CVE-2009-0088
The WordPerfect 6.x Converter (WPFT632.CNV, 1998.1.27.0) in Microsoft Office Word 2000 SP3 and Microsoft Office Converter Pack does not properly validate the length of an unspecified string, which allows remote attackers to execute arbitrary code via a crafted WordPerfect 6.x file, related to an unspecified counter and control structures on the stack, aka "Word 2000 WordPerfect 6.x Converter Stack Corruption Vulnerability."
CVE-2009-0089
Windows HTTP Services (aka WinHTTP) in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, and Vista Gold allows remote web servers to impersonate arbitrary https web sites by using DNS spoofing to "forward a connection" to a different https web site that has a valid certificate matching its own domain name, but not a certificate matching the domain name of the host requested by the user, aka "Windows HTTP Services Certificate Name Mismatch Vulnerability."
CVE-2009-0100
Microsoft Office Excel 2000 SP3, 2002 SP3, 2003 SP3, and 2007 SP1; Excel in Microsoft Office 2004 and 2008 for Mac; Microsoft Office Excel Viewer and Excel Viewer 2003 SP3; and Microsoft Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP1 do not properly parse the Excel spreadsheet file format, which allows remote attackers to execute arbitrary code via a crafted spreadsheet that contains a malformed object with "an offset and a two-byte value" that trigger a memory calculation error, aka "Memory Corruption Vulnerability."
CVE-2009-0235
Stack-based buffer overflow in the Word 97 text converter in WordPad in Microsoft Windows 2000 SP4, XP SP2 and SP3, and Server 2003 SP1 and SP2 allows remote attackers to execute arbitrary code via a crafted Word 97 file that triggers memory corruption, related to use of inconsistent integer data sizes for an unspecified length field, aka "WordPad Word 97 Text Converter Stack Overflow Vulnerability."
CVE-2009-0237
Cross-site scripting (XSS) vulnerability in cookieauth.dll in the HTML forms authentication component in Microsoft Forefront Threat Management Gateway, Medium Business Edition (TMG MBE); and Internet Security and Acceleration (ISA) Server 2006, 2006 Supportability Update, and 2006 SP1; allows remote attackers to inject arbitrary web script or HTML via "authentication input" to this component, aka "Cross-Site Scripting Vulnerability."
CVE-2009-0550
Windows HTTP Services (aka WinHTTP) in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008; and WinINet in Microsoft Internet Explorer 5.01 SP4, 6 SP1, 6 and 7 on Windows XP SP2 and SP3, 6 and 7 on Windows Server 2003 SP1 and SP2, 7 on Windows Vista Gold and SP1, and 7 on Windows Server 2008; allows remote web servers to capture and replay NTLM credentials, and execute arbitrary code, via vectors related to absence of a "credential-reflection protections" opt-in step, aka "Windows HTTP Services Credential Reflection Vulnerability" and "WinINet Credential Reflection Vulnerability."
CVE-2009-0551
Microsoft Internet Explorer 6 SP1, 6 and 7 on Windows XP SP2 and SP3, 6 and 7 on Windows Server 2003 SP1 and SP2, 7 on Windows Vista Gold and SP1, and 7 on Windows Server 2008 does not properly handle transition errors in a request for one HTTP document followed by a request for a second HTTP document, which allows remote attackers to execute arbitrary code via vectors involving (1) multiple crafted pages on a web site or (2) a web page with crafted inline content such as banner advertisements, aka "Page Transition Memory Corruption Vulnerability."
CVE-2009-0552
Unspecified vulnerability in Microsoft Internet Explorer 5.01 SP4, 6 SP1, 6 on Windows XP SP2 and SP3, and 6 on Windows Server 2003 SP1 and SP2 allows remote attackers to execute arbitrary code via a web page that triggers presence of an object in memory that was (1) not properly initialized or (2) deleted, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2009-0553
Microsoft Internet Explorer 6 SP1, 6 and 7 on Windows XP SP2 and SP3, 6 and 7 on Windows Server 2003 SP1 and SP2, 7 on Windows Vista Gold and SP1, and 7 on Windows Server 2008 allows remote attackers to execute arbitrary code via a web page that triggers presence of an object in memory that was (1) not properly initialized or (2) deleted, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2009-0554
Microsoft Internet Explorer 5.01 SP4, 6 SP1, 6 and 7 on Windows XP SP2 and SP3, 6 and 7 on Windows Server 2003 SP1 and SP2, 7 on Windows Vista Gold and SP1, and 7 on Windows Server 2008 allows remote attackers to execute arbitrary code via a web page that triggers presence of an object in memory that was (1) not properly initialized or (2) deleted, aka "Uninitialized Memory Corruption Vulnerability."
CVE-2009-0681
PGP Desktop before 9.10 allows local users to (1) cause a denial of service (crash) via a crafted IOCTL request to pgpdisk.sys, and (2) cause a denial of service (crash) and execute arbitrary code via a crafted IRP in an IOCTL request to pgpwded.sys.
CVE-2009-0972
Unspecified vulnerability in the Workspace Manager component in Oracle Database 11.1.0.6, 11.1.0.7, 10.2.0.3, 10.2.0.4, 10.1.0.5, 9.2.0.8, and 9.2.0.8DV allows remote authenticated users to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-0973
Unspecified vulnerability in the Cluster Ready Services component in Oracle Database 10.1.0.5 allows remote attackers to affect availability via unknown vectors.
CVE-2009-0974
Unspecified vulnerability in the Portal component in Oracle Application Server 10.1.2.3 and 10.1.4.2 allows remote attackers to affect integrity via unknown vectors, a different vulnerability than CVE-2009-0983 and CVE-2009-3407.
CVE-2009-0975
Unspecified vulnerability in the Workspace Manager component in Oracle Database 10.2.0.4 and 11.1.0.6 allows remote authenticated users to affect confidentiality and integrity via unknown vectors, a different vulnerability than CVE-2009-0978.
CVE-2009-0976
Unspecified vulnerability in the Workspace Manager component in Oracle Database 10.2.0.4 and 11.1.0.6 allows remote authenticated users to affect confidentiality and integrity, related to LTADM.
CVE-2009-0977
Unspecified vulnerability in the Advanced Queuing component in Oracle Database 9.2.0.8, 9.2.0.8DV, 10.1.0.5, and 10.2.0.3 allows remote authenticated users to affect confidentiality and integrity, related to DBMS_AQIN.  NOTE: the previous information was obtained from the April 2009 CPU.  Oracle has not commented on reliable researcher claims that this issue is SQL injection in the GRANT_TYPE_ACCESS procedure in the DBMS_AQADM_SYS package.
CVE-2009-0978
Unspecified vulnerability in the Workspace Manager component in Oracle Database 10.2.0.4 and 11.1.0.6 allows remote authenticated users to affect confidentiality and integrity via unknown vectors, a different vulnerability than CVE-2009-0975.
CVE-2009-0979
Unspecified vulnerability in the Resource Manager component in Oracle Database 9.2.0.8 and 9.2.0.8DV allows remote authenticated users to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-0980
Unspecified vulnerability in the SQLX Functions component in Oracle Database 10.2.0.3 and 11.1.0.6 allows remote authenticated users to affect integrity and availability, related to AGGXQIMP.
CVE-2009-0981
Unspecified vulnerability in the Application Express component in Oracle Database 11.1.0.7 allows remote authenticated users to affect confidentiality, related to APEX.  NOTE: the previous information was obtained from the April 2009 CPU.  Oracle has not commented on reliable researcher claims that this issue allows remote authenticated users to obtain APEX password hashes from the WWV_FLOW_USERS table via a SELECT statement.
CVE-2009-0982
Unspecified vulnerability in the PeopleSoft Enterprise PeopleTools component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.49.19 allows remote authenticated users to affect integrity via unknown vectors.
CVE-2009-0983
Unspecified vulnerability in the Portal component in Oracle Application Server 10.1.2.3 and 10.1.4.2 allows remote attackers to affect integrity via unknown vectors, a different vulnerability than CVE-2009-0974 and CVE-2009-3407.
CVE-2009-0984
Unspecified vulnerability in the Database Vault component in Oracle Database 9.2.0.8DV, 10.2.0.4, and 11.1.0.6 allows remote authenticated users to affect confidentiality and integrity, related to DBMS_SYS_SQL.
CVE-2009-0985
Unspecified vulnerability in the Core RDBMS component in Oracle Database 10.1.0.5, 10.2.0.4, and 11.1.0.6 allows remote authenticated users with the IMP_FULL_DATABASE role to affect confidentiality, integrity, and availability.
CVE-2009-0986
Unspecified vulnerability in the Workspace Manager component in Oracle Database 10.2.0.4 and 11.1.0.6 allows remote authenticated users to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-0988
Unspecified vulnerability in the Password Policy component in Oracle Database 11.1.0.6 allows remote authenticated users to affect confidentiality via unknown vectors.
CVE-2009-0989
Unspecified vulnerability in the BI Publisher component in Oracle Application Server 5.6.2, 10.1.3.2.1, and 10.1.3.3.3 allows remote authenticated users to affect confidentiality and integrity via unknown vectors, a different vulnerability than CVE-2009-0990.
CVE-2009-0990
Unspecified vulnerability in the BI Publisher component in Oracle Application Server 5.6.2, 10.1.3.2.1, and 10.1.3.3.3 allows remote authenticated users to affect confidentiality and integrity via unknown vectors, a different vulnerability than CVE-2009-0989.
CVE-2009-0991
Unspecified vulnerability in the Listener component in Oracle Database 9.2.0.8, 9.2.0.8DV, 10.1.0.5, 10.2.0.4, and 11.1.0.7 allows remote attackers to affect availability via unknown vectors, a different vulnerability than CVE-2009-1970.
CVE-2009-0992
Unspecified vulnerability in the Advanced Queuing component in Oracle Database 10.1.0.5, 10.2.0.4, and 11.1.0.7 allows remote authenticated users to affect confidentiality and integrity, related to DBMS_AQIN. NOTE: the previous information was obtained from the April 2009 CPU. Oracle has not commented on reliable researcher claims that this issue is SQL injection in the DEQ_EXEJOB procedure.
CVE-2009-0993
Unspecified vulnerability in the OPMN component in Oracle Application Server 10.1.2.3 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.  NOTE: the previous information was obtained from the April 2009 CPU.  Oracle has not commented on reliable researcher claims that this issue is a format string vulnerability that allows remote attackers to execute arbitrary code via format string specifiers in an HTTP POST URI, which are not properly handled when logging to opmn/logs/opmn.log.
CVE-2009-0994
Unspecified vulnerability in the BI Publisher component in Oracle Application Server 5.6.2, 10.1.3.2.1, 10.1.3.3.3, and 10.1.3.4 allows remote authenticated users to affect confidentiality via unknown vectors, a different vulnerability than CVE-2009-1017.
CVE-2009-0995
Unspecified vulnerability in the Oracle Applications Framework component in Oracle E-Business Suite 12.0.6 and 11i10CU2 allows remote attackers to affect integrity via unknown vectors.
CVE-2009-0996
Unspecified vulnerability in the BI Publisher component in Oracle Application Server 10.1.3.2.1, 10.1.3.3.3, and 10.1.3.4 allows remote authenticated users to affect confidentiality via unknown vectors.
CVE-2009-0997
Unspecified vulnerability in the Database Vault component in Oracle Database 11.1.0.6 allows remote authenticated users to affect confidentiality, related to DBMS_SYS_SQL.
CVE-2009-0998
Unspecified vulnerability in the PeopleSoft Enterprise HRMS - eBenefits component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.9.18 and 9.0.8 allows remote authenticated users to affect confidentiality and integrity via unknown vectors.
CVE-2009-0999
Unspecified vulnerability in the Oracle Application Object Library component in Oracle E-Business Suite 12.0.6 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-1000
The Oracle Applications Framework component in Oracle E-Business Suite 12.0.6 and 11i10CU2 uses default passwords for unspecified "FND Applications Users (not DB users)," which has unknown impact and attack vectors.
CVE-2009-1001
Unspecified vulnerability in Oracle BEA WebLogic Portal 8.1 Gold through SP6 allows remote authenticated users to gain privileges via unknown vectors.
CVE-2009-1002
Unspecified vulnerability in Oracle BEA WebLogic Server 10.3, 10.0 Gold through MP1, 9.2 Gold through MP3, 9.1, 9.0, 8.1 Gold through SP6, and 7.0 Gold through SP7 allows remote attackers to gain privileges via unknown vectors.
CVE-2009-1003
Unspecified vulnerability in the WebLogic Server component in BEA Product Suite 10.3, 10.0 MP1, 9.2 MP3, 9.1, and 9.0 allows remote attackers to affect integrity via unknown vectors related to "access to source code of web pages."
CVE-2009-1004
Unspecified vulnerability in the WebLogic Server component in BEA Product Suite 10.3 allows remote attackers to affect confidentiality and integrity via unknown vectors.
CVE-2009-1005
Unspecified vulnerability in the Oracle Data Service Integrator (AquaLogic Data Services Platform) component in BEA Product Suite 10.3.0, 3.2, 3.0.1, and 3.0 allows local users to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-1006
Unspecified vulnerability in the JRockit component in BEA Product Suite R27.6.2 and earlier, with SDK/JRE 1.4.2, JRE/JDK 5, and JRE/JDK 6, allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors.
CVE-2009-1008
Unspecified vulnerability in the Outside In Technology component in Oracle Application Server 8.2.2 and 8.3.0 allows local users to affect confidentiality, integrity, and availability, related to HTML, a different vulnerability than CVE-2009-1010.
CVE-2009-1009
Unspecified vulnerability in the Outside In Technology component in Oracle Application Server 8.1.9 allows local users to affect confidentiality, integrity, and availability, related to HTML.
CVE-2009-1010
Unspecified vulnerability in the Outside In Technology component in Oracle Application Server 8.2.2 and 8.3.0 allows local users to affect confidentiality, integrity, and availability, related to HTML, a different vulnerability than CVE-2009-1008.
CVE-2009-1011
Unspecified vulnerability in the Outside In Technology component in Oracle Application Server 8.2.2 and 8.3.0 allows local users to affect confidentiality, integrity, and availability, related to HTML.  NOTE: the previous information was obtained from the April 2009 CPU.  Oracle has not commented on reliable researcher claims that this issue is for multiple integer overflows in a function that parses an optional data stream within a Microsoft Office file, leading to a heap-based buffer overflow.
CVE-2009-1012
Unspecified vulnerability in the plug-ins for Apache and IIS web servers in Oracle BEA WebLogic Server 7.0 Gold through SP7, 8.1 Gold through SP6, 9.0, 9.1, 9.2 Gold through MP3, 10.0 Gold through MP1, and 10.3 allows remote attackers to affect confidentiality, integrity, and availability.  NOTE: the previous information was obtained from the April 2009 CPU.  Oracle has not commented on claims from a reliable researcher that this is an integer overflow in an unspecified plug-in that parses HTTP requests, which leads to a heap-based buffer overflow.
CVE-2009-1013
Unspecified vulnerability in the PeopleSoft Enterprise PeopleTools component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.49.19 allows remote attackers to affect confidentiality and integrity via unknown vectors, a different vulnerability than CVE-2009-1014.
CVE-2009-1014
Unspecified vulnerability in the PeopleSoft Enterprise PeopleTools component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.49.19 allows remote attackers to affect confidentiality and integrity via unknown vectors, a different vulnerability than CVE-2009-1013.
CVE-2009-1016
Unspecified vulnerability in the WebLogic Server component in BEA Product Suite 10.3, 10.0 MP1, 9.2 MP3, 9.1, 9.0, 8.1 SP6, and 7.0 SP7 allows remote authenticated users to affect confidentiality, integrity, and availability, related to IIS.  NOTE: the previous information was obtained from the April 2009 CPU.  Oracle has not commented on claims from a reliable researcher that this is a stack-based buffer overflow involving an unspecified Server Plug-in and a crafted SSL certificate.
CVE-2009-1017
Unspecified vulnerability in the BI Publisher component in Oracle Application Server 5.6.2, 10.1.3.2.1, 10.1.3.3.3, and 10.1.3.4 allows remote authenticated users to affect confidentiality via unknown vectors, a different vulnerability than CVE-2009-0994.
CVE-2009-1119
Multiple heap-based buffer overflows in EMC RepliStor 6.2 before SP5 and 6.3 before SP2 allow remote attackers to execute arbitrary code via a crafted message to (1) ctrlservice.exe or (2) rep_srv.exe, possibly related to an integer overflow.
