CVE-2011-1222
Buffer overflow in the Journal Based Backup (JBB) feature in the backup-archive client in IBM Tivoli Storage Manager (TSM) before 5.4.3.4, 5.5.x before 5.5.3, 6.x before 6.1.4, and 6.2.x before 6.2.2 on Windows and AIX allows local users to gain privileges via unspecified vectors.
CVE-2011-1223
Buffer overflow in the Alternate Data Stream (aka ADS or named stream) functionality in the backup-archive client in IBM Tivoli Storage Manager (TSM) before 5.4.3.4, 5.5.x before 5.5.3, 6.x before 6.1.4, and 6.2.x before 6.2.2 on Windows allows local users to gain privileges via unspecified vectors.
CVE-2011-2501
The png_format_buffer function in pngerror.c in libpng 1.0.x before 1.0.55, 1.2.x before 1.2.45, 1.4.x before 1.4.8, and 1.5.x before 1.5.4 allows remote attackers to cause a denial of service (application crash) via a crafted PNG image that triggers an out-of-bounds read during the copying of error-message data.  NOTE: this vulnerability exists because of a CVE-2004-0421 regression. NOTE: this is called an off-by-one error by some sources.
CVE-2011-2690
Buffer overflow in libpng 1.0.x before 1.0.55, 1.2.x before 1.2.45, 1.4.x before 1.4.8, and 1.5.x before 1.5.4, when used by an application that calls the png_rgb_to_gray function but not the png_set_expand function, allows remote attackers to overwrite memory with an arbitrary amount of data, and possibly have unspecified other impact, via a crafted PNG image.
CVE-2011-2691
The png_err function in pngerror.c in libpng 1.0.x before 1.0.55, 1.2.x before 1.2.45, 1.4.x before 1.4.8, and 1.5.x before 1.5.4 makes a function call using a NULL pointer argument instead of an empty-string argument, which allows remote attackers to cause a denial of service (application crash) via a crafted PNG image.
Per: http://cwe.mitre.org/data/definitions/476.html
'CWE-476: NULL Pointer Dereference'
CVE-2011-2692
The png_handle_sCAL function in pngrutil.c in libpng 1.0.x before 1.0.55, 1.2.x before 1.2.45, 1.4.x before 1.4.8, and 1.5.x before 1.5.4 does not properly handle invalid sCAL chunks, which allows remote attackers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via a crafted PNG image that triggers the reading of uninitialized memory.
CVE-2011-2750
NFRAgent.exe in Novell File Reporter 1.0.4.2 and earlier allows remote attackers to delete arbitrary files via a full pathname in an SRS OPERATION 4 CMD 5 request to /FSF/CMD.
CVE-2011-2751
SQL injection vulnerability in Parodia before 6.809 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2011-2752
CRLF injection vulnerability in SquirrelMail 1.4.21 and earlier allows remote attackers to modify or add preference values via a \n (newline) character, a different vulnerability than CVE-2010-4555.
CVE-2011-2753
Multiple cross-site request forgery (CSRF) vulnerabilities in SquirrelMail 1.4.21 and earlier allow remote attackers to hijack the authentication of unspecified victims via vectors involving (1) the empty trash implementation and (2) the Index Order (aka options_order) page, a different issue than CVE-2010-4555.
CVE-2011-2754
Cross-site scripting (XSS) vulnerability in the PageBuilder2 (aka Page Builder) theme in IBM WebSphere Portal 7.x before 7.0.0.1 CF006, as used in IBM Web Content Manager (WCM) and other products, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-2755
Directory traversal vulnerability in FileDownload.jsp in ManageEngine ServiceDesk Plus 8.0 before Build 8012 allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2011-2756
FileDownload.jsp in ManageEngine ServiceDesk Plus 8.0 before Build 8012 does not require authentication, which allows remote attackers to read files from a specific directory via unspecified vectors.
CVE-2011-2757
Directory traversal vulnerability in FileDownload.jsp in ManageEngine ServiceDesk Plus 8.0.0.12 and earlier allows remote attackers to read arbitrary files via a .. (dot dot) in the FILENAME parameter.  NOTE: this might overlap the US-CERT VU#543310 issue.
CVE-2011-2758
IDSWebApp in the Web Administration Tool in IBM Tivoli Directory Server (TDS) 6.2 before 6.2.0.3-TIV-ITDS-IF0004 does not require authentication for access to LDAP Server log files, which allows remote attackers to obtain sensitive information via a crafted URL.
CVE-2011-2759
The login page of IDSWebApp in the Web Administration Tool in IBM Tivoli Directory Server (TDS) 6.2 before 6.2.0.3-TIV-ITDS-IF0004 does not have an off autocomplete attribute for authentication fields, which makes it easier for remote attackers to obtain access by leveraging an unattended workstation.
CVE-2011-2760
Brocade BigIron RX switches allow remote attackers to bypass ACL rules by using 179 as the source port of a packet.
