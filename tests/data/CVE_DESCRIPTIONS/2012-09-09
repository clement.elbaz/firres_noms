CVE-2011-4942
Multiple cross-site scripting (XSS) vulnerabilities in admin/configuration.php in Geeklog before 1.7.1sr1 allow remote attackers to inject arbitrary web script or HTML via the (1) subgroup or (2) conf_group parameters.  NOTE: this vulnerability might require a user-assisted attack or a bypass of a CSRF protection mechanism.
CVE-2011-5159
Cross-site scripting (XSS) vulnerability in admin/configuration.php in Geeklog before 1.7.1sr1 allows remote attackers to inject arbitrary web script or HTML via the sub_group parameter, a different vulnerability than CVE-2011-4942.
CVE-2011-5160
Cross-site scripting (XSS) vulnerability in setup.php in OpenEMR 4 allows remote attackers to inject arbitrary web script or HTML via the site parameter.
CVE-2011-5161
Unrestricted file upload vulnerability in the patient photograph functionality in OpenEMR 4 allows remote attackers to execute arbitrary PHP code by uploading a file with an executable extension followed by a safe extension, then accessing it via a direct request to the patient directory under documents/.
Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'
CVE-2012-1151
Multiple format string vulnerabilities in dbdimp.c in DBD::Pg (aka DBD-Pg or libdbd-pg-perl) module before 2.19.0 for Perl allow remote PostgreSQL database servers to cause a denial of service (process crash) via format string specifiers in (1) a crafted database warning to the pg_warn function or (2) a crafted DBD statement to the dbd_st_prepare function.
CVE-2012-1152
Multiple format string vulnerabilities in the error reporting functionality in the YAML::LibYAML (aka YAML-LibYAML and perl-YAML-LibYAML) module 0.38 for Perl allow remote attackers to cause a denial of service (process crash) via format string specifiers in a (1) YAML stream to the Load function, (2) YAML node to the load_node function, (3) YAML mapping to the load_mapping function, or (4) YAML sequence to the load_sequence function.
CVE-2012-1578
Multiple cross-site request forgery (CSRF) vulnerabilities in MediaWiki 1.17.x before 1.17.3 and 1.18.x before 1.18.2 allow remote attackers to hijack the authentication of users with the block permission for requests that (1) block a user via a request to the Block module or (2) unblock a user via a request to the Unblock module.
CVE-2012-1579
The resource loader in MediaWiki 1.17.x before 1.17.3 and 1.18.x before 1.18.2 includes private data such as CSRF tokens in a JavaScript file, which allows remote attackers to obtain sensitive information.
CVE-2012-1580
Cross-site request forgery (CSRF) vulnerability in Special:Upload in MediaWiki 1.17.x before 1.17.3 and 1.18.x before 1.18.2 allows remote attackers to hijack the authentication of unspecified victims for requests that upload files.
CVE-2012-1581
MediaWiki 1.17.x before 1.17.3 and 1.18.x before 1.18.2 uses weak random numbers for password reset tokens, which makes it easier for remote attackers to change the passwords of arbitrary users.
CVE-2012-1582
Cross-site scripting (XSS) vulnerability in the wikitext parser in MediaWiki 1.17.x before 1.17.3 and 1.18.x before 1.18.2 allows remote attackers to inject arbitrary web script or HTML via a crafted page with "forged strip item markers," as demonstrated using the CharInsert extension.
CVE-2012-1648
Cross-site scripting (XSS) vulnerability in the Cool Aid module before 6.x-1.9 for Drupal allows remote authenticated users with the administer coolaid permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1649
Cool Aid module before 6.x-1.9 for Drupal does not enforce access restrictions, which allows remote authenticated users with the administer coolaid permission to modify arbitrary pages via unspecified vectors.
CVE-2012-1911
Multiple SQL injection vulnerabilities in PHP Address Book 6.2.12 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) to_group parameter to group.php or (2) id parameter to vcard.php.  NOTE: the edit.php vector is already covered by CVE-2008-2565.
CVE-2012-1912
Cross-site scripting (XSS) vulnerability in preferences.php in PHP Address Book 7.0 and earlier allows remote attackers to inject arbitrary web script or HTML via the from parameter.  NOTE: the index.php vector is already covered by CVE-2008-2566.
CVE-2012-2115
SQL injection vulnerability in interface/login/validateUser.php in OpenEMR 4.1.0 and possibly earlier allows remote attackers to execute arbitrary SQL commands via the u parameter.
CVE-2012-2315
admin/Auth in OpenKM 5.1.7 and other versions before 5.1.8-2 does not properly enforce privileges for changing user roles, which allows remote authenticated users to assign administrator privileges to arbitrary users via the userEdit action.
CVE-2012-2316
Cross-site request forgery (CSRF) vulnerability in servlet/admin/AuthServlet.java in OpenKM 5.1.7 and other versions before 5.1.8-2 allows remote attackers to hijack the authentication of administrators for requests that execute arbitrary code via the script parameter to admin/scripting.jsp.
CVE-2012-4885
The wikitext parser in MediaWiki 1.17.x before 1.17.3 and 1.18.x before 1.18.2 allows remote attackers to cause a denial of service (infinite loop) via certain input, as demonstrated by the padleft function.
