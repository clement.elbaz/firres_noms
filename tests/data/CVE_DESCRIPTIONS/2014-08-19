CVE-2014-3341
The SNMP module in Cisco NX-OS 7.0(3)N1(1) and earlier on Nexus 5000 and 6000 devices provides different error messages for invalid requests depending on whether the VLAN ID exists, which allows remote attackers to enumerate VLANs via a series of requests, aka Bug ID CSCup85616.
CVE-2014-3464
The EJB invocation handler implementation in Red Hat JBossWS, as used in JBoss Enterprise Application Platform (EAP) 6.2.0 and 6.3.0, does not properly enforce the method level restrictions for outbound messages, which allows remote authenticated users to access otherwise restricted JAX-WS handlers by leveraging permissions to the EJB class. NOTE: this vulnerability exists because of an incomplete fix for CVE-2013-2133.
CVE-2014-3472
The isCallerInRole function in SimpleSecurityManager in JBoss Application Server (AS) 7, as used in Red Hat JBoss Enterprise Application Platform (JBEAP) 6.3.0, does not properly check caller roles, which allows remote authenticated users to bypass access restrictions via unspecified vectors.
CVE-2014-3490
RESTEasy 2.3.1 before 2.3.8.SP2 and 3.x before 3.0.9, as used in Red Hat JBoss Enterprise Application Platform (EAP) 6.3.0, does not disable external entities when the resteasy.document.expand.entity.references parameter is set to false, which allows remote attackers to read arbitrary files and have other unspecified impact via unspecified vectors, related to an XML External Entity (XXE) issue.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2012-0818.
<a href="http://cwe.mitre.org/data/definitions/611.html" rel="nofollow">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-3504
The (1) serf_ssl_cert_issuer, (2) serf_ssl_cert_subject, and (3) serf_ssl_cert_certificate functions in Serf 0.2.0 through 1.3.x before 1.3.7 does not properly handle a NUL byte in a domain name in the subject's Common Name (CN) field of an X.509 certificate, which allows man-in-the-middle attackers to spoof arbitrary SSL servers via a crafted certificate issued by a legitimate Certification Authority.
<a href="http://cwe.mitre.org/data/definitions/297.html" target="_blank">CWE-297: Improper Validation of Certificate with Host Mismatch</a>
CVE-2014-3522
The Serf RA layer in Apache Subversion 1.4.0 through 1.7.x before 1.7.18 and 1.8.x before 1.8.10 does not properly handle wildcards in the Common Name (CN) or subjectAltName field of the X.509 certificate, which allows man-in-the-middle attackers to spoof servers via a crafted certificate.
<a href="http://cwe.mitre.org/data/definitions/297.html" target="_blank">CWE-297: Improper Validation of Certificate with Host Mismatch</a>
CVE-2014-3528
Apache Subversion 1.0.0 through 1.7.x before 1.7.17 and 1.8.x before 1.8.10 uses an MD5 hash of the URL and authentication realm to store cached credentials, which makes it easier for remote servers to obtain the credentials via a crafted authentication realm.
CVE-2014-3903
Cross-site scripting (XSS) vulnerability in the Cakifo theme 1.x before 1.6.2 for WordPress allows remote authenticated users to inject arbitrary web script or HTML via crafted Exif data.
CVE-2014-3906
SQL injection vulnerability in OSK Advance-Flow 4.41 and earlier and Advance-Flow Forms 4.41 and earlier allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-4615
The notifier middleware in OpenStack PyCADF 0.5.0 and earlier, Telemetry (Ceilometer) 2013.2 before 2013.2.4 and 2014.x before 2014.1.2, Neutron 2014.x before 2014.1.2 and Juno before Juno-2, and Oslo allows remote authenticated users to obtain X_AUTH_TOKEN values by reading the message queue (v2/meters/http.request).
CVE-2014-5033
KDE kdelibs before 4.14 and kauth before 5.1 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, related to CVE-2013-4288 and "PID reuse race conditions."
CVE-2014-5333
Adobe Flash Player before 13.0.0.241 and 14.x before 14.0.0.176 on Windows and OS X and before 11.2.202.400 on Linux, Adobe AIR before 14.0.0.178 on Windows and OS X and before 14.0.0.179 on Android, Adobe AIR SDK before 14.0.0.178, and Adobe AIR SDK & Compiler before 14.0.0.178 do not properly restrict the SWF file format, which allows remote attackers to conduct cross-site request forgery (CSRF) attacks against JSONP endpoints, and obtain sensitive information, via a crafted OBJECT element with SWF content satisfying the character-set requirements of a callback API, in conjunction with a manipulation involving a '$' (dollar sign) or '(' (open parenthesis) character. NOTE: this issue exists because of an incomplete fix for CVE-2014-4671.
CVE-2014-5343
Cross-site scripting (XSS) vulnerability in Feng Office allows remote attackers to inject arbitrary web script or HTML via a client Name field.
CVE-2014-5344
Multiple cross-site scripting (XSS) vulnerabilities in the Mobiloud (mobiloud-mobile-app-plugin) plugin before 2.3.8 for WordPress allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2014-5345
Cross-site scripting (XSS) vulnerability in upgrade.php in the Disqus Comment System plugin before 2.76 for WordPress allows remote attackers to inject arbitrary web script or HTML via the step parameter.
CVE-2014-5346
Multiple cross-site request forgery (CSRF) vulnerabilities in the Disqus Comment System plugin 2.77 for WordPress allow remote attackers to hijack the authentication of administrators for requests that (1) activate or (2) deactivate the plugin via the active parameter to wp-admin/edit-comments.php, (3) import comments via an import_comments action, or (4) export comments via an export_comments action to wp-admin/index.php.
CVE-2014-5347
Multiple cross-site request forgery (CSRF) vulnerabilities in the Disqus Comment System plugin before 2.76 for WordPress allow remote attackers to hijack the authentication of administrators for requests that conduct cross-site scripting (XSS) attacks via the (1) disqus_replace, (2) disqus_public_key, or (3) disqus_secret_key parameter to wp-admin/edit-comments.php in manage.php or that (4) reset or (5) delete plugin options via the reset parameter to wp-admin/edit-comments.php.
CVE-2014-5348
Cross-site scripting (XSS) vulnerability in apps/zxtm/locallog.cgi in Riverbed Stingray (aka SteelApp) Traffic Manager Virtual Appliance 9.6 patchlevel 9620140312 allows remote attackers to inject arbitrary web script or HTML via the logfile parameter.
CVE-2014-5349
Stack-based buffer overflow in Baidu Spark Browser 26.5.9999.3511 allows remote attackers to cause a denial of service (application crash) via nested calls to the window.print JavaScript function.
CVE-2014-5350
Multiple directory traversal vulnerabilities in Bitdefender GravityZone before 5.1.11.432 allow remote attackers to read arbitrary files via a (1) .. (dot dot) in the id parameter to webservice/CORE/downloadFullKitEpc/a/1 in the Web Console or (2) %2E%2E (encoded dot dot) in the default URI to port 7074 on the Update Server.
