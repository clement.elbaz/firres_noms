CVE-2015-1503
Multiple directory traversal vulnerabilities in IceWarp Mail Server before 11.2 allow remote attackers to read arbitrary files via a (1) .. (dot dot) in the file parameter to a webmail/client/skins/default/css/css.php page or .../. (dot dot dot slash dot) in the (2) script or (3) style parameter to webmail/old/calendar/minimizer/index.php.
CVE-2017-17539
The presence of a hardcoded account in Fortinet FortiWLC 7.0.11 and earlier allows attackers to gain unauthorized read/write access via a remote shell.
CVE-2017-17540
The presence of a hardcoded account in Fortinet FortiWLC 8.3.3 allows attackers to gain unauthorized read/write access via a remote shell.
CVE-2017-2592
python-oslo-middleware before versions 3.8.1, 3.19.1, 3.23.1 is vulnerable to an information disclosure. Software using the CatchError class could include sensitive values in a traceback's error message. System users could exploit this flaw to obtain sensitive information from OpenStack component error logs (for example, keystone tokens).
CVE-2017-2594
hawtio before versions 2.0-beta-1, 2.0-beta-2 2.0-m1, 2.0-m2, 2.0-m3, and 1.5 is vulnerable to a path traversal that leads to a NullPointerException with a full stacktrace. An attacker could use this flaw to gather undisclosed information from within hawtio's root.
CVE-2017-2606
Jenkins before versions 2.44, 2.32.2 is vulnerable to an information exposure in the internal API that allows access to item names that should not be visible (SECURITY-380). This only affects anonymous users (other users legitimately have access) that were able to get a list of items via an UnprotectedRootAction.
CVE-2017-2611
Jenkins before versions 2.44, 2.32.2 is vulnerable to an insufficient permission check for periodic processes (SECURITY-389). The URLs /workspaceCleanup and /fingerprintCleanup did not perform permission checks, allowing users with read access to Jenkins to trigger these background processes (that are otherwise performed daily), possibly causing additional load on Jenkins master and agents.
CVE-2018-1000168
nghttp2 version >= 1.10.0 and nghttp2 <= v1.31.0 contains an Improper Input Validation CWE-20 vulnerability in ALTSVC frame handling that can result in segmentation fault leading to denial of service. This attack appears to be exploitable via network client. This vulnerability appears to have been fixed in >= 1.31.1.
CVE-2018-1000173
A session fixaction vulnerability exists in Jenkins Google Login Plugin 1.3 and older in GoogleOAuth2SecurityRealm.java that allows unauthorized attackers to impersonate another user if they can control the pre-authentication session.
CVE-2018-1000174
An open redirect vulnerability exists in Jenkins Google Login Plugin 1.3 and older in GoogleOAuth2SecurityRealm.java that allows attackers to redirect users to an arbitrary URL after successful login.
CVE-2018-1000175
A path traversal vulnerability exists in Jenkins HTML Publisher Plugin 1.15 and older in HtmlPublisherTarget.java that allows attackers able to configure the HTML Publisher build step to override arbitrary files on the Jenkins master.
CVE-2018-1000176
An exposure of sensitive information vulnerability exists in Jenkins Email Extension Plugin 2.61 and older in src/main/resources/hudson/plugins/emailext/ExtendedEmailPublisher/global.groovy and ExtendedEmailPublisherDescriptor.java that allows attackers with control of a Jenkins administrator's web browser (e.g. malicious extension) to retrieve the configured SMTP password.
CVE-2018-1000177
A cross-site scripting vulnerability exists in Jenkins S3 Plugin 0.10.12 and older in src/main/resources/hudson/plugins/s3/S3ArtifactsProjectAction/jobMain.jelly that allows attackers able to control file names of uploaded files to define file names containing JavaScript that would be executed in another user's browser when that user performs some UI actions.
CVE-2018-1000178
A heap corruption of type CWE-120 exists in quassel version 0.12.4 in quasselcore in void DataStreamPeer::processMessage(const QByteArray &msg) datastreampeer.cpp line 62 that allows an attacker to execute code remotely.
CVE-2018-1000179
A NULL Pointer Dereference of CWE-476 exists in quassel version 0.12.4 in the quasselcore void CoreAuthHandler::handle(const Login &msg) coreauthhandler.cpp line 235 that allows an attacker to cause a denial of service.
CVE-2018-10380
kwallet-pam in KDE KWallet before 5.12.6 allows local users to obtain ownership of arbitrary files via a symlink attack.
CVE-2018-10734
KONGTOP DVR devices A303, A403, D303, D305, and D403 contain a backdoor that prints the login password via a Print_Password function call in certain circumstances.
CVE-2018-10796
In 2345 Security Guard 3.7, the driver file (2345NetFirewall.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x00222014.
CVE-2018-10798
A hang issue was discovered in Brave before 0.14.0 (on, for example, Linux). The vulnerability is caused by mishandling of JavaScript code that triggers the reload of a page continuously with an interval of 1 second.
CVE-2018-10799
A hang issue was discovered in Brave before 0.14.0 (on, for example, Linux). This vulnerability is caused by the mishandling of a long URL formed by window.location+='?\u202a\uFEFF\u202b'; concatenation in a SCRIPT element.
CVE-2018-10801
TIFFClientOpen in tif_unix.c in LibTIFF 3.8.2 has memory leaks, as demonstrated by bmp2tiff.
CVE-2018-10804
ImageMagick version 7.0.7-28 contains a memory leak in WriteTIFFImage in coders/tiff.c.
CVE-2018-10805
ImageMagick version 7.0.7-28 contains a memory leak in ReadYCBCRImage in coders/ycbcr.c.
CVE-2018-10806
An issue was discovered in Frog CMS 0.9.5. There is a reflected Cross Site Scripting Vulnerability via the file[current_name] parameter to the admin/?/plugin/file_manager/rename URI. This can be used in conjunction with CSRF.
CVE-2018-10809
In 2345 Security Guard 3.7, the driver file (2345NetFirewall.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x00222040. NOTE: this vulnerability exists because of an incomplete fix for CVE-2018-8873.
CVE-2018-10812
The Bitpie application through 3.2.4 for Android and iOS uses cleartext storage for digital currency initial keys, which allows local users to steal currency by leveraging root access to read /com.biepie/shared_prefs/com.bitpie_preferences.xml (on Android) or a plist file in the app data folder (on iOS).
CVE-2018-1239
Dell EMC Unity Operating Environment (OE) versions prior to 4.3.0.1522077968 are affected by multiple OS command injection vulnerabilities. A remote application admin user could potentially exploit the vulnerabilities to execute arbitrary OS commands as system root on the system where Dell EMC Unity is installed.
CVE-2018-1247
RSA Authentication Manager Security Console, version 8.3 and earlier, contains a XML External Entity (XXE) vulnerability. This could potentially allow admin users to cause a denial of service or extract server data via injecting a maliciously crafted DTD in an XML file submitted to the application.
CVE-2018-1248
RSA Authentication Manager Security Console, Operation Console and Self-Service Console, version 8.3 and earlier, is affected by a Host header injection vulnerability. This could allow a remote attacker to potentially poison HTTP cache and subsequently redirect users to arbitrary web domains.
CVE-2018-6510
A cross-site scripting vulnerability in Puppet Enterprise Console of Puppet Enterprise allows a user to inject scripts into the Puppet Enterprise Console when using the Orchestrator. Affected releases are Puppet Puppet Enterprise: 2017.3.x versions prior to 2017.3.6.
CVE-2018-6511
A cross-site scripting vulnerability in Puppet Enterprise Console of Puppet Enterprise allows a user to inject scripts into the Puppet Enterprise Console when using the Puppet Enterprise Console. Affected releases are Puppet Puppet Enterprise: 2017.3.x versions prior to 2017.3.6.
CVE-2018-6920
In FreeBSD before 11.1-STABLE(r332303), 11.1-RELEASE-p10, 10.4-STABLE(r332321), and 10.4-RELEASE-p9, due to insufficient initialization of memory copied to userland in the Linux subsystem and Atheros wireless driver, small amounts of kernel memory may be disclosed to userland processes. Unprivileged authenticated local users may be able to access small amounts of privileged kernel data.
CVE-2018-6921
In FreeBSD before 11.1-STABLE(r332066) and 11.1-RELEASE-p10, due to insufficient initialization of memory copied to userland in the network subsystem, small amounts of kernel memory may be disclosed to userland processes. Unprivileged authenticated local users may be able to access small amounts of privileged kernel data.
CVE-2018-8897
A statement in the System Programming Guide of the Intel 64 and IA-32 Architectures Software Developer's Manual (SDM) was mishandled in the development of some or all operating-system kernels, resulting in unexpected behavior for #DB exceptions that are deferred by MOV SS or POP SS, as demonstrated by (for example) privilege escalation in Windows, macOS, some Xen configurations, or FreeBSD, or a Linux kernel crash. The MOV to SS and POP SS instructions inhibit interrupts (including NMIs), data breakpoints, and single step trap exceptions until the instruction boundary following the next instruction (SDM Vol. 3A; section 6.8.3). (The inhibited data breakpoints are those on memory accessed by the MOV to SS or POP to SS instruction itself.) Note that debug exceptions are not inhibited by the interrupt enable (EFLAGS.IF) system flag (SDM Vol. 3A; section 2.3). If the instruction following the MOV to SS or POP to SS instruction is an instruction like SYSCALL, SYSENTER, INT 3, etc. that transfers control to the operating system at CPL < 3, the debug exception is delivered after the transfer to CPL < 3 is complete. OS kernels may not expect this order of events and may therefore experience unexpected behavior when it occurs.
