CVE-2014-0929
Cross-site request forgery (CSRF) vulnerability in the Profiles component in IBM Connections through 3.0.1.1 CR3 allows remote authenticated users to hijack the authentication of arbitrary users for requests that trigger follow actions.
CVE-2014-0936
IBM Security AppScan Source 8.0 through 9.0, when the publish-assessment permission is not properly restricted for the configured database server, transmits cleartext assessment data, which allows remote attackers to obtain sensitive information by sniffing the network.
CVE-2014-0961
Cross-site request forgery (CSRF) vulnerability in IBM Tivoli Identity Manager (ITIM) 5.0 before 5.0.0.15 and 5.1 before 5.1.0.15 and IBM Security Identity Manager (ISIM) 6.0 before 6.0.0.2 allows remote authenticated users to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2014-2506
EMC Documentum Content Server before 6.7 SP1 P28, 6.7 SP2 before P14, 7.0 before P15, and 7.1 before P05 allows remote authenticated users to obtain super-user privileges for system-object creation, and bypass intended restrictions on data access and server actions, via unspecified vectors.
CVE-2014-2507
EMC Documentum Content Server before 6.7 SP1 P28, 6.7 SP2 before P14, 7.0 before P15, and 7.1 before P05 allows remote authenticated users to execute arbitrary commands via shell metacharacters in arguments to unspecified methods.
CVE-2014-2508
EMC Documentum Content Server before 6.7 SP1 P28, 6.7 SP2 before P14, 7.0 before P15, and 7.1 before P05 allows remote authenticated users to conduct Documentum Query Language (DQL) injection attacks and bypass intended restrictions on database actions via vectors involving DQL hints.
CVE-2014-3036
Unspecified vulnerability in IBM API Management 3.0.0.0, when basic authentication is used for APIs, allows remote attackers to bypass intended restrictions on topology access, and obtain sensitive information, via unknown vectors.
CVE-2014-3038
IBM SPSS Modeler 16.0 before 16.0.0.1 on UNIX does not properly drop group privileges, which allows local users to bypass intended file-access restrictions by leveraging (1) gid 0 or (2) root's group memberships.
CVE-2014-3048
Unspecified vulnerability on the IBM System Storage Virtualization Engine TS7700 allows local users to gain privileges by leveraging the TSSC service-user role to enter a crafted SSH command.
CVE-2014-3278
The web framework in VOSS in Cisco Unified Communications Domain Manager (CDM) does not properly implement access control, which allows remote attackers to enumerate accounts by visiting an unspecified BVSMWeb web page, aka Bug IDs CSCun39619 and CSCun45572.
CVE-2014-3281
The web framework in VOSS in Cisco Unified Communications Domain Manager (CDM) does not properly implement access control, which allows remote attackers to obtain potentially sensitive user information by visiting an unspecified BVSMWeb web page, aka Bug IDs CSCun46071 and CSCun46101.
CVE-2014-3286
The web framework in Cisco WebEx Meeting Server does not properly restrict the content of reply messages, which allows remote attackers to obtain sensitive information via a crafted URL, aka Bug IDs CSCuj81685, CSCuj81688, CSCuj81665, CSCuj81744, and CSCuj81661.
CVE-2014-3291
Cisco Wireless LAN Controller (WLC) devices allow remote attackers to cause a denial of service (NULL pointer dereference and device restart) via a zero value in Cisco Discovery Protocol packet data that is not properly handled during SNMP polling, aka Bug ID CSCuo12321.
Per: http://cwe.mitre.org/data/definitions/476.html

"CWE-476: NULL Pointer Dereference"
CVE-2014-3977
libodm.a in IBM AIX 6.1 and 7.1, and VIOS 2.2.x, allows local users to overwrite arbitrary files via a symlink attack on a temporary file. NOTE: this vulnerability exists because of an incomplete fix for CVE-2012-2179.
CVE-2014-3981
acinclude.m4, as used in the configure script in PHP 5.5.13 and earlier, allows local users to overwrite arbitrary files via a symlink attack on the /tmp/phpglibccheck file.
CVE-2014-3982
include/tests_webservers in Lynis before 1.5.5 on AIX allows local users to overwrite arbitrary files via a symlink attack on a /tmp/lynis.##### file.
CVE-2014-3986
include/tests_webservers in Lynis before 1.5.5 allows local users to overwrite arbitrary files via a symlink attack on a /tmp/lynis.*.unsorted file with an easily determined name.
