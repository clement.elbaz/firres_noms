CVE-2010-5299
Stack-based buffer overflow in MicroP 0.1.1.1600 allows remote attackers to execute arbitrary code via a crafted .mppl file.  NOTE: it has been reported that the overflow is in the lpFileName parameter of the CreateFileA function, but the overflow is probably caused by a separate, unnamed function.
CVE-2012-5649
Apache CouchDB before 1.0.4, 1.1.x before 1.1.2, and 1.2.x before 1.2.1 allows remote attackers to execute arbitrary code via a JSONP callback, related to Adobe Flash.
CVE-2013-0289
Isync 0.4 before 1.0.6, does not verify that the server hostname matches a domain name in the subject's Common Name (CN) or subjectAltName field of the X.509 certificate, which allows man-in-the-middle attackers to spoof SSL servers via an arbitrary valid certificate.
CVE-2013-1668
The uploadFile function in upload/index.php in CosCMS before 1.822 allows remote administrators to execute arbitrary commands via shell metacharacters in the name of an uploaded file.
CVE-2013-1864
The Portable Tool Library (aka PTLib) before 2.10.10, as used in Ekiga before 4.0.1, does not properly detect recursion during entity expansion, which allows remote attackers to cause a denial of service (memory and CPU consumption) via a crafted PXML document containing a large number of nested entity references, aka a "billion laughs attack."
CVE-2013-2107
Cross-site request forgery (CSRF) vulnerability in the Mail On Update plugin before 5.2.0 for WordPress allows remote attackers to hijack the authentication of administrators for requests that change the "List of alternative recipients" via the mailonupdate_mailto parameter in the mail-on-update page to wp-admin/options-general.php.  NOTE: a third party claims that 5.2.1 and 5.2.2 are also vulnerable, but the issue might require a separate CVE identifier since this might reflect an incomplete fix.
CVE-2013-2712
Cross-site scripting (XSS) vulnerability in services/get_article.php in KrisonAV CMS before 3.0.2 allows remote attackers to inject arbitrary web script or HTML via the content parameter.
CVE-2013-2713
Cross-site request forgery (CSRF) vulnerability in users_maint.html in KrisonAV CMS before 3.0.2 allows remote attackers to hijack the authentication of administrators for requests that create user accounts via a crafted request.
CVE-2013-2756
Apache CloudStack 4.0.0 before 4.0.2 and Citrix CloudPlatform (formerly Citrix CloudStack) 3.0.x before 3.0.6 Patch C allows remote attackers to bypass the console proxy authentication by leveraging knowledge of the source code.
CVE-2013-2757
Citrix CloudPlatform (formerly Citrix CloudStack) 3.0.x before 3.0.6 Patch C does not properly restrict access to VNC ports on the management network, which allows remote attackers to have unspecified impact via unknown vectors.
CVE-2013-2758
Apache CloudStack 4.0.0 before 4.0.2 and Citrix CloudPlatform (formerly Citrix CloudStack) 3.0.x before 3.0.6 Patch C uses a hash of a predictable sequence, which makes it easier for remote attackers to guess the console access URL via a brute force attack.
CVE-2013-4223
The Gentoo Nullmailer package before 1.11-r2 uses world-readable permissions for /etc/nullmailer/remotes, which allows local users to obtain SMTP authentication credentials by reading the file.
CVE-2014-3442
Winamp 5.666 and earlier allows remote attackers to cause a denial of service (memory corruption and crash) via a malformed .FLV file, related to f263.w5s.
CVE-2014-3450
Unspecified vulnerability in Panda Gold Protection and Global Protection 2014 7.01.01 and earlier, Internet Security 2014 19.01.01 and earlier, and AV Pro 2014 13.01.01 and earlier allows local users to gain privileges via unspecified vectors.
CVE-2014-3801
OpenStack Orchestration API (Heat) 2013.2 through 2013.2.3 and 2014.1, when creating the stack for a template using a provider template, allows remote authenticated users to obtain the provider template URL via the resource-type-list.
CVE-2014-3848
The iMember360 plugin before 3.9.001 for WordPress does not properly restrict access, which allows remote attackers to obtain database credentials via the i4w_dbinfo parameter.
CVE-2014-3849
The iMember360 plugin 3.8.012 through 3.9.001 for WordPress does not properly restrict access, which allows remote attackers to delete arbitrary users via a request containing a user name in the Email parameter and the API key in the i4w_clearuser parameter.
