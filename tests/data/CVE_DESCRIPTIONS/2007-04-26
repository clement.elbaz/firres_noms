CVE-2007-1683
Stack-based buffer overflow in the DoWebMenuAction function in the IncrediMail IMMenuShellExt ActiveX control (ImShExt.dll) allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2007-2282
Cisco Network Services (CNS) NetFlow Collection Engine (NFC) before 6.0 has an nfcuser account with the default password nfcuser, which allows remote attackers to modify the product configuration and, when installed on Linux, obtain login access to the host operating system.
The vendor has addressed this issue through the update 6.0.0 of the NetFlow Collection Engine.
CVE-2007-2283
Buffer overflow in Fresh View 7.15 allows user-assisted remote attackers to execute arbitrary code via a crafted .PSP file.
CVE-2007-2284
Buffer overflow in ABC-View Manager 1.42 allows user-assisted remote attackers to execute arbitrary code via a crafted .PSP file.
CVE-2007-2285
Directory traversal vulnerability in examples/layout/feed-proxy.php in Jack Slocum Ext 1.0 alpha1 (Ext JS) allows remote attackers to read arbitrary files via a .. (dot dot) in the feed parameter.  NOTE: analysis by third party researchers indicates that this issue might be platform dependent.
CVE-2007-2286
PHP remote file inclusion vulnerability in config.php in Built2Go PHP Link Portal 1.79 allows remote attackers to execute arbitrary PHP code via a URL in the full_path_to_db parameter.
CVE-2007-2287
PHP remote file inclusion vulnerability in accept.php in comus 2.0 Final allows remote attackers to execute arbitrary PHP code via a URL in the DOCUMENT_ROOT parameter.
CVE-2007-2288
PHP remote file inclusion vulnerability in info.php in Doruk100.net doruk100net allows remote attackers to execute arbitrary PHP code via a URL in the file parameter.
CVE-2007-2289
PHP remote file inclusion vulnerability in admin/includes/spaw/dialogs/insert_link.php in download engine (Download-Engine) 1.4.1 allows remote authenticated users to execute arbitrary PHP code via a URL in the spaw_root parameter, a different vector than CVE-2007-2255.  NOTE: this may be an issue in SPAW.
CVE-2007-2290
Multiple PHP remote file inclusion vulnerabilities in B2 Weblog and News Publishing Tool 0.6.1 allow remote attackers to execute arbitrary PHP code via a URL in the b2inc parameter to (1) b2archives.php, (2) b2categories.php, or (3) b2mail.php.  NOTE: this may overlap CVE-2002-1466.
CVE-2007-2291
CRLF injection vulnerability in the Digest Authentication support for Microsoft Internet Explorer 7.0.5730.11 allows remote attackers to conduct HTTP response splitting attacks via a LF (%0a) in the username attribute.
CVE-2007-2292
CRLF injection vulnerability in the Digest Authentication support for Mozilla Firefox before 2.0.0.8 and SeaMonkey before 1.1.5 allows remote attackers to conduct HTTP request splitting attacks via LF (%0a) bytes in the username attribute.
CVE-2007-2293
Multiple stack-based buffer overflows in the process_sdp function in chan_sip.c of the SIP channel T.38 SDP parser in Asterisk before 1.4.3 allow remote attackers to execute arbitrary code via a long (1) T38FaxRateManagement or (2) T38FaxUdpEC SDP parameter in an SIP message, as demonstrated using SIP INVITE.
CVE-2007-2294
The Manager Interface in Asterisk before 1.2.18 and 1.4.x before 1.4.3 allows remote attackers to cause a denial of service (crash) by using MD5 authentication to authenticate a user that does not have a password defined in manager.conf, resulting in a NULL pointer dereference.
Successful exploitation requires that the Management Interface is enabled and a user without a password is configured in the manager.conf file.
CVE-2007-2295
Heap-based buffer overflow in the JVTCompEncodeFrame function in Apple Quicktime 7.1.5 and other versions before 7.2 allows remote attackers to execute arbitrary code via a crafted H.264 MOV file.
CVE-2007-2296
Integer overflow in the FlipFileTypeAtom_BtoN function in Apple Quicktime 7.1.5, and other versions before 7.2, allows remote attackers to execute arbitrary code via a crafted M4V (MP4) file.
CVE-2007-2297
The SIP channel driver (chan_sip) in Asterisk before 1.2.18 and 1.4.x before 1.4.3 does not properly parse SIP UDP packets that do not contain a valid response code, which allows remote attackers to cause a denial of service (crash).
CVE-2007-2298
Multiple PHP remote file inclusion vulnerabilities in Garennes 0.6.1 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the repertoire_config parameter to index.php in (1) cpe/, (2) direction/, or (3) professeurs/.
CVE-2007-2299
Multiple SQL injection vulnerabilities in Frogss CMS 0.7 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) dzial parameter to (a) katalog.php, or the (2) t parameter to (b) forum.php or (c) forum/viewtopic.php, different vectors than CVE-2006-4536.
CVE-2007-2300
Multiple cross-site scripting (XSS) vulnerabilities in Endy Kristanto Surat kabar / News Management Online (aka phpwebnews) 0.2 and earlier allow remote attackers to inject arbitrary web script or HTML via the m_txt parameter to (1) iklan.php, (2) index.php, or (3) bukutamu.php.
CVE-2007-2301
Multiple PHP remote file inclusion vulnerabilities in audioCMS arash 0.1.4 allow remote attackers to execute arbitrary PHP code via a URL in the arashlib_dir parameter to (1) edit.inc.php and (2) list_features.inc.php in arash_lib/include, and (3) arash_gadmin.class.php and (4) arash_sadmin.class.php in arash_lib/class/.
CVE-2007-2302
PHP remote file inclusion vulnerability in autoindex.php in Expow 0.8 allows remote attackers to execute arbitrary PHP code via a URL in the cfg_file parameter.
CVE-2007-2303
Directory traversal vulnerability in includes/footer.php in News Manager Deluxe (NMDeluxe) 1.0.1 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the template parameter.
CVE-2007-2304
Multiple directory traversal vulnerabilities in Quick and Dirty Blog (QDBlog) 0.4, and possibly earlier, allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the theme parameter to categories.php and other unspecified files.
CVE-2007-2305
Multiple SQL injection vulnerabilities in authenticate.php in Quick and Dirty Blog (QDBlog) 0.4, and possibly earlier, allow remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters.
CVE-2007-2306
Multiple cross-site scripting (XSS) vulnerabilities in the Virtual War (VWar) 1.5.0 R15 and earlier module for PHP-Nuke, when register_globals is enabled, allow remote attackers to inject arbitrary web script or HTML via the (1) memberlist parameter to extra/login.php and the (2) title parameter to extra/today.php.
CVE-2007-2307
PHP remote file inclusion vulnerability in engine/engine.inc.php in WebKalk2 1.9.0 allows remote attackers to execute arbitrary PHP code via a URL in the absolute_path parameter.
CVE-2007-2308
Cross-site scripting (XSS) vulnerability in cas.php in FloweRS 2.0 allows remote attackers to inject arbitrary web script or HTML via the rok parameter.
CVE-2007-2309
Cross-site scripting (XSS) vulnerability in cas.php in FloweRS 2.0 allows remote attackers to inject arbitrary web script or HTML via the den parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2310
Cross-site scripting (XSS) vulnerability in plugins/spaw/img_popup.php in BloofoxCMS 0.2.2 allows remote attackers to inject arbitrary web script or HTML via the img_url parameter.
CVE-2007-2311
** DISPUTED **  PHP remote file inclusion vulnerability in install/index.php in BlooFoxCMS 0.2.2 allows remote attackers to execute arbitrary PHP code via a URL in the content_php parameter.  NOTE: this issue has been disputed by a reliable third party, stating that content_php is initialized before use.
CVE-2007-2312
Multiple SQL injection vulnerabilities in the Virtual War (VWar) 1.5.0 R15 module for PHP-Nuke allow remote attackers to execute arbitrary SQL commands via the n parameter to extra/online.php and other unspecified scripts in extra/.  NOTE: this might be same vulnerability as CVE-2006-4142; however, there is an intervening vendor fix announcement.
CVE-2007-2313
PHP remote file inclusion vulnerability in getinfo1.php in the Shotcast 1.0 RC2 module for mxBB allows remote attackers to execute arbitrary PHP code via a URL in the mx_root_path parameter.
CVE-2007-2314
Multiple SQL injection vulnerabilities in Crea-Book 1.0, and possibly earlier, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) pseudo or (2) passe parameter to (a) configurer.php, (b) connect.php, (c) delete.php, (d) delete2.php, (e) index.php, (f) infos.php, (g) membres.php, (h) modif-infos.php, (i) modif-message.php, (j) modif.php, (k) uninstall.php, or (l) uninstall_table.php in admin/, different vectors than CVE-2007-2000.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2315
MiniShare 1.5.4, and possibly earlier, allows remote attackers to cause a denial of service (application crash) via a flood of requests for new connections.
CVE-2007-2316
Unspecified vulnerability in the admin script in Open Business Management (OBM) before 2.0.0 allows remote attackers to have an unknown impact by calling the script "in txt mode from a browser."
CVE-2007-2317
Multiple PHP remote file inclusion vulnerabilities in MiniBB Forum 1.5a and earlier, as used by TOSMO/Mambo 4.0.12 and probably other products, allow remote attackers to execute arbitrary PHP code via a URL in the absolute_path parameter to bb_plugins.php in (1) components/minibb/ or (2) components/com_minibb, or (3) configuration.php.  NOTE: the com_minibb.php vector is already covered by CVE-2006-3690.
CVE-2007-2318
Multiple format string vulnerabilities in FileZilla before 2.2.32 allow remote attackers to execute arbitrary code via format string specifiers in (1) FTP server responses or (2) data sent by an FTP server.  NOTE: some of these details are obtained from third party information.
CVE-2007-2319
PHP remote file inclusion vulnerability in the AutoStand 1.1 and earlier module for Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter to mod_as_category.php in (1) modules/mod_as_category/ or (2) modules/.
CVE-2007-2320
SQL injection vulnerability in kontakt.php in Papoo 3.02 and earlier allows remote attackers to execute arbitrary SQL commands via the menuid parameter, a different vector than CVE-2005-4478.
