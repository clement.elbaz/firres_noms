CVE-2017-2751
A BIOS password extraction vulnerability has been reported on certain consumer notebooks with firmware F.22 and others. The BIOS password was stored in CMOS in a way that allowed it to be extracted. This applies to consumer notebooks launched in early 2014.
CVE-2018-12087
Failure to validate certificates in OPC Foundation UA Client Applications communicating without security allows attackers with control over a piece of network infrastructure to decrypt passwords.
CVE-2018-14800
Delta Electronics ISPSoft version 3.0.5 and prior allow an attacker, by opening a crafted file, to cause the application to read past the boundary allocated to a stack object, which could allow execution of code under the context of the application.
CVE-2018-16048
An issue was discovered in GitLab Community and Enterprise Edition before 11.0.6, 11.1.x before 11.1.5, and 11.2.x before 11.2.2. There is Missing Authorization Control for API Repository Storage.
CVE-2018-16049
An issue was discovered in GitLab Community and Enterprise Edition before 11.0.6, 11.1.x before 11.1.5, and 11.2.x before 11.2.2. There is Sensitive Data Disclosure in Sidekiq Logs through an Error Message.
CVE-2018-16050
An issue was discovered in GitLab Community and Enterprise Edition 11.1.x before 11.1.5 and 11.2.x before 11.2.2. There is Persistent XSS in the Merge Request Changes View.
CVE-2018-16051
An issue was discovered in GitLab Community and Enterprise Edition before 11.0.6, 11.1.x before 11.1.5, and 11.2.x before 11.2.2. There is Orphaned Upload Files Exposure.
CVE-2018-17053
Cross-site scripting (XSS) vulnerability in Identity Server in Progress Sitefinity CMS versions 10.0 through 11.0 allows remote attackers to inject arbitrary web script or HTML via vectors related to login request parameters, a different vulnerability than CVE-2018-17054.
CVE-2018-17054
Cross-site scripting (XSS) vulnerability in Identity Server in Progress Sitefinity CMS versions 10.0 through 11.0 allows remote attackers to inject arbitrary web script or HTML via vectors related to login request parameters, a different vulnerability than CVE-2018-17053.
CVE-2018-17408
Stack-based buffer overflows in Zahir Accounting Enterprise Plus 6 through build 10b allow remote attackers to execute arbitrary code via a crafted CSV file that is accessed through the Import CSV File menu.
CVE-2018-17428
An issue was discovered in OPAC EasyWeb Five 5.7. There is SQL injection via the w2001/index.php?scelta=campi biblio parameter.
CVE-2018-17540
The gmp plugin in strongSwan before 5.7.1 has a Buffer Overflow via a crafted certificate.
CVE-2018-17552
SQL Injection in login.php in Naviwebs Navigate CMS 2.8 allows remote attackers to bypass authentication via the navigate-user cookie.
CVE-2018-17553
An "Unrestricted Upload of File with Dangerous Type" issue with directory traversal in navigate_upload.php in Naviwebs Navigate CMS 2.8 allows authenticated attackers to achieve remote code execution via a POST request with engine=picnik and id=../../../navigate_info.php.
CVE-2018-17562
Multi-Tech FaxFinder before 5.1.6 has SQL Injection via a status/call_details?oid= URI, allowing an attacker to extract the underlying database schema to further disclose other fax server information through different injection points.
CVE-2018-17880
On D-Link DIR-823G 2018-09-19 devices, the GoAhead configuration allows /HNAP1 RunReboot commands without authentication to trigger a reboot.
CVE-2018-17881
On D-Link DIR-823G 2018-09-19 devices, the GoAhead configuration allows /HNAP1 SetPasswdSettings commands without authentication to trigger an admin password change.
CVE-2018-1793
IBM WebSphere Application Server 7.0, 8.0, 8.5, and 9.0 using SAML ear is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 148948.
CVE-2018-17938
Zimbra Collaboration before 8.8.10 GA allows text content spoofing via a loginErrorCode value.
CVE-2018-1794
IBM WebSphere Application Server 7.0, 8.0, 8.5, and 9.0 using OAuth ear is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 148949.
CVE-2018-17942
The convert_to_decimal function in vasnprintf.c in Gnulib before 2018-09-23 has a heap-based buffer overflow because memory is not allocated for a trailing '\0' character during %f processing.
CVE-2018-17946
The Tribulant Slideshow Gallery plugin before 1.6.6.1 for WordPress has XSS via the id, method, Gallerymessage, Galleryerror, or Galleryupdated parameter.
CVE-2018-17947
The Snazzy Maps plugin before 1.1.5 for WordPress has XSS via the text or tab parameter.
CVE-2018-17965
ImageMagick 7.0.7-28 has a memory leak vulnerability in WriteSGIImage in coders/sgi.c.
CVE-2018-17966
ImageMagick 7.0.7-28 has a memory leak vulnerability in WritePDBImage in coders/pdb.c.
CVE-2018-17967
ImageMagick 7.0.7-28 has a memory leak vulnerability in ReadBGRImage in coders/bgr.c.
CVE-2018-17969
Samsung SCX-6545X V2.00.03.01 03-23-2012 devices allows remote attackers to discover cleartext credentials via iso.3.6.1.4.1.236.11.5.11.81.10.1.5.0 and iso.3.6.1.4.1.236.11.5.11.81.10.1.6.0 SNMP requests.
CVE-2018-17972
An issue was discovered in the proc_pid_stack function in fs/proc/base.c in the Linux kernel through 4.18.11. It does not ensure that only root may inspect the kernel stack of an arbitrary task, allowing a local attacker to exploit racy stack unwinding and leak kernel task stack contents.
CVE-2018-17974
An issue was discovered in Tcpreplay 4.3.0 beta1. A heap-based buffer over-read was triggered in the function dlt_en10mb_encode() of the file plugins/dlt_en10mb/en10mb.c, due to inappropriate values in the function memmove(). The length (pktlen + ctx -> l2len) can be larger than source value (packet + ctx->l2len) because the function fails to ensure the length of a packet is valid. This leads to Denial of Service.
CVE-2018-3946
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's PDF Reader version 9.1.0.5096. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-3964
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's Foxit PDF Reader version 9.1.0.5096. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-3965
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's Foxit PDF Reader version 9.1.0.5096. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-3966
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's Foxit PDF Reader version 9.1.0.5096. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-3967
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's Foxit PDF Reader version 9.1.0.5096. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-3993
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's Foxit PDF Reader version 9.2.0.9297. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-3994
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's Foxit PDF Reader version 9.2.0.9297. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-3995
An exploitable use-after-free vulnerability exists in the JavaScript engine of Foxit Software's PDF Reader, version 9.2.0.9297. A specially crafted PDF document can trigger a previously freed object in memory to be reused, resulting in arbitrary code execution. An attacker needs to trick the user to open the malicious file to trigger this vulnerability. If the browser plugin extension is enabled, visiting a malicious site can also trigger the vulnerability.
CVE-2018-5921
A potential security vulnerability has been identified with certain HP printers and MFPs in 2405129_000052 and other firmware versions. This vulnerability is known as Cross Site Request Forgery, and could potentially be exploited remotely to allow elevation of privilege.
CVE-2018-6689
Authentication Bypass vulnerability in McAfee Data Loss Prevention Endpoint (DLPe) 10.0.x earlier than 10.0.510, and 11.0.x earlier than 11.0.600 allows attackers to bypass local security protection via specific conditions.
CVE-2018-6695
SSH host keys generation vulnerability in the server in McAfee Threat Intelligence Exchange Server (TIE Server) 1.3.0, 2.0.x, 2.1.x, 2.2.0 allows man-in-the-middle attackers to spoof servers via acquiring keys from another environment.
