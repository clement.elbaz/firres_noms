CVE-2015-0708
Cisco IOS 15.4S, 15.4SN, and 15.5S and IOS XE 3.13S and 3.14S allow remote attackers to cause a denial of service (device crash) by including an IA_NA option in a DHCPv6 Solicit message on the local network, aka Bug ID CSCur29956.
CVE-2015-0709
Cisco IOS 15.5S and IOS XE allow remote authenticated users to cause a denial of service (device crash) by leveraging knowledge of the RADIUS secret and sending crafted RADIUS packets, aka Bug ID CSCur21348.
CVE-2015-0710
The Overlay Transport Virtualization (OTV) implementation in Cisco IOS XE 3.10S allows remote attackers to cause a denial of service (device reload) via a series of packets that are considered oversized and trigger improper fragmentation handling, aka Bug IDs CSCup37676 and CSCup30335.
CVE-2015-0711
The hamgr service in the IPv6 Proxy Mobile (PM) implementation in Cisco StarOS 18.1.0.59776 on ASR 5000 devices allows remote attackers to cause a denial of service (service reload and call-processing outage) via malformed PM packets, aka Bug ID CSCut94711.
CVE-2015-1321
Use-after-free vulnerability in the file picker implementation in Oxide before 1.6.5 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a crafted webpage.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-1322
Directory traversal vulnerability in the Ubuntu network-manager package for Ubuntu (vivid) before 0.9.10.0-4ubuntu15.1, Ubuntu 14.10 before 0.9.8.8-0ubuntu28.1, and Ubuntu 14.04 LTS before 0.9.8.8-0ubuntu7.1 allows local users to change the modem device configuration or read arbitrary files via a .. (dot dot) in the file name in a request to read modem device contexts (com.canonical.NMOfono.ReadImsiContexts).
CVE-2015-1397
SQL injection vulnerability in the getCsvFile function in the Mage_Adminhtml_Block_Widget_Grid class in Magento Community Edition (CE) 1.9.1.0 and Enterprise Edition (EE) 1.14.1.0 allows remote administrators to execute arbitrary SQL commands via the popularity[field_expr] parameter when the popularity[from] or popularity[to] parameter is set.
CVE-2015-1398
Multiple directory traversal vulnerabilities in Magento Community Edition (CE) 1.9.1.0 and Enterprise Edition (EE) 1.14.1.0 allow remote authenticated users to include and execute certain PHP files via (1) .. (dot dot) sequences in the PATH_INFO to index.php or (2) vectors involving a block value in the ___directive parameter to the Cms_Wysiwyg controller in the Adminhtml module, related to the blockDirective function and the auto loading mechanism.  NOTE: vector 2 might not cross privilege boundaries, since administrators might already have the privileges to execute code and upload files.
CVE-2015-1399
PHP remote file inclusion vulnerability in the fetchView function in the Mage_Core_Block_Template_Zend class in Magento Community Edition (CE) 1.9.1.0 and Enterprise Edition (EE) 1.14.1.0 allows remote administrators to execute arbitrary PHP code via a URL in unspecified vectors involving the setScriptPath function.  NOTE: it is not clear whether this issue crosses privilege boundaries, since administrators might already have privileges to include arbitrary files.
CVE-2015-3026
Icecast before 2.4.2, when a stream_auth handler is defined for URL authentication, allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via a request without login credentials, as demonstrated by a request to "admin/killsource?mount=/test.ogg."
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-3447
Multiple cross-site scripting (XSS) vulnerabilities in macIpSpoofView.html in Dell SonicWall SonicOS 7.5.0.12 and 6.x allow remote attackers to inject arbitrary web script or HTML via the (1) searchSpoof or (2) searchSpoofIpDet parameter.
CVE-2015-3448
REST client for Ruby (aka rest-client) before 1.7.3 logs usernames and passwords, which allows local users to obtain sensitive information by reading the log.
CVE-2015-3457
Magento Community Edition (CE) 1.9.1.0 and Enterprise Edition (EE) 1.14.1.0 allow remote attackers to bypass authentication via the forwarded parameter.
CVE-2015-3458
The fetchView function in the Mage_Core_Block_Template_Zend class in Magento Community Edition (CE) 1.9.1.0 and Enterprise Edition (EE) 1.14.1.0 does not restrict the stream wrapper used in a template path, which allows remote administrators to include and execute arbitrary PHP files via the phar:// stream wrapper, related to the setScriptPath function.  NOTE: it is not clear whether this issue crosses privilege boundaries, since administrators might already have privileges to include arbitrary files.
CVE-2015-3459
The communication module on the Hospira LifeCare PCA Infusion System before 7.0 does not require authentication for root TELNET sessions, which allows remote attackers to modify the pump configuration via unspecified commands.
