CVE-2016-8717
An exploitable Use of Hard-coded Credentials vulnerability exists in the Moxa AWK-3131A Wireless Access Point running firmware 1.1. The device operating system contains an undocumented, privileged (root) account with hard-coded credentials, giving attackers full control of affected devices.
CVE-2018-0194
Multiple vulnerabilities in the CLI parser of Cisco IOS XE Software could allow an authenticated, local attacker to inject arbitrary commands into the CLI of the affected software, which could allow the attacker to gain access to the underlying Linux shell of an affected device and execute commands with root privileges on the device. The vulnerabilities exist because the affected software does not sufficiently sanitize command arguments before passing commands to the Linux shell for execution. An attacker could exploit these vulnerabilities by submitting a malicious CLI command to the affected software. A successful exploit could allow the attacker to break from the CLI of the affected software, which could allow the attacker to gain access to the underlying Linux shell on an affected device and execute arbitrary commands with root privileges on the device. Cisco Bug IDs: CSCuz03145, CSCuz56419, CSCva31971, CSCvb09542.
CVE-2018-1038
The Windows kernel in Windows 7 SP1 and Windows Server 2008 R2 SP1 allows an elevation of privilege vulnerability due to the way it handles objects in memory, aka "Windows Kernel Elevation of Privilege Vulnerability."
CVE-2018-1092
The ext4_iget function in fs/ext4/inode.c in the Linux kernel through 4.15.15 mishandles the case of a root directory with a zero i_links_count, which allows attackers to cause a denial of service (ext4_process_freed_data NULL pointer dereference and OOPS) via a crafted ext4 image.
CVE-2018-1093
The ext4_valid_block_bitmap function in fs/ext4/balloc.c in the Linux kernel through 4.15.15 allows attackers to cause a denial of service (out-of-bounds read and system crash) via a crafted ext4 image because balloc.c and ialloc.c do not validate bitmap block numbers.
CVE-2018-1094
The ext4_fill_super function in fs/ext4/super.c in the Linux kernel through 4.15.15 does not always initialize the crc32c checksum driver, which allows attackers to cause a denial of service (ext4_xattr_inode_hash NULL pointer dereference and system crash) via a crafted ext4 image.
CVE-2018-1095
The ext4_xattr_check_entries function in fs/ext4/xattr.c in the Linux kernel through 4.15.15 does not properly validate xattr sizes, which causes misinterpretation of a size as an error code, and consequently allows attackers to cause a denial of service (get_acl NULL pointer dereference and system crash) via a crafted ext4 image.
CVE-2018-1295
In Apache Ignite 2.3 or earlier, the serialization mechanism does not have a list of classes allowed for serialization/deserialization, which makes it possible to run arbitrary code when 3-rd party vulnerable classes are present in Ignite classpath. The vulnerability can be exploited if the one sends a specially prepared form of a serialized object to one of the deserialization endpoints of some Ignite components - discovery SPI, Ignite persistence, Memcached endpoint, socket steamer.
CVE-2018-6247
NVIDIA Windows GPU Display Driver contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgkDdiEscape where a NULL pointer dereference may lead to denial of service or possible escalation of privileges.
CVE-2018-6248
NVIDIA Windows GPU Display Driver contains a vulnerability in the kernel mode layer handler for DxgkDdiEscape where the software uses a sequential operation to read or write a buffer, but it uses an incorrect length value that causes it to access memory that is outside of the bounds of the buffer which may lead to denial of service or possible escalation of privileges.
CVE-2018-6249
NVIDIA GPU Display Driver contains a vulnerability in kernel mode layer handler where a NULL pointer dereference may lead to denial of service or potential escalation of privileges.
CVE-2018-6250
NVIDIA Windows GPU Display Driver contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgkDdiEscape where a NULL pointer dereference occurs which may lead to denial of service or possible escalation of privileges.
CVE-2018-6251
NVIDIA Windows GPU Display Driver contains a vulnerability in the DirectX 10 Usermode driver, where a specially crafted pixel shader can cause writing to unallocated memory, leading to denial of service or potential code execution.
CVE-2018-6252
NVIDIA Windows GPU Display Driver contains a vulnerability in the kernel mode layer handler for DxgkDdiEscape where the software allows an actor access to restricted functionality that is unnecessary to production usage, and which may result in denial of service.
CVE-2018-6253
NVIDIA GPU Display Driver contains a vulnerability in the DirectX and OpenGL Usermode drivers where a specially crafted pixel shader can cause infinite recursion leading to denial of service.
CVE-2018-6659
Reflected Cross-Site Scripting vulnerability in McAfee ePolicy Orchestrator (ePO) 5.3.2, 5.3.1, 5.3.0 and 5.9.0 allows remote authenticated users to exploit an XSS issue via not sanitizing the user input.
CVE-2018-6660
Directory Traversal vulnerability in McAfee ePolicy Orchestrator (ePO) 5.3.2, 5.3.1, 5.3.0 and 5.9.0 allows administrators to use Windows alternate data streams, which could be used to bypass the file extensions, via not properly validating the path when exporting a particular XML file.
CVE-2018-6661
DLL Side-Loading vulnerability in Microsoft Windows Client in McAfee True Key before 4.20.110 allows local users to gain privilege elevation via not verifying a particular DLL file signature.
CVE-2018-9127
Botan 2.2.0 - 2.4.0 (fixed in 2.5.0) improperly handled wildcard certificates and could accept certain certificates as valid for hostnames when, under RFC 6125 rules, they should not match. This only affects certificates issued to the same domain as the host, so to impersonate a host one must already have a wildcard certificate matching other hosts in the same domain. For example, b*.example.com would match some hostnames that do not begin with a 'b' character.
CVE-2018-9163
A stored Cross-site scripting (XSS) vulnerability in Zoho ManageEngine Recovery Manager Plus before 5.3 (Build 5350) allows remote authenticated users (with Add New Technician permissions) to inject arbitrary web script or HTML via the loginName field to technicianAction.do.
CVE-2018-9173
Cross-site scripting (XSS) vulnerability in admin/template/js/uploadify/uploadify.swf in GetSimple CMS 3.3.13 allows remote attackers to inject arbitrary web script or HTML, as demonstrated by the movieName parameter.
CVE-2018-9174
sys_verifies.php in DedeCMS 5.7 allows remote attackers to execute arbitrary PHP code via the refiles array parameter, because the contents of modifytmp.inc are under an attacker's control.
CVE-2018-9175
DedeCMS 5.7 allows remote attackers to execute arbitrary PHP code via the egroup parameter to uploads/dede/stepselect_main.php because code within the database is accessible to uploads/dede/sys_cache_up.php.
CVE-2018-9183
The Joom Sky JS Jobs extension before 1.2.1 for Joomla! has XSS.
CVE-2018-9230
** DISPUTED ** In OpenResty through 1.13.6.1, URI parameters are obtained using the ngx.req.get_uri_args and ngx.req.get_post_args functions that ignore parameters beyond the hundredth one, which might allow remote attackers to bypass intended access restrictions or interfere with certain Web Application Firewall (ngx_lua_waf or X-WAF) products. NOTE: the vendor has reported that 100 parameters is an intentional default setting, but is adjustable within the API. The vendor's position is that a security-relevant misuse of the API by a WAF product is a vulnerability in the WAF product, not a vulnerability in OpenResty.
