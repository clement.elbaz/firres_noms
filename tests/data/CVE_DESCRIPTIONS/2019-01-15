CVE-2017-18356
In the Automattic WooCommerce plugin before 3.2.4 for WordPress, an attack is possible after gaining access to the target site with a user account that has at least Shop manager privileges. The attacker then constructs a specifically crafted string that will turn into a PHP object injection involving the includes/shortcodes/class-wc-shortcode-products.php WC_Shortcode_Products::get_products() use of cached queries within shortcodes.
CVE-2017-18357
Shopware before 5.3.4 has a PHP Object Instantiation issue via the sort parameter to the loadPreviewAction() method of the Shopware_Controllers_Backend_ProductStream controller, with resultant XXE via instantiation of a SimpleXMLElement object.
CVE-2017-18358
LimeSurvey before 2.72.4 has Stored XSS by using the Continue Later (aka Resume later) feature to enter an email address, which is mishandled in the admin panel.
CVE-2017-6921
In Drupal 8 prior to 8.3.4; The file REST resource does not properly validate some fields when manipulating files. A site is only affected by this if the site has the RESTful Web Services (rest) module enabled, the file REST resource is enabled and allows PATCH requests, and an attacker can get or register a user account on the site with permissions to upload files and to modify the file resource.
CVE-2017-6924
In Drupal 8 prior to 8.3.7; When using the REST API, users without the correct permission can post comments via REST that are approved even if the user does not have permission to post approved comments. This issue only affects sites that have the RESTful Web Services (rest) module enabled, the comment entity REST resource enabled, and where an attacker can access a user account on the site with permissions to post comments, or where anonymous users can post comments.
CVE-2017-6925
In versions of Drupal 8 core prior to 8.3.7; There is a vulnerability in the entity access system that could allow unwanted access to view, create, update, or delete entities. This only affects entities that do not use or do not have UUIDs, and entities that have different access restrictions on different revisions of the same entity.
CVE-2018-14662
It was found Ceph versions before 13.2.4 that authenticated ceph users with read only permissions could steal dm-crypt encryption keys used in ceph disk encryption.
CVE-2018-15440
A vulnerability in the web-based management interface of Cisco Identity Services Engine (ISE) could allow an unauthenticated, remote attacker to conduct a stored cross-site scripting (XSS) attack against a user of the web interface of an affected system. The vulnerability is due to insufficient sanitization of user-supplied data that is written to log files and displayed in certain web pages of the web-based management interface of an affected device. An attacker could exploit this vulnerability by convincing a user of the interface to click a specific link or view an affected log file. The injected script code may be executed in the context of the web-based management interface or allow the attacker to access sensitive browser-based information.
CVE-2018-15463
A vulnerability in the web-based management interface of Cisco Identity Services Engine (ISE) could allow an unauthenticated, remote attacker to conduct a reflected cross-site scripting (XSS) attack against a user of the web-based interface. The vulnerability is due to insufficient input validation of some parameters passed to the web-based management interface of an affected device. An attacker could exploit this vulnerability by convincing a user of the interface to click a specific link. A successful exploit could allow the attacker to execute arbitrary script code in the context of the web-based management interface or allow the attacker to access sensitive browser-based information.
CVE-2018-16846
It was found in Ceph versions before 13.2.4 that authenticated ceph RGW users can cause a denial of service against OMAPs holding bucket indices.
CVE-2018-1772
IBM SPSS Analytic Server 3.1.1.1 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 148689.
CVE-2018-20712
A heap-based buffer over-read exists in the function d_expression_1 in cp-demangle.c in GNU libiberty, as distributed in GNU Binutils 2.31.1. A crafted input can cause segmentation faults, leading to denial-of-service, as demonstrated by c++filt.
CVE-2018-20713
Shopware before 5.4.3 allows SQL Injection by remote authenticated users, aka SW-21404.
CVE-2018-20714
The logging system of the Automattic WooCommerce plugin before 3.4.6 for WordPress is vulnerable to a File Deletion vulnerability. This allows deletion of woocommerce.php, which leads to certain privilege checks not being in place, and therefore a shop manager can escalate privileges to admin.
CVE-2018-20715
The DB abstraction layer of OXID eSales 4.10.6 is vulnerable to SQL injection via the oxid or synchoxid parameter to the oxConfig::getRequestParameter() method in core/oxconfig.php.
CVE-2018-20716
CubeCart before 6.1.13 has SQL Injection via the validate[] parameter of the "I forgot my Password!" feature.
CVE-2018-20717
In the orders section of PrestaShop before 1.7.2.5, an attack is possible after gaining access to a target store with a user role with the rights of at least a Salesman or higher privileges. The attacker can then inject arbitrary PHP objects into the process and abuse an object chain in order to gain Remote Code Execution. This occurs because protection against serialized objects looks for a 0: followed by an integer, but does not consider 0:+ followed by an integer.
CVE-2018-20718
In Pydio before 8.2.2, an attack is possible via PHP Object Injection because a user is allowed to use the $phpserial$a:0:{} syntax to store a preference. An attacker either needs a "public link" of a file, or access to any unprivileged user account for creation of such a link.
CVE-2018-20719
In Tiki before 17.2, the user task component is vulnerable to a SQL Injection via the tiki-user_tasks.php show_history parameter.
CVE-2018-6345
The function number_format is vulnerable to a heap overflow issue when its second argument ($dec_points) is excessively large. The internal implementation of the function will cause a string to be created with an invalid length, which can then interact poorly with other functions. This affects all supported versions of HHVM (3.30.1 and 3.27.5 and below).
CVE-2018-7603
In Drupal's 3rd party module search auto complete prior to versions 7.x-4.8 there is a Cross Site Scripting vulnerability. This Search Autocomplete module enables you to autocomplete textfield using data from your website (nodes, comments, etc.). The module doesn't sufficiently filter user-entered text among the autocompletion items leading to a Cross Site Scripting (XSS) vulnerability. This vulnerability can be exploited by any user allowed to create one of the autocompletion item, for instance, nodes, users, comments.
