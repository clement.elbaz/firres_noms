CVE-2016-9597
It was found that Red Hat JBoss Core Services erratum RHSA-2016:2957 for CVE-2016-3705 did not actually include the fix for the issue found in libxml2, making it vulnerable to a Denial of Service attack due to a Stack Overflow. This is a regression CVE for the same issue as CVE-2016-3705.
CVE-2017-7482
In the Linux kernel before version 4.12, Kerberos 5 tickets decoded when using the RXRPC keys incorrectly assumes the size of a field. This could lead to the size-remaining variable wrapping and the data pointer going over the end of the buffer. This could possibly lead to memory corruption and possible privilege escalation.
CVE-2017-7514
A cross-site scripting (XSS) flaw was found in how the failed action entry is processed in Red Hat Satellite before version 5.8.0. A user able to specify a failed action could exploit this flaw to perform XSS attacks against other Satellite users.
CVE-2017-7518
A flaw was found in the Linux kernel before version 4.12 in the way the KVM module processed the trap flag(TF) bit in EFLAGS during emulation of the syscall instruction, which leads to a debug exception(#DB) being raised in the guest stack. A user/process inside a guest could use this flaw to potentially escalate their privileges inside the guest. Linux guests are not affected by this.
CVE-2018-10847
prosody before versions 0.10.2, 0.9.14 is vulnerable to an Authentication Bypass. Prosody did not verify that the virtual host associated with a user session remained the same across stream restarts. A user may authenticate to XMPP host A and migrate their authenticated session to XMPP host B of the same Prosody instance.
CVE-2018-10883
A flaw was found in the Linux kernel's ext4 filesystem. A local user can cause an out-of-bounds write in jbd2_journal_dirty_metadata(), a denial of service, and a system crash by mounting and operating on a crafted ext4 filesystem image.
CVE-2018-10898
A vulnerability was found in openstack-tripleo-heat-templates before version 8.0.2-40. When deployed using Director using default configuration, Opendaylight in RHOSP13 is configured with easily guessable default credentials.
CVE-2018-10903
A flaw was found in python-cryptography versions between >=1.9.0 and <2.3. The finalize_with_tag API did not enforce a minimum tag length. If a user did not validate the input length prior to passing it to finalize_with_tag an attacker could craft an invalid payload with a shortened tag (e.g. 1 byte) such that they would have a 1 in 256 chance of passing the MAC check. GCM tag forgeries can cause key leakage.
CVE-2018-13280
Use of insufficiently random values vulnerability in SYNO.Encryption.GenRandomKey in Synology DiskStation Manager (DSM) before 6.2-23739 allows man-in-the-middle attackers to compromise non-HTTPS sessions via unspecified vectors.
CVE-2018-14736
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A buffer over-read can occur in pbc_wmessage_string in wmessage.c for PTYPE_ENUM.
CVE-2018-14737
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A NULL pointer dereference can occur in pbc_wmessage_string in wmessage.c.
CVE-2018-14738
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A SEGV can occur in pbc_rmessage_message in rmessage.c.
CVE-2018-14739
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A SEGV can occur in pbc_pattern_set_default in pattern.c.
CVE-2018-14740
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A SEGV can occur in set_field_one in bootstrap.c while making a query.
CVE-2018-14741
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A SEGV can occur in pbc_pattern_pack in pattern.c.
CVE-2018-14742
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A SEGV can occur in set_field_one in bootstrap.c during a memcpy.
CVE-2018-14743
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A SEGV can occur in wiretype_decode in context.c.
CVE-2018-14744
An issue was discovered in libpbc.a in cloudwu PBC through 2017-03-02. A use-after-free can occur in _pbcM_sp_query in map.c.
CVE-2018-3772
Concatenating unsanitized user input in the `whereis` npm module < 0.4.1 allowed an attacker to execute arbitrary commands. The `whereis` module is deprecated and it is recommended to use the `which` npm module instead.
CVE-2018-3773
There is a stored Cross-Site Scripting vulnerability in Open Graph meta properties read by the `metascrape` npm module <= 3.9.2.
CVE-2018-9064
In Lenovo xClarity Administrator versions earlier than 2.1.0, an authenticated LXCA user may abuse a web API debug call to retrieve the credentials for the System Manager user.
CVE-2018-9065
In Lenovo xClarity Administrator versions earlier than 2.1.0, an attacker that gains access to the underlying LXCA file system user may be able to retrieve a credential store containing the service processor user names and passwords for servers previously managed by that LXCA instance, and potentially decrypt those credentials more easily than intended.
CVE-2018-9066
In Lenovo xClarity Administrator versions earlier than 2.1.0, an authenticated LXCA user can, under specific circumstances, inject additional parameters into a specific web API call which can result in privileged command execution within LXCA's underlying operating system.
