CVE-2013-7419
Cross-site scripting (XSS) vulnerability in includes/refreshDate.php in the Joomlaskin JS Multi Hotel (aka JS MultiHotel and Js-Multi-Hotel) plugin 2.2.1 for WordPress allows remote attackers to inject arbitrary web script or HTML via the roomid parameter.
CVE-2014-3570
The BN_sqr implementation in OpenSSL before 0.9.8zd, 1.0.0 before 1.0.0p, and 1.0.1 before 1.0.1k does not properly calculate the square of a BIGNUM value, which might make it easier for remote attackers to defeat cryptographic protection mechanisms via unspecified vectors, related to crypto/bn/asm/mips.pl, crypto/bn/asm/x86_64-gcc.c, and crypto/bn/bn_asm.c.
CVE-2014-3571
OpenSSL before 0.9.8zd, 1.0.0 before 1.0.0p, and 1.0.1 before 1.0.1k allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted DTLS message that is processed with a different read operation for the handshake header than for the handshake body, related to the dtls1_get_record function in d1_pkt.c and the ssl3_read_n function in s3_pkt.c.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2014-3572
The ssl3_get_key_exchange function in s3_clnt.c in OpenSSL before 0.9.8zd, 1.0.0 before 1.0.0p, and 1.0.1 before 1.0.1k allows remote SSL servers to conduct ECDHE-to-ECDH downgrade attacks and trigger a loss of forward secrecy by omitting the ServerKeyExchange message.
CVE-2014-8027
The RBAC component in Cisco Secure Access Control System (ACS) allows remote authenticated users to obtain Network Device Administrator privileges for Create, Delete, Read, and Update operations via crafted HTTP requests, aka Bug ID CSCuq79034.
CVE-2014-8028
Multiple cross-site scripting (XSS) vulnerabilities in the web framework in Cisco Secure Access Control System (ACS) allow remote attackers to inject arbitrary web script or HTML via unspecified parameters, aka Bug ID CSCuq79019.
CVE-2014-8029
Open redirect vulnerability in the web interface in Cisco Secure Access Control System (ACS) allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via an unspecified parameter, aka Bug ID CSCuq74150.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2014-8030
Cross-site scripting (XSS) vulnerability in sendPwMail.do in Cisco WebEx Meetings Server allows remote attackers to inject arbitrary web script or HTML via the email parameter, aka Bug ID CSCuj40381.
CVE-2014-8031
Cross-site request forgery (CSRF) vulnerability in Cisco WebEx Meetings Server allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCuj40456.
CVE-2014-8032
The OutlookAction LI in Cisco WebEx Meetings Server allows remote authenticated users to obtain sensitive encrypted-password information via unspecified vectors, aka Bug IDs CSCuj40453 and CSCuj40449.
CVE-2014-8033
The play/modules component in Cisco WebEx Meetings Server allows remote attackers to obtain administrator access via crafted API requests, aka Bug ID CSCuj40421.
CVE-2014-8275
OpenSSL before 0.9.8zd, 1.0.0 before 1.0.0p, and 1.0.1 before 1.0.1k does not enforce certain constraints on certificate data, which allows remote attackers to defeat a fingerprint-based certificate-blacklist protection mechanism by including crafted data within a certificate's unsigned portion, related to crypto/asn1/a_verify.c, crypto/dsa/dsa_asn1.c, crypto/ecdsa/ecs_vrf.c, and crypto/x509/x_all.c.
CVE-2014-9269
Cross-site scripting (XSS) vulnerability in helper_api.php in MantisBT 1.1.0a1 through 1.2.x before 1.2.18, when Extended project browser is enabled, allows remote attackers to inject arbitrary web script or HTML via the project cookie.
CVE-2014-9271
Cross-site scripting (XSS) vulnerability in file_download.php in MantisBT before 1.2.18 allows remote authenticated users to inject arbitrary web script or HTML via a Flash file with an image extension, related to inline attachments, as demonstrated by a .swf.jpeg filename.
CVE-2014-9272
The string_insert_href function in MantisBT 1.2.0a1 through 1.2.x before 1.2.18 does not properly validate the URL protocol, which allows remote attackers to conduct cross-site scripting (XSS) attacks via the javascript:// protocol.
CVE-2014-9498
Cross-site scripting (XSS) vulnerability in the Webform Invitation module 7.x-1.x before 7.x-1.3 and 7.x-2.x before 7.x-2.4 for Drupal allows remote authenticated users with the Webform: Create new content, Webform: Edit own content, or Webform: Edit any content permission to inject arbitrary web script or HTML via a node title.
CVE-2014-9499
Cross-site scripting (XSS) vulnerability in the Godwin's Law module before 7.x-1.1 for Drupal, when using the dblog module, allows remote authenticated users to inject arbitrary web script or HTML via a Watchdog message.
CVE-2014-9500
Cross-site scripting (XSS) vulnerability in the Moip module 7.x-1.x before 7.x-1.4 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors to the notification page callback.
CVE-2014-9501
Cross-site scripting (XSS) vulnerability in the Poll Chart Block module 7.x-1.x before 7.x-1.2 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a poll node title.
CVE-2014-9505
Cross-site scripting (XSS) vulnerability in the School Administration module 7.x-1.x before 7.x-1.8 for Drupal allows remote authenticated users with permission to create or edit a class node to inject arbitrary web script or HTML via a node title.
CVE-2014-9510
Cross-site request forgery (CSRF) vulnerability in the administration console in TP-Link TL-WR840N (V1) router with firmware before 3.13.27 build 141120 allows remote attackers to hijack the authentication of administrators for requests that change router settings via a configuration file import.
CVE-2014-9529
Race condition in the key_gc_unused_keys function in security/keys/gc.c in the Linux kernel through 3.18.2 allows local users to cause a denial of service (memory corruption or panic) or possibly have unspecified other impact via keyctl commands that trigger access to a key structure member during garbage collection of a key.
CVE-2014-9584
The parse_rock_ridge_inode_internal function in fs/isofs/rock.c in the Linux kernel before 3.18.2 does not validate a length value in the Extensions Reference (ER) System Use Field, which allows local users to obtain sensitive information from kernel memory via a crafted iso9660 image.
CVE-2014-9585
The vdso_addr function in arch/x86/vdso/vma.c in the Linux kernel through 3.18.2 does not properly choose memory locations for the vDSO area, which makes it easier for local users to bypass the ASLR protection mechanism by guessing a location at the end of a PMD.
CVE-2015-0204
The ssl3_get_key_exchange function in s3_clnt.c in OpenSSL before 0.9.8zd, 1.0.0 before 1.0.0p, and 1.0.1 before 1.0.1k allows remote SSL servers to conduct RSA-to-EXPORT_RSA downgrade attacks and facilitate brute-force decryption by offering a weak ephemeral RSA key in a noncompliant role, related to the "FREAK" issue.  NOTE: the scope of this CVE is only client code based on OpenSSL, not EXPORT_RSA issues associated with servers or other TLS implementations.
CVE-2015-0205
The ssl3_get_cert_verify function in s3_srvr.c in OpenSSL 1.0.0 before 1.0.0p and 1.0.1 before 1.0.1k accepts client authentication with a Diffie-Hellman (DH) certificate without requiring a CertificateVerify message, which allows remote attackers to obtain access without knowledge of a private key via crafted TLS Handshake Protocol traffic to a server that recognizes a Certification Authority with DH support.
CVE-2015-0206
Memory leak in the dtls1_buffer_record function in d1_pkt.c in OpenSSL 1.0.0 before 1.0.0p and 1.0.1 before 1.0.1k allows remote attackers to cause a denial of service (memory consumption) by sending many duplicate records for the next epoch, leading to failure of replay detection.
CVE-2015-0921
XML external entity (XXE) vulnerability in the Server Task Log in McAfee ePolicy Orchestrator (ePO) before 4.6.9 and 5.x before 5.1.2 allows remote authenticated users to read arbitrary files via the conditionXML parameter to the taskLogTable to orionUpdateTableFilter.do.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-0922
McAfee ePolicy Orchestrator (ePO) before 4.6.9 and 5.x before 5.1.2 uses the same secret key across different customers' installations, which allows attackers to obtain the administrator password by leveraging knowledge of the encrypted password.
