CVE-2010-5326
The Invoker Servlet on SAP NetWeaver Application Server Java platforms, possibly before 7.3, does not require authentication, which allows remote attackers to execute arbitrary code via an HTTP or HTTPS request, as exploited in the wild in 2013 through 2016, aka a "Detour" attack.
CVE-2011-5326
imlib2 before 1.4.9 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) by drawing a 2x1 ellipse.
CVE-2014-9742
The Miller-Rabin primality check in Botan before 1.10.8 and 1.11.x before 1.11.9 improperly uses a single random base, which makes it easier for remote attackers to defeat cryptographic protection mechanisms via a DH group.
CVE-2014-9762
imlib2 before 1.4.7 allows remote attackers to cause a denial of service (segmentation fault) via a GIF image without a colormap.
CVE-2014-9763
imlib2 before 1.4.7 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted PNM file.
CVE-2014-9764
imlib2 before 1.4.7 allows remote attackers to cause a denial of service (segmentation fault) via a crafted GIF file.
CVE-2014-9771
Integer overflow in imlib2 before 1.4.7 allows remote attackers to cause a denial of service (memory consumption or application crash) via a crafted image, which triggers an invalid read operation.
<a href="http://cwe.mitre.org/data/definitions/190.html">CWE-190: Integer Overflow or Wraparound</a>
CVE-2015-5726
The BER decoder in Botan 0.10.x before 1.10.10 and 1.11.x before 1.11.19 allows remote attackers to cause a denial of service (application crash) via an empty BIT STRING in ASN.1 data.
CVE-2015-5727
The BER decoder in Botan 1.10.x before 1.10.10 and 1.11.x before 1.11.19 allows remote attackers to cause a denial of service (memory consumption) via unspecified vectors, related to a length field.
CVE-2015-7827
Botan before 1.10.13 and 1.11.x before 1.11.22 make it easier for remote attackers to conduct million-message attacks by measuring time differences, related to decoding of PKCS#1 padding.
CVE-2015-8099
F5 BIG-IP LTM, AFM, Analytics, APM, ASM, Link Controller, and PEM 11.3.x, 11.4.x before 11.4.1 HF10, 11.5.x before 11.5.4, 11.6.x before 11.6.1, and 12.x before 12.0.0 HF1; BIG-IP AAM 11.4.x before 11.4.1 HF10, 11.5.x before 11.5.4, 11.6.x before 11.6.1, and 12.x before 12.0.0 HF1; BIG-IP DNS 12.x before 12.0.0 HF1; BIG-IP Edge Gateway, WebAccelerator, and WOM 11.3.0; BIG-IP GTM 11.3.x, 11.4.x before 11.4.1 HF10, 11.5.x before 11.5.4, and 11.6.x before 11.6.1; BIG-IP PSM 11.3.x and 11.4.x before 11.4.1 HF10; Enterprise Manager 3.0.0 through 3.1.1; BIG-IQ Cloud and BIG-IQ Security 4.0.0 through 4.5.0; BIG-IQ Device 4.2.0 through 4.5.0; BIG-IQ ADC 4.5.0; BIG-IQ Centralized Management 4.6.0; and BIG-IQ Cloud and Orchestration 1.0.0 on the 3900, 6900, 8900, 8950, 11000, 11050, PB100 and PB200 platforms, when software SYN cookies are configured on virtual servers, allow remote attackers to cause a denial of service (High-Speed Bridge hang) via an invalid TCP segment.
CVE-2015-8312
Off-by-one error in afs_pioctl.c in OpenAFS before 1.6.16 might allow local users to cause a denial of service (memory overwrite and system crash) via a pioctl with an input buffer size of 4096 bytes.
CVE-2016-1578
Use-after-free vulnerability in Oxide allows remote attackers to cause a denial of service (application crash) or execute arbitrary code via unspecified vectors, related to responding synchronously to permission requests.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2016-1580
The setup_snappy_os_mounts function in the ubuntu-core-launcher package before 1.0.27.1 improperly determines the mount point of bind mounts when using snaps, which might allow remote attackers to obtain sensitive information or gain privileges via a snap with a name starting with "ubuntu-core."
CVE-2016-2099
Use-after-free vulnerability in validators/DTD/DTDScanner.cpp in Apache Xerces C++ 3.1.3 and earlier allows context-dependent attackers to have unspecified impact via an invalid character in an XML document.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2016-2194
The ressol function in Botan before 1.10.11 and 1.11.x before 1.11.27 allows remote attackers to cause a denial of service (infinite loop) via unspecified input to the OS2ECP function, related to a composite modulus.
CVE-2016-2195
Integer overflow in the PointGFp constructor in Botan before 1.10.11 and 1.11.x before 1.11.27 allows remote attackers to overwrite memory and possibly execute arbitrary code via a crafted ECC point, which triggers a heap-based buffer overflow.
CVE-2016-2196
Heap-based buffer overflow in the P-521 reduction function in Botan 1.11.x before 1.11.27 allows remote attackers to cause a denial of service (memory overwrite and crash) or execute arbitrary code via unspecified vectors.
CVE-2016-2849
Botan before 1.10.13 and 1.11.x before 1.11.29 do not use a constant-time algorithm to perform a modular inverse on the signature nonce k, which might allow remote attackers to obtain ECDSA secret keys via a timing side-channel attack.
CVE-2016-2850
Botan 1.11.x before 1.11.29 does not enforce TLS policy for (1) signature algorithms and (2) ECC curves, which allows remote attackers to conduct downgrade attacks via unspecified vectors.
CVE-2016-2860
The newEntry function in ptserver/ptprocs.c in OpenAFS before 1.6.17 allows remote authenticated users from foreign Kerberos realms to bypass intended access restrictions and create arbitrary groups as administrators by leveraging mishandling of the creator ID.
CVE-2016-3993
Off-by-one error in the __imlib_MergeUpdate function in lib/updates.c in imlib2 before 1.4.9 allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via crafted coordinates.
CVE-2016-3994
The GIF loader in imlib2 before 1.4.9 allows remote attackers to cause a denial of service (application crash) or obtain sensitive information via a crafted image, which triggers an out-of-bounds read.
CVE-2016-4024
Integer overflow in imlib2 before 1.4.9 on 32-bit platforms allows remote attackers to execute arbitrary code via large dimensions in an image, which triggers an out-of-bounds heap memory write operation.
CVE-2016-4536
The client in OpenAFS before 1.6.17 does not properly initialize the (1) AFSStoreStatus, (2) AFSStoreVolumeStatus, (3) VldbListByAttributes, and (4) ListAddrByAttributes structures, which might allow remote attackers to obtain sensitive memory information by leveraging access to RPC call traffic.
