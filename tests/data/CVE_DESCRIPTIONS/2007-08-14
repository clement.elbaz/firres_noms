CVE-2007-0943
Unspecified vulnerability in Internet Explorer 5.01 and 6 SP1 allows remote attackers to execute arbitrary code via crafted Cascading Style Sheets (CSS) strings that trigger memory corruption during parsing, related to use of out-of-bounds pointers.
CVE-2007-0948
Heap-based buffer overflow in Microsoft Virtual PC 2004 and PC for Mac 7.1 and 7, and Virtual Server 2005 and 2005 R2, allows local guest OS administrators to execute arbitrary code on the host OS via unspecified vectors related to "interaction and initialization of components."
CVE-2007-1749
Integer underflow in the CDownloadSink class code in the Vector Markup Language (VML) component (VGX.DLL), as used in Internet Explorer 5.01, 6, and 7 allows remote attackers to execute arbitrary code via compressed content with an invalid buffer size, which triggers a heap-based buffer overflow.
CVE-2007-2216
The tblinf32.dll (aka vstlbinf.dll) ActiveX control for Internet Explorer 5.01, 6 SP1, and 7 uses an incorrect IObjectsafety implementation, which allows remote attackers to execute arbitrary code by requesting the HelpString property, involving a crafted DLL file argument to the TypeLibInfoFromFile function, which overwrites the HelpStringDll property to call the DLLGetDocumentation function in another DLL file, aka "ActiveX Object Vulnerability."
CVE-2007-2223
Microsoft XML Core Services (MSXML) 3.0 through 6.0 allows remote attackers to execute arbitrary code via the substringData method on a (1) TextNode or (2) XMLDOM object, which causes an integer overflow that leads to a buffer overflow.
CVE-2007-2224
Object linking and embedding (OLE) Automation, as used in Microsoft Windows 2000 SP4, XP SP2, Server 2003 SP1 and SP2, Office 2004 for Mac, and Visual Basic 6.0 allows remote attackers to execute arbitrary code via the substringData method on a TextNode object, which causes an integer overflow that leads to a buffer overflow.
CVE-2007-3032
Unspecified vulnerability in Windows Vista Contacts Gadget in Windows Vista allows user-assisted remote attackers to execute arbitrary code via crafted contact information that is not properly handled when it is imported.
CVE-2007-3033
Cross-site scripting (XSS) vulnerability in Windows Vista Feed Headlines Gadget (aka Sidebar RSS Feeds Gadget) in Windows Vista allows user-assisted remote attackers to execute arbitrary code via an RSS feed with crafted HTML attributes, which are not properly removed and are rendered in the local zone.
CVE-2007-3034
Integer overflow in the AttemptWrite function in Graphics Rendering Engine (GDI) on Microsoft Windows 2000 SP4, XP SP2, and Server 2003 SP1 allows remote attackers to execute arbitrary code via a crafted metafile (image) with a large record length value, which triggers a heap-based buffer overflow.
CVE-2007-3035
Unspecified vulnerability in Microsoft Windows Media Player 7.1, 9, 10, and 11 allows remote attackers to execute arbitrary code via a skin file (WMZ or WMD) with crafted header information that is not properly handled during decompression, aka "Windows Media Player Code Execution Vulnerability Decompressing Skins."
CVE-2007-3037
Microsoft Windows Media Player 7.1, 9, 10, and 11 allows remote attackers to execute arbitrary code via a skin file (WMZ or WMD) with crafted header information that causes a size mismatch between compressed and decompressed data and triggers a heap-based buffer overflow, aka "Windows Media Player Code Execution Vulnerability Parsing Skins."
CVE-2007-3041
Unspecified vulnerability in the pdwizard.ocx ActiveX object for Internet Explorer 5.01, 6 SP1, and 7 allows remote attackers to execute arbitrary code via unknown vectors related to Microsoft Visual Basic 6 objects and memory corruption, aka "ActiveX Object Memory Corruption Vulnerability."
CVE-2007-3382
Apache Tomcat 6.0.0 to 6.0.13, 5.5.0 to 5.5.24, 5.0.0 to 5.0.30, 4.1.0 to 4.1.36, and 3.3 to 3.3.2 treats single quotes ("'") as delimiters in cookies, which might cause sensitive information such as session IDs to be leaked and allow remote attackers to conduct session hijacking attacks.
CVE-2007-3385
Apache Tomcat 6.0.0 to 6.0.13, 5.5.0 to 5.5.24, 5.0.0 to 5.0.30, 4.1.0 to 4.1.36, and 3.3 to 3.3.2 does not properly handle the \" character sequence in a cookie value, which might cause sensitive information such as session IDs to be leaked to remote attackers and enable session hijacking attacks.
CVE-2007-3386
Cross-site scripting (XSS) vulnerability in the Host Manager Servlet for Apache Tomcat 6.0.0 to 6.0.13 and 5.5.0 to 5.5.24 allows remote attackers to inject arbitrary HTML and web script via crafted requests, as demonstrated using the aliases parameter to an html/add action.
CVE-2007-3848
Linux kernel 2.4.35 and other versions allows local users to send arbitrary signals to a child process that is running at higher privileges by causing a setuid-root parent process to die, which delivers an attacker-controlled parent process death signal (PR_SET_PDEATHSIG).
CVE-2007-3852
The init script (sysstat.in) in sysstat 5.1.2 up to 7.1.6 creates /tmp/sysstat.run insecurely, which allows local users to execute arbitrary code.
CVE-2007-3890
Microsoft Excel in Office 2000 SP3, Office XP SP3, Office 2003 SP2, and Office 2004 for Mac allows remote attackers to execute arbitrary code via a Workspace with a certain index value that triggers memory corruption.
CVE-2007-3891
Unspecified vulnerability in Windows Vista Weather Gadgets in Windows Vista allows remote attackers to execute arbitrary code via crafted HTML attributes.
CVE-2007-4320
PHP remote file inclusion vulnerability in admin/addons/archive/archive.php in Ncaster 1.7.2 allows remote attackers to execute arbitrary PHP code via a URL in the adminfolder parameter.
CVE-2007-4321
fail2ban 0.8 and earlier does not properly parse sshd log files, which allows remote attackers to add arbitrary hosts to the /etc/hosts.deny file and cause a denial of service by adding arbitrary IP addresses to the sshd log file, as demonstrated by logging in via ssh with a client protocol version identification containing an IP address string, a different vector than CVE-2006-6302.
CVE-2007-4322
BlockHosts before 2.0.4 does not properly parse (1) sshd and (2) vsftpd log files, which allows remote attackers to add arbitrary deny entries to the /etc/hosts.allow file and cause a denial of service by adding arbitrary IP addresses to a daemon log file, as demonstrated by connecting through ssh with a client protocol version identification containing an IP address string, or connecting through ftp with a username containing an IP address string, different vectors than CVE-2007-2765.
CVE-2007-4323
DenyHosts 2.6 does not properly parse sshd log files, which allows remote attackers to add arbitrary hosts to the /etc/hosts.deny file and cause a denial of service by adding arbitrary IP addresses to the sshd log file, as demonstrated by logging in via ssh with a client protocol version identification containing an IP address string, a different vector than CVE-2006-6301.
CVE-2007-4324
ActionScript 3 (AS3) in Adobe Flash Player 9.0.47.0, and other versions and other 9.0.124.0 and earlier versions, allows remote attackers to bypass the Security Sandbox Model, obtain sensitive information, and port scan arbitrary hosts via a Flash (SWF) movie that specifies a connection to make, then uses timing discrepancies from the SecurityErrorEvent error to determine whether a port is open or not.  NOTE: 9.0.115.0 introduces support for a workaround, but does not fix the vulnerability.
CVE-2007-4325
PHP remote file inclusion vulnerability in index.php in Gaestebuch 1.5 allows remote attackers to execute arbitrary PHP code via a URL in the config[root_ordner] parameter.
CVE-2007-4326
Multiple PHP remote file inclusion vulnerabilities in Bilder Uploader 1.3 allow remote attackers to execute arbitrary PHP code via a URL in the config[root_ordner] parameter to (1) gruppen.php, (2) bild.php, (3) feed.php, (4) mitglieder.php, (5) online.php, (6) profil.php, and possibly other unspecified PHP scripts.
CVE-2007-4327
Multiple PHP remote file inclusion vulnerabilities in File Uploader 1.1 allow remote attackers to execute arbitrary PHP code via a URL in the config[root_ordner] parameter to (1) index.php or (2) datei.php.
CVE-2007-4328
Multiple PHP remote file inclusion vulnerabilities in Mapos Bilder Galerie 1.0 allow remote attackers to execute arbitrary PHP code via a URL in the config[root_ordner] parameter to (1) index.php, (2) galerie.php, or (3) anzagien.php.  NOTE: A later report states that 1.1 is also affected, but that the filename for vector 3 is anzeigen.php.
CVE-2007-4329
Multiple PHP remote file inclusion vulnerabilities in Web News 1.1 allow remote attackers to execute arbitrary PHP code via a URL in the config[root_ordner] parameter to (1) index.php, (2) news.php, or (3) feed.php.
CVE-2007-4330
PHP remote file inclusion vulnerability in shoutbox.php in Shoutbox 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the root parameter.
CVE-2007-4331
PHP remote file inclusion vulnerability in index.php in FindNix allows remote attackers to include the contents of arbitrary URLs and conduct cross-site scripting (XSS) attacks via a URL in the page parameter.
CVE-2007-4332
SQL injection vulnerability in article.php in Article Dashboard, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the id parameter in a print action.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
Enable "magic_quotes_gpc" and filter malicious characters and character sequences in a web proxy.

CVE-2007-4333
Multiple cross-site scripting (XSS) vulnerabilities in signup.php in Article Dashboard allow remote attackers to inject arbitrary web script or HTML via the (1) f_emailaddress, (2) f_reemailaddress, and other unspecified parameters.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-4334
Cross-site scripting (XSS) vulnerability in whois.php in Php-stats 0.1.9.2 allows remote attackers to inject arbitrary web script or HTML via the IP parameter.
CVE-2007-4335
Format string vulnerability in the SMTP server component in Qbik WinGate 5.x and 6.x before 6.2.2 allows remote attackers to cause a denial of service (service crash) via format string specifiers in certain unexpected commands, which trigger a crash during error logging.
CVE-2007-4336
Buffer overflow in the Live Picture Corporation DXSurface.LivePicture.FlashPix.1 (DirectTransform FlashPix) ActiveX control in DXTLIPI.DLL 6.0.2.827, as packaged in Microsoft DirectX Media 6.0 SDK, allows remote attackers to execute arbitrary code via a long SourceUrl property value.
CVE-2007-4337
Multiple buffer overflows in the httplib_parse_sc_header function in lib/http.c in Streamripper before 1.62.2 allow remote attackers to execute arbitrary code via long (1) Location and (2) Server HTTP headers, a different vulnerability than CVE-2006-3124.
CVE-2007-4338
index.php in Ryan Haudenschilt Family Connections (FCMS) before 0.9 allows remote attackers to access an arbitrary account by placing the account's name in the value of an fcms_login_id cookie.  NOTE: this can be leveraged for code execution via a POST with PHP code in the content parameter.
CVE-2007-4339
Multiple PHP remote file inclusion vulnerabilities in PHPCentral Poll Script 1.0 allow remote attackers to execute arbitrary PHP code via a URL in the _SERVER[DOCUMENT_ROOT] parameter in (1) poll.php and (2) pollarchive.php.  NOTE: a reliable third party states that this issue is resultant from a variable extraction error in functions.php.
CVE-2007-4340
PHP remote file inclusion vulnerability in index.php in phpDVD 1.0.4 allows remote attackers to execute arbitrary PHP code via a URL in the dvd_config_file parameter.
CVE-2007-4341
PHP remote file inclusion vulnerability in adm/my_statistics.php in Omnistar Lib2 PHP 0.2 allows remote attackers to execute arbitrary PHP code via a URL in the DOCUMENT_ROOT parameter.
CVE-2007-4342
PHP remote file inclusion vulnerability in include.php in PHPCentral Login 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the _SERVER[DOCUMENT_ROOT] parameter.  NOTE: a third party disputes this vulnerability because of the special nature of the SERVER superglobal array.
