CVE-2011-1740
EMC Avamar 4.x, 5.0.x, and 6.0.x before 6.0.0-592 allows remote authenticated users to modify client data or obtain sensitive information about product activities by leveraging privileged access to a different domain.
CVE-2011-2738
Multiple unspecified vulnerabilities in Cisco Unified Service Monitor before 8.6, as used in Unified Operations Manager before 8.6 and CiscoWorks LAN Management Solution 3.x and 4.x before 4.1; and multiple EMC Ionix products including Application Connectivity Monitor (Ionix ACM) 2.3 and earlier, Adapter for Alcatel-Lucent 5620 SAM EMS (Ionix ASAM) 3.2.0.2 and earlier, IP Management Suite (Ionix IP) 8.1.1.1 and earlier, and other Ionix products; allow remote attackers to execute arbitrary code via crafted packets to TCP port 9002, aka Bug IDs CSCtn42961 and CSCtn64922, related to a buffer overflow.
CVE-2011-2834
Double free vulnerability in libxml2, as used in Google Chrome before 14.0.835.163, allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to XPath handling.
CVE-2011-2835
Race condition in Google Chrome before 14.0.835.163 allows attackers to cause a denial of service or possibly have unspecified other impact via vectors related to the certificate cache.
CVE-2011-2836
Google Chrome before 14.0.835.163 does not require Infobar interaction before use of the Windows Media Player plug-in, which makes it easier for remote attackers to have an unspecified impact via crafted Flash content.
CVE-2011-2837
Google Chrome before 14.0.835.163 on Linux does not use the PIC and PIE compiler options for position-independent code, which has unspecified impact and attack vectors.
CVE-2011-2838
Google Chrome before 14.0.835.163 does not properly consider the MIME type during the loading of a plug-in, which has unspecified impact and remote attack vectors.
CVE-2011-2840
Google Chrome before 14.0.835.163 allows user-assisted remote attackers to spoof the URL bar via vectors related to "unusual user interaction."
CVE-2011-2841
Google Chrome before 14.0.835.163 does not properly perform garbage collection during the processing of PDF documents, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted document.
CVE-2011-2842
The installer in Google Chrome before 14.0.835.163 on Mac OS X does not properly handle lock files, which has unspecified impact and attack vectors.
CVE-2011-2843
Google Chrome before 14.0.835.163 does not properly handle media buffers, which allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2011-2844
Google Chrome before 14.0.835.163 does not properly process MP3 files, which allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2011-2846
Use-after-free vulnerability in Google Chrome before 14.0.835.163 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to unload event handling.
CVE-2011-2847
Use-after-free vulnerability in the document loader in Google Chrome before 14.0.835.163 allows remote attackers to cause a denial of service or possibly have unspecified other impact via a crafted document.
CVE-2011-2848
Google Chrome before 14.0.835.163 allows user-assisted remote attackers to spoof the URL bar via vectors related to the forward button.
CVE-2011-2849
The WebSockets implementation in Google Chrome before 14.0.835.163 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/476.html
'CWE-476: NULL Pointer Dereference'
CVE-2011-2850
Google Chrome before 14.0.835.163 does not properly handle Khmer characters, which allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2011-2851
Google Chrome before 14.0.835.163 does not properly handle video, which allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2011-2852
Off-by-one error in Google V8, as used in Google Chrome before 14.0.835.163, allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2011-2853
Use-after-free vulnerability in Google Chrome before 14.0.835.163 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to plug-in handling.
CVE-2011-2854
Use-after-free vulnerability in Google Chrome before 14.0.835.163 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to "ruby / table style handing."
CVE-2011-2855
Google Chrome before 14.0.835.163 does not properly handle Cascading Style Sheets (CSS) token sequences, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors that lead to a "stale node."
CVE-2011-2856
Google V8, as used in Google Chrome before 14.0.835.163, allows remote attackers to bypass the Same Origin Policy via unspecified vectors.
CVE-2011-2857
Use-after-free vulnerability in Google Chrome before 14.0.835.163 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to the focus controller.
CVE-2011-2858
Google Chrome before 14.0.835.163 does not properly handle triangle arrays, which allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2011-2859
Google Chrome before 14.0.835.163 uses incorrect permissions for non-gallery pages, which has unspecified impact and attack vectors.
CVE-2011-2860
Use-after-free vulnerability in Google Chrome before 14.0.835.163 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to table styles.
CVE-2011-2861
Google Chrome before 14.0.835.163 does not properly handle strings in PDF documents, which allows remote attackers to have an unspecified impact via a crafted document that triggers an incorrect read operation.
CVE-2011-2862
Google V8, as used in Google Chrome before 14.0.835.163, does not properly restrict access to built-in objects, which has unspecified impact and remote attack vectors.
CVE-2011-2864
Google Chrome before 14.0.835.163 does not properly handle Tibetan characters, which allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2011-2874
Google Chrome before 14.0.835.163 does not perform an expected pin operation for a self-signed certificate during a session, which has unspecified impact and remote attack vectors.
CVE-2011-2875
Google V8, as used in Google Chrome before 14.0.835.163, does not properly perform object sealing, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors that leverage "type confusion."
CVE-2011-3234
Google Chrome before 14.0.835.163 does not properly handle boxes, which allows remote attackers to cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2011-3345
ulp/sdp/sdp_proc.c in the ib_sdp module (aka ib_sdp.ko) in the ofa_kernel package in the InfiniBand driver implementation in OpenFabrics Enterprise Distribution (OFED) before 1.5.3 does not properly handle certain non-array variables, which allows local users to cause a denial of service (stack memory corruption and system crash) by reading the /proc/net/sdpstats file.
CVE-2011-3423
Cross-site scripting (XSS) vulnerability in the Managed File Transfer server in TIBCO Managed File Transfer Internet Server before 7.1.1 and Managed File Transfer Command Center before 7.1.1, and the server in TIBCO Slingshot before 1.8.1, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-3424
Session fixation vulnerability in the Managed File Transfer server in TIBCO Managed File Transfer Internet Server before 7.1.1 and Managed File Transfer Command Center before 7.1.1, and the server in TIBCO Slingshot before 1.8.1, allows remote attackers to hijack web sessions via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/384.html
'CWE-384: Session Fixation'
CVE-2011-3575
Stack-based buffer overflow in the NSFComputeEvaluateExt function in Nnotes.dll in IBM Lotus Domino 8.5.2 allows remote authenticated users to execute arbitrary code via a long tHPRAgentName parameter in an fmHttpPostRequest OpenForm action to WebAdmin.nsf.
CVE-2011-3576
Cross-site scripting (XSS) vulnerability in IBM Lotus Domino 8.5.2 allows remote attackers to inject arbitrary web script or HTML via the PanelIcon parameter in an fmpgPanelHeader ReadForm action to WebAdmin.nsf.
