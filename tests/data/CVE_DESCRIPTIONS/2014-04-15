CVE-2008-3277
Untrusted search path vulnerability in a certain Red Hat build script for the ibmssh executable in ibutils packages before ibutils-1.5.7-2.el6 in Red Hat Enterprise Linux (RHEL) 6 and ibutils-1.2-11.2.el5 in Red Hat Enterprise Linux (RHEL) 5 allows local users to gain privileges via a Trojan Horse program in refix/lib/, related to an incorrect RPATH setting in the ELF header.
CVE-2010-2236
The monitoring probe display in spacewalk-java before 2.1.148-1 and Red Hat Network (RHN) Satellite 4.0.0 through 4.2.0 and 5.1.0 through 5.3.0, and Proxy 5.3.0, allows remote authenticated users with permissions to administer monitoring probes to execute arbitrary code via unspecified vectors, related to backticks.
CVE-2011-3628
Untrusted search path vulnerability in pam_motd (aka the MOTD module) in libpam-modules before 1.1.3-2ubuntu2.1 on Ubuntu 11.10, before 1.1.2-2ubuntu8.4 on Ubuntu 11.04, before 1.1.1-4ubuntu2.4 on Ubuntu 10.10, before 1.1.1-2ubuntu5.4 on Ubuntu 10.04 LTS, and before 0.99.7.1-5ubuntu6.5 on Ubuntu 8.04 LTS, when using certain configurations such as "session optional pam_motd.so", allows local users to gain privileges by modifying the PATH environment variable to reference a malicious command, as demonstrated via uname.
Per: http://cwe.mitre.org/data/definitions/426.html

"CWE-426: Untrusted Search Path"
CVE-2012-0214
The pkgAcqMetaClearSig::Failed method in apt-pkg/acquire-item.cc in Advanced Package Tool (APT) 0.8.11 through 0.8.15.10 and 0.8.16 before 0.8.16~exp13, when updating from repositories that use InRelease files, allows man-in-the-middle attackers to install arbitrary packages by preventing a user from downloading the new InRelease file, which leaves the original InRelease file active and makes it more difficult to detect that the Packages file is modified and unsigned.
CVE-2013-5704
The mod_headers module in the Apache HTTP Server 2.2.22 allows remote attackers to bypass "RequestHeader unset" directives by placing a header in the trailer portion of data sent with chunked transfer coding.  NOTE: the vendor states "this is not a security issue in httpd as such."
CVE-2013-5705
apache2/modsecurity.c in ModSecurity before 2.7.6 allows remote attackers to bypass rules by using chunked transfer coding with a capitalized Chunked value in the Transfer-Encoding HTTP header.
CVE-2013-6456
The LXC driver (lxc/lxc_driver.c) in libvirt 1.0.1 through 1.2.1 allows local users to (1) delete arbitrary host devices via the virDomainDeviceDettach API and a symlink attack on /dev in the container; (2) create arbitrary nodes (mknod) via the virDomainDeviceAttach API and a symlink attack on /dev in the container; and cause a denial of service (shutdown or reboot host OS) via the (3) virDomainShutdown or (4) virDomainReboot API and a symlink attack on /dev/initctl in the container, related to "paths under /proc/$PID/root" and the virInitctlSetRunLevel function.
CVE-2013-7368
Multiple cross-site scripting (XSS) vulnerabilities in Gnew 2013.1 allow remote attackers to inject arbitrary web script or HTML via the gnew_template parameter to (1) users/profile.php, (2) articles/index.php, or (3) admin/polls.php; (4) category_id parameter to news/submit.php; news_id parameter to (5) news/send.php or (6) comments/add.php; or (7) post_subject or (8) thread_id parameter to posts/edit.php.
CVE-2014-0053
The default configuration of the Resources plugin 1.0.0 before 1.2.6 for Pivotal Grails 2.0.0 before 2.3.6 does not properly restrict access to files in the WEB-INF directory, which allows remote attackers to obtain sensitive information via a direct request.  NOTE: this identifier has been SPLIT due to different researchers and different vulnerability types. See CVE-2014-2857 for the META-INF variant and CVE-2014-2858 for the directory traversal.
CVE-2014-0105
The auth_token middleware in the OpenStack Python client library for Keystone (aka python-keystoneclient) before 0.7.0 does not properly retrieve user tokens from memcache, which allows remote authenticated users to gain privileges in opportunistic circumstances via a large number of requests, related to an "interaction between eventlet and python-memcached."
CVE-2014-0107
The TransformerFactory in Apache Xalan-Java before 2.7.2 does not properly restrict access to certain properties when FEATURE_SECURE_PROCESSING is enabled, which allows remote attackers to bypass expected restrictions and load arbitrary classes or access external resources via a crafted (1) xalan:content-header, (2) xalan:entities, (3) xslt:content-header, or (4) xslt:entities property, or a Java property that is bound to the XSLT 1.0 system-property function.
CVE-2014-0138
The default configuration in cURL and libcurl 7.10.6 before 7.36.0 re-uses (1) SCP, (2) SFTP, (3) POP3, (4) POP3S, (5) IMAP, (6) IMAPS, (7) SMTP, (8) SMTPS, (9) LDAP, and (10) LDAPS connections, which might allow context-dependent attackers to connect as other users via a request, a similar issue to CVE-2014-0015.
CVE-2014-0139
cURL and libcurl 7.1 before 7.36.0, when using the OpenSSL, axtls, qsossl or gskit libraries for TLS, recognize a wildcard IP address in the subject's Common Name (CN) field of an X.509 certificate, which might allow man-in-the-middle attackers to spoof arbitrary SSL servers via a crafted certificate issued by a legitimate Certification Authority.
CVE-2014-0157
Cross-site scripting (XSS) vulnerability in the Horizon Orchestration dashboard in OpenStack Dashboard (aka Horizon) 2013.2 before 2013.2.4 and icehouse before icehouse-rc2 allows remote attackers to inject arbitrary web script or HTML via the description field of a Heat template.
CVE-2014-0167
The Nova EC2 API security group implementation in OpenStack Compute (Nova) 2013.1 before 2013.2.4 and icehouse before icehouse-rc2 does not enforce RBAC policies for (1) add_rules, (2) remove_rules, (3) destroy, and other unspecified methods in compute/api.py when using non-default policies, which allows remote authenticated users to gain privileges via these API requests.
CVE-2014-0341
Multiple cross-site scripting (XSS) vulnerabilities in PivotX before 2.3.9 allow remote authenticated users to inject arbitrary web script or HTML via the title field to (1) templates_internal/pages.tpl, (2) templates_internal/home.tpl, or (3) templates_internal/entries.tpl; (4) an event field to objects.php; or the (5) email or (6) nickname field to pages.php, related to templates_internal/users.tpl.
CVE-2014-0342
Multiple unrestricted file upload vulnerabilities in fileupload.php in PivotX before 2.3.9 allow remote authenticated users to execute arbitrary PHP code by uploading a file with a (1) .php or (2) .php# extension, and then accessing it via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/434.html

"CWE-434: Unrestricted Upload of File with Dangerous Type"
CVE-2014-0348
The Artiva Agency Single Sign-On (SSO) implementation in Artiva Workstation 1.3.x before 1.3.9, Artiva Rm 3.1 MR7, Artiva Healthcare 5.2 MR5, and Artiva Architect 3.2 MR5, when the domain-name option is enabled, allows remote attackers to login to arbitrary domain accounts by using the corresponding username on a Windows client machine.
CVE-2014-0353
The ZyXEL Wireless N300 NetUSB NBG-419N router with firmware 1.00(BFQ.6)C0 allows remote attackers to bypass authentication by using %2F sequences in place of / (slash) characters.
CVE-2014-0354
The ZyXEL Wireless N300 NetUSB NBG-419N router with firmware 1.00(BFQ.6)C0 has a hardcoded password of qweasdzxc for an unspecified account, which allows remote attackers to obtain index.asp login access via an HTTP request.
CVE-2014-0355
Multiple stack-based buffer overflows on the ZyXEL Wireless N300 NetUSB NBG-419N router with firmware 1.00(BFQ.6)C0 allow man-in-the-middle attackers to execute arbitrary code via (1) a long temp attribute in a yweather:condition element in a forecastrss file that is processed by the checkWeather function; the (2) WeatherCity or (3) WeatherDegree variable to the detectWeather function; unspecified input to the (4) UpnpAddRunRLQoS, (5) UpnpDeleteRunRLQoS, or (6) UpnpDeletePortCheckType function; or (7) the SET COUNTRY udps command.
CVE-2014-0356
The ZyXEL Wireless N300 NetUSB NBG-419N router with firmware 1.00(BFQ.6)C0 allows remote attackers to execute arbitrary code via shell metacharacters in input to the (1) detectWeather, (2) set_language, (3) SystemCommand, or (4) NTPSyncWithHost function in management.c, or a (5) SET COUNTRY, (6) SET WLAN SSID, (7) SET WLAN CHANNEL, (8) SET WLAN STATUS, or (9) SET WLAN COUNTRY udps command.
CVE-2014-0357
Amtelco miSecureMessages allows remote attackers to read the messages of arbitrary users via an XML request containing a valid license key and a modified contactID value, as demonstrated by a request from the iOS or Android application.
CVE-2014-0358
Multiple directory traversal vulnerabilities in Xangati XSR before 11 and XNR before 7 allow remote attackers to read arbitrary files via a .. (dot dot) in (1) the file parameter in a getUpgradeStatus action to servlet/MGConfigData, (2) the download parameter in a download action to servlet/MGConfigData, (3) the download parameter in a port_svc action to servlet/MGConfigData, (4) the file parameter in a getfile action to servlet/Installer, or (5) the binfile parameter to servlet/MGConfigData.
CVE-2014-0359
Xangati XSR before 11 and XNR before 7 allows remote attackers to execute arbitrary commands via shell metacharacters in a gui_input_test.pl params parameter to servlet/Installer.
CVE-2014-0514
The Adobe Reader Mobile application before 11.2 for Android does not properly restrict use of JavaScript, which allows remote attackers to execute arbitrary code via a crafted PDF document, a related issue to CVE-2012-6636.
CVE-2014-0642
EMC Documentum Content Server before 6.7 SP1 P26, 6.7 SP2 before P13, 7.0 before P13, and 7.1 before P02 allows remote authenticated users to bypass intended access restrictions and read metadata from certain folders via unspecified vectors.
CVE-2014-0921
The server in IBM MessageSight 1.x before 1.1.0.0-IBM-IMA-IT01015 allows remote attackers to cause a denial of service (daemon crash and message data loss) via malformed headers during a WebSockets connection upgrade.
CVE-2014-0922
IBM MessageSight 1.x before 1.1.0.0-IBM-IMA-IT01015 allows remote attackers to cause a denial of service (resource consumption) via WebSockets MQ Telemetry Transport (MQTT) data.
CVE-2014-0923
IBM MessageSight 1.x before 1.1.0.0-IBM-IMA-IT01015 allows remote attackers to cause a denial of service (daemon restart) via crafted MQ Telemetry Transport (MQTT) authentication data.
CVE-2014-0924
IBM MessageSight 1.x before 1.1.0.0-IBM-IMA-IT01015 does not verify that all of the characters of a password are correct, which makes it easier for remote authenticated users to bypass intended access restrictions by leveraging knowledge of a password substring.
CVE-2014-1986
The Content Provider in the KOKUYO CamiApp application 1.21.1 and earlier for Android allows attackers to bypass intended access restrictions and read database information via a crafted application.
CVE-2014-2384
vmx86.sys in VMware Workstation 10.0.1 build 1379776 and VMware Player 6.0.1 build 1379776 on Windows might allow local users to cause a denial of service (read access violation and system crash) via a crafted buffer in an IOCTL call.  NOTE: the researcher reports "Vendor rated issue as non-exploitable."
CVE-2014-2580
The netback driver in Xen, when using certain Linux versions that do not allow sleeping in softirq context, allows local guest administrators to cause a denial of service ("scheduling while atomic" error and host crash) via a malformed packet, which causes a mutex to be taken when trying to disable the interface.
CVE-2014-2690
Citrix VDI-in-a-Box 5.3.x before 5.3.6 and 5.4.x before 5.4.3 allows local users to obtain administrator credentials by reading the log.
CVE-2014-2828
The V3 API in OpenStack Identity (Keystone) 2013.1 before 2013.2.4 and icehouse before icehouse-rc2 allows remote attackers to cause a denial of service (CPU consumption) via a large number of the same authentication method in a request, aka "authentication chaining."
CVE-2014-2842
Juniper ScreenOS 6.3 and earlier allows remote attackers to cause a denial of service (crash and restart or failover) via a malformed SSL/TLS packet.
CVE-2014-2857
The default configuration of the Resources plugin 1.0.0 before 1.2.6 for Pivotal Grails 2.0.0 through 2.3.6 does not properly restrict access to files in the META-INF directory, which allows remote attackers to obtain sensitive information via a direct request.  NOTE: this issue was SPLIT from CVE-2014-0053 due to different researchers per ADT5.
CVE-2014-2858
Directory traversal vulnerability in the Resources plugin 1.0.0 before 1.2.6 for Pivotal Grails 2.0.0 through 2.3.6 allows remote attackers to obtain sensitive information via unspecified vectors related to a "configured block." NOTE: this issue was SPLIT from CVE-2014-0053 per ADT2 due to different vulnerability types.
CVE-2014-2859
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to bypass intended access restrictions via a direct request.
CVE-2014-2860
Multiple cross-site scripting (XSS) vulnerabilities in PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allow remote attackers to inject arbitrary web script or HTML via a crafted HTTP request to a (1) ColdFusion or (2) JavaScript component.
CVE-2014-2861
Incomplete blacklist vulnerability in PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to conduct cross-site scripting (XSS) attacks via a crafted string, as demonstrated by bypassing a protection mechanism that removes only the "alert" string.
Per: https://cwe.mitre.org/data/definitions/184.html "CWE-184: Incomplete Blacklist"
CVE-2014-2862
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 does not check authorization in unspecified situations, which allows remote authenticated users to perform actions via unknown vectors.
CVE-2014-2863
Multiple absolute path traversal vulnerabilities in PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allow remote attackers to have an unspecified impact via a full pathname in a parameter.
CVE-2014-2864
Multiple directory traversal vulnerabilities in PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allow remote attackers to have an unspecified impact via a filename parameter containing directory traversal sequences.
CVE-2014-2865
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to bypass intended access restrictions via a '\0' character, as demonstrated by using this character within a pathname on the drive containing the web root directory of a ColdFusion installation.
CVE-2014-2866
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 relies on client JavaScript code for access restrictions, which allows remote attackers to perform unspecified operations by modifying this code.
CVE-2014-2867
Unrestricted file upload vulnerability in PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to execute arbitrary code by uploading a ColdFusion page, and then accessing it via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/434.html "CWE-434: Unrestricted Upload of File with Dangerous Type"
CVE-2014-2868
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to modify the flow of execution of ColdFusion code by using an HTTP GET request to set a ColdFusion variable.
Per http://cwe.mitre.org/data/definitions/472.html "CWE-472: External Control of Assumed-Immutable Web Parameter"
CVE-2014-2869
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to obtain sensitive information via requests to unspecified URIs, as demonstrated by pathname, SQL server, e-mail address, and IP address information.
CVE-2014-2870
The default configuration of PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 uses cleartext for storage of credentials in a database, which makes it easier for context-dependent attackers to obtain sensitive information via unspecified vectors.
CVE-2014-2871
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 relies on an HTTP session for entering credentials on login pages, which allows remote attackers to obtain sensitive information by sniffing the network.
CVE-2014-2872
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to obtain potentially sensitive information from a directory listing via unspecified vectors.
CVE-2014-2873
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 does not require authentication for access to log files, which allows remote attackers to obtain sensitive server information by using a predictable name in a request for a file.
CVE-2014-2874
PaperThin CommonSpot before 7.0.2 and 8.x before 8.0.3 allows remote attackers to execute arbitrary code via shell metacharacters in an unspecified context.
