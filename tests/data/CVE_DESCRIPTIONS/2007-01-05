CVE-2007-0059
Cross-zone scripting vulnerability in Apple Quicktime 3 to 7.1.3 allows remote user-assisted attackers to execute arbitrary code and list filesystem contents via a QuickTime movie (.MOV) with an HREF Track (HREFTrack) that contains an automatic action tag with a local URI, which is executed in a local zone during preview, as exploited by a MySpace worm.
CVE-2007-0075
AspBB stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing user passwords via a direct request for db/aspbb.mdb.
CVE-2007-0076
Openforum stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing user passwords via a direct request for openforum.mdb.
CVE-2007-0077
lblog stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for a certain file in admin/db/newFolder/.
CVE-2007-0078
BattleBlog stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for database/blankmaster.mdb.
CVE-2007-0079
rblog stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for (1) data/admin.mdb or (2) data/rblog.mdb.
CVE-2007-0080
** DISPUTED **  Buffer overflow in the SMB_Connect_Server function in FreeRadius 1.1.3 and earlier allows attackers to execute arbitrary code related to the server desthost field of an SMB_Handle_Type instance.  NOTE: the impact of this issue has been disputed by a reliable third party and the vendor, who states that exploitation is limited "only to local administrators who have write access to the server configuration files."  CVE concurs with the dispute.
-- Official Vendor Statement from the FreeRADIUS Server project

This issue is not a security vulnerability.  The exploit is available only to local administrators who have write access to the server configuration files.  As such, this issue has no security impact on any system running FreeRADIUS.

-- Official Vendor Statement from the FreeRADIUS Server project

A buffer overflow in the SMB_Connect_Server function in FreeRADIUS 1.1.4 and earlier allows attackers to execute arbitrary code related to the server desthost field of an SMB_Handle_Type instance.  This issue can not be exploited remotely, and can only be exploited by administrators who have write access to the server configuration files.
CVE-2007-0081
Sunbelt Kerio Personal Firewall (SKPF) 4.3.268 and 4.3.246, and possibly other versions allows local users to provide a Trojan horse iphlpapi.dll to SKPF by placing it in the installation directory.
CVE-2007-0082
users_adm/start1.php in IMGallery 2.5 and earlier does not properly handle files with multiple extensions, which allows remote authenticated users to upload and execute arbitrary PHP scripts.
CVE-2007-0083
Cross-site scripting (XSS) vulnerability in Nuked Klan 1.7 and earlier allows remote attackers to inject arbitrary web script or HTML via a javascript: URI in a getURL statement in a .swf file, as demonstrated by "Remote Cookie Disclosure."  NOTE: it could be argued that this is an issue in Shockwave instead of Nuked Klan.
CVE-2007-0084
** DISPUTED **  Buffer overflow in the Windows NT Message Compiler (MC) 1.00.5239 on Microsoft Windows XP allows local users to gain privileges via a long MC-filename.  NOTE: this issue has been disputed by a reliable third party who states that the compiler is not a privileged program, so privilege boundaries cannot be crossed.
CVE-2007-0085
Unspecified vulnerability in sys/dev/pci/vga_pci.c in the VGA graphics driver for wscons in OpenBSD 3.9 and 4.0, when the kernel is compiled with the PCIAGP option and a non-AGP device is being used, allows local users to gain privileges via unspecified vectors, possibly related to agp_ioctl NULL pointer reference.
CVE-2007-0086
** DISPUTED **  The Apache HTTP Server, when accessed through a TCP connection with a large window size, allows remote attackers to cause a denial of service (network bandwidth consumption) via a Range header that specifies multiple copies of the same fragment.  NOTE: the severity of this issue has been disputed by third parties, who state that the large window size required by the attack is not normally supported or configured by the server, or that a DDoS-style attack would accomplish the same goal.
CVE-2007-0087
** DISPUTED **  Microsoft Internet Information Services (IIS), when accessed through a TCP connection with a large window size, allows remote attackers to cause a denial of service (network bandwidth consumption) via a Range header that specifies multiple copies of the same fragment.  NOTE: the severity of this issue has been disputed by third parties, who state that the large window size required by the attack is not normally supported or configured by the server, or that a DDoS-style attack would accomplish the same goal.
CVE-2007-0088
Multiple directory traversal vulnerabilities in openmedia allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) src parameter to page.php or the (2) format parameter to search_form.php.
CVE-2007-0089
jgbbs stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for db/bbs.mdb.
CVE-2007-0090
WineGlass stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for db/data.mdb.
CVE-2007-0091
newsCMSlite stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for newsCMS.mdb.
CVE-2007-0092
SQL injection vulnerability in productdetail.asp in E-SMARTCART 1.0 allows remote attackers to execute arbitrary SQL commands via the product_id parameter.
CVE-2007-0093
SQL injection vulnerability in page.php in Simple Web Content Management System allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-0094
Sven Moderow GuestBook 0.3a stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for (1) gbook97.mdb or (2) gbook.mdb in ~db/.
CVE-2007-0095
phpMyAdmin 2.9.1.1 allows remote attackers to obtain sensitive information via a direct request for themes/darkblue_orange/layout.inc.php, which reveals the path in an error message.
CVE-2007-0096
CarbonCommunities stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing passwords via a direct request for DataBase/Carbon2.4d.mdb.
CVE-2007-0097
Multiple stack-based buffer overflows in the (1) LoadTree and (2) ReadHeader functions in PAISO.DLL 1.7.3.0 (1.7.3 beta) in ConeXware PowerArchiver 2006 9.64.02 allow user-assisted attackers to execute arbitrary code via a crafted ISO file containing a file within several nested directories.
CVE-2007-0098
Directory traversal vulnerability in language.php in VerliAdmin 0.3 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the lang cookie, as demonstrated by injecting PHP sequences into an Apache HTTP Server log file, which is then included by language.php.
