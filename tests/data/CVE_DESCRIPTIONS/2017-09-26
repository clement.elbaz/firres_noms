CVE-2014-0997
WiFiMonitor in Android 4.4.4 as used in the Nexus 5 and 4, Android 4.2.2 as used in the LG D806, Android 4.2.2 as used in the Samsung SM-T310, Android 4.1.2 as used in the Motorola RAZR HD, and potentially other unspecified Android releases before 5.0.1 and 5.0.2 does not properly handle exceptions, which allows remote attackers to cause a denial of service (reboot) via a crafted 802.11 probe response frame.
CVE-2014-8156
The D-Bus security policy files in /etc/dbus-1/system.d/*.conf in fso-gsmd 0.12.0-3, fso-frameworkd 0.9.5.9+git20110512-4, and fso-usaged 0.12.0-2 as packaged in Debian, the upstream cornucopia.git (fsoaudiod, fsodatad, fsodeviced, fsogsmd, fsonetworkd, fsotdld, fsousaged) git master on 2015-01-19, the upstream framework.git 0.10.1 and git master on 2015-01-19, phonefsod 0.1+git20121018-1 as packaged in Debian, Ubuntu and potentially other packages, and potentially other fso modules do not properly filter D-Bus message paths, which might allow local users to cause a denial of service (dbus-daemon memory consumption), or execute arbitrary code as root by sending a crafted D-Bus message to any D-Bus system service.
CVE-2014-8170
ovirt_safe_delete_config in ovirtfunctions.py and other unspecified locations in ovirt-node 3.0.0-474-gb852fd7 as packaged in Red Hat Enterprise Virtualization 3 do not properly quote input strings, which allows remote authenticated users and physically proximate attackers to execute arbitrary commands via a ; (semicolon) in an input string.
CVE-2014-8889
Dropbox SDK for Android before 1.6.2 might allow remote attackers to obtain sensitive information via crafted malware or via a drive-by download attack.
CVE-2015-0238
selinux-policy as packaged in Red Hat OpenShift 2 allows attackers to obtain process listing information via a privilege escalation attack.
CVE-2015-0874
Smartphone Passbook 1.0.0 does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to obtain sensitive information from encrypted communications via a crafted certificate.
CVE-2015-3248
openhpi/Makefile.am in OpenHPI before 3.6.0 uses world-writable permissions for /var/lib/openhpi directory, which allows local users, when quotas are not properly setup, to fill the filesystem hosting /var/lib and cause a denial of service (disk consumption).
CVE-2015-5069
The (1) filesystem::get_wml_location function in filesystem.cpp and (2) is_legal_file function in filesystem_boost.cpp in Battle for Wesnoth before 1.12.3 and 1.13.x before 1.13.1 allow remote attackers to obtain sensitive information via vectors related to inclusion of .pbl files from WML.
CVE-2015-5070
The (1) filesystem::get_wml_location function in filesystem.cpp and (2) is_legal_file function in filesystem_boost.cpp in Battle for Wesnoth before 1.12.4 and 1.13.x before 1.13.1, when a case-insensitive filesystem is used, allow remote attackers to obtain sensitive information via vectors related to inclusion of .pbl files from WML.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2015-5069.
CVE-2015-7390
SQL injection vulnerability in TestLink before 1.9.14 allows remote attackers to execute arbitrary SQL commands via the apikey parameter to lnl.php.
CVE-2015-7391
Multiple cross-site scripting (XSS) vulnerabilities in TestLink before 1.9.14 allow remote attackers to inject arbitrary web script or HTML via the (1) selected_end_date or (2) selected_start_date parameter to lib/results/tcCreatedPerUserOnTestProject.php; the (3) containerType parameter to lib/testcases/containerEdit.php; the (4) filter_tc_id or (5) filter_testcase_name parameter to lib/testcases/listTestCases.php; the (6) useRecursion parameter to lib/testcases/tcImport.php; the (7) targetTestCase or (8) created_by parameter to lib/testcases/tcSearch.php; or the (9) HTTP Referer header to third_party/user_contribution/fakeRemoteExecServer/client4fakeXMLRPCTestRunner.php.
CVE-2015-7670
Multiple SQL injection vulnerabilities in includes/update.php in the Support Ticket System plugin before 1.2.1 for WordPress allow remote attackers to execute arbitrary SQL commands via the (1) user or (2) id parameter.
CVE-2015-8707
Password reset tokens in Magento CE before 1.9.2.2, and Magento EE before 1.14.2.2 are passed via a GET request and not canceled after use, which allows remote attackers to obtain user passwords via a crafted external service with access to the referrer field.
CVE-2017-1000252
The KVM subsystem in the Linux kernel through 4.13.3 allows guest OS users to cause a denial of service (assertion failure, and hypervisor hang or crash) via an out-of bounds guest_irq value, related to arch/x86/kvm/vmx.c and virt/kvm/eventfd.c.
CVE-2017-12154
The prepare_vmcs02 function in arch/x86/kvm/vmx.c in the Linux kernel through 4.13.3 does not ensure that the "CR8-load exiting" and "CR8-store exiting" L0 vmcs02 controls exist in cases where L1 omits the "use TPR shadow" vmcs12 control, which allows KVM L2 guest OS users to obtain read and write access to the hardware CR8 register.
CVE-2017-13129
Cross-site request forgery (CSRF) vulnerability in ZKTeco ZKTime Web 2.0.1.12280 allows remote authenticated users to hijack the authentication of administrators for requests that add administrators by leveraging lack of anti-CSRF tokens.
CVE-2017-14001
An Improper Neutralization of Special Elements used in an OS Command issue was discovered in Digium Asterisk GUI 2.1.0 and prior. An OS command injection vulnerability has been identified that may allow the execution of arbitrary code on the system through the inclusion of OS commands in the URL request of the program.
CVE-2017-1425
IBM Business Process Manager 8.0.1.1 and 8.5.7 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 127478.
CVE-2017-14602
A vulnerability has been identified in the management interface of Citrix NetScaler Application Delivery Controller (ADC) and NetScaler Gateway 10.1 before build 135.18, 10.5 before build 66.9, 10.5e before build 60.7010.e, 11.0 before build 70.16, 11.1 before build 55.13, and 12.0 before build 53.13 (except for build 41.24) that, if exploited, could allow an attacker with access to the NetScaler management interface to gain administrative access to the appliance.
CVE-2017-14703
SQL injection vulnerability in Cash Back Comparison Script 1.0 allows remote attackers to execute arbitrary SQL commands via the PATH_INFO to search/.
CVE-2017-14704
Multiple unrestricted file upload vulnerabilities in the (1) imageSubmit and (2) proof_submit functions in Claydip Laravel Airbnb Clone 1.0 allow remote authenticated users to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in images/profile.
CVE-2017-14737
A cryptographic cache-based side channel in the RSA implementation in Botan before 1.10.17, and 1.11.x and 2.x before 2.3.0, allows a local attacker to recover information about RSA secret keys, as demonstrated by CacheD. This occurs because an array is indexed with bits derived from a secret key.
CVE-2017-14739
The AcquireResampleFilterThreadSet function in magick/resample-private.h in ImageMagick 7.0.7-4 mishandles failed memory allocation, which allows remote attackers to cause a denial of service (NULL Pointer Dereference in DistortImage in MagickCore/distort.c, and application crash) via unspecified vectors.
CVE-2017-14741
The ReadCAPTIONImage function in coders/caption.c in ImageMagick 7.0.7-3 allows remote attackers to cause a denial of service (infinite loop) via a crafted font file.
CVE-2017-14743
Faleemi FSC-880 00.01.01.0048P2 devices allow unauthenticated SQL injection via the Username element in an XML document to /onvif/device_service, as demonstrated by reading the admin password.
CVE-2017-14744
UEditor 1.4.3.3 has XSS via the SRC attribute of an IFRAME element.
CVE-2017-14745
The *_get_synthetic_symtab functions in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29, interpret a -1 value as a sorting count instead of an error flag, which allows remote attackers to cause a denial of service (integer overflow and application crash) or possibly have unspecified other impact via a crafted ELF file, related to elf32-i386.c and elf64-x86-64.c.
CVE-2017-14748
Race condition in Blizzard Overwatch 1.15.0.2 allows remote authenticated users to cause a denial of service (season bans and SR losses for other users) by leaving a competitive match at a specific time during the initial loading of that match.
CVE-2017-14749
JerryScript 1.0 allows remote attackers to cause a denial of service (jmem_heap_alloc_block_internal heap memory corruption) or possibly execute arbitrary code via a crafted .js file, because unrecognized \ characters cause incorrect 0x00 characters in bytecode.literal data.
CVE-2017-14751
The Intense WP "WP Jobs" plugin 1.5 for WordPress has XSS, related to the Job Qualification field.
CVE-2017-1527
IBM Business Process Manager 7.5, 8.0, and 8.5 is vulnerable to a XML External Entity Injection (XXE) attack when processing XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources. IBM X-Force ID: 130156.
CVE-2017-1530
IBM Business Process Manager 7.5, 8.0, and 8.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 130409.
CVE-2017-1531
IBM Business Process Manager 7.5, 8.0, and 8.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 130410.
CVE-2017-1539
IBM Business Process Manager 7.5, 8.0, and 8.5 is vulnerable to privilege escalation by not properly distinguishing internal group memberships from user registry group memberships. By manipulating LDAP group membership an attack might gain privileged access. IBM X-Force ID: 130807.
CVE-2017-5192
When using the local_batch client from salt-api in SaltStack Salt before 2015.8.13, 2016.3.x before 2016.3.5, and 2016.11.x before 2016.11.2, external authentication is not respected, enabling all authentication to be bypassed.
CVE-2017-5200
Salt-api in SaltStack Salt before 2015.8.13, 2016.3.x before 2016.3.5, and 2016.11.x before 2016.11.2 allows arbitrary command execution on a salt-master via Salt's ssh_client.
CVE-2017-7969
A cross-site request forgery vulnerability exists on the Secure Gateway component of Schneider Electric's PowerSCADA Anywhere v1.0 redistributed with PowerSCADA Expert v8.1 and PowerSCADA Expert v8.2 and Citect Anywhere version 1.0 for multiple state-changing requests. This type of attack requires some level of social engineering in order to get a legitimate user to click on or access a malicious link/site containing the CSRF attack.
CVE-2017-7970
A vulnerability exists in Schneider Electric's PowerSCADA Anywhere v1.0 redistributed with PowerSCADA Expert v8.1 and PowerSCADA Expert v8.2 and Citect Anywhere version 1.0 that allows the ability to specify Arbitrary Server Target Nodes in connection requests to the Secure Gateway and Server components.
CVE-2017-7971
A vulnerability exists in Schneider Electric's PowerSCADA Anywhere v1.0 redistributed with PowerSCADA Expert v8.1 and PowerSCADA Expert v8.2 and Citect Anywhere version 1.0 that allows the use of outdated cipher suites and improper verification of peer SSL Certificate.
CVE-2017-7972
A vulnerability exists in Schneider Electric's PowerSCADA Anywhere v1.0 redistributed with PowerSCADA Expert v8.1 and PowerSCADA Expert v8.2 and Citect Anywhere version 1.0 that allows the ability to escape out of remote PowerSCADA Anywhere applications and launch other processes.
CVE-2017-7973
A SQL injection vulnerability exists in Schneider Electric's U.motion Builder software versions 1.2.1 and prior in which an unauthenticated user can use calls to various paths allowing performance of arbitrary SQL commands against the underlying database.
CVE-2017-7974
A path traversal information disclosure vulnerability exists in Schneider Electric's U.motion Builder software versions 1.2.1 and prior in which an unauthenticated user can execute arbitrary code and exfiltrate files.
CVE-2017-9956
An authentication bypass vulnerability exists in Schneider Electric's U.motion Builder software versions 1.2.1 and prior in which the system contains a hard-coded valid session. An attacker can use that session ID as part of the HTTP cookie of a web request, resulting in authentication bypass
CVE-2017-9957
A vulnerability exists in Schneider Electric's U.motion Builder software versions 1.2.1 and prior in which the web service contains a hidden system account with a hardcoded password. An attacker can use this information to log into the system with high-privilege credentials.
CVE-2017-9958
An improper access control vulnerability exists in Schneider Electric's U.motion Builder software versions 1.2.1 and prior in which an improper handling of the system configuration can allow an attacker to execute arbitrary code under the context of root.
CVE-2017-9959
A vulnerability exists in Schneider Electric's U.motion Builder software versions 1.2.1 and prior in which the system accepts reboot in session from unauthenticated users, supporting a denial of service condition.
CVE-2017-9960
An information disclosure vulnerability exists in Schneider Electric's U.motion Builder software versions 1.2.1 and prior in which the system response to error provides more information than should be available to an unauthenticated user.
CVE-2017-9961
A vulnerability exists in Schneider Electric's Pro-Face GP Pro EX version 4.07.000 that allows an attacker to execute arbitrary code. Malicious code installation requires an access to the computer. By placing a specific DLL/OCX file, an attacker is able to force the process to load arbitrary DLL and execute arbitrary code in the context of the process.
CVE-2017-9962
Schneider Electric's ClearSCADA versions released prior to August 2017 are susceptible to a memory allocation vulnerability, whereby malformed requests can be sent to ClearSCADA client applications to cause unexpected behavior. Client applications affected include ViewX and the Server Icon.
