CVE-2009-4831
Cerulean Studios Trillian 3.1 Basic does not check SSL certificates during MSN authentication, which allows remote attackers to obtain MSN credentials via a man-in-the-middle attack with a spoofed SSL certificate.
CVE-2009-4832
The dlpcrypt.sys kernel driver 0.1.1.27 in DESlock+ 4.0.2 allows local users to gain privileges via a crafted IOCTL 0x80012010 request to the DLPCryptCore device.
CVE-2009-4833
MySQL Connector/NET before 6.0.4, when using encryption, does not verify SSL certificates during connection, which allows remote attackers to perform a man-in-the-middle attack with a spoofed SSL certificate.
CVE-2010-0817
Cross-site scripting (XSS) vulnerability in _layouts/help.aspx in Microsoft SharePoint Server 2007 12.0.0.6421 and possibly earlier, and SharePoint Services 3.0 SP1 and SP2, versions, allows remote attackers to inject arbitrary web script or HTML via the cid0 parameter.
CVE-2010-1166
The fbComposite function in fbpict.c in the Render extension in the X server in X.Org X11R7.1 allows remote authenticated users to cause a denial of service (memory corruption and daemon crash) or possibly execute arbitrary code via a crafted request, related to an incorrect macro definition.
CVE-2010-1597
Stack-based buffer overflow in zgtips.dll in ZipGenius 6.3.1.2552 allows user-assisted remote attackers to execute arbitrary code via a ZIP file containing an entry with a long filename.
CVE-2010-1598
phpThumb.php in phpThumb() 1.7.9 and possibly other versions, when ImageMagick is installed, allows remote attackers to execute arbitrary commands via the fltr[] parameter, as discovered in the wild in April 2010.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2010-1599
SQL injection vulnerability in loadorder.php in NKInFoWeb 2.5 and 5.2.2.0 allows remote attackers to execute arbitrary SQL commands via the id_sp parameter.
CVE-2010-1600
SQL injection vulnerability in the Media Mall Factory (com_mediamall) component 1.0.4 for Joomla! allows remote attackers to execute arbitrary SQL commands via the category parameter to index.php.
CVE-2010-1601
Directory traversal vulnerability in the JA Comment (com_jacomment) component for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the view parameter to index.php.
CVE-2010-1602
Directory traversal vulnerability in the ZiMB Comment (com_zimbcomment) component 0.8.1 for Joomla! allows remote attackers to read arbitrary files and possibly have unspecified other impact via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-1603
Directory traversal vulnerability in the ZiMB Core (aka ZiMBCore or com_zimbcore) component 0.1 in the ZiMB Manager collection for Joomla! allows remote attackers to read arbitrary files and possibly have unspecified other impact via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-1604
Multiple SQL injection vulnerabilities in admin_login.php in NCT Jobs Portal Script allow remote attackers to execute arbitrary SQL commands via the (1) user parameter (aka login field) and (2) passwd parameter (aka password field).  NOTE: some of these details are obtained from third party information.
CVE-2010-1605
Multiple SQL injection vulnerabilities in isearch.php in NCT Jobs Portal Script allow remote attackers to execute arbitrary SQL commands via the (1) anyword and (2) cityname parameters.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2010-1606
Multiple cross-site scripting (XSS) vulnerabilities in NCT Jobs Portal Script allow remote attackers to inject arbitrary web script or HTML via the (1) search, (2) Keywords, (3) Tags, or (4) Desired City field.
CVE-2010-1607
Directory traversal vulnerability in wmi.php in the Webmoney Web Merchant Interface (aka WMI or com_wmi) component 1.5.0 for Joomla! allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-1608
Stack-based buffer overflow in IBM Lotus Notes 8.5 and 8.5fp1, and possibly other versions, allows remote attackers to execute arbitrary code via unknown attack vectors, as demonstrated by the vd_ln module in VulnDisco 9.0.  NOTE: as of 20100222, this disclosure has no actionable information. However, because the VulnDisco author is a reliable researcher, the issue is being assigned a CVE identifier for tracking purposes.
CVE-2010-1609
Cross-site scripting (XSS) vulnerability in SAP NetWeaver 2004 before SP21 and 2004s before SP13 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-1610
Cross-site request forgery (CSRF) vulnerability in index.php in OpenCart 1.4 allows remote attackers to hijack the authentication of an application administrator for requests that create an administrative account via a POST request with the route parameter set to "user/user/insert." NOTE: some of these details are obtained from third party information.
CVE-2010-1611
Cross-site request forgery (CSRF) vulnerability in AlegroCart 1.1 allows remote attackers to hijack the authentication of the administrator for requests that reset the administrator password via a POST to admin/ with an update action.
CVE-2010-1612
The IBM WebSphere DataPower XML Accelerator XA35, Low Latency Appliance XM70, Integration Appliance XI50, B2B Appliance XB60, and XML Security Gateway XS40 SOA Appliances before 3.8.0.0, when a QLOGIC Ethernet interface is used, allow remote attackers to cause a denial of service (interface outage) via malformed ICMP packets to the 0.0.0.0 destination IP address.
CVE-2010-1613
Moodle 1.8.x and 1.9.x before 1.9.8 does not enable the "Regenerate session id during login" setting by default, which makes it easier for remote attackers to conduct session fixation attacks.
CVE-2010-1614
Multiple cross-site scripting (XSS) vulnerabilities in Moodle 1.8.x before 1.8.12 and 1.9.x before 1.9.8 allow remote attackers to inject arbitrary web script or HTML via vectors related to (1) the Login-As feature or (2) when the global search feature is enabled, unspecified global search forms in the Global Search Engine.  NOTE: vector 1 might be resultant from a cross-site request forgery (CSRF) vulnerability.
CVE-2010-1615
Multiple SQL injection vulnerabilities in Moodle 1.8.x before 1.8.12 and 1.9.x before 1.9.8 allow remote attackers to execute arbitrary SQL commands via vectors related to (1) the add_to_log function in mod/wiki/view.php in the wiki module, or (2) "data validation in some forms elements" related to lib/form/selectgroups.php.
CVE-2010-1616
Moodle 1.8.x and 1.9.x before 1.9.8 can create new roles when restoring a course, which allows teachers to create new accounts even if they do not have the moodle/user:create capability.
CVE-2010-1617
user/view.php in Moodle 1.8.x before 1.8.12 and 1.9.x before 1.9.8 does not properly check a role, which allows remote authenticated users to obtain the full names of other users via the course profile page.
CVE-2010-1618
Cross-site scripting (XSS) vulnerability in the phpCAS client library before 1.1.0, as used in Moodle 1.8.x before 1.8.12 and 1.9.x before 1.9.8, allows remote attackers to inject arbitrary web script or HTML via a crafted URL, which is not properly handled in an error message.
CVE-2010-1619
Cross-site scripting (XSS) vulnerability in the fix_non_standard_entities function in the KSES HTML text cleaning library (weblib.php), as used in Moodle 1.8.x before 1.8.12 and 1.9.x before 1.9.8, allows remote attackers to inject arbitrary web script or HTML via crafted HTML entities.
