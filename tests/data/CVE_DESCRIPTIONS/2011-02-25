CVE-2010-4227
The xdrDecodeString function in XNFS.NLM in Novell Netware 6.5 before SP8 allows remote attackers to cause a denial of service (abend) or execute arbitrary code via a crafted, signed value in a NFS RPC request to port UDP 1234, leading to a stack-based buffer overflow.
CVE-2011-0037
Microsoft Malware Protection Engine before 1.1.6603.0, as used in Microsoft Malicious Software Removal Tool (MSRT), Windows Defender, Security Essentials, Forefront Client Security, Forefront Endpoint Protection 2010, and Windows Live OneCare, allows local users to gain privileges via a crafted value of an unspecified user registry key.
CVE-2011-0332
Integer overflow in Foxit Reader before 4.3.1.0218 and Foxit Phantom before 2.3.3.1112 allows remote attackers to execute arbitrary code via crafted ICC chunks in a PDF file, which triggers a heap-based buffer overflow.
CVE-2011-0372
The CGI implementation on Cisco TelePresence endpoint devices with software 1.2.x through 1.5.x allows remote attackers to execute arbitrary commands via a malformed request, related to "command injection vulnerabilities," aka Bug ID CSCtb31640.
CVE-2011-0373
The CGI implementation on Cisco TelePresence endpoint devices with software 1.2.x through 1.5.x allows remote authenticated users to execute arbitrary commands via a malformed request, related to "command injection vulnerabilities," aka Bug ID CSCtb31685.
CVE-2011-0374
The CGI implementation on Cisco TelePresence endpoint devices with software 1.2.x through 1.5.x allows remote authenticated users to execute arbitrary commands via a malformed request, related to "command injection vulnerabilities," aka Bug ID CSCtb31659.
CVE-2011-0375
The CGI implementation on Cisco TelePresence endpoint devices with software 1.2.x through 1.6.x allows remote authenticated users to execute arbitrary commands via a malformed request, related to "command injection vulnerabilities," aka Bug ID CSCth24671.
CVE-2011-0376
The TFTP implementation on Cisco TelePresence endpoint devices with software 1.2.x through 1.5.x, 1.6.0, and 1.6.1 allows remote attackers to obtain sensitive information via a GET request, aka Bug ID CSCte43876.
CVE-2011-0377
Cisco TelePresence endpoint devices with software 1.2.x through 1.6.x allow remote attackers to cause a denial of service (service crash) via a malformed SOAP request in conjunction with a spoofed TelePresence Manager that supplies an invalid IP address, aka Bug ID CSCth03605.
CVE-2011-0378
The XML-RPC implementation on Cisco TelePresence endpoint devices with software 1.2.x through 1.5.x allows remote attackers to execute arbitrary commands via a TCP request, related to a "command injection vulnerability," aka Bug ID CSCtb52587.
CVE-2011-0379
Buffer overflow on Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 1.6.x; Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, and 1.6.x; Cisco TelePresence endpoint devices with software 1.2.x through 1.6.x; and Cisco TelePresence Manager 1.2.x, 1.3.x, 1.4.x, 1.5.x, and 1.6.2 allows remote attackers to execute arbitrary code via a crafted Cisco Discovery Protocol packet, aka Bug IDs CSCtd75769, CSCtd75766, CSCtd75754, and CSCtd75761.
CVE-2011-0380
Cisco TelePresence Manager 1.2.x through 1.6.x allows remote attackers to bypass authentication and invoke arbitrary methods via a malformed SOAP request, aka Bug ID CSCtc59562.
CVE-2011-0381
Cisco TelePresence Manager 1.2.x through 1.6.x allows remote attackers to perform unspecified actions and consequently execute arbitrary code via a crafted request to the Java RMI interface, related to a "command injection vulnerability," aka Bug ID CSCtf97085.
CVE-2011-0382
The CGI subsystem on Cisco TelePresence Recording Server devices with software 1.6.x before 1.6.2 allows remote attackers to execute arbitrary commands via a request to TCP port 443, related to a "command injection vulnerability," aka Bug ID CSCtf97221.
CVE-2011-0383
The Java Servlet framework on Cisco TelePresence Recording Server devices with software 1.6.x before 1.6.2 and Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, and 1.6.x does not require administrative authentication for unspecified actions, which allows remote attackers to execute arbitrary code via a crafted request, aka Bug IDs CSCtf42005 and CSCtf42008.
CVE-2011-0384
The Java Servlet framework on Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, and 1.6.x does not require administrative authentication for unspecified actions, which allows remote attackers to execute arbitrary code via a crafted request, aka Bug ID CSCtf01253.
CVE-2011-0385
The administrative web interface on Cisco TelePresence Recording Server devices with software 1.6.x and Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, and 1.6.x allows remote attackers to create or overwrite arbitrary files, and possibly execute arbitrary code, via a crafted request, aka Bug IDs CSCth85786 and CSCth61065.
CVE-2011-0386
The XML-RPC implementation on Cisco TelePresence Recording Server devices with software 1.6.x and 1.7.x before 1.7.1 allows remote attackers to overwrite files and consequently execute arbitrary code via a malformed request, aka Bug ID CSCti50739.
CVE-2011-0387
The administrative web interface on Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, and 1.6.x allows remote authenticated users to cause a denial of service or have unspecified other impact via vectors involving access to a servlet, aka Bug ID CSCtf97164.
CVE-2011-0388
Cisco TelePresence Recording Server devices with software 1.6.x and Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, and 1.6.x do not properly restrict remote access to the Java servlet RMI interface, which allows remote attackers to cause a denial of service (memory consumption and web outage) via multiple crafted requests, aka Bug IDs CSCtg35830 and CSCtg35825.
CVE-2011-0389
Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, and 1.6.x allow remote attackers to cause a denial of service (process crash) via a crafted Real-Time Transport Control Protocol (RTCP) UDP packet, aka Bug ID CSCth60993.
CVE-2011-0390
The XML-RPC implementation on Cisco TelePresence Multipoint Switch (CTMS) devices with software 1.0.x, 1.1.x, 1.5.x, 1.6.x, and 1.7.0 allows remote attackers to cause a denial of service (process crash) via a crafted request, aka Bug ID CSCtj44534.
CVE-2011-0391
Cisco TelePresence Recording Server devices with software 1.6.x allow remote attackers to cause a denial of service (thread consumption and device outage) via a malformed request, related to an "ad hoc recording" issue, aka Bug ID CSCtf97205.
CVE-2011-0392
Cisco TelePresence Recording Server devices with software 1.6.x do not require authentication for an XML-RPC interface, which allows remote attackers to perform unspecified actions via a session on TCP port 8080, aka Bug ID CSCtg35833.
CVE-2011-0393
Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 7.0 before 7.0(8.12), 7.1 and 7.2 before 7.2(5.2), 8.0 before 8.0(5.21), 8.1 before 8.1(2.49), 8.2 before 8.2(3.6), and 8.3 before 8.3(2.7) and Cisco PIX Security Appliances 500 series devices, when transparent firewall mode is configured but IPv6 is not configured, allow remote attackers to cause a denial of service (packet buffer exhaustion and device outage) via IPv6 traffic, aka Bug ID CSCtj04707.
CVE-2011-0394
Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 7.0 before 7.0(8.11), 7.1 and 7.2 before 7.2(5.1), 8.0 before 8.0(5.19), 8.1 before 8.1(2.47), 8.2 before 8.2(2.19), and 8.3 before 8.3(1.8); Cisco PIX Security Appliances 500 series devices; and Cisco Firewall Services Module (aka FWSM) 3.1 before 3.1(20), 3.2 before 3.2(20), 4.0 before 4.0(15), and 4.1 before 4.1(5) allow remote attackers to cause a denial of service (device reload) via a malformed Skinny Client Control Protocol (SCCP) message, aka Bug IDs CSCtg69457 and CSCtl84952.
CVE-2011-0395
Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 8.0 before 8.0(5.20), 8.1 before 8.1(2.48), 8.2 before 8.2(3), and 8.3 before 8.3(2.1), when the RIP protocol and the Cisco Phone Proxy functionality are configured, allow remote attackers to cause a denial of service (device reload) via a RIP update, aka Bug ID CSCtg66583.
CVE-2011-0396
Cisco Adaptive Security Appliances (ASA) 5500 series devices with software 8.0 before 8.0(5.23), 8.1 before 8.1(2.49), 8.2 before 8.2(4.1), and 8.3 before 8.3(2.13), when a Certificate Authority (CA) is configured, allow remote attackers to read arbitrary files via unspecified vectors, aka Bug ID CSCtk12352.
CVE-2011-0717
Session fixation vulnerability in Red Hat Network (RHN) Satellite Server 5.4 allows remote attackers to hijack web sessions via unspecified vectors related to Spacewalk.
Per: http://cwe.mitre.org/data/definitions/384.html 
'CWE-384: Session Fixation'
CVE-2011-0718
Red Hat Network (RHN) Satellite Server 5.4 does not use a time delay after a failed login attempt, which makes it easier for remote attackers to conduct brute force password guessing attacks.
CVE-2011-0926
A certain ActiveX control in CSDWebInstaller.ocx in Cisco Secure Desktop (CSD) does not properly verify the signature of an unspecified downloaded program, which allows remote attackers to execute arbitrary code by spoofing the CSD installation process, a different vulnerability than CVE-2010-0589.
CVE-2011-1018
logwatch.pl in Logwatch 7.3.6 allows remote attackers to execute arbitrary commands via shell metacharacters in a log file name, as demonstrated via a crafted username to a Samba server.
CVE-2011-1036
The XML Security Database Parser class in the XMLSecDB ActiveX control in the HIPSEngine component in the Management Server before 8.1.0.88, and the client before 1.6.450, in CA Host-Based Intrusion Prevention System (HIPS) 8.1, as used in CA Internet Security Suite (ISS) 2010, allows remote attackers to download an arbitrary program onto a client machine, and execute this program, via vectors involving the SetXml and Save methods.
CVE-2011-1100
Multiple SQL injection vulnerabilities in admin/index.php in Pixelpost 1.7.3 allow remote authenticated users to execute arbitrary SQL commands via the (1) findfid, (2) id, (3) selectfcat, (4) selectfmon, or (5) selectftag parameter in an images action.
CVE-2011-1101
Multiple unspecified vulnerabilities in a third-party component of the Citrix Licensing Administration Console 11.6, formerly License Management Console, allow remote attackers to (1) access unauthorized "license administration functionality" or (2) cause a denial of service via unknown vectors.
CVE-2011-1102
Cross-site scripting (XSS) vulnerability in the WebReporting module in F-Secure Policy Manager 7.x, 8.00 before hotfix 2, 8.1x before hotfix 3 on Windows and hotfix 2 on Linux, and 9.00 before hotfix 4 on Windows and hotfix 2 on Linux, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-1103
The WebReporting module in F-Secure Policy Manager 7.x, 8.00 before hotfix 2, 8.1x before hotfix 3 on Windows and hotfix 2 on Linux, and 9.00 before hotfix 4 on Windows and hotfix 2 on Linux, allows remote attackers to obtain sensitive information via a request to an invalid report, which reveals the installation path in an error message, as demonstrated with requests to (1) report/infection-table.html or (2) report/productsummary-table.html.
