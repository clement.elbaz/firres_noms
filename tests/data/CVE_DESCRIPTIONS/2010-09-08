CVE-2009-4895
Race condition in the tty_fasync function in drivers/char/tty_io.c in the Linux kernel before 2.6.32.6 allows local users to cause a denial of service (NULL pointer dereference and system crash) or possibly have unspecified other impact via unknown vectors, related to the put_tty_queue and __f_setown functions.  NOTE: the vulnerability was addressed in a different way in 2.6.32.9.
CVE-2010-2066
The mext_check_arguments function in fs/ext4/move_extent.c in the Linux kernel before 2.6.35 allows local users to overwrite an append-only file via a MOVE_EXT ioctl call that specifies this file as a donor.
CVE-2010-2492
Buffer overflow in the ecryptfs_uid_hash macro in fs/ecryptfs/messaging.c in the eCryptfs subsystem in the Linux kernel before 2.6.35 might allow local users to gain privileges or cause a denial of service (system crash) via unspecified vectors.
CVE-2010-2495
The pppol2tp_xmit function in drivers/net/pppol2tp.c in the L2TP implementation in the Linux kernel before 2.6.34 does not properly validate certain values associated with an interface, which allows attackers to cause a denial of service (NULL pointer dereference and OOPS) or possibly have unspecified other impact via vectors related to a routing change.
CVE-2010-2524
The DNS resolution functionality in the CIFS implementation in the Linux kernel before 2.6.35, when CONFIG_CIFS_DFS_UPCALL is enabled, relies on a user's keyring for the dns_resolver upcall in the cifs.upcall userspace helper, which allows local users to spoof the results of DNS queries and perform arbitrary CIFS mounts via vectors involving an add_key call, related to a "cache stuffing" issue and MS-DFS referrals.
CVE-2010-2798
The gfs2_dirent_find_space function in fs/gfs2/dir.c in the Linux kernel before 2.6.35 uses an incorrect size value in calculations associated with sentinel directory entries, which allows local users to cause a denial of service (NULL pointer dereference and panic) and possibly have unspecified other impact by renaming a file in a GFS2 filesystem, related to the gfs2_rename function in fs/gfs2/ops_inode.c.
CVE-2010-2803
The drm_ioctl function in drivers/gpu/drm/drm_drv.c in the Direct Rendering Manager (DRM) subsystem in the Linux kernel before 2.6.27.53, 2.6.32.x before 2.6.32.21, 2.6.34.x before 2.6.34.6, and 2.6.35.x before 2.6.35.4 allows local users to obtain potentially sensitive information from kernel memory by requesting a large memory-allocation amount.
CVE-2010-2955
The cfg80211_wext_giwessid function in net/wireless/wext-compat.c in the Linux kernel before 2.6.36-rc3-next-20100831 does not properly initialize certain structure members, which allows local users to leverage an off-by-one error in the ioctl_standard_iw_point function in net/wireless/wext-core.c, and obtain potentially sensitive information from kernel heap memory, via vectors involving an SIOCGIWESSID ioctl call that specifies a large buffer size.
CVE-2010-2958
Cross-site scripting (XSS) vulnerability in libraries/Error.class.php in phpMyAdmin 3.x before 3.3.6 allows remote attackers to inject arbitrary web script or HTML via vectors related to a PHP backtrace and error messages (aka debugging messages), a different vulnerability than CVE-2010-3056.
CVE-2010-2959
Integer overflow in net/can/bcm.c in the Controller Area Network (CAN) implementation in the Linux kernel before 2.6.27.53, 2.6.32.x before 2.6.32.21, 2.6.34.x before 2.6.34.6, and 2.6.35.x before 2.6.35.4 allows attackers to execute arbitrary code or cause a denial of service (system crash) via crafted CAN traffic.
CVE-2010-2960
The keyctl_session_to_parent function in security/keys/keyctl.c in the Linux kernel 2.6.35.4 and earlier expects that a certain parent session keyring exists, which allows local users to cause a denial of service (NULL pointer dereference and system crash) or possibly have unspecified other impact via a KEYCTL_SESSION_TO_PARENT argument to the keyctl function.
CVE-2010-3004
Unspecified vulnerability in HP Operations Agent 7.36 and 8.6 on Windows allows remote attackers to execute arbitrary code via unknown vectors.
Per: http://h20000.www2.hp.com/bizsupport/TechSupport/Document.jsp?objectID=c02497800

'A potential security vulnerability has been identified with HP Operations Agent running on Windows.'
Per: http://h20000.www2.hp.com/bizsupport/TechSupport/Document.jsp?objectID=c02497800

'RESOLUTION

HP has provided hotfixes for Operations Agent v7.36 and v8.6 to resolve this vulnerability. Please contact your HP Software support channel to request the hotfixes below.

For Operations agent v7.36 request hotfixes for QCCR1A106920 and QCCR1A106834.

For Operations agent v8.6 request hotfixes for QCCR1A106558 and 
QCCR1A106917."

CVE-2010-3005
Unspecified vulnerability in HP Operations Agent 7.36 and 8.6 on Windows allows local users to gain privileges via unknown vectors.
CVE-2010-3198
ZServer in Zope 2.10.x before 2.10.12 and 2.11.x before 2.11.7 allows remote attackers to cause a denial of service (crash of worker threads) via vectors that trigger uncaught exceptions.
CVE-2010-3264
The engine installer in Novell Identity Manager (aka IDM) 3.6.1 stores admin tree credentials in /tmp/idmInstall.log, which allows local users to obtain sensitive information by reading this file.
