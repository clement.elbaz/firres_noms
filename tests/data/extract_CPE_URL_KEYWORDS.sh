#!/bin/bash
# $1 = path to yearly CVE file from NIST
# $2 = day to collect
# $3 = input file to blend the results with
(
cat $3 2>/dev/null
(
jq -r --arg dt "$2" '.CVE_Items [] | select(.publishedDate | startswith($dt)) | .configurations.nodes [] | .cpe_match []? | .cpe23Uri' $1
jq -r --arg dt "$2" '.CVE_Items [] | select(.publishedDate | startswith($dt)) | .configurations.nodes [] | .children [] ? | .cpe_match [] | .cpe23Uri' $1
) | while read line; do echo $line | sed 's/:/\n/g' | sed -n '4,5p;11p' ;done | grep -v '^[*-]$'
) | sort | uniq